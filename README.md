## <img src="Assets/Imagenes/LogoJuego.png" width="50" alt="Whiskwarper Logo"> _Whiskwarper_ <img src="Assets/Imagenes/LogoJuego.png" width="50" alt="Whiskwarper Logo">
Juego de puzzles y exploración que tiene como mecánica principal el que la cámara se divida en dos para poder ver dos dimensiones al mismo tiempo e interactuar entre ellas.

## 📄 Descripción
Proyecto final desarrollado durante el segundo año de Grado Superior de Desarrollo de Aplicaciones Multiplataforma + Perfil videojuegos y ocio digital (DAMvi).

## 💻 Tecnologías
![C#](https://img.shields.io/badge/c%23-%23239120.svg?style=for-the-badge&logo=csharp&logoColor=white)
![Unity](https://img.shields.io/badge/unity-%23000000.svg?style=for-the-badge&logo=unity&logoColor=white)
![Blender](https://img.shields.io/badge/blender-%23F5792A.svg?style=for-the-badge&logo=blender&logoColor=white)

## 📽️ Gameplay y presentación ante el tribunal de DAMvi
[![](http://img.youtube.com/vi/7tLU-diHgAs/0.jpg)](http://www.youtube.com/watch?v=7tLU-diHgAs "Tráiler Whiskwarper")
[![](http://img.youtube.com/vi/qRlJYI4Xm34/0.jpg)](http://www.youtube.com/watch?v=qRlJYI4Xm34 "Presentación y demostración ante el tribunal de DAMvi")


## 📝 Documentación
+ [Game Design Document (GDD)](https://gitlab.com/slimmyteam/unity/whiskwarper/-/blob/main/Documentation/GDD_Whiskwarper.pdf)
+ [Manual de usuario e instalación](https://gitlab.com/slimmyteam/unity/whiskwarper/-/blob/main/Documentation/Manual_de_instal_laci%C3%B3_i_d_usuari_Whiskwarper.pdf)
+ [Diseño y código](https://gitlab.com/slimmyteam/unity/whiskwarper/-/blob/main/Documentation/Codi_Whiskwarper.pdf)
+ [Memoria](https://gitlab.com/slimmyteam/unity/whiskwarper/-/blob/main/Documentation/Mem%C3%B2ria_Whiskwarper.pdf)

## ©️ Desarrolladores
- Ethan Corchero Martín.
- Ana María Gómez León.
- Selene Milanés Rodríguez.