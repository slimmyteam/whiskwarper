using UnityEngine;


public class ActuadorNPC : MonoBehaviour
{
    [SerializeField]
    private MonoBehaviour m_ScriptReaccionador;



    public void ActuarNPC()
    {
        if (m_ScriptReaccionador is IReaccionador reaccionador)
            reaccionador.Reaccionar();
    }
}
