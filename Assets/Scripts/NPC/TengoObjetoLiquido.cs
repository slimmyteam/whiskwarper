using UnityEngine;

public class TengoObjetoLiquido : MonoBehaviour
{
    [SerializeField]
    private LiquidosSO m_Objeto;
    public LiquidosSO Objeto { get { return m_Objeto; } set { m_Objeto = value; } }
}
