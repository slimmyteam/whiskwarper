﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class NPCGenericoController : MonoBehaviour, INPC
{
    [SerializeField]
    private Enums.EnumNPCS m_TipoNPC;
    public Enums.EnumNPCS ObtenerTipoNPC { get { return m_TipoNPC; } }

    [Header("Navmesh Area")]
    private NavMeshAgent m_Agent;
    private NavMeshHit m_hit;
    [SerializeField]
    private string m_NombreAreaCaminar;
    [SerializeField]
    private float m_tiempoEsperaCaminarMin, m_tiempoEsperaCaminarMax;
    private int m_AreaPatrullaje;
    private Vector3 m_PosicionDeDestino;

    [Header("Limites mapa")]
    [SerializeField]
    private float m_MapaXMin;
    [SerializeField]
    private float m_MapaXMax;
    [SerializeField]
    private float m_MapaZMin;
    [SerializeField]
    private float m_MapaZMax;

    [SerializeField]
    private Coroutine m_Andar;

    private void Awake()
    {
        m_Agent = GetComponent<NavMeshAgent>();
        m_AreaPatrullaje = 1 << NavMesh.GetAreaFromName(m_NombreAreaCaminar);
    }

    private void Start()
    {
        m_Andar = StartCoroutine(Andar());
    }

    public void ComenzarDialogoNPC()
    {
        if (!TryGetComponent<Dialogo>(out _)) return;

        Dialogo[] dialogosNPC = GetComponents<Dialogo>();

        Dialogo dialogoNPC = dialogosNPC[0];

        if (dialogosNPC.Length > 1)
        {
            Dialogo dialogoNPCARepetir = dialogosNPC[1];

            if (dialogoNPC.DialogoHecho)
            {
                DialogueManager.Instance.ComenzarDialogo(dialogoNPCARepetir);
                return;
            }
        }

        DialogueManager.Instance.ComenzarDialogo(dialogoNPC);
    }

    private IEnumerator Andar()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(m_tiempoEsperaCaminarMin, m_tiempoEsperaCaminarMax));
            while (!NavMesh.SamplePosition(new Vector3(Random.Range(m_MapaXMin, m_MapaXMax), 0, Random.Range(m_MapaZMin, m_MapaZMax)), out m_hit, Mathf.Infinity, m_AreaPatrullaje))
            {
                yield return null;
            }

            Debug.DrawLine(transform.position, m_hit.position, Color.red + Color.blue, 5f);
            m_PosicionDeDestino = m_hit.position;
            m_Agent.SetDestination(m_PosicionDeDestino);
        }
    }

    private IEnumerator VolverAAndar()
    {
        yield return new WaitForSeconds(6);
        m_Andar = StartCoroutine(Andar());

    }

    public void PararDeAndar()
    {
        StopCoroutine(m_Andar);
        StartCoroutine(VolverAAndar());
    }
}
