using UnityEngine;

public class TengoObjetoHabilidades : MonoBehaviour
{
    [SerializeField]
    private Enums.EnumHabilidades m_Objeto;
    public Enums.EnumHabilidades Objeto { get { return m_Objeto; } set { m_Objeto = value; } }
}
