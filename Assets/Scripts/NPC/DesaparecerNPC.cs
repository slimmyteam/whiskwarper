using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesaparecerNPC : MonoBehaviour, IReaccionador
{
    [SerializeField]
    private Collider m_SustitutoCollider;
    public void Reaccionar()
    {
        m_SustitutoCollider.enabled = true;
        Destroy(gameObject);
    }

    
}
