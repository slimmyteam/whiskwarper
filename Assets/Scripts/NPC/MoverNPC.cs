using UnityEngine;

public class MoverNPC : MonoBehaviour, IReaccionador
{
    [SerializeField]
    private Transform m_posicionFinal;
    public void Reaccionar()
    {
        transform.position = m_posicionFinal.position;
        transform.rotation = m_posicionFinal.rotation;
    }
}
