using UnityEngine;

public class NPCController : MonoBehaviour, INPC, IGuardable
{
    [Header("Parametros NPC")]
    [SerializeField]
    private Enums.EnumNPCS m_TipoNPC;
    public Enums.EnumNPCS ObtenerTipoNPC { get { return m_TipoNPC; } }

    [Header("Guardar partida")]
    [SerializeField]
    private int m_IdNPC;
    [SerializeField]
    private GEInt m_NPCResuelto;

    private Dialogo[] m_DialogosNPC;

    private void Awake()
    {
        m_DialogosNPC = GetComponents<Dialogo>();
    }

    public void ComenzarDialogoNPC()
    {
        if (!TryGetComponent<Dialogo>(out _)) 
            return;

        Dialogo dialogoNPC = m_DialogosNPC[0];

        if (m_DialogosNPC.Length > 1)
        {
            Dialogo dialogoNPCARepetir = m_DialogosNPC[1];

            if (dialogoNPC.DialogoHecho)
            {
                DialogueManager.Instance.ComenzarDialogo(dialogoNPCARepetir);
                if (TryGetComponent(out ActuadorNPC actuador))
                    DialogueManager.Instance.actuadorNPC = actuador;
                return;
            }
        }

        DialogueManager.Instance.ComenzarDialogo(dialogoNPC);
        AvisarEstadoResuelto();
    }

    public void AvisarEstadoResuelto()
    {
        if(m_IdNPC != 0)
            m_NPCResuelto.Raise(m_IdNPC);
    }

    public void FinalizarDialogo()
    {
        m_DialogosNPC[0].DialogoHecho = true;
    }
}
