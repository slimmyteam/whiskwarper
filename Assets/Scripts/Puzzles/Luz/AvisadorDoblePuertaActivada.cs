using UnityEngine;

public class AvisadorDoblePuertaActivada : MonoBehaviour, IMecanismo, IGuardable
{
    [SerializeField]
    private AvisadorDoblePuertaActivada m_OtroAvisador;
    public bool activo;
    [SerializeField]
    private MonoBehaviour m_BotonPuerta;

    [Header("Guardar partida")]
    [SerializeField]
    private int m_IdPuzzle;
    [SerializeField]
    private GEInt m_PuzzleResuelto;

    public void EjecutarMecanismo()
    {
        activo = true;
        if (m_OtroAvisador.activo && m_BotonPuerta is IMecanismo mecanismo)
            mecanismo.EjecutarMecanismo();
    }

    void Start()
    {
        activo = false;
    }
    public void AvisarEstadoResuelto()
    {
        if (m_PuzzleResuelto != null)
            m_PuzzleResuelto.Raise(m_IdPuzzle);
    }
}
