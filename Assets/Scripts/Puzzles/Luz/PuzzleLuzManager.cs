using UnityEngine;

public class PuzzleLuzManager : MonoBehaviour, IPuzzle
{
    [Header("Puzzle SO")]
    [SerializeField]
    private PuzzlesLuzSO m_PuzzleLuzSO;
    public PuzzlesLuzSO PuzzleLuzSO { get { return m_PuzzleLuzSO; } }
    [SerializeField]
    private int m_MargenCantidadEspejos;

    private void Awake()
    {
        m_PuzzleLuzSO.CantidadEspejos = gameObject.transform.childCount + m_MargenCantidadEspejos;
    }

    public void PuzzleFinalizado()
    {
        m_PuzzleLuzSO.PuzzleFinalizado = true;

        for (int i = 0; i < gameObject.transform.childCount; ++i)
        {
            gameObject.transform.GetChild(i).gameObject.tag = "ObjetoInamovible";

            if (gameObject.transform.GetChild(i).gameObject.TryGetComponent(out IMecanismo Mecanismo))
            {
                Mecanismo.EjecutarMecanismo();
            }
        }
    }
}
