using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class RayosController : MonoBehaviour
{
    [Header("Parametros Rayos")]
    [SerializeField]
    private Enums.EnumPuertasLuz m_TipoRayo;
    [SerializeField]
    private int m_NumDeReflects; //Numero de rayos
    [SerializeField]
    private int m_TamanyoPorDefecto = 20; //Tamanyo del rayo global
    [SerializeField]
    private LayerMask m_MasksRayos;
    [SerializeField]
    private LineRenderer m_LineRenderer;
    private RaycastHit m_Hit;
    private RaycastHit m_AnteriorHit;
    private Ray Rayo;

    [Header("Referencias")]
    [SerializeField]
    private PuzzleLuzManager m_PuzzleManager;
    [SerializeField]
    private GameObject m_CamaraIzquierda;
    [SerializeField]
    private GameObject m_CamaraDerecha;

    [Header("Portal")]
    [SerializeField]
    private float m_MargenPortal;

    [SerializeField]
    private bool m_TocadaPuerta = false;
    [SerializeField]
    private List<int> m_ListaPosicionesDePortal = new();

    void Start()
    {
        m_LineRenderer = GetComponent<LineRenderer>();
        m_NumDeReflects = m_PuzzleManager.PuzzleLuzSO.CantidadEspejos; //Creamos tantos rayos como espejos hayan
        m_TamanyoPorDefecto *= m_NumDeReflects; //Por cada rayo, aumentamos el tamanyo
    }

    void Update()
    {
        CrearRayos();
    }

    private void CrearRayos()
    {
        m_ListaPosicionesDePortal.Clear();
        Rayo = new Ray(transform.position, transform.forward);

        m_LineRenderer.positionCount = 1;
        m_LineRenderer.SetPosition(0, transform.position);

        float TamanyoRestante = m_TamanyoPorDefecto;

        for (int i = 0; i < m_NumDeReflects; ++i)
        {
            if (Physics.Raycast(Rayo.origin, Rayo.direction, out m_Hit, m_TamanyoPorDefecto, m_MasksRayos))
            {
                Debug.DrawRay(Rayo.origin, Rayo.direction, Color.green);

                Vector3 posicion = m_Hit.point;

                if (!m_ListaPosicionesDePortal.Contains(m_LineRenderer.positionCount - 3) && m_Hit.transform.gameObject.transform.childCount > 0 && LayerMask.LayerToName(m_Hit.transform.gameObject.transform.GetChild(0).gameObject.layer) == "Portal")
                {
                    m_ListaPosicionesDePortal.Add(m_LineRenderer.positionCount - 1);

                    m_LineRenderer.positionCount++;
                    m_LineRenderer.SetPosition(m_LineRenderer.positionCount - 1, posicion);

                    if (EstoyALaIzquierda(m_Hit.point.x))
                    {
                        posicion = new Vector3(m_CamaraDerecha.transform.GetChild(0).transform.position.x + m_MargenPortal, m_AnteriorHit.point.y, m_Hit.point.z);
                    }
                    else
                    {
                        posicion = new Vector3(m_CamaraIzquierda.transform.GetChild(0).transform.position.x - m_MargenPortal, m_AnteriorHit.point.y, m_Hit.point.z);
                    }


                }
                m_AnteriorHit = m_Hit;
                m_LineRenderer.positionCount++;
                m_LineRenderer.SetPosition(m_LineRenderer.positionCount - 1, posicion);

                TamanyoRestante -= Vector3.Distance(Rayo.origin, posicion);

                m_TocadaPuerta = m_Hit.transform.gameObject.TryGetComponent(out MecanismoPuertaLuz PuertaLuz);

                if (m_TocadaPuerta && PuertaLuz.EsDelMismoTipo(m_TipoRayo))
                {
                    CrearLineRendererFinal();
                    m_PuzzleManager.PuzzleFinalizado();
                    return; // Comprobamos si es puerta final para que no devuelva rayo
                }

                if (m_Hit.transform.gameObject.tag != "Espejo" && m_Hit.transform.gameObject.tag != "Portal" && m_Hit.transform.gameObject.tag != "MainCamera") return;
                
                Rayo = new Ray(posicion, Vector3.Reflect(Rayo.direction, m_Hit.normal));
            }
            else
            {
                m_LineRenderer.positionCount++;
                m_LineRenderer.SetPosition(m_LineRenderer.positionCount - 1, Rayo.origin + (Rayo.direction * TamanyoRestante));
            }
        }
    }

    private bool EstoyALaIzquierda(float x)
    {
        if (x < 0) return true;
        return false;
    }

    private void CrearLineRendererFinal()
    {
        // Obtenemos las posiciones del LineRenderer original
        Vector3[] posiciones = new Vector3[m_LineRenderer.positionCount];
        m_LineRenderer.GetPositions(posiciones);

        // Creamos un nuevo GameObject con un LineRenderer
        GameObject RayoFinal = new GameObject("RayoFinal");
        RayoFinal.transform.parent = transform.parent;
        LineRenderer LineRendererRayoFinal = RayoFinal.AddComponent<LineRenderer>();

        LineRendererRayoFinal.positionCount = posiciones.Length;
        LineRendererRayoFinal.widthCurve = m_LineRenderer.widthCurve;
        LineRendererRayoFinal.colorGradient = m_LineRenderer.colorGradient;
        LineRendererRayoFinal.numCornerVertices = m_LineRenderer.numCornerVertices;
        LineRendererRayoFinal.numCapVertices = m_LineRenderer.numCapVertices;
        LineRendererRayoFinal.useWorldSpace = m_LineRenderer.useWorldSpace;
        LineRendererRayoFinal.material = m_LineRenderer.material;

        // Asignamos las posiciones al nuevo LineRenderer
        LineRendererRayoFinal.SetPositions(posiciones);

        Destroy(gameObject);
    }
}
