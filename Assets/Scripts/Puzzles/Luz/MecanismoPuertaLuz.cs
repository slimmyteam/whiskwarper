using System.Collections;
using UnityEngine;


public class MecanismoPuertaLuz : MonoBehaviour, IReaccionador
{
    [SerializeField]
    private Enums.EnumPuertasLuz m_TipoPuerta;
    public Enums.EnumPuertasLuz TipoPuerta { get { return m_TipoPuerta; } }
    public float m_Duracion = 1f;
    private float m_PosicionAbierta;
    [SerializeField]
    private MonoBehaviour m_BotonPuerta;

    private void Start()
    {
        m_PosicionAbierta = transform.position.y - 10;
    }

    public bool EsDelMismoTipo(Enums.EnumPuertasLuz enumRayo)
    {
        if (m_TipoPuerta == enumRayo)
        {
            Reaccionar();
            return true;
        }
        return false;
    }


    private IEnumerator BajarPuerta()
    {
        Vector3 posicionInicial = transform.position;
        Vector3 posicionFinal = new Vector3(transform.position.x, m_PosicionAbierta, transform.position.z);
        float elapsedTime = 0f;

        while (elapsedTime < m_Duracion)
        {
            float t = elapsedTime / m_Duracion;
            transform.position = Vector3.Lerp(posicionInicial, posicionFinal, t);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.position = posicionFinal;
        yield return null;
        GetComponent<BoxCollider>().enabled = false;
    }

    public void Reaccionar()
    {
        if (m_BotonPuerta is IMecanismo mecanismo)
            mecanismo.EjecutarMecanismo();
    }
}
