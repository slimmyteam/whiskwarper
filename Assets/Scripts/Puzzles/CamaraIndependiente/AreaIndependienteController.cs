using UnityEngine;

public class AreaIndependienteController : MonoBehaviour, IMecanismo
{
    private PlayerController m_PlayerController;
    private bool m_PlayerFijo;

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_EventoMantenerOffset;
    [SerializeField]
    private GELimitesCamara m_EventoLimitesCamara;

    [Header("Limites Camara")]
    [SerializeField]
    private StructContainerScript.LimitesDeCamara m_LimitesDeCamara;

    [Header("CamaraCongelada")]
    [SerializeField]
    CongelarCamara m_CongelarCamara;

    public void EjecutarMecanismo()
    {
        if (m_PlayerController != null && !m_CongelarCamara.CamaraCongelada)
        {
            m_PlayerFijo = !m_PlayerFijo;
            m_PlayerController.CambiarControlCamara(m_PlayerFijo);
            m_PlayerController.transform.position = new Vector3(transform.position.x, m_PlayerController.transform.position.y, transform.position.z);
            m_EventoMantenerOffset.Raise();
            m_EventoLimitesCamara.Raise(m_LimitesDeCamara);
        }
    }

    void Start()
    {
        m_PlayerFijo = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out PlayerController playerController))
            m_PlayerController = playerController;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.TryGetComponent(out PlayerController _))
            m_PlayerController = null;
    }
}
