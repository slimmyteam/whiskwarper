using System.Collections.Generic;
using UnityEngine;

public class PuzzleBossRicochetManager : MonoBehaviour, IPuzzle
{
    [Header("Puzzle Boss Ricochet")]
    [SerializeField]
    private PuzzleBossRicochetSO m_PuzzleBossRicochetSO;
    [SerializeField]
    private BossRicochet m_BossRicochet;
    [SerializeField]
    private GameObject[] m_Pilares;
    [SerializeField]
    private GameObject[] m_PilaresFinalRuta;
    [SerializeField]
    private LiquidosSO m_SOLava, m_SOHielo;

    [SerializeField]
    List<LiquidosSO> SOLiquidos;
    [SerializeField]
    LiquidosSO[] SOLiquidosDesordenados;

    private void Awake()
    {
        m_Pilares = new GameObject[transform.childCount];
        SOLiquidos = new List<LiquidosSO>();
        SOLiquidosDesordenados = new LiquidosSO[m_Pilares.Length];
        ObtenerGameObjectsHijos();
    }

    private void Start()
    {
        AsignarMaterialAleatorioAPilares();
    }

    private void ObtenerGameObjectsHijos()
    {
        for (int i = 0; i < transform.childCount; ++i)
            m_Pilares[i] = transform.GetChild(i).gameObject;
    }

    public void AsignarMaterialAleatorioAPilares()
    {
        int cantidadLava = m_Pilares.Length / 2;

        for (int i = 0; i < m_Pilares.Length; ++i)
        {
            if (cantidadLava > 0)
            {
                SOLiquidos.Add(m_SOLava);
                cantidadLava--;
            }
            else
            {
                SOLiquidos.Add(m_SOHielo);
            }
        }

        DesordenarArray();
    }

    private void DesordenarArray()
    {
        for (int i = 0; i < m_Pilares.Length; ++i)
        {
            int numRandom = Random.Range(0, SOLiquidos.Count);
            SOLiquidosDesordenados[i] = SOLiquidos[numRandom];
            SOLiquidos.RemoveAt(numRandom);
        }

        AsignarMaterialAPilar();
    }

    private void AsignarMaterialAPilar()
    {
        for (int i = 0; i < m_Pilares.Length; ++i)
        {
            m_Pilares[i].gameObject.GetComponent<PilarElementalController>().AsignarMaterialAlPilar(SOLiquidosDesordenados[i]);
        }

        SOLiquidosDesordenados = new LiquidosSO[m_Pilares.Length]; // Limpiamos array
        ComprobarQueAlMenosUnoEsDeElementoBoss();
    }

    /// <summary>
    /// Para que no se de la situacion en que el player no pueda hacer vulnerable al boss porque no hay pilares del elemento contrario, se controla con esta funcion.
    /// De esta forma, los pilares finales en los cuales se puede devolver los proyectiles, siempre habra uno de cada mapa que sera del elemento contrario al boss
    /// </summary>
    private void ComprobarQueAlMenosUnoEsDeElementoBoss()
    {
        for (int i = 0; i < m_PilaresFinalRuta.Length; ++i)
        {
            if (m_PilaresFinalRuta[i].gameObject.GetComponent<PilarElementalController>().SOElemento.TipoLiquido != m_BossRicochet.Elemento)
            {
                break;
            }

            if (i == m_PilaresFinalRuta.Length - 1)
            {
                LiquidosSO liquidoDiferenteABoss = m_BossRicochet.Elemento == Enums.EnumLiquidos.LAVA ? m_SOHielo : m_SOLava;

                m_PilaresFinalRuta[i].gameObject.GetComponent<PilarElementalController>().SOElemento = liquidoDiferenteABoss;
            }
        }
    }

    public void PuzzleFinalizado()
    {
        m_PuzzleBossRicochetSO.PuzzleFinalizado = true;
    }
}
