using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CintaController : MonoBehaviour
{
    [SerializeField]
    private float m_Velocidad;
    private Rigidbody m_Rigidbody;
    
    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        m_Rigidbody.position -= m_Velocidad * Time.deltaTime * transform.right;
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Velocidad * Time.deltaTime * transform.right);
    }
}
