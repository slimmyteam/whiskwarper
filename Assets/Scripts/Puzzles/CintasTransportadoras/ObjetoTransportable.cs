using Unity.VisualScripting;
using UnityEngine;

[RequireComponent(typeof(ControlDeLayers))]
public class ObjetoTransportable : MonoBehaviour
{
    [SerializeField]
    private bool m_ConObjetivo;
    [SerializeField]
    private Vector3 m_PosicionOriginal;
    [SerializeField]
    private LayerMask m_LayersEquivocadas; //Layers que hacen regresar el objeto a la posicion inicial si las toca
    [SerializeField]
    private LayerMask m_LayersExcepcion; //Layers que no hacen regresar el objeto, pero tampoco finalizan el puzle (es decir, se tienen que ignorar) - por ejemplo, "portal"

    //Componente
    private Rigidbody m_Rigidbody;
    private TeletransporteObjetos m_TeletransporteObjetos;
    private ControlDeLayers m_ControlDeLayers;

    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_TeletransporteObjetos = GetComponent<TeletransporteObjetos>();
        m_ControlDeLayers = GetComponent<ControlDeLayers>();
    }

    private void Start()
    {
        m_PosicionOriginal = transform.position;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (m_ConObjetivo && m_ControlDeLayers.ContieneLayer(m_LayersEquivocadas, collision.gameObject.layer))
        {
            transform.position = m_PosicionOriginal;
        }
        else if (m_Rigidbody != null && !collision.gameObject.CompareTag("TransportadorObjetos") && !m_ControlDeLayers.ContieneLayer(m_LayersExcepcion, collision.gameObject.layer)) //Cuando llega a su objetivo, no se puede teletransportar otra vez
        {
            m_LayersEquivocadas &= ~(1 << LayerMask.NameToLayer("Player"));
            Destroy(m_TeletransporteObjetos);
            Destroy(m_Rigidbody);
        }
    }
}
