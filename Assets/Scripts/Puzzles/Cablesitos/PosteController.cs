using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent (typeof(BoxCollider))]
public class PosteController : MonoBehaviour, IMecanismoReaccionadorObjetoCargado
{
    [Header("Referecias")]
    [SerializeField]
    private GameObject m_Cable;

    [Header("Guardar partida")]
    [SerializeField]
    private int m_IdPuzzle;
    [SerializeField]
    private GEInt m_PuzzleResuelto;

    [Header("Estado del poste")]
    [SerializeField]
    private bool m_TieneEnergia;
    public bool TieneEnergia { get => m_TieneEnergia; set => m_TieneEnergia = value; }

    //Componentes
    private ParticleSystem m_ParticleSystem;

    private void Awake()
    {
        m_ParticleSystem = GetComponentInChildren<ParticleSystem>();
    }

    private void Start()
    {
        if (m_TieneEnergia)
            m_ParticleSystem.Play();
    }

    public bool ReaccionaObjetoCargado(string nombreObjetoCargado)
    {
        if (m_TieneEnergia)
            return false;

        if (nombreObjetoCargado == "NodoCableCargado")
        {
            EjecutarMecanismo();
            return true;
        }
        return false;

    }
    public void EjecutarMecanismo()
    {
        m_TieneEnergia = true;
        m_ParticleSystem.Play();
        if (m_Cable != null)
            m_Cable.GetComponent<CableController>().RecibirEnergia();
    }

    public void ReaccionaContenidoCubo(ScriptableObject contenidoCubo)
    {

    }
}
