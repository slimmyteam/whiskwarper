using System.Collections.Generic;
using UnityEngine;

public class CableController : MonoBehaviour, IRompible
{
    [Header("Prefabs")]
    [SerializeField]
    private GameObject m_NodoCablePrefab;
    [SerializeField]
    private GameObject m_EfectoPortalPrefab;

    [Header("Control de nodos del cable")]
    [SerializeField]
    private List<GameObject> m_NodosCable1 = new();
    [SerializeField]
    private List<GameObject> m_NodosCable2 = new();
    [SerializeField]
    private GameObject m_UltimoTrozo;
    [SerializeField]
    private GameObject m_OrigenCable;
    private GameObject m_OrigenCable2;
    [SerializeField]
    private float m_LongitudActualPrimerCable;
    [SerializeField]
    private float m_LongitudActualSegundoCable;
    [SerializeField]
    private float m_LongitudCableActualPorTrozo;
    [SerializeField]
    private float m_MaximaLongitud;
    [SerializeField]
    private float m_DistanciaMaximaPorCadaTrozo;
    [SerializeField]
    private float m_DistanciaMinimaPorCadaTrozo;
    private float m_AlturaSuelo;
    [SerializeField]
    private float m_MargenSuelo;
    private Vector3 m_PosicionFinalCable1;

    [Header("Conectar cable")]
    [SerializeField]
    private float m_RadioEsferaConexion;

    [Header("Estado del cable")]
    [SerializeField]
    private bool m_TieneEnergia;
    [SerializeField]
    private bool m_Enchufado;
    [SerializeField]
    private bool m_CruzandoDimension;

    //Componentes
    private LineRenderer m_LineRenderer1;
    private LineRenderer m_LineRenderer2;

    private void Awake()
    {
        m_LineRenderer1 = transform.GetChild(0).GetComponent<LineRenderer>();
        m_LineRenderer2 = transform.GetChild(1).GetComponent<LineRenderer>();
    }

    void Start()
    {
        foreach (Transform child in transform)
        {
            if (child.TryGetComponent<NodoCable>(out _))
            {
                m_NodosCable1.Add(child.gameObject);
                if (m_TieneEnergia)
                    child.name = "NodoCableCargado";
            }
        }
        m_UltimoTrozo = m_NodosCable1[^1];
        m_LineRenderer1.positionCount = m_NodosCable1.Count;
        m_AlturaSuelo = m_UltimoTrozo.transform.position.y + m_MargenSuelo;
        if (m_Enchufado)
            ActualizarPosicionesCable();
    }

    private void Update()
    {
        if (!m_Enchufado)
        {
            ComprobarLongitud();
            ActualizarPosicionesCable();
        }
    }

    private void ActualizarPosicionesCable()
    {
        m_LineRenderer1.positionCount = m_NodosCable1.Count;
        m_LineRenderer2.positionCount = m_NodosCable2.Count;
        for (int i = 0; i < m_NodosCable1.Count; i++)
        {
            m_LineRenderer1.SetPosition(i, m_NodosCable1[i].transform.position);
        }
        for (int i = 0; i < m_NodosCable2.Count; i++)
        {
            m_LineRenderer2.SetPosition(i, m_NodosCable2[i].transform.position);
        }
    }

    private void ComprobarLongitud()
    {
        m_LongitudActualPrimerCable = Vector3.Distance(m_NodosCable1[0].transform.position, m_NodosCable1[^1].transform.position);
        m_LongitudActualSegundoCable = m_NodosCable2.Count > 1 ? Vector3.Distance(m_NodosCable2[0].transform.position, m_NodosCable2[^1].transform.position) : 0;
        float longitudActual = m_LongitudActualPrimerCable + m_LongitudActualSegundoCable;
        m_LongitudCableActualPorTrozo = !m_CruzandoDimension ? m_LongitudActualPrimerCable / m_NodosCable1.Count : m_LongitudActualSegundoCable / m_NodosCable2.Count;

        if (longitudActual > m_MaximaLongitud)
        {
            HacerSoltarCable();
            return;
        }
        if (m_LongitudCableActualPorTrozo > m_DistanciaMaximaPorCadaTrozo && m_UltimoTrozo.GetComponent<NodoCable>().SiendoArrastrado)
            GenerarMasCable();
        else if (m_NodosCable1.Count > 2 && m_LongitudCableActualPorTrozo < m_DistanciaMinimaPorCadaTrozo)
            DesaparecerJoint();
    }

    private void GenerarMasCable()
    {
        GameObject nodoQueMover = !m_CruzandoDimension ? m_NodosCable1[m_NodosCable1.Count / 2] : m_NodosCable2[m_NodosCable2.Count / 2];
        Vector3 sumaPosiciones = !m_CruzandoDimension ? m_UltimoTrozo.transform.position + m_OrigenCable.transform.position : m_UltimoTrozo.transform.position + m_OrigenCable2.transform.position;
        Vector3 posicionIntermedia = sumaPosiciones / 2;
        Rigidbody nodoAlQueConectarse = !m_CruzandoDimension ? m_NodosCable1[m_NodosCable1.Count / 2 - 1].GetComponent<Rigidbody>() : m_NodosCable2[m_NodosCable2.Count / 2 - 1].GetComponent<Rigidbody>();

        GameObject nuevoNodo = InstanciarNuevoJoint(nodoQueMover, nodoAlQueConectarse, posicionIntermedia);

        if (!m_CruzandoDimension)
        {
            m_LineRenderer1.positionCount++;
            m_NodosCable1.Insert(m_NodosCable1.Count / 2, nuevoNodo);
        }
        else
        {
            m_LineRenderer2.positionCount++;
            m_NodosCable2.Insert(m_NodosCable2.Count / 2, nuevoNodo);
        }
    }

    private GameObject InstanciarNuevoJoint(GameObject nodoQueMover, Rigidbody nodoAlQueConectarse, Vector3 posicion)
    {
        GameObject nuevoNodo = Instantiate(m_NodoCablePrefab, transform);
        nuevoNodo.SetActive(true);
        if (nodoAlQueConectarse != null)
        {
            nuevoNodo.GetComponent<NodoCable>().IniciarNodoIntermedio(nodoAlQueConectarse, posicion, gameObject, m_AlturaSuelo, m_TieneEnergia);
            if (nodoQueMover != null)
                nodoQueMover.GetComponent<NodoCable>().CambiarConnectedBody(nuevoNodo.GetComponent<Rigidbody>());
        }
        else
            nuevoNodo.GetComponent<NodoCable>().IniciarNodoInicio(posicion, gameObject, m_AlturaSuelo, m_TieneEnergia);
        return nuevoNodo;
    }

    private void DesaparecerJoint()
    {
        if (m_CruzandoDimension && m_NodosCable2.Count <= 2)
            return;
        GameObject nodoQueBorrar = !m_CruzandoDimension ? m_NodosCable1[m_NodosCable1.Count / 2] : m_NodosCable2[m_NodosCable2.Count / 2];
        GameObject nodoQueCambiarConfiguracion = !m_CruzandoDimension ? m_NodosCable1[m_NodosCable1.Count / 2 + 1] : m_NodosCable2[m_NodosCable2.Count / 2 + 1];
        Rigidbody nodoAlQueConectarse = !m_CruzandoDimension ? m_NodosCable1[m_NodosCable1.Count / 2 - 1].GetComponent<Rigidbody>() : m_NodosCable2[m_NodosCable2.Count / 2 - 1].GetComponent<Rigidbody>();
        nodoQueCambiarConfiguracion.GetComponent<NodoCable>().CambiarConnectedBody(nodoAlQueConectarse);
        if (!m_CruzandoDimension)
            m_NodosCable1.Remove(nodoQueBorrar);
        else
            m_NodosCable2.Remove(nodoQueBorrar);
        Destroy(nodoQueBorrar);

    }

    public void HacerSoltarCable()
    {
        if (m_UltimoTrozo.GetComponent<NodoCable>().SiendoArrastrado)
            m_UltimoTrozo.GetComponent<NodoCable>().HacerSoltarCable();
    }

    public void SoltarCableSiCruzandoDimension()
    {
        if (m_CruzandoDimension)
            HacerSoltarCable();
    }

    public void RomperCable()
    {
        GameObject primerTrozo = m_NodosCable1[0];
        m_NodosCable1.Clear();
        m_NodosCable2.Clear();
        m_NodosCable1.Add(primerTrozo);
        m_NodosCable1.Add(m_UltimoTrozo);
        foreach (Transform child in transform)
        {
            if (child.TryGetComponent<NodoCable>(out _) && !m_NodosCable1.Contains(child.gameObject))
                Destroy(child.gameObject);
        }
        m_UltimoTrozo.transform.position = new Vector3(primerTrozo.transform.position.x, m_AlturaSuelo - m_MargenSuelo, primerTrozo.transform.position.z);
        m_UltimoTrozo.GetComponent<NodoCable>().CambiarConnectedBody(primerTrozo.GetComponent<Rigidbody>());
        m_CruzandoDimension = false;
        m_Enchufado = false;
    }

    public void ComprobarCruzandoDimension()
    {
        if (m_UltimoTrozo.GetComponent<NodoCable>().SiendoArrastrado)
            m_PosicionFinalCable1 = m_UltimoTrozo.transform.position;
    }

    public void ComprobarFinTeletransporte()
    {
        if (m_UltimoTrozo.GetComponent<NodoCable>().SiendoArrastrado)
            CruzandoDimension(m_PosicionFinalCable1, m_UltimoTrozo.transform.position);
    }

    private void CruzandoDimension(Vector3 posicionPrimeraDimension, Vector3 posicionSegundaDimension)
    {
        if (m_CruzandoDimension)
        {
            HacerSoltarCable();
            return;
        }

        m_NodosCable1.Remove(m_UltimoTrozo);
        GameObject nodoQueBorrar = m_NodosCable1[^1];
        m_NodosCable1.Remove(nodoQueBorrar);
        Destroy(nodoQueBorrar);

        Rigidbody nodoAlQueConectarse = m_NodosCable1[^1].GetComponent<Rigidbody>();
        float margenPortal = posicionPrimeraDimension.x < 0 ? 0.5f : -0.5f;
        Vector3 posicionRealPrimeraDimension = new(posicionPrimeraDimension.x, nodoAlQueConectarse.transform.position.y, nodoAlQueConectarse.transform.position.z);
        GameObject finCable1 = InstanciarNuevoJoint(null, nodoAlQueConectarse, posicionRealPrimeraDimension + Vector3.right * margenPortal);
        nodoAlQueConectarse.transform.position += Vector3.right * margenPortal;
        m_NodosCable1.Add(finCable1);
        foreach (GameObject nodo in m_NodosCable1)
        {
            nodo.GetComponent<Rigidbody>().isKinematic = true;
        }

        Vector3 posicionRealSegundaDimension = new(posicionSegundaDimension.x, posicionRealPrimeraDimension.y, posicionRealPrimeraDimension.z);
        GameObject inicioCable2 = InstanciarNuevoJoint(null, null, posicionRealSegundaDimension - Vector3.right * (margenPortal / 10));
        inicioCable2.GetComponent<Rigidbody>().isKinematic = true;
        m_OrigenCable2 = inicioCable2;
        m_UltimoTrozo.GetComponent<NodoCable>().CambiarConnectedBody(inicioCable2.GetComponent<Rigidbody>());
        m_NodosCable2.Add(inicioCable2);
        m_NodosCable2.Add(m_UltimoTrozo);

        m_CruzandoDimension = true;
    }

    public void RecibirEnergia()
    {
        if (m_TieneEnergia)
            return;
        m_TieneEnergia = true;

        foreach (GameObject nodo in m_NodosCable1)
        {
            nodo.GetComponent<NodoCable>().RecibirEnergia();
        }
    }

    public void ConectarCable()
    {
        foreach (GameObject nodo in m_NodosCable1)
        {
            nodo.GetComponent<Rigidbody>().isKinematic = true;
            nodo.GetComponent<NodoCable>().SerEnchufado();
        }

        foreach (GameObject nodo in m_NodosCable2)
        {
            nodo.GetComponent<Rigidbody>().isKinematic = true;
            nodo.GetComponent<NodoCable>().SerEnchufado();
        }

        Collider[] colliders = Physics.OverlapSphere(m_UltimoTrozo.transform.position, m_RadioEsferaConexion, Physics.AllLayers);
        foreach (Collider collider in colliders)
        {
            if (collider.gameObject.CompareTag("Enchufable"))
            {
                m_UltimoTrozo.transform.position = new(collider.transform.position.x, m_UltimoTrozo.transform.position.y, collider.transform.position.z);
                break;
            }
        }

        ActualizarPosicionesCable();

        if (m_CruzandoDimension)
        {
            InstanciarPortal(m_NodosCable1[^1].transform.position);
            InstanciarPortal(m_NodosCable2[0].transform.position);
        }

        m_Enchufado = true;
    }

    private void InstanciarPortal(Vector3 posicion)
    {
        GameObject portal = Instantiate(m_EfectoPortalPrefab, transform);
        portal.SetActive(true);
        portal.transform.position = posicion;
    }

    public void Romperse()
    {
        RomperCable();
    }
}
