using UnityEngine;

[RequireComponent (typeof(AudioSource))]
public class EnchufeController : MonoBehaviour, IMecanismoReaccionadorObjetoCargado, IRompible, IGuardable
{
    [Header("Estado del enchufe")]
    [SerializeField]
    private bool m_Activado;

    [Header("Guardar partida")]
    [SerializeField]
    private int m_IdPuzzle;
    [SerializeField]
    private GEInt m_PuzzleResuelto;

    [Header("Referencias")]
    [SerializeField]
    private GameObject m_MecanismoAsociado;

    [Header("SFX")]
    [SerializeField]
    private AudioClip m_AudioEnchufado;

    //Componentes
    private ParticleSystem m_ParticleSystem;

    private void Awake()
    {
        m_ParticleSystem = GetComponentInChildren<ParticleSystem>();
    }

    private void Start()
    {
        if (m_Activado)
            m_ParticleSystem.Play();
    }

    /// <summary>
    /// Implementa la funcion de IMecanismoReaccionadorObjetoCargado. Si el objeto cargado es un nodo de cable cargado y el mecanismo no se encuentra activado ya,
    /// activa el mecanismo.
    /// </summary>
    /// <param name="nombreObjetoCargado">Nombre del objeto que interactua con el mecanismo</param>
    /// <returns>bool: true si se ha activado el mecanismo, false si no se ha activado</returns>
    public bool ReaccionaObjetoCargado(string nombreObjetoCargado)
    {
        if (nombreObjetoCargado == "NodoCableCargado")
        {
            EjecutarMecanismo();
            return true;
        }
        return false;
    }

    /// <summary>
    /// Se activa cuando recibe contacto electrico. Activa el mecanismo asociado.
    /// </summary>
    public void ReaccionaContactoElectrico()
    {
        if (m_Activado)
            return;
        EjecutarMecanismo();
    }

    /// <summary>
    /// Marca el mecanismo como activado y hace que el mecanismo asociado reaccione.
    /// </summary>
    public void EjecutarMecanismo()
    {
        gameObject.GetComponent<AudioSource>().loop = false;
        gameObject.GetComponent<AudioSource>().clip = m_AudioEnchufado;
        gameObject.GetComponent<AudioSource>().Play();
        m_Activado = true;
        m_ParticleSystem.Play();
        m_MecanismoAsociado.GetComponent<IReaccionador>().Reaccionar();
        AvisarEstadoResuelto();
    }

    public void ReaccionaContenidoCubo(ScriptableObject contenidoCubo)
    {

    }

    public void Romperse()
    {
        m_Activado = false;
    }

    public void AvisarEstadoResuelto()
    {
        if(m_PuzzleResuelto != null)
            m_PuzzleResuelto.Raise(m_IdPuzzle);
    }
}
