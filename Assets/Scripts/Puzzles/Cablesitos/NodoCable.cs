using UnityEngine;

public class NodoCable : MonoBehaviour, ICargable
{
    private ConfigurableJoint m_Joint;
    private Rigidbody m_Rigidbody;

    [Header("Referencias")]
    [SerializeField]
    private GameObject m_Cable;

    [Header("Estado del nodo")]
    [SerializeField]
    private bool m_SiendoTransportado;
    public bool SiendoArrastrado { get { return m_SiendoTransportado; } }
    [SerializeField]
    private bool m_Enchufado;

    [Header("Parametros")]
    [SerializeField]
    private float m_AlturaSuelo;

    private void Awake()
    {
        m_Joint = GetComponent<ConfigurableJoint>();
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        m_Rigidbody.maxDepenetrationVelocity = 0;
        m_Rigidbody.inertiaTensor = Vector3.one;
    }

    private void Update()
    {
        if (m_SiendoTransportado)
            transform.forward = m_Joint.connectedBody.gameObject.transform.forward;
        if (transform.position.y < m_AlturaSuelo)
        {
            transform.position = new Vector3(transform.position.x, m_AlturaSuelo, transform.position.z);
        }
    }

    public void IniciarNodoIntermedio(Rigidbody rigidbodyOrigen, Vector3 posicion, GameObject cable, float alturaSuelo, bool tieneEnergia)
    {
        IniciarNodoInicio(posicion, cable, alturaSuelo, tieneEnergia);
        CambiarConnectedBody(rigidbodyOrigen);
        m_Rigidbody.isKinematic = false;
        m_Rigidbody.velocity = Vector3.zero;
    }

    public void IniciarNodoInicio(Vector3 posicion, GameObject cable, float alturaSuelo, bool tieneEnergia)
    {
        transform.position = posicion;
        m_Cable = cable;
        m_AlturaSuelo = alturaSuelo;
        m_Rigidbody.isKinematic = true;
        if (tieneEnergia)
            RecibirEnergia();
    }

    public void CambiarConnectedBody(Rigidbody nuevoConnectedBody)
    {
        m_Joint.connectedBody = nuevoConnectedBody;
    }

    public void HacerSoltarCable()
    {
        transform.GetComponentInParent<InteraccionesController>().Interactuar(false);
    }

    public void RecibirEnergia()
    {
        gameObject.name = "NodoCableCargado";
    }

    #region Interface cargable

    public void RecogerObjeto(GameObject posicionCabeza)
    {
        if (!m_Rigidbody.isKinematic || m_Enchufado)
            return;

        transform.SetParent(posicionCabeza.transform);
        transform.position = posicionCabeza.transform.position - transform.up * 0.5f;
        m_SiendoTransportado = true;
    }

    public string RecuperarNombreObjetoCargable()
    {
        if (m_SiendoTransportado)
            return gameObject.name;
        else
            return "";
    }

    public void SoltarObjeto(Vector3 posicionDejar)
    {
        transform.SetParent(m_Cable.transform);
        m_SiendoTransportado = false;
        m_Cable.GetComponent<CableController>().RomperCable();
    }

    public void UsarObjeto()
    {
        Debug.Log("USAR OBJETO");
        transform.SetParent(m_Cable.transform);
        m_SiendoTransportado = false;
        m_Cable.GetComponent<CableController>().ConectarCable();
    }

    public void SerEnchufado()
    {
        m_Enchufado = true;
    }

    public void SerArrastrado(GameObject boomerang)
    {
    }

    public void DesligarseBoomerang()
    {
    }
    #endregion
}
