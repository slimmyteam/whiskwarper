using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PuertaElectrica : MonoBehaviour, IReaccionador
{
    public float m_Duracion = 1f;
    private float m_PosicionAbierta;

    [Header("Estado de la puerta")]
    [SerializeField]
    private bool m_PuertaAbierta;

    [Header("SFX")]
    [SerializeField]
    private AudioClip m_AudioPuertaAbre;

    void Start()
    {
        m_PosicionAbierta = -transform.position.y;
        if(m_PuertaAbierta)
            StartCoroutine(BajarPuerta());
    }

    public void Reaccionar()
    {
        if(!m_PuertaAbierta)
            StartCoroutine(BajarPuerta());
    }

    private IEnumerator BajarPuerta()
    {
        Vector3 posicionInicial = transform.position;
        Vector3 posicionFinal = new Vector3(transform.position.x, m_PosicionAbierta, transform.position.z);
        float elapsedTime = 0f;
        this.gameObject.GetComponent<AudioSource>().loop = false;
        this.gameObject.GetComponent<AudioSource>().clip = m_AudioPuertaAbre;
        this.gameObject.GetComponent<AudioSource>().Play();
        while (elapsedTime < m_Duracion)
        {
            float t = elapsedTime / m_Duracion;
            transform.position = Vector3.Lerp(posicionInicial, posicionFinal, t);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.position = posicionFinal;
        yield return null;

        m_PuertaAbierta = true;
        gameObject.SetActive(false);
    }

}
