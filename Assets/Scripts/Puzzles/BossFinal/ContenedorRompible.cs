using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContenedorRompible : MonoBehaviour, IRompible
{
    //Componentes
    private ObjetoReaccionadorAContenidoCubo m_ObjetoReaccionador;

    private void Awake()
    {
        m_ObjetoReaccionador = GetComponent<ObjetoReaccionadorAContenidoCubo>();
    }

    public void Romperse()
    {
        m_ObjetoReaccionador.PerderLiquido();
    }
}
