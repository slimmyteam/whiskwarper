using UnityEngine;

public class CanonController : MonoBehaviour, IReaccionador {

    [Header("Referencias")]
    [SerializeField]
    private ObjetoReaccionadorAContenidoCubo m_MecanismoConectado;

    [Header("Caracteristicas")]
    [SerializeField]
    private BossFinalController.DebilidadBossFinal m_LiquidoDisparable;

    [SerializeField]
    private BossFinalController m_BossFinalController;

    public void Reaccionar()
    {
        m_BossFinalController.ComprobarAtaqueCanon(m_LiquidoDisparable);
        m_MecanismoConectado.PerderLiquido();
       /* if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit))
        {
            Debug.DrawLine(transform.position, hit.point, Color.magenta);
            if (hit.collider.gameObject.name == "PuntoDebil")
            {
                hit.transform.GetComponentInParent<BossFinalController>().ComprobarAtaqueCanon(m_LiquidoDisparable);
                m_MecanismoConectado.PerderLiquido();
            }
        }*/
    }
}
