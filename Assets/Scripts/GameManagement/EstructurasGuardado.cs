using System;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public struct EnemigosStruct
{
    public GameObject enemigo;
    public int idEnemigo;
    public bool enemigoVivo;

    public EnemigosStruct(GameObject enemigo, int id, bool vivo)
    {
        this.enemigo = enemigo;
        idEnemigo = id;
        enemigoVivo = vivo;
    }
}

[Serializable]
public struct PuzzlesStruct
{
    public GameObject puzzle;
    public int idPuzzle;
    public bool puzzleResuelto;

    public PuzzlesStruct(GameObject puzzle, int id, bool resuelto)
    {
        this.puzzle = puzzle;
        idPuzzle = id;
        puzzleResuelto = resuelto;
    }
}

[Serializable]
public struct NPCsStruct
{
    public GameObject npc;
    public int idNPC;
    public bool npcResuelto;

    public NPCsStruct(GameObject npc, int id, bool resuelto)
    {
        this.npc = npc;
        idNPC = id;
        npcResuelto = resuelto;
    }
}

[Serializable]
public struct TriggersProgresoStruct
{
    public GameObject trigger;
    public int idTrigger;
    public bool resuelto;

    public TriggersProgresoStruct(GameObject trigger, int idTrigger, bool resuelto)
    {
        this.trigger = trigger;
        this.idTrigger = idTrigger;
        this.resuelto = resuelto;
    }

}


[Serializable]
public class SaveData
{

    public GameData gameData;

    [Serializable]
    public struct GameData
    {
        [Header("Estatua de guardado")]
        public int idEstatuaGuardado;

        [Header("Player")]
        //Stats
        public float vidaPlayer;

        //Pociones
        public bool pocionesDesbloqueadas;
        public int cantidadPocionVida;
        public int cantidadPocionFuerza;
        public int cantidadPocionVelocidad;
        public int cantidadPocionInvisibilidad;

        //Habilidades
        public bool escudoDesbloqueado;
        public bool escudoMagicoDesbloqueado;
        public bool arcoDesbloqueado;
        public bool boomerangDesbloqueado;
        public bool cuboDesbloqueado;
        public bool bombaDesbloqueada;
        public bool minaDesbloqueada;

        [Header("Enemigos")]
        public List<EnemigosDatosGuardado> enemigos; //Enemigos - id, vivo

        [Header("NPCs")]
        public List<NPCsDatosGuardado> npcs; //NPC - id, resuelto

        [Header("Puzzles")]
        public List<PuzzlesDatosGuardado> puzzles; //NPC - id, resuelto

        [Header("Bosses")]
        public List<BossesDatosGuardado> bosses; //Bosses - id, vivo

        [Header("Slimes")]
        public int numeroSlimes;
        public List<SlimesDatosGuardado> slimes; //Slimes - id, recogido

        [Header("Progreso")]
        public List<TriggersProgresoDatosGuardado> triggersProgreso; //TriggersProgreso - id, resuelto
        public int trozosOrbe;
        public float porcentajeProgresion;
        public float volumenMusica;
        public float volumenEfectos;

        public GameData(int vidaPlayer)
        {
            idEstatuaGuardado = 0;

            this.vidaPlayer = vidaPlayer;

            pocionesDesbloqueadas = false;
            cantidadPocionVida = 0;
            cantidadPocionFuerza = 0;
            cantidadPocionVelocidad = 0;
            cantidadPocionInvisibilidad = 0;

            escudoDesbloqueado = false;
            escudoMagicoDesbloqueado = false;
            arcoDesbloqueado = false;
            boomerangDesbloqueado = false;
            cuboDesbloqueado = false;
            bombaDesbloqueada = false;
            minaDesbloqueada = false;

            enemigos = new()
            {

            };

            npcs = new()
            {

            };

            puzzles = new()
            {

            };

            bosses = new()
            {
                new BossesDatosGuardado(1, false), //NO HAY BOSS EN MAPA 1
                new BossesDatosGuardado(2, true), //RICOCHET
                new BossesDatosGuardado(3, true), //EXPLOSIVO
                new BossesDatosGuardado(4, true), //BOSS FINAL
            };

            slimes = new()
            {
                new SlimesDatosGuardado(1, false),
                new SlimesDatosGuardado(2, false),
                new SlimesDatosGuardado(3, false),
            };

            triggersProgreso = new()
            {
                new TriggersProgresoDatosGuardado(1, false),
                new TriggersProgresoDatosGuardado(2, false),
                new TriggersProgresoDatosGuardado(3, false),
                new TriggersProgresoDatosGuardado(4, false),
                new TriggersProgresoDatosGuardado(5, false),
                new TriggersProgresoDatosGuardado(6, false),
                new TriggersProgresoDatosGuardado(7, false),
                new TriggersProgresoDatosGuardado(8, false),
                new TriggersProgresoDatosGuardado(9, false),
            };

            numeroSlimes = 0;
            trozosOrbe = 1;
            porcentajeProgresion = 0;
            volumenEfectos = 1;
            volumenMusica = 1;
        }

        public GameData(int idEstatuaGuardado, int vidaPlayer, bool pocionesDesbloqueadas, int cantidadPocionVida, int cantidadPocionFuerza, int cantidadPocionVelocidad, int cantidadPocionInvisibilidad,
            bool escudoDesbloqueado, bool escudoMagicoDesbloqueado, bool arcoDesbloqueado, bool boomerangDesbloqueado, bool cuboDesbloqueado, bool bombaDesbloqueada, bool minaDesbloqueada,
            List<EnemigosDatosGuardado> enemigos, List<NPCsDatosGuardado> npcs, List<PuzzlesDatosGuardado> puzzles, List<BossesDatosGuardado> bosses, List<TriggersProgresoDatosGuardado> triggersProgreso, int numeroSlimes,
            List<SlimesDatosGuardado> slimes, int trozosOrbe, float porcentajeProgresion, float volumenEfectos, float volumenMusica)
        {
            this.idEstatuaGuardado = idEstatuaGuardado;

            this.vidaPlayer = vidaPlayer;

            this.pocionesDesbloqueadas = pocionesDesbloqueadas;
            this.cantidadPocionVida = cantidadPocionVida;
            this.cantidadPocionFuerza = cantidadPocionFuerza;
            this.cantidadPocionVelocidad = cantidadPocionVelocidad;
            this.cantidadPocionInvisibilidad = cantidadPocionInvisibilidad;

            this.escudoDesbloqueado = escudoDesbloqueado;
            this.escudoMagicoDesbloqueado = escudoMagicoDesbloqueado;
            this.arcoDesbloqueado = arcoDesbloqueado;
            this.boomerangDesbloqueado = boomerangDesbloqueado;
            this.cuboDesbloqueado = cuboDesbloqueado;
            this.bombaDesbloqueada = bombaDesbloqueada;
            this.minaDesbloqueada = minaDesbloqueada;

            this.enemigos = enemigos;

            this.npcs = npcs;

            this.puzzles = puzzles;

            this.bosses = bosses;

            this.slimes = slimes;

            this.triggersProgreso = triggersProgreso;

            this.numeroSlimes = numeroSlimes;

            this.trozosOrbe = trozosOrbe;
            this.porcentajeProgresion = porcentajeProgresion;
            this.volumenEfectos = volumenEfectos;
            this.volumenMusica = volumenMusica;
        }
    }

}

[Serializable]
public class EnemigosDatosGuardado
{
    public int idEnemigo;
    public bool vivo;

    public EnemigosDatosGuardado(int idEnemigo, bool vivo)
    {
        this.idEnemigo = idEnemigo;
        this.vivo = vivo;
    }
}

[Serializable]
public class PuzzlesDatosGuardado
{
    public int idPuzzle;
    public bool resuelto;

    public PuzzlesDatosGuardado(int idPuzzle, bool resuelto)
    {
        this.idPuzzle = idPuzzle;
        this.resuelto = resuelto;
    }
}

[Serializable]
public class NPCsDatosGuardado
{
    public int idNPC;
    public bool resuelto;

    public NPCsDatosGuardado(int idNPC, bool resuelto)
    {
        this.idNPC = idNPC;
        this.resuelto = resuelto;
    }
}

[Serializable]
public class BossesDatosGuardado
{
    public int idBoss;
    public bool vivo;

    public BossesDatosGuardado(int id, bool vivo)
    {
        idBoss = id;
        this.vivo = vivo;
    }
}

[Serializable]
public class SlimesDatosGuardado
{
    public int idSlime;
    public bool obtenido;

    public SlimesDatosGuardado(int id, bool recogido)
    {
        idSlime = id;
        obtenido = recogido;
    }
}

[Serializable]
public class TriggersProgresoDatosGuardado
{
    public int idTrigger;
    public bool resuelto;

    public TriggersProgresoDatosGuardado(int idTrigger, bool resuelto)
    {
        this.idTrigger = idTrigger;
        this.resuelto = resuelto;
    }
}