using UnityEngine;

public class AudioControllerEfectos : MonoBehaviour
{
    private AudioSource m_AudioSource;
    [SerializeField]
    private float m_VolumenMaximo;

    private void Awake()
    {
        m_AudioSource = GetComponent<AudioSource>();
        m_VolumenMaximo = m_AudioSource.volume;
    }

    private void Start()
    {
        ActualizarVolumenEfecto();
    }

    public void ActualizarVolumenEfecto()
    {
        m_AudioSource.volume = m_VolumenMaximo * GameManager.Instance.ObtenerConfiguracionVolumenEfectos();
    }
}
