using UnityEngine;

[RequireComponent (typeof(BoxCollider))]
public class TriggerCerrarPuertaBoss : MonoBehaviour
{
    [Header("Referencias")]
    [SerializeField]
    private GameObject m_PuertaBoss;

    [Header("Game Events")]
    [SerializeField]
    private GameEvent m_EmpezarBoss;

    private void OnTriggerExit(Collider other)
    {
        if (m_PuertaBoss.activeInHierarchy)
            return;
        m_PuertaBoss.SetActive(true);
        m_EmpezarBoss.Raise();
    }
}
