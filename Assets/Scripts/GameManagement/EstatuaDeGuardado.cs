using UnityEngine;

public class EstatuaDeGuardado : MonoBehaviour
{
    [Header("Guardar Partida")]
    [SerializeField]
    private int m_IdEstatua;
    public int IdEstatua { get => m_IdEstatua;}
    [SerializeField]
    private GEInt m_GuardarPartida;
    private Vector3 m_PosicionSpawneo;
    public Vector3 PosicionSpawneo { get => m_PosicionSpawneo; }

    //Componentes
    private ParticleSystem m_ParticleSystem;

    private void Awake()
    {
        m_PosicionSpawneo = transform.GetChild(0).transform.position;
        m_ParticleSystem = GetComponentInChildren<ParticleSystem>();
    }

    public void GuardarPartida()
    {
        m_GuardarPartida.Raise(m_IdEstatua);
        m_ParticleSystem.Play();
    }
}
