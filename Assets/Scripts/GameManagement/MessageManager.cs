using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MessageManager : MonoBehaviour
{
    [Header("Referencias elementos UI")]
    [SerializeField]
    private TextMeshProUGUI m_tituloMensaje;
    [SerializeField]
    private Image m_imagenMensaje;
    private Image m_imagenMensajeSprite;
    [SerializeField]
    private TextMeshProUGUI m_nombreMensaje;
    [SerializeField]
    private TextMeshProUGUI m_informacionMensaje;
    [SerializeField]
    private Mensaje m_MensajeActivo;

    [Header("Parametros MessageManager")]
    public static MessageManager Instance;
    [SerializeField]
    private float m_tiempoEsperaMostrarMensaje;

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_MensajeAbierto;

    //Componentes
    public Animator m_animatorMensaje;

    private void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }

        m_imagenMensajeSprite = m_imagenMensaje.GetComponent<Image>();
    }

    public void ComienzaEsperaParaMostrarMensaje(Mensaje mensaje)
    {
        m_MensajeAbierto?.Raise();
        m_MensajeActivo = mensaje;
        StartCoroutine(EsperaParaMostrarMensaje());
    }

    private IEnumerator EsperaParaMostrarMensaje()
    {
        yield return new WaitForSeconds(m_tiempoEsperaMostrarMensaje);
        MostrarMensaje();
    }

    private void MostrarMensaje()
    {
        m_animatorMensaje.SetBool("MensajeAbierto", true);
        m_tituloMensaje.text = m_MensajeActivo.TituloMensaje;
        m_imagenMensajeSprite.sprite = m_MensajeActivo.SpriteMensaje;
        m_nombreMensaje.text = m_MensajeActivo.NombreMensaje;
        m_informacionMensaje.text = m_MensajeActivo.InformacionMensaje;
    }

    public void FinalizarMensaje()
    {
        m_MensajeActivo.MensajeMostrado = true;
        m_animatorMensaje.SetBool("MensajeAbierto", false);
        m_MensajeAbierto?.Raise();
    }
}
