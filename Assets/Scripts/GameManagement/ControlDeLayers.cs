using UnityEngine;

public class ControlDeLayers : MonoBehaviour
{

    /// <summary>
    /// Comprueba si la LayerMask contiene la layer indicada
    /// </summary>
    /// <param name="layerMask">LayerMask donde comprobar si tiene la layer</param>
    /// <param name="layer">int layer que se quiere comprobar si esta</param>
    /// <returns>bool: indica si contiene la layer (true) o no (false)</returns>
    public bool ContieneLayer(LayerMask layerMask, int layer)
    {
        return ((layerMask & (1 << layer)) > 0);
    }
}
