using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [Header("Enemigos")]
    [SerializeField]
    private EnemigosStruct[] m_ListaEnemigos;
    public EnemigosStruct[] ListaEnemigos { get => m_ListaEnemigos; }
    [SerializeField]
    private bool m_BossDeZonaVivo;
    public bool BossDeZonaVivo { get => m_BossDeZonaVivo; set => m_BossDeZonaVivo = value; }

    [Header("Puzzles")]
    [SerializeField]
    private PuzzlesStruct[] m_ListaPuzzles;
    public PuzzlesStruct[] ListaPuzzles { get => m_ListaPuzzles; }

    [Header("NPCs")]
    [SerializeField]
    private NPCsStruct[] m_ListaNPCs;
    public NPCsStruct[] ListaNPCs { get => m_ListaNPCs; }

    [Header("Triggers de progreso")]
    [SerializeField]
    private TriggersProgresoStruct[] m_ListaTriggersProgreso;
    public TriggersProgresoStruct[] ListaTriggersProgreso { get => m_ListaTriggersProgreso; }

    [Header("Estatuas")]
    [SerializeField]
    private List<GameObject> m_EstatuasGuardado;

    [Header("Otras referencias")]
    [SerializeField]
    private List<GameObject> m_TriggersCambioEscenaEnemigoVivo;
    [SerializeField]
    private List<GameObject> m_TriggersCambioEscenaEnemigoMuerto;
    [SerializeField]
    private GameObject m_Player;
    [SerializeField]
    private GameObject[] m_ListaZonasQueDesactivar;

    private void Start()
    {
        m_Player = GameObject.Find("Player");
    }

    #region Gestion de nivel

    public void IniciarNivel(int estatuaGuardado)
    {
        //COLOCAR ENEMIGOS VIVOS
        foreach (EnemigosStruct enemigosStruct in m_ListaEnemigos)
        {
            enemigosStruct.enemigo.SetActive(enemigosStruct.enemigoVivo);
        }

        //COLOCAR PUZZLES RESUELTOS
        foreach (PuzzlesStruct puzzlesStruct in m_ListaPuzzles)
        {
            if (puzzlesStruct.puzzleResuelto)
            {
                Debug.Log("PUZZLE RESUELTO " + puzzlesStruct.idPuzzle);
                puzzlesStruct.puzzle.GetComponent<IMecanismo>().EjecutarMecanismo();
            }
        }

        //MARCAR DIALOGOS NPC COMO TERMINADOS
        foreach (NPCsStruct npcStruct in m_ListaNPCs)
        {
            if (npcStruct.npcResuelto)
            {
                npcStruct.npc.GetComponent<NPCController>().FinalizarDialogo();
                if (npcStruct.npc.TryGetComponent(out ActuadorNPC actuador))
                {
                    actuador.ActuarNPC();
                }
            }
        }

        //MARCAR TRIGGERS DE PROGRESO YA ACTIVADOS
        foreach (TriggersProgresoStruct trigger in m_ListaTriggersProgreso)
        {
            if (trigger.resuelto)
            {
                trigger.trigger.GetComponent<TriggerCambioEscena>().ResolverTrigger();
            }
        }

        ActivarTriggerCambioEscena();

        Vector3 posicionEstatua = Vector3.zero;

        foreach (GameObject estatua in m_EstatuasGuardado)
        {
            if (estatua.GetComponent<EstatuaDeGuardado>().IdEstatua == estatuaGuardado)
                posicionEstatua = estatua.transform.position - estatua.transform.forward * 2;
        }

        StartCoroutine(DesactivarZonasDespuesEsperarMecanismos());

        m_Player = GameObject.Find("Player");
        if (posicionEstatua != Vector3.zero)
            m_Player.transform.position = posicionEstatua;
    }

    private void ActivarTriggerCambioEscena()
    {
        foreach (GameObject trigger in m_TriggersCambioEscenaEnemigoVivo)
        {
            trigger.SetActive(m_BossDeZonaVivo);
        }
        foreach (GameObject trigger in m_TriggersCambioEscenaEnemigoMuerto)
        {
            trigger.SetActive(!m_BossDeZonaVivo);
        }
    }

    public void EnemigoMuerto(int id)
    {
        int posicion = RecuperarPosicionEnemigo(id);
        m_ListaEnemigos[posicion].enemigoVivo = false;
    }

    public void PuzzleResuelto(int id)
    {
        int posicion = RecuperarPosicionPuzzle(id);
        m_ListaPuzzles[posicion].puzzleResuelto = true;
    }

    public void NPCResuelto(int id)
    {
        int posicion = RecuperarPosicionNPC(id);
        m_ListaNPCs[posicion].npcResuelto = true;
    }

    public void TriggerResuelto(int id)
    {
        int posicion = RecuperarPosicionTrigger(id);
        Debug.Log("TRIGGER RESUELTO " + id + " POS " + posicion);
        m_ListaTriggersProgreso[posicion].resuelto = true;
    }

    private IEnumerator DesactivarZonasDespuesEsperarMecanismos()
    {
        yield return new WaitForSeconds(5f);

        foreach (GameObject zona in m_ListaZonasQueDesactivar)
        {
            zona.SetActive(false);
        }
    }

    #endregion

    #region Restaurar nivel
    public void RestaurarNivel(int estatuaGuardado, List<EnemigosDatosGuardado> listaEnemigos, List<PuzzlesDatosGuardado> listaPuzzles, List<NPCsDatosGuardado> listaNPC,
        List<TriggersProgresoDatosGuardado> listaTriggersProgreso, bool bossVivo)
    {
        //RESTAURAR ENEMIGOS
        foreach (EnemigosDatosGuardado datosEnemigos in listaEnemigos)
        {
            int posicion = RecuperarPosicionEnemigo(datosEnemigos.idEnemigo);
            if (posicion != -1)
                m_ListaEnemigos[posicion].enemigoVivo = datosEnemigos.vivo;
            //Debug.Log("POSICION " + posicion+ " VIVO "+datosEnemigos.vivo);
        }

        //RESTAURAR PUZZLES
        foreach (PuzzlesDatosGuardado datosPuzzles in listaPuzzles)
        {
            int posicion = RecuperarPosicionPuzzle(datosPuzzles.idPuzzle);
            //Debug.Log("PUZLE " + datosPuzzles.idPuzzle + " POS " + posicion);
            if (posicion != -1)
                m_ListaPuzzles[posicion].puzzleResuelto = datosPuzzles.resuelto;
        }

        //RESTAURAR NPCS
        foreach (NPCsDatosGuardado datosNPC in listaNPC)
        {
            int posicion = RecuperarPosicionNPC(datosNPC.idNPC);
            if (posicion != -1)
                m_ListaNPCs[posicion].npcResuelto = datosNPC.resuelto;
        }

        //RESTAURAR TRIGERS
        foreach (TriggersProgresoDatosGuardado trigger in listaTriggersProgreso)
        {
            int posicion = RecuperarPosicionTrigger(trigger.idTrigger);
            Debug.Log("Trigger " + trigger +" resuelto "+trigger.resuelto+ " Pos "+posicion);
            if (posicion != -1)
                m_ListaTriggersProgreso[posicion].resuelto = trigger.resuelto;
        }

        m_BossDeZonaVivo = bossVivo;

        IniciarNivel(estatuaGuardado);

    }

    public int RecuperarPosicionEnemigo(int id)
    {
        for (int i = 0; i < m_ListaEnemigos.Length; i++)
        {
            if (m_ListaEnemigos[i].idEnemigo == id)
                return i;
        }

        return -1;
    }

    public int RecuperarPosicionPuzzle(int id)
    {
        for (int i = 0; i < m_ListaPuzzles.Length; i++)
        {
            if (m_ListaPuzzles[i].idPuzzle == id)
                return i;
        }

        return -1;
    }

    public int RecuperarPosicionNPC(int id)
    {
        for (int i = 0; i < m_ListaNPCs.Length; i++)
        {
            if (m_ListaNPCs[i].idNPC == id)
                return i;
        }

        return -1;
    }
    public int RecuperarPosicionTrigger(int id)
    {
        for (int i = 0; i < m_ListaTriggersProgreso.Length; i++)
        {
            if (m_ListaTriggersProgreso[i].idTrigger == id)
                return i;
        }

        return -1;
    }

    public EnemigosStruct RecuperarEnemigoStruct(int id) => m_ListaEnemigos.FirstOrDefault(x => x.idEnemigo == id);

    public PuzzlesStruct RecuperarPuzzleStruct(int id) => m_ListaPuzzles.FirstOrDefault(x => x.idPuzzle == id);

    public NPCsStruct RecuperarNPCStruct(int id) => m_ListaNPCs.FirstOrDefault(x => x.idNPC == id);

    #endregion
}
