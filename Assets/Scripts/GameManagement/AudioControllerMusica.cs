using UnityEngine;

public class AudioControllerMusica : MonoBehaviour
{
    private AudioSource m_AudioSource;
    [SerializeField]
    private float m_VolumenMaximo;

    private void Awake()
    {
        m_AudioSource = GetComponent<AudioSource>();
        m_VolumenMaximo = m_AudioSource.volume;
    }

    private void Start()
    {
        ActualizarVolumenMusica();
    }

    public void ActualizarVolumenMusica()
    {
        m_AudioSource.volume = m_VolumenMaximo * GameManager.Instance.ObtenerConfiguracionVolumenMusica();
    }
}
