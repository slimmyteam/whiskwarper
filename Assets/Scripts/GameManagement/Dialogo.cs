using UnityEngine;

public class Dialogo : MonoBehaviour
{
    [SerializeField]
    private string m_nombreNPC;
    public string NombreNPC { get => m_nombreNPC; set => m_nombreNPC = value; }
    [SerializeField]
    [TextArea]
    private string[] m_frases;
    public string[] Frases { get => m_frases; set => m_frases = value; }
    [SerializeField]
    private bool m_dialogoHecho;
    public bool DialogoHecho { get => m_dialogoHecho; set => m_dialogoHecho = value; }
    [SerializeField]
    private Mensaje m_mensaje;
    public Mensaje Mensaje { get => m_mensaje; set => m_mensaje = value; }
}
