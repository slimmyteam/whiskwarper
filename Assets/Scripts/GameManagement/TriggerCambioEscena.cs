using System.Collections;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class TriggerCambioEscena : MonoBehaviour, IGuardable
{
    [Header("Escena destino")]
    [SerializeField]
    private string m_NombreEscenaDestino;
    [SerializeField]
    private Vector3 m_PosicionDestinoPlayer;
    [SerializeField]
    private bool m_ConsigueTrozoOrbe, m_AumentaProgreso, m_ProgresoAumentado;
    public bool ProgresoAumentado { get => m_ProgresoAumentado; set => m_ProgresoAumentado = value; }
    [Range(0, 100)]
    [SerializeField]
    private int m_CantidadProgreso;

    [Header("Referencias")]
    [SerializeField]
    private GameObject m_TriggerEquivalente;

    [Header("Guardar partida")]
    [SerializeField]
    private int m_TriggerId;
    [SerializeField]
    private GEInt m_TriggerResueltoEvent;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name != "Player")
            return;

        if (!m_ProgresoAumentado)
        {
            if (m_ConsigueTrozoOrbe)
                GameManager.Instance.TrozoOrbeConseguido();

            if (m_AumentaProgreso)
            {
                GameManager.Instance.AumentarProgreso(m_CantidadProgreso);
                ResolverTrigger();
                if(m_TriggerEquivalente != null)
                    m_TriggerEquivalente.GetComponent<TriggerCambioEscena>().ResolverTrigger();
                AvisarEstadoResuelto();
            }

        }        
            StartCoroutine(CambioEscena());
    }

    private IEnumerator CambioEscena()
    {
        GameManager.Instance.ActivarShaderCambioEscena();
        yield return new WaitForSeconds(2);
        ControlDePartida.Instance.CambiarEscenaSinEstatua(m_NombreEscenaDestino, m_PosicionDestinoPlayer);
    }

    public void ResolverTrigger()
    {
        m_ProgresoAumentado = true;
    }

    public void AvisarEstadoResuelto()
    {
        m_TriggerResueltoEvent.Raise(m_TriggerId);
    }
}
