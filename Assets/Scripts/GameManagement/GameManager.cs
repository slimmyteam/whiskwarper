using UnityEngine;

[RequireComponent(typeof(Teletransporte))]
[RequireComponent(typeof(ControlDePartida))]
public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public static Teletransporte _Teletransporte;
    public static TransicionEscenas _TransicionEscenas;
    public static int NumeroSlimesRecolectados;

    [Header("Scriptable objects")]
    [SerializeField]
    private UIManagementSO m_UIMenuManager;
    public UIManagementSO UIMenuManager { get => m_UIMenuManager; set => m_UIMenuManager = value; }


    private void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }

        Application.targetFrameRate = 60;
        DontDestroyOnLoad(gameObject);

        _Teletransporte = GetComponent<Teletransporte>();
        _TransicionEscenas = GetComponent<TransicionEscenas>();

        NumeroSlimesRecolectados = 0;
        
    }

    #region Progreso juego
    /// <summary>
    /// Aumenta el progreso del juego segun la zona/reto que hayas superado.
    /// Por cada slime conseguido: aumenta el progreso en 5.
    /// Por cada mapa superado: el primero 10, los otros dos 20.
    /// Por cada boss derrotado: los dos primeros 10, el ultimo 15.
    /// </summary>
    /// <param name="cantidadProgreso"></param>
    public void AumentarProgreso(int cantidadProgreso)
    {
        m_UIMenuManager.Progreso += cantidadProgreso;
    }

    public float ObtenerProgreso()
    {
        return m_UIMenuManager.Progreso;
    }

    public void SetearProgreso(float cantidadProgreso)
    {
        m_UIMenuManager.Progreso = cantidadProgreso;
    }

    #endregion

    #region Trozos orbe
    public void ResetearTrozosOrbe()
    {
        m_UIMenuManager.CantidadTrozos = 1;
    }

    public void TrozoOrbeConseguido()
    {
        m_UIMenuManager.CantidadTrozos++;
    }

    public int ObtenerTrozosOrbe()
    {
        return m_UIMenuManager.CantidadTrozos;
    }

    #endregion

    #region Slimes ocultos
    public void AumentarNumeroSlimes(int slime)
    {
        int slimesTotales = ControlDePartida.Instance.AnadirSlime(slime);
        if (slimesTotales != -1)
        {
            NumeroSlimesRecolectados = slimesTotales;
            switch (slime)
            {
                case 1:
                    m_UIMenuManager.TrofeoSlimeBronce = true;
                    break;
                case 2:
                    m_UIMenuManager.TrofeoSlimePlata = true;
                    break;
                case 3:
                    m_UIMenuManager.TrofeoSlimeOro = true;
                    break;
            }
            AumentarProgreso(5);
        }
    }

    public int ObtenerNumeroSlimes()
    {
        return NumeroSlimesRecolectados;
    }

    public bool ObtenerTrofeoSlimePlata()
    {
        return m_UIMenuManager.TrofeoSlimePlata;
    }

    public bool ObtenerTrofeoSlimeOro()
    {
        return m_UIMenuManager.TrofeoSlimeOro;
    }

    public bool ObtenerTrofeoSlimeBronce()
    {
        return m_UIMenuManager.TrofeoSlimeBronce;
    }
    #endregion

    #region Configuracion volumen efectos y musica
    public float ObtenerConfiguracionVolumenEfectos()
    {
        return m_UIMenuManager.ConfiguracionVolumenEfectos;
    }

    public void SetearConfiguracionVolumenEfectos(float valorSlider)
    {
        if (valorSlider < 0.01f)
            valorSlider = 0;
        m_UIMenuManager.ConfiguracionVolumenEfectos = valorSlider;
    }

    public float ObtenerConfiguracionVolumenMusica()
    {
        return m_UIMenuManager.ConfiguracionVolumenMusica;
    }

    public void SetearConfiguracionVolumenMusica(float valorSlider)
    {
        if (valorSlider < 0.01f)
            valorSlider = 0;
        m_UIMenuManager.ConfiguracionVolumenMusica = valorSlider;
    }
    #endregion

    #region Iniciar y cargar partida
    public void IniciarPartida()
    {
        ControlDePartida.Instance.IniciarPartida();
    }

    public void CargarPartida()
    {
        ControlDePartida.Instance.CargarPartida();
    }

    public void ActivarShaderCamaraMuerte()
    {
        GetComponent<TransicionEscenas>().IniciarCorutinaCamaraDeLaMuerte();
    }

    public void ActivarShaderVolverALaVida()
    {
        GetComponent<TransicionEscenas>().IniciarCorutinaVolverALaVida();
    }

    public void ActivarShaderCambioEscena()
    {
        GetComponent<TransicionEscenas>().IniciarCorutinaCambioEscenaShader();
    }
    #endregion

    #region Desbloquear pociones
    public void ActivarPociones()
    {
        m_UIMenuManager.PocionesActivas = true;
    }

    public bool ObtenerPocionesDesbloqueadas()
    {
        return m_UIMenuManager.PocionesActivas;
    }

    #endregion
}