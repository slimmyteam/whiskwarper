using UnityEngine;

public class Mensaje : MonoBehaviour
{
    [SerializeField]
    private string m_tituloMensaje;
    public string TituloMensaje { get => m_tituloMensaje; set => m_tituloMensaje = value; }
    [SerializeField]
    private Sprite m_spriteMensaje;
    public Sprite SpriteMensaje { get => m_spriteMensaje; set => m_spriteMensaje = value; }
    [SerializeField]
    private Sprite m_spriteSlime;
    public Sprite SpriteSlime { get => m_spriteSlime; set => m_spriteSlime = value; }
    [SerializeField]
    private string m_nombreMensaje;
    public string NombreMensaje { get => m_nombreMensaje; set => m_nombreMensaje = value; }
    [SerializeField]
    [TextArea]
    private string m_informacionMensaje;
    public string InformacionMensaje { get => m_informacionMensaje; set => m_informacionMensaje = value; }
    [SerializeField]
    private bool m_mensajeMostrado;
    public bool MensajeMostrado { get => m_mensajeMostrado; set => m_mensajeMostrado = value; }

}