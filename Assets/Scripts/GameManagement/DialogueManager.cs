using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogueManager : MonoBehaviour
{
    [Header("Referencias elementos UI")]
    [SerializeField]
    private TextMeshProUGUI m_NombreNPC;
    [SerializeField]
    private TextMeshProUGUI m_FrasesDialogo;

    [Header("Parametros DialogueManager")]
    public static DialogueManager Instance;
    private Dialogo m_DialogoActivo;
    private Queue<string> m_ColaDeFrases;
    [SerializeField]
    [Range(0.01f, 0.05f)]
    private float m_VelocidadDialogosConfigurada;
    [SerializeField]
    private float m_VelocidadDialogosRapida;
    [SerializeField]
    private float m_VelocidadDialogos;

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_DialogoAbierto;

    //Componentes
    public Animator animatorCuadroDialogo;
    public ActuadorNPC actuadorNPC;

    //Corutinas
    private bool m_CorutinaEscribirFraseActiva = false;

    private void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }
    }

    private void Start()
    {
        m_ColaDeFrases = new Queue<string>();
        actuadorNPC = null;
        m_VelocidadDialogos = m_VelocidadDialogosConfigurada;
        m_VelocidadDialogosRapida = 0;
    }

    public void ComenzarDialogo(Dialogo dialogo)
    {
        m_DialogoAbierto.Raise();
        animatorCuadroDialogo.SetBool("DialogoAbierto", true);

        m_NombreNPC.text = dialogo.NombreNPC;
        m_DialogoActivo = dialogo;

        m_ColaDeFrases.Clear();

        foreach (string frase in dialogo.Frases)
        {
            m_ColaDeFrases.Enqueue(frase);
        }

        MostrarSiguienteFrase();
    }

    public void MostrarSiguienteFrase()
    {
        if (m_ColaDeFrases.Count == 0)
        {
            FinalizarDialogo();
            EjecutarDialogoConMensaje();
            return;
        }

        if (m_CorutinaEscribirFraseActiva)
        {
            m_VelocidadDialogos = m_VelocidadDialogosRapida;
            return;
        }

        m_VelocidadDialogos = m_VelocidadDialogosConfigurada;
        string frase = m_ColaDeFrases.Dequeue();
        StopAllCoroutines();
        StartCoroutine(EscribirFrase(frase));
    }

    public void FinalizarDialogo()
    {
        m_DialogoActivo.DialogoHecho = true;
        animatorCuadroDialogo.SetBool("DialogoAbierto", false);
        m_DialogoAbierto?.Raise();
        if (actuadorNPC != null)
        {
            actuadorNPC.ActuarNPC();
            actuadorNPC = null;
        }
    }

    private IEnumerator EscribirFrase(string frase)
    {
        m_CorutinaEscribirFraseActiva = true;
        m_FrasesDialogo.text = "";

        foreach (char letra in frase.ToCharArray())
        {
            m_FrasesDialogo.text += letra;
            yield return new WaitForSeconds(m_VelocidadDialogos);
        }
        m_CorutinaEscribirFraseActiva = false;
    }

    private void EjecutarDialogoConMensaje()
    {
        if (!m_DialogoActivo.Mensaje)
        {
            return;
        }

        MessageManager.Instance.ComienzaEsperaParaMostrarMensaje(m_DialogoActivo.Mensaje);
    }
}