using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class TransicionEscenas : MonoBehaviour
{
    [Header("Shader Camara Muerte")]
    [SerializeField]
    private UniversalRendererData m_UniversalRendererData;
    List<ScriptableRendererFeature> m_RendererFeatures;
    [SerializeField]
    private Material materialCamaraMuerte;
    private int vignettePowerID;
    [SerializeField]
    private float valorAltovignettePowerID = 1.89f, valorBajovignettePowerID = 0f; //1.89f
    [SerializeField]
    private float tiempoCamaraMuerte = 0.3f, tiempoShaderCamaraMuerte = 0.01f, tiempoShaderVueltaALaVida = 0.1f;

    private void Start()
    {
        m_RendererFeatures = new List<ScriptableRendererFeature>();
        m_RendererFeatures = m_UniversalRendererData.rendererFeatures;
        m_RendererFeatures[4].SetActive(false);
        vignettePowerID = Shader.PropertyToID("_VignettePower");
        materialCamaraMuerte.SetFloat(vignettePowerID, valorAltovignettePowerID);
    }

    //-------------------------------[ EVENTOS ]-------------------------------//
    public void IniciarCorutinaCamaraDeLaMuerte()
    {
        StartCoroutine(ActivarCamaraDeLaMuerte());
    }

    public void IniciarCorutinaVolverALaVida()
    {
        StartCoroutine(ActivarVueltaALaVida());
    }

    public void IniciarCorutinaCambioEscenaShader()
    {
        StartCoroutine(ActivarCambioEscena());
    }

    //-------------------------------[ CORUTINAS ]-------------------------------//
    private IEnumerator ActivarCamaraDeLaMuerte()
    {
        yield return StartCoroutine(ActivarCambioEscena());      
        GameManager.Instance.CargarPartida();
    }

    private IEnumerator ActivarVueltaALaVida()
    {
        m_RendererFeatures[4].SetActive(true);

        yield return new WaitForSeconds(3);
        while (materialCamaraMuerte.GetFloat(vignettePowerID) + tiempoCamaraMuerte < valorAltovignettePowerID)
        {
            materialCamaraMuerte.SetFloat(vignettePowerID, materialCamaraMuerte.GetFloat(vignettePowerID) + tiempoCamaraMuerte);

            yield return new WaitForSeconds(tiempoShaderVueltaALaVida);
        }
        materialCamaraMuerte.SetFloat(vignettePowerID, valorAltovignettePowerID);
        m_RendererFeatures[4].SetActive(false);
    }

    private IEnumerator ActivarCambioEscena()
    {
        m_RendererFeatures[4].SetActive(true);
        while (materialCamaraMuerte.GetFloat(vignettePowerID) - tiempoCamaraMuerte > valorBajovignettePowerID)
        {
            materialCamaraMuerte.SetFloat(vignettePowerID, materialCamaraMuerte.GetFloat(vignettePowerID) - tiempoCamaraMuerte);

            yield return new WaitForSeconds(tiempoShaderCamaraMuerte);
        }
        materialCamaraMuerte.SetFloat(vignettePowerID, valorBajovignettePowerID);

       // m_RendererFeatures[4].SetActive(false);

    }
}
