using UnityEngine;


public class BattleManager : MonoBehaviour
{
    [Header("Referencias")]
    [SerializeField]
    private PuertaElectrica m_PuertaBoss;
    [SerializeField]
    private GameObject m_Boss;

    public virtual void ActivarBoss()
    {
        m_Boss.SetActive(true);
    }

    public void AbrirPuertaBoss()
    {
        m_PuertaBoss.Reaccionar();
    }

    private void OnTriggerEnter(Collider other)
    {
        ActivarBoss();
    }
}
