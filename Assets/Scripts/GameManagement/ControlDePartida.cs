using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlDePartida : MonoBehaviour
{
    public static ControlDePartida Instance;

    [Header("Cambio de escena")]
    [SerializeField]
    private GameObject m_Player;
    private Vector3 m_DestinoPlayer;
    [SerializeField]
    private List<string> m_Escenas = new();
    private UIVida m_UIVida;

    //Componentes player
    private PlayerController m_PlayerController;
    private PocionesController m_PocionesController;
    private PlayerHabilidades m_PlayerHabilidades;

    [Header("Datos de partida")]
    public SaveData m_Partida = new();
    private string m_SaveFileName;

    [Header("Game Events")]
    [SerializeField]
    private GameEvent m_ActualizarMenusUI;

    private void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }
        DontDestroyOnLoad(gameObject);
        m_SaveFileName = Application.persistentDataPath + "/savegame.json";
    }

    void Start()
    {
        m_Partida.gameData = new SaveData.GameData(10);
        BuscarPlayer();
        LeerDatosDeGuardado();
        RestaurarGameManager();
    }


    #region Inicio del juego

    /// <summary>
    /// Crea unos datos de guardados basicos, con la vida del player a 10 y la posicion de respawn en la primera estatua de guardado
    /// </summary>
    public void IniciarPartida()
    {
        m_Partida.gameData = new SaveData.GameData(10);
        GameManager.NumeroSlimesRecolectados = 0;
        GameManager.Instance.UIMenuManager.TrofeoSlimeBronce = false;
        GameManager.Instance.UIMenuManager.TrofeoSlimePlata = false;
        GameManager.Instance.UIMenuManager.TrofeoSlimeOro = false;
        GameManager.Instance.SetearProgreso(0);
        GameManager.Instance.ResetearTrozosOrbe();
        GuardarPartida(101);
        RestaurarGameManager();
        SceneManager.LoadScene(m_Escenas[0]);
        SceneManager.sceneLoaded += IniciarPlayer;
    }

    private void IniciarPlayer(Scene scene, LoadSceneMode sceneMode)
    {
        BuscarPlayer();
        m_PlayerController.InitPlayer();
        SceneManager.sceneLoaded -= IniciarPlayer;
    }

    #endregion

    #region Cambio de escena

    public void CambiarEscena(string escena, Vector3 posicionPlayer)
    {
        m_DestinoPlayer = posicionPlayer;
        RobarseDatosDelLevelManager(SceneManager.GetActiveScene());
        SceneManager.sceneLoaded += RestaurarEscenaAlCargar;
        SceneManager.LoadScene(escena);
        GameManager.Instance.ActivarShaderVolverALaVida();
    }

    public void CambiarEscenaSinEstatua(string escena, Vector3 posicionPlayer)
    {
        m_Partida.gameData.idEstatuaGuardado = -1;
        CambiarEscena(escena, posicionPlayer);
    }

    #endregion

    #region Restaurar escena

    private void RestaurarEscena()
    {
        BuscarPlayer();
        BuscarUIVida();
        LevelManager levelManager = BuscarLevelManager();

        //RESTAURAR PLAYER
        if (m_Player != null)
        {
            m_PlayerController.PlayerSO.vidaActual = m_Partida.gameData.vidaPlayer;

            //--- Habilidades
            if (m_Partida.gameData.arcoDesbloqueado)
                m_PlayerHabilidades.DesbloquearHabilidad(Enums.EnumHabilidades.ARCO);
            if (m_Partida.gameData.boomerangDesbloqueado)
                m_PlayerHabilidades.DesbloquearHabilidad(Enums.EnumHabilidades.BOOMERANG);
            if (m_Partida.gameData.escudoDesbloqueado)
                m_PlayerHabilidades.DesbloquearHabilidad(Enums.EnumHabilidades.ESCUDO);
            if (m_Partida.gameData.escudoMagicoDesbloqueado)
                m_PlayerHabilidades.DesbloquearHabilidad(Enums.EnumHabilidades.ESCUDOMAGICO);
            if (m_Partida.gameData.cuboDesbloqueado)
                m_PlayerHabilidades.DesbloquearHabilidad(Enums.EnumHabilidades.CUBO);
            if (m_Partida.gameData.bombaDesbloqueada)
                m_PlayerHabilidades.DesbloquearHabilidad(Enums.EnumHabilidades.BOMBAS);
            if (m_Partida.gameData.minaDesbloqueada)
                m_PlayerHabilidades.DesbloquearHabilidad(Enums.EnumHabilidades.MINAS);

            //--- Pociones
            m_PocionesController.VaciarPociones();
            if (m_Partida.gameData.pocionesDesbloqueadas)
                m_PocionesController.ActivarPociones();
            m_PocionesController.RellenarPocion(Enums.EnumPociones.VIDA, m_Partida.gameData.cantidadPocionVida);
            m_PocionesController.RellenarPocion(Enums.EnumPociones.FUERZA, m_Partida.gameData.cantidadPocionFuerza);
            m_PocionesController.RellenarPocion(Enums.EnumPociones.VELOCIDAD, m_Partida.gameData.cantidadPocionVelocidad);
            m_PocionesController.RellenarPocion(Enums.EnumPociones.INVISIBILIDAD, m_Partida.gameData.cantidadPocionInvisibilidad);
        }

        //NIVEL

        int boss = 0;
        switch (SceneManager.GetActiveScene().name)
        {
            case "MapaZona1":
                boss = 1;
                break;
            case "MapaZona2":
                boss = 2;
                break;
            case "MapaZona3":
                boss = 3;
                break;
        }

        bool bossVivo = false;

        for (int i = 0; i < 4; i++)
        {
            if (m_Partida.gameData.bosses[i].idBoss == boss)
                bossVivo = m_Partida.gameData.bosses[i].vivo;
        }

        if (levelManager != null)
        {
            levelManager.RestaurarNivel(m_Partida.gameData.idEstatuaGuardado, m_Partida.gameData.enemigos, m_Partida.gameData.puzzles, m_Partida.gameData.npcs,
                m_Partida.gameData.triggersProgreso, bossVivo);
            m_Partida.gameData.idEstatuaGuardado = -1;
        }

        //UI
        if (m_UIVida != null)
            m_UIVida.ActualizarVida(m_Partida.gameData.vidaPlayer);

    }

    private void RestaurarEscenaAlCargar(Scene scene, LoadSceneMode loadSceneMode)
    {
        RestaurarEscena();
        m_DestinoPlayer = Vector3.zero;
        SceneManager.sceneLoaded -= RestaurarEscenaAlCargar;
    }

    #endregion

    #region Recuperar y actualizar datos de la partida

    public void RobarseDatosDelLevelManager(Scene current)
    {
        LevelManager levelManager = FindFirstObjectByType<LevelManager>();

        if (levelManager != null)
        {
            foreach (EnemigosStruct enemigo in levelManager.ListaEnemigos) //ENEMIGOS
            {
                int posicion = RecuperarPosicionEnemigoDePartida(enemigo.idEnemigo);
                if (posicion == -1)
                    m_Partida.gameData.enemigos.Add(new EnemigosDatosGuardado(enemigo.idEnemigo, enemigo.enemigoVivo));
                else
                    m_Partida.gameData.enemigos[posicion].vivo = enemigo.enemigoVivo;
            }

            foreach (NPCsStruct npc in levelManager.ListaNPCs) //PUZZLES
            {
                int posicion = RecuperarPosicionNPCDePartida(npc.idNPC);
                if (posicion == -1)
                    m_Partida.gameData.npcs.Add(new NPCsDatosGuardado(npc.idNPC, npc.npcResuelto));
                else
                    m_Partida.gameData.npcs[posicion].resuelto = npc.npcResuelto;
            }

            foreach (PuzzlesStruct puzzle in levelManager.ListaPuzzles) //NPCs
            {
                int posicion = RecuperarPosicionPuzzleDePartida(puzzle.idPuzzle);
                //Debug.Log("PUZLE " + puzzle.idPuzzle + " POS " + posicion);
                if (posicion == -1)
                    m_Partida.gameData.puzzles.Add(new PuzzlesDatosGuardado(puzzle.idPuzzle, puzzle.puzzleResuelto));
                else
                    m_Partida.gameData.puzzles[posicion].resuelto = puzzle.puzzleResuelto;
            }

            foreach (TriggersProgresoStruct trigger in levelManager.ListaTriggersProgreso)
            {
                int posicion = RecuperarPosicionTriggerProgresoDePartida(trigger.idTrigger);
                if(posicion == -1)
                    m_Partida.gameData.triggersProgreso.Add(new TriggersProgresoDatosGuardado(trigger.idTrigger, trigger.resuelto));
                else
                    m_Partida.gameData.triggersProgreso[posicion].resuelto = trigger.resuelto;
            }

            int escena = m_Escenas.IndexOf(current.name);
            switch (escena)
            {
                case 0:
                    break;
                case 1:
                case 2:
                    m_Partida.gameData.bosses[1].vivo = levelManager.BossDeZonaVivo;
                    break;
                case 3:
                case 4:
                    m_Partida.gameData.bosses[2].vivo = levelManager.BossDeZonaVivo;
                    break;
                case 5:
                    m_Partida.gameData.bosses[3].vivo = levelManager.BossDeZonaVivo;
                    break;
            }
        }

        if (m_Player != null)
            ObtenerDatosPlayer();
    }

    private void ObtenerDatosPlayer()
    {
        BuscarPlayer();
        m_Partida.gameData.vidaPlayer = (int)m_PlayerController.PlayerSO.vidaActual;

        m_Partida.gameData.pocionesDesbloqueadas = m_PocionesController.PocionesActivas;
        m_Partida.gameData.cantidadPocionVida = m_PocionesController.ObtenerPocionPorTipo(Enums.EnumPociones.VIDA).CantidadActual;
        m_Partida.gameData.cantidadPocionVelocidad = m_PocionesController.ObtenerPocionPorTipo(Enums.EnumPociones.VELOCIDAD).CantidadActual;
        m_Partida.gameData.cantidadPocionInvisibilidad = m_PocionesController.ObtenerPocionPorTipo(Enums.EnumPociones.INVISIBILIDAD).CantidadActual;
        m_Partida.gameData.cantidadPocionFuerza = m_PocionesController.ObtenerPocionPorTipo(Enums.EnumPociones.FUERZA).CantidadActual;

        m_Partida.gameData.escudoDesbloqueado = m_PlayerHabilidades.ComprobarSiTengoHabilidad(Enums.EnumHabilidades.ESCUDO);
        m_Partida.gameData.escudoMagicoDesbloqueado = m_PlayerHabilidades.ComprobarSiTengoHabilidad(Enums.EnumHabilidades.ESCUDOMAGICO);
        m_Partida.gameData.arcoDesbloqueado = m_PlayerHabilidades.ComprobarSiTengoHabilidad(Enums.EnumHabilidades.ARCO);
        m_Partida.gameData.boomerangDesbloqueado = m_PlayerHabilidades.ComprobarSiTengoHabilidad(Enums.EnumHabilidades.BOOMERANG);
        m_Partida.gameData.cuboDesbloqueado = m_PlayerHabilidades.ComprobarSiTengoHabilidad(Enums.EnumHabilidades.CUBO);
        m_Partida.gameData.bombaDesbloqueada = m_PlayerHabilidades.ComprobarSiTengoHabilidad(Enums.EnumHabilidades.BOMBAS);
        m_Partida.gameData.minaDesbloqueada = m_PlayerHabilidades.ComprobarSiTengoHabilidad(Enums.EnumHabilidades.MINAS);
    }

    public void MatarBoss()
    {
        int escena = RecuperarEscenaCargada();
        switch (escena)
        {
            case 0: //MAPA 1
            case 1: //MAPA 2
                break;
            case 2: //BOSS RICOCHET
                Debug.Log("######" + escena);
                m_Partida.gameData.bosses[1].vivo = false;
                break;
            case 3: //MAPA 3
                break;
            case 4: //BOSS EXPLOSIVO
                m_Partida.gameData.bosses[2].vivo = false;
                break;
            case 5: //BOSS FINAL
                m_Partida.gameData.bosses[3].vivo = false;
                break;
        }
    }

    #endregion

    #region Guardar y cargar partida

    public void GuardarPartida(int estatua)
    {
        m_Partida.gameData.idEstatuaGuardado = estatua;
        RobarseDatosDelLevelManager(SceneManager.GetActiveScene());
        m_Partida.gameData.numeroSlimes = GameManager.NumeroSlimesRecolectados;
        m_Partida.gameData.slimes[0].obtenido = GameManager.Instance.ObtenerTrofeoSlimeBronce();
        m_Partida.gameData.slimes[1].obtenido = GameManager.Instance.ObtenerTrofeoSlimePlata();
        m_Partida.gameData.slimes[2].obtenido = GameManager.Instance.ObtenerTrofeoSlimeOro();
        m_Partida.gameData.porcentajeProgresion = GameManager.Instance.ObtenerProgreso();
        m_Partida.gameData.volumenMusica = GameManager.Instance.ObtenerConfiguracionVolumenMusica();
        m_Partida.gameData.volumenEfectos = GameManager.Instance.ObtenerConfiguracionVolumenEfectos();

        string jsonData = JsonConvert.SerializeObject(m_Partida);

        try
        {
            Debug.Log("Guardando partida ...");

            File.WriteAllText(m_SaveFileName, jsonData.ToString());
            m_Partida.gameData.idEstatuaGuardado = -1;
        }
        catch (Exception e)
        {
            Debug.Log("Error al guardar la partida: " + e);
        }
    }

    public void CargarPartida()
    {
        LeerDatosDeGuardado();
        RestaurarGameManager();

        int mapa = m_Partida.gameData.idEstatuaGuardado;
        string escena = "";

        while (mapa >= 10)
            mapa /= 10;

        switch (mapa)
        {
            case 1:
                escena = "MapaZona1";
                break;
            case 2:
                escena = "MapaZona2";
                break;
            case 3:
                escena = "MapaZona3";
                break;
            case 4:
                escena = "MapaBossFinal";
                break;

        }

        SceneManager.LoadScene(escena);
        SceneManager.sceneLoaded += RestaurarEscena;
        GameManager.Instance.ActivarShaderVolverALaVida();

    }

    public void LeerDatosDeGuardado()
    {
        try
        {
            Debug.Log("Cargando partida ...");

            string jsonData = File.ReadAllText(m_SaveFileName);

            JsonUtility.FromJsonOverwrite(jsonData, m_Partida);

        }

        catch (Exception e)
        {
            Debug.Log("Error al cargar la partida: " + e);
        }
    }

    public void RestaurarGameManager()
    {
        GameManager.NumeroSlimesRecolectados = m_Partida.gameData.numeroSlimes;
        GameManager.Instance.UIMenuManager.TrofeoSlimeBronce = m_Partida.gameData.slimes[0].obtenido;
        GameManager.Instance.UIMenuManager.TrofeoSlimePlata = m_Partida.gameData.slimes[1].obtenido;
        GameManager.Instance.UIMenuManager.TrofeoSlimeOro = m_Partida.gameData.slimes[2].obtenido;
        GameManager.Instance.UIMenuManager.PocionesActivas = m_Partida.gameData.pocionesDesbloqueadas;
        GameManager.Instance.SetearProgreso(m_Partida.gameData.porcentajeProgresion);
        GameManager.Instance.SetearConfiguracionVolumenMusica(m_Partida.gameData.volumenMusica);
        GameManager.Instance.SetearConfiguracionVolumenEfectos(m_Partida.gameData.volumenEfectos);
        GameManager.Instance.ResetearTrozosOrbe();

        m_ActualizarMenusUI.Raise();
    }

    private void RestaurarEscena(Scene arg0, LoadSceneMode arg1)
    {
        RestaurarEscena();
        SceneManager.sceneLoaded -= RestaurarEscena;
    }

    #endregion

    #region Utilidades

    private void BuscarPlayer()
    {
        m_Player = GameObject.Find("Player");
        //Debug.Log("Ha encontrado player: " + m_Player + " y lo tiene que enviar a la posici�n: " + m_DestinoPlayer);
        if (m_Player != null)
        {
            m_PlayerController = m_Player.GetComponent<PlayerController>();
            m_PocionesController = m_Player.GetComponent<PocionesController>();
            m_PlayerHabilidades = m_Player.GetComponent<PlayerHabilidades>();
            if (m_DestinoPlayer != Vector3.zero)
                m_Player.transform.position = m_DestinoPlayer;
        }
    }

    private void BuscarUIVida()
    {
        m_UIVida = FindFirstObjectByType<UIVida>();
    }

    private LevelManager BuscarLevelManager()
    {
        LevelManager levelManager = FindFirstObjectByType<LevelManager>();
        return levelManager;
    }

    private int RecuperarEscenaCargada() => m_Escenas.IndexOf(SceneManager.GetActiveScene().name);

    public int RecuperarPosicionEnemigoDePartida(int id)
    {
        for (int i = 0; i < m_Partida.gameData.enemigos.Count; i++)
        {
            if (m_Partida.gameData.enemigos[i].idEnemigo == id)
                return i;
        }

        return -1;
    }

    public int RecuperarPosicionPuzzleDePartida(int id)
    {
        for (int i = 0; i < m_Partida.gameData.puzzles.Count; i++)
        {
            if (m_Partida.gameData.puzzles[i].idPuzzle == id)
                return i;
        }

        return -1;
    }

    public int RecuperarPosicionNPCDePartida(int id)
    {
        for (int i = 0; i < m_Partida.gameData.npcs.Count; i++)
        {
            if (m_Partida.gameData.npcs[i].idNPC == id)
                return i;
        }

        return -1;
    }

    public int RecuperarPosicionTriggerProgresoDePartida(int id)
    {
        for (int i = 0; i < m_Partida.gameData.triggersProgreso.Count; i++)
        {
            if (m_Partida.gameData.triggersProgreso[i].idTrigger == id)
                return i;
        }

        return -1;
    }

    #endregion

    #region Slimes

    public int AnadirSlime(int slime)
    {
        if (m_Partida.gameData.slimes[slime - 1].obtenido)
            return -1;
        m_Partida.gameData.slimes[slime - 1].obtenido = true;
        m_Partida.gameData.numeroSlimes += 1;
        return m_Partida.gameData.numeroSlimes;
    }

    #endregion
}
