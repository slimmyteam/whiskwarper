using UnityEngine;

public class ReboteSierra : MonoBehaviour
{
    public float fuerzaRebote;

    void OnTriggerStay(Collider other)
    {
        Rigidbody otherRigidbody = other.GetComponent<Rigidbody>();

        if (otherRigidbody != null)
        {
            Vector3 direccion;
            // Calculamos la direccio opuesta al vector de la colision
            if (gameObject.name == "ReboteDerecha")
            {
                direccion = other.transform.position - transform.position;
            }
            else
            {
                direccion = transform.position - other.transform.position;
            }

            // Aplicamos una fuerza en la direccion opuesta para simular el rebote
            otherRigidbody.AddForce(direccion.normalized * fuerzaRebote, ForceMode.Force);
        }
    }
}
