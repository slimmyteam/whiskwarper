using UnityEngine;

public class StructContainerScript : MonoBehaviour
{
    [System.Serializable]
    public struct AnimacionesDuracion
    {
        public string nombreAnimacion;
        public float duracionTransicionAnimacion;
    }

    [System.Serializable]
    public struct LimitesDeCamara
    {
        public float LimiteSuperior;
        public float LimiteInferior;
        public float LimiteIzquierda;
        public float LimiteDerecha;
    }
}
