using UnityEngine;

[RequireComponent(typeof(ControlDeLayers))]
public class CargadorDeZona : MonoBehaviour
{
    [SerializeField]
    private LayerMask m_LayerPlayer;
    [SerializeField]
    private GameObject[] Zonas;
    private ControlDeLayers m_ControlDeLayers;

    private void Awake()
    {
        m_ControlDeLayers = GetComponent<ControlDeLayers>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (m_ControlDeLayers.ContieneLayer(m_LayerPlayer,other.gameObject.layer))
        {
            foreach (GameObject go in Zonas)
            {
                if (!go.activeSelf)
                    go.SetActive(true);
            }
        }
    }
}
