using UnityEngine;

public class UIOrbe : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem m_GlowOrbe;
    [SerializeField]
    private Color m_CamaraVistaDoble, m_CamaraFreezeada, m_CamaraBloqueada, m_CamaraSeMueve;
    private ParticleSystem.MainModule m_MainParticleSystem;
    [SerializeField]
    private bool m_VistaDoble, m_Freezeada, m_Bloqueada, m_Moviendose;
    [SerializeField]
    private GameObject m_BloqueoTrozo2, m_BloqueoTrozo3;
    [SerializeField]
    private GameEvent m_TrozoConseguido;

    private void Awake()
    {
        m_MainParticleSystem = m_GlowOrbe.main;
    }

    private void Start()
    {
        DesactivarBloqueosOrbe();
    }

    #region Trozos orbe
    private void DesactivarBloqueosOrbe()
    {
        if (GameManager.Instance.ObtenerTrozosOrbe() == 2)
        {
            m_BloqueoTrozo2.SetActive(false);
        }
        if (GameManager.Instance.ObtenerTrozosOrbe() == 3)
        {
            m_BloqueoTrozo3.SetActive(false);
        }
    }
    #endregion

    #region Efecto glow orbe segun estado camara
    public void DesactivarGlow()
    {
        m_GlowOrbe.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
    }

    public void VistaDobleCamara()
    {
        m_VistaDoble = !m_VistaDoble;
        if (!m_VistaDoble)
        {
            DesactivarGlow();
            return;
        }
        m_MainParticleSystem.startColor = m_CamaraVistaDoble;
        m_GlowOrbe.Play();
    }

    public void CongelarCamara()
    {
        m_Freezeada = !m_Freezeada;
        if (!m_Freezeada)
        {
            DesactivarGlow();

            if (m_VistaDoble)
            {
                m_MainParticleSystem.startColor = m_CamaraVistaDoble;
                m_GlowOrbe.Play();
            }
            return;
        }

        m_MainParticleSystem.startColor = m_CamaraFreezeada;
        m_GlowOrbe.Play();
    }

    public void BloquearCamara()
    {
        print("Entra en BloquearCamara");
        m_Bloqueada = !m_Bloqueada;
        if (!m_Bloqueada)
        {
            DesactivarGlow();
            return;
        }
        m_MainParticleSystem.startColor = m_CamaraBloqueada;
        m_GlowOrbe.Play();
    }

    public void ModoMoverCamara()
    {
        print("Entra en ModoMoverCamara");
        m_Moviendose = !m_Moviendose;
        if (!m_Moviendose)
        {
            DesactivarGlow();
            return;
        }
        m_MainParticleSystem.startColor = m_CamaraSeMueve;
        m_GlowOrbe.Play();
    }
    #endregion
}
