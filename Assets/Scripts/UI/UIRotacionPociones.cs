using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UIRotacionPociones : MonoBehaviour
{
    [SerializeField]
    private List<Sprite> m_ListaImagenesPociones = new(), m_ListaImagenesHabilidades = new();
    [SerializeField]
    private RectTransform m_RectTransformIzquierda, m_RectTransformCentro, m_RectTransformDerecha;
    [SerializeField]
    private RectTransform m_RectTransformIzquierdaHab, m_RectTransformCentroHab, m_RectTransformDerechaHab;
    [SerializeField]
    private Image m_BloqueadorPociones, m_BloqueadorHabilidades;
    [SerializeField]
    private Image m_ImagenIzquierda, m_ImagenCentro, m_ImagenDerecha;
    [SerializeField]
    private Image m_ImagenIzquierdaRelleno, m_ImagenCentroRelleno, m_ImagenDerechaRelleno;
    [SerializeField]
    private Image m_ImagenIzquierdaHab, m_ImagenCentroHab, m_ImagenDerechaHab;
    [SerializeField]
    private float m_Duracion;
    [SerializeField]
    private bool m_PocionesActivadas, m_HaAcabadoDeRotarPocion = true, m_HaAcabadoDeRotarHabilidad = true;
    [SerializeField]
    private GameEvent m_HaDejadoDeRotarPocion, m_HaDejadoDeRotarHabilidad;
    [SerializeField]
    private Animator m_AnimatorFlechaIzquierdaPociones, m_AnimatorFlechaDerechaPociones, m_AnimatorFlechaArribaHabilidades, m_AnimatorFlechaAbajoHabilidades;
    [SerializeField]
    private List<PocionesSO> m_listaPocionesSO = new List<PocionesSO>();

    private void Start()
    {
        if (GameManager.Instance.ObtenerPocionesDesbloqueadas()) m_PocionesActivadas = true;

        GestionarBloqueadorPocionesHabilidades();

        if (m_ListaImagenesHabilidades.Count > 0)
            m_ImagenCentroHab.sprite = m_ListaImagenesHabilidades.First();

        m_ImagenCentro.sprite = m_ListaImagenesPociones.First();
        m_ImagenCentroRelleno.sprite = m_ListaImagenesPociones.First();
        m_ImagenIzquierdaRelleno.sprite = m_ListaImagenesPociones.Last();
        m_ImagenDerechaRelleno.sprite = m_ListaImagenesPociones[1];
        m_ImagenCentroRelleno.fillAmount = ComprobarRellenoPocion(m_ImagenCentroRelleno);
    }

    #region Desbloquear pociones y habilidades
    public void ActivarPociones()
    {
        m_PocionesActivadas = true;
        GestionarBloqueadorPocionesHabilidades();
    }

    private void GestionarBloqueadorPocionesHabilidades()
    {
        m_BloqueadorPociones.enabled = !m_PocionesActivadas;
        m_BloqueadorHabilidades.enabled = !m_PocionesActivadas;
    }

    public void DesbloquearHabilidad(Sprite imagenHabilidad)
    {
        m_ListaImagenesHabilidades.Add(imagenHabilidad);
        string nombreHabilidadASustituir = "";
        foreach (Sprite imagen in m_ListaImagenesHabilidades)
        {
            switch (imagen.name)
            {
                case "HabilidadBoomerang":
                    nombreHabilidadASustituir = "HabilidadArco";
                    break;
                case "HabilidadEscudoMagico":
                    nombreHabilidadASustituir = "HabilidadEscudo";
                    break;
                case "HabilidadMina":
                    nombreHabilidadASustituir = "HabilidadBomba";
                    break;
            }

        }
        if (nombreHabilidadASustituir != "")
            SustituirImagenHabilidadPorMejora(nombreHabilidadASustituir);
    }

    private void SustituirImagenHabilidadPorMejora(string nombreHabilidad)
    {
        Sprite spriteABorrar = null;
        foreach (Sprite sprite in m_ListaImagenesHabilidades)
        {
            if (sprite.name == nombreHabilidad)
            {
                spriteABorrar = sprite;
            }
        }
        if (spriteABorrar != null)
            m_ListaImagenesHabilidades.Remove(spriteABorrar);
    }
    #endregion

    #region Rotacion de pociones
    public void RotarPocionesDerecha()
    {
        if (!m_PocionesActivadas) return;
        if (!m_HaAcabadoDeRotarPocion) return;
        m_AnimatorFlechaIzquierdaPociones.SetBool("Mover", true);
        m_ListaImagenesPociones.Add(m_ListaImagenesPociones[0]);
        m_ListaImagenesPociones.RemoveAt(0);
        m_ImagenDerecha.sprite = m_ListaImagenesPociones[0];
        m_ImagenDerechaRelleno.sprite = m_ListaImagenesPociones[0];
        m_ImagenDerechaRelleno.fillAmount = ComprobarRellenoPocion(m_ImagenDerechaRelleno);
        m_HaAcabadoDeRotarPocion = false;
        StartCoroutine(MoverAPosicion(m_ImagenDerecha.rectTransform, m_RectTransformCentro, m_ImagenDerecha, m_ImagenDerechaRelleno, m_RectTransformDerecha, m_RectTransformIzquierda, m_ImagenCentro, m_ImagenCentroRelleno, m_RectTransformCentro, Enums.EnumNPCS.POCIONES));
    }

    public void RotarPocionesIzquierda()
    {
        if (!m_PocionesActivadas) return;
        if (!m_HaAcabadoDeRotarPocion) return;
        m_AnimatorFlechaDerechaPociones.SetBool("Mover", true);
        m_ListaImagenesPociones.Insert(0, m_ListaImagenesPociones[^1]);
        m_ListaImagenesPociones.RemoveAt(m_ListaImagenesPociones.Count - 1);
        m_ImagenIzquierda.sprite = m_ListaImagenesPociones[0];
        m_ImagenIzquierdaRelleno.sprite = m_ListaImagenesPociones[0];
        m_ImagenIzquierdaRelleno.fillAmount = ComprobarRellenoPocion(m_ImagenIzquierdaRelleno);
        m_HaAcabadoDeRotarPocion = false;
        StartCoroutine(MoverAPosicion(m_ImagenIzquierda.rectTransform, m_RectTransformCentro, m_ImagenIzquierda, m_ImagenIzquierdaRelleno, m_RectTransformIzquierda, m_RectTransformDerecha, m_ImagenCentro, m_ImagenCentroRelleno, m_RectTransformCentro, Enums.EnumNPCS.POCIONES));
    }
    #endregion

    #region Rotacion de habilidades
    public void RotarHabilidadesIzquierda()
    {
        if (!m_HaAcabadoDeRotarHabilidad) return;
        if (m_ListaImagenesHabilidades.Count == 0) return;
        m_AnimatorFlechaArribaHabilidades.SetBool("Mover", true);
        m_ListaImagenesHabilidades.Add(m_ListaImagenesHabilidades[0]);
        m_ListaImagenesHabilidades.RemoveAt(0);
        m_ImagenDerechaHab.sprite = m_ListaImagenesHabilidades[0];
        m_HaAcabadoDeRotarHabilidad = false;
        StartCoroutine(MoverAPosicion(m_ImagenDerechaHab.rectTransform, m_RectTransformCentroHab, m_ImagenDerechaHab, null, m_RectTransformDerechaHab, m_RectTransformIzquierdaHab, m_ImagenCentroHab, null, m_RectTransformCentroHab, Enums.EnumNPCS.HABILIDADES));
    }

    public void RotarHabilidadesDerecha()
    {
        if (!m_HaAcabadoDeRotarHabilidad) return;
        if (m_ListaImagenesHabilidades.Count == 0) return;
        m_AnimatorFlechaAbajoHabilidades.SetBool("Mover", true);
        m_ListaImagenesHabilidades.Insert(0, m_ListaImagenesHabilidades[^1]);
        m_ListaImagenesHabilidades.RemoveAt(m_ListaImagenesHabilidades.Count - 1);
        m_ImagenIzquierdaHab.sprite = m_ListaImagenesHabilidades[0];
        m_HaAcabadoDeRotarHabilidad = false;
        StartCoroutine(MoverAPosicion(m_ImagenIzquierdaHab.rectTransform, m_RectTransformCentroHab, m_ImagenIzquierdaHab, null, m_RectTransformIzquierdaHab, m_RectTransformDerechaHab, m_ImagenCentroHab, null, m_RectTransformCentroHab, Enums.EnumNPCS.HABILIDADES));
    }
    #endregion

    #region Animacion rotacion
    private IEnumerator MoverAPosicion(RectTransform origenImagen, RectTransform destinoImagen, Image imagenACambiar, Image imagenACambiarRelleno, RectTransform transformImagenACambiar, RectTransform destinoImagenCentro, Image imagenCentro, Image imagenCentroRelleno, RectTransform transformCentro, Enums.EnumNPCS tipo)
    {
        Vector2 startPosition = origenImagen.anchoredPosition;
        float elapsedTime = 0;
        if (imagenACambiarRelleno)
            imagenACambiarRelleno.fillAmount = ComprobarRellenoPocion(imagenACambiarRelleno);

        while (elapsedTime < m_Duracion)
        {
            origenImagen.anchoredPosition = Vector2.Lerp(startPosition, destinoImagen.anchoredPosition, elapsedTime / m_Duracion);
            imagenCentro.rectTransform.anchoredPosition = Vector2.Lerp(transformCentro.anchoredPosition, destinoImagenCentro.anchoredPosition, elapsedTime / m_Duracion);
            if (imagenCentroRelleno)
                imagenCentroRelleno.rectTransform.anchoredPosition = Vector2.Lerp(transformCentro.anchoredPosition, destinoImagenCentro.anchoredPosition, elapsedTime / m_Duracion);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        // Nos aseguramos que la imagen llegue a la posicion de destino
        origenImagen.anchoredPosition = destinoImagen.anchoredPosition;
        imagenCentro.sprite = imagenACambiar.sprite;
        if (imagenCentroRelleno)
        {
            imagenCentroRelleno.sprite = imagenACambiarRelleno.sprite;
            imagenCentroRelleno.fillAmount = ComprobarRellenoPocion(imagenCentroRelleno);
            imagenCentroRelleno.rectTransform.anchoredPosition = transformCentro.anchoredPosition;
        }

        imagenCentro.rectTransform.anchoredPosition = transformCentro.anchoredPosition;

        imagenACambiar.sprite = null;
        imagenACambiar.rectTransform.anchoredPosition = transformImagenACambiar.anchoredPosition;
        if (imagenACambiarRelleno)
            imagenACambiarRelleno.rectTransform.anchoredPosition = transformImagenACambiar.anchoredPosition;

        if (tipo == Enums.EnumNPCS.POCIONES)
        {
            m_HaAcabadoDeRotarPocion = true;
            m_HaDejadoDeRotarPocion.Raise();
        }
        else
        {
            m_HaAcabadoDeRotarHabilidad = true;
            m_HaDejadoDeRotarHabilidad.Raise();
        }
    }
    #endregion

    #region Rellenar pocion
    private float ComprobarRellenoPocion(Image m_imagen)
    {
        float fillAmountImagen = 1; ;
        switch (m_ListaImagenesPociones[0].name)
        {
            case "PocionFuerza":
                fillAmountImagen = CalcularRellenoPocion(m_listaPocionesSO[0], 1);
                break;
            case "PocionVelocidad":
                fillAmountImagen = CalcularRellenoPocion(m_listaPocionesSO[1], 1);
                break;
            case "PocionVida":
                fillAmountImagen = CalcularRellenoPocion(m_listaPocionesSO[2], 1);
                break;
            case "PocionInvisibilidad":
                fillAmountImagen = CalcularRellenoPocion(m_listaPocionesSO[3], 1);
                break;
        }

        if (fillAmountImagen <= 0)
        {
            fillAmountImagen = 0;
        }

        return fillAmountImagen;
    }

    private float CalcularRellenoPocion(PocionesSO pocionesSO, float fillAmountActual)
    {
        float cantidadARestar = (float)pocionesSO.CantidadActual / (float)pocionesSO.CantidadMax;
        return fillAmountActual - cantidadARestar;
    }

    public void RellenarPocionVida(System.Enum pocionARellenar, int cantidad)
    {
        if (m_ListaImagenesPociones[0].name == "PocionVida")
            m_ImagenCentroRelleno.fillAmount = 0;
    }

    public void RellenarPocion(System.Enum pocionRellenada, int cantidad)
    {
        switch (pocionRellenada)
        {
            case Enums.EnumPociones.FUERZA:
                if (m_ListaImagenesPociones[0].name == "PocionFuerza")
                {
                    m_ImagenCentroRelleno.fillAmount = CalcularRellenoPocion(m_listaPocionesSO[0], 1);
                }
                break;
            case Enums.EnumPociones.VELOCIDAD:
                if (m_ListaImagenesPociones[0].name == "PocionVelocidad")
                {
                    m_ImagenCentroRelleno.fillAmount = CalcularRellenoPocion(m_listaPocionesSO[1], 1);
                }
                break;
            case Enums.EnumPociones.VIDA:
                if (m_ListaImagenesPociones[0].name == "PocionVida")
                {
                    m_ImagenCentroRelleno.fillAmount = CalcularRellenoPocion(m_listaPocionesSO[2], 1);
                }
                break;
            case Enums.EnumPociones.INVISIBILIDAD:
                if (m_ListaImagenesPociones[0].name == "PocionInvisibilidad")
                {
                    m_ImagenCentroRelleno.fillAmount = CalcularRellenoPocion(m_listaPocionesSO[3], 1);
                }
                break;
        }
    }

    public void PocionUsada(System.Enum pocionUsada, int cantidad)
    {
        switch (pocionUsada)
        {
            case Enums.EnumPociones.FUERZA:
                if (m_ListaImagenesPociones[0].name == "PocionFuerza")
                {
                    m_ImagenCentroRelleno.fillAmount = 1;
                }
                break;
            case Enums.EnumPociones.VELOCIDAD:
                if (m_ListaImagenesPociones[0].name == "PocionVelocidad")
                {
                    m_ImagenCentroRelleno.fillAmount = 1;
                }
                break;
            case Enums.EnumPociones.VIDA:
                if (m_ListaImagenesPociones[0].name == "PocionVida")
                {
                    m_ImagenCentroRelleno.fillAmount = 1;
                }
                break;
            case Enums.EnumPociones.INVISIBILIDAD:
                if (m_ListaImagenesPociones[0].name == "PocionInvisibilidad")
                {
                    m_ImagenCentroRelleno.fillAmount = 1;
                }
                break;
        }
    }
    #endregion

}
