using UnityEngine;

public class VolverAnimacionAnterior : MonoBehaviour
{
    [SerializeField]
    private string m_parametroCambioAnimacion;
    [SerializeField]
    private bool m_activarODesactivarAnimacion;
    private Animator m_animator;

    private void Awake()
    {
        m_animator = GetComponent<Animator>();
    }

    public void CambiarAnimacion()
    {
       m_animator.SetBool(m_parametroCambioAnimacion, m_activarODesactivarAnimacion);
    }
}
