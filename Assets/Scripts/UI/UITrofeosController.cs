using UnityEngine;
using UnityEngine.UI;

public class UITrofeosController : MonoBehaviour
{
    [Header("Imagenes trofeos conseguidos")]
    [SerializeField]
    private Image m_ImageSlimeGoldoPlata;
    [SerializeField]
    private Image m_ImageSlimeGoldoOro;
    [SerializeField]
    private Image m_ImageSlimeGoldoBronce;

    [Header("Sprites trofeos conseguidos")]
    [SerializeField]
    private Sprite m_SlimeGoldoPlata;
    [SerializeField]
    private Sprite m_SlimeGoldoOro;
    [SerializeField]
    private Sprite m_SlimeGoldoBronce;

    private void Start()
    {
        ComprobarTrofeosConseguidosAnteriormente();
    }

    public void ComprobarTrofeosConseguidosAnteriormente()
    {
        if (GameManager.Instance.ObtenerTrofeoSlimePlata())
        {
            DesbloquearTrofeoSlime(2);
        }
        if (GameManager.Instance.ObtenerTrofeoSlimeOro())
        {
            DesbloquearTrofeoSlime(3);
        }
        if (GameManager.Instance.ObtenerTrofeoSlimeBronce())
        {
            DesbloquearTrofeoSlime(1);
        }
    }

    public void DesbloquearTrofeoSlime(int trofeoSlime)
    {
        switch (trofeoSlime)
        {
            case 1:
                m_ImageSlimeGoldoBronce.sprite = m_SlimeGoldoBronce;
                break;
            case 2:
                m_ImageSlimeGoldoPlata.sprite = m_SlimeGoldoPlata;
                break;
            case 3:
                m_ImageSlimeGoldoOro.sprite = m_SlimeGoldoOro;
                break;
        }
    }

}
