using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIProgresoController : MonoBehaviour
{
    private Slider m_Slider;
    private TextMeshProUGUI m_TextoCantidadProgreso;

    private Image m_CuadradoSliderRelleno;

    private void Awake()
    {
        m_Slider = GetComponent<Slider>();
        m_TextoCantidadProgreso = transform.GetChild(3).gameObject.GetComponent<TextMeshProUGUI>();
        m_CuadradoSliderRelleno = transform.GetChild(2).transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<Image>();
        m_CuadradoSliderRelleno.enabled = false;
    }

    private void Start()
    {
        CalcularYMostrarProgreso();
    }

    public void CalcularYMostrarProgreso()
    {
        float progreso = GameManager.Instance.ObtenerProgreso();
        m_Slider.value = progreso / 100;
        m_TextoCantidadProgreso.text = progreso + "%";
        if(m_Slider.value == 1)
            m_CuadradoSliderRelleno.enabled=true;

    }
}
