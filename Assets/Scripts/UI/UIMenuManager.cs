using UnityEngine;
using UnityEngine.EventSystems;

public class UIMenuManager : MonoBehaviour
{
    [Header("Referencias a animators")]
    [SerializeField]
    private Animator m_AnimatorMenuInicial;
    [SerializeField]
    private Animator m_AnimatorMenuPausa;
    [SerializeField]
    private Animator m_AnimatorMenuConfiguracion;
    private string m_ultimoBotonPresionado;

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_CerrarMenuPausa;

    #region Gestion menus
    public void AbrirCerrarMenuInicial()
    {
        m_AnimatorMenuInicial.SetBool("MenuInicialAbierto", !m_AnimatorMenuInicial.GetBool("MenuInicialAbierto"));
    }

    public void AbrirCerrarMenuPausa()
    {
        m_CerrarMenuPausa.Raise();
        m_AnimatorMenuPausa.SetBool("MenuPausaAbierto", !m_AnimatorMenuPausa.GetBool("MenuPausaAbierto"));
    }

    public void AbrirCerrarMenuConfiguracion()
    {
        m_CerrarMenuPausa.Raise();
        m_AnimatorMenuConfiguracion.SetBool("MenuConfiguracionAbierto", !m_AnimatorMenuConfiguracion.GetBool("MenuConfiguracionAbierto"));

        if (m_AnimatorMenuConfiguracion.GetBool("MenuConfiguracionAbierto"))
        {
            m_ultimoBotonPresionado = EventSystem.current.currentSelectedGameObject.name;
        }

        if (m_ultimoBotonPresionado == "BotonConfiguracionMP")
        {
            AbrirCerrarMenuPausa();
        }
        else
        {
            AbrirCerrarMenuInicial();
        }
    }
    #endregion

    #region Gestion Juego
    public void IniciarPartida()
    {
        GameManager.Instance.IniciarPartida();
    }

    public void CargarPartida()
    {
        GameManager.Instance.CargarPartida();
    }

    public void CerrarJuego()
    {
       // UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }
    #endregion
}
