using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIVida : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> m_ListaVidas;
    [SerializeField]
    private float m_CantidadAQuitar;

    [Header("Referencias a objetos")]
    [SerializeField]
    private SOPlayer m_PlayerSO;

    private void Awake()
    {
        m_ListaVidas = new List<GameObject>();
        foreach (Transform child in transform)
        {
            m_ListaVidas.Add(child.gameObject);
        }
        m_ListaVidas.Reverse(); //Giramos la lista para comenzar cogiendo los corazones del final
    }

    public void ActualizarVida(float vida)
    {
        float danoRecibido = 10 - vida;
        PlayerPierdeVida(danoRecibido);
    }

    public void PlayerPierdeVida(float danoRecibido)
    {
        m_CantidadAQuitar = danoRecibido * 0.5f;

        foreach (GameObject corazon in m_ListaVidas)
        {
            Image imagenCorazon = corazon.GetComponent<Image>();

            while (m_CantidadAQuitar > 0 && imagenCorazon.fillAmount > 0.01)
            {
                ReducirCorazones(m_CantidadAQuitar, imagenCorazon);
            }

            if (m_CantidadAQuitar <= 0) return;
        }
    }

    private void ReducirCorazones(float cantidadAQuitar, Image imagenCorazon)
    {
        float vidaActualCorazon = imagenCorazon.fillAmount;
        float vidaDestino = vidaActualCorazon - cantidadAQuitar < 0 ? 0 : vidaActualCorazon - cantidadAQuitar;
        imagenCorazon.fillAmount = vidaActualCorazon - cantidadAQuitar;
        m_CantidadAQuitar -= vidaActualCorazon - vidaDestino;
    }

    public void CurarVidaUI()
    {
        float curaRestante = 1;

        m_ListaVidas.Reverse();
        foreach (GameObject corazon in m_ListaVidas)
        {
            Image imagenCorazon = corazon.GetComponent<Image>();

            if(curaRestante>0 && imagenCorazon.fillAmount < 1)
            {
                float vidaActualCorazon = imagenCorazon.fillAmount;
                float vidaRestante = 1 -vidaActualCorazon;
                if(vidaRestante >= 1)
                {
                    imagenCorazon.fillAmount +=curaRestante;
                    curaRestante = 0;
                }
                else
                {
                    imagenCorazon.fillAmount += vidaRestante;
                    curaRestante -= vidaRestante;
                }
            }
        }
        m_ListaVidas.Reverse();
    }

}
