using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SliderVolumenController : MonoBehaviour
{
    [Header("Referencias a objetos")]
    private Slider m_Slider;
    private TextMeshProUGUI m_TextoPorcentaje;
    private Image m_CuadradoSliderRelleno;
    [SerializeField]
    private GameEvent m_ConfiguracionSonidoEfectos;
    [SerializeField]
    private GameEvent m_ConfiguracionSonidoMusica;

    private void Awake()
    {
        m_Slider = GetComponent<Slider>();
        m_TextoPorcentaje = m_Slider.transform.GetChild(3).gameObject.GetComponent<TextMeshProUGUI>();
        m_CuadradoSliderRelleno = transform.GetChild(2).transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<Image>();
        m_CuadradoSliderRelleno.enabled = false;
    }

    private void Start()
    {
        if (m_ConfiguracionSonidoMusica)
            m_Slider.value = GameManager.Instance.ObtenerConfiguracionVolumenMusica();
        else if (m_ConfiguracionSonidoEfectos)
            m_Slider.value = GameManager.Instance.ObtenerConfiguracionVolumenEfectos();
    }

    public void EnviarValorSliderAGameManager()
    {
        if (m_ConfiguracionSonidoEfectos) m_ConfiguracionSonidoEfectos.Raise();
        if (m_ConfiguracionSonidoMusica) m_ConfiguracionSonidoMusica.Raise();

        if (m_Slider.value == 1) m_CuadradoSliderRelleno.enabled = true;
        else m_CuadradoSliderRelleno.enabled = false;

        float porcentajeFloat = m_Slider.value * 100;
        int porcentage = Mathf.RoundToInt(porcentajeFloat);
        m_TextoPorcentaje.text = porcentage + "%";
        if (m_ConfiguracionSonidoMusica)
            GameManager.Instance.SetearConfiguracionVolumenMusica(m_Slider.value);
        else if (m_ConfiguracionSonidoEfectos)
            GameManager.Instance.SetearConfiguracionVolumenEfectos(m_Slider.value);
    }
}
