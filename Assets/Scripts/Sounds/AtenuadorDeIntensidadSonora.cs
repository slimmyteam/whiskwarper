using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtenuadorDeIntensidadSonora : MonoBehaviour
{
    [SerializeField]
    private float m_IntensidadAtenuada;
    
    public float ReducirSonido(float intensidad)
    {
        return intensidad - m_IntensidadAtenuada;
    }
}
