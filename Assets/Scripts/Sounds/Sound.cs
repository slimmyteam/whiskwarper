using System;
using UnityEngine;
public class Sound
{
    public readonly float soundRange;
    public readonly Transform soundTransform;
    public readonly SoundType.Type soundType;
    [Range(0, 5)]
    public float intensity;

    public Sound(float soundRange, Transform soundPosition, SoundType.Type soundType, float intensity)
    {
        this.soundRange = soundRange;
        this.soundTransform = soundPosition;
        this.soundType = soundType;
        this.intensity = intensity;

    }

    public void MakeSound()
    {
        Collider[] collisions = Physics.OverlapSphere(soundTransform.position, soundRange);
        for (int i = 0; i < collisions.Length; i++)
        {
            if (collisions[i].TryGetComponent(out IReactToSound target))
            {
                target.ReactToSound(this);
            }
        }
    }

}
