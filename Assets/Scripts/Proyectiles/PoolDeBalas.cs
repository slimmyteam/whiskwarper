using System.Collections.Generic;
using UnityEngine;

public class PoolDeBalas : MonoBehaviour
{
    [Header("Parametros de la pool")]
    [SerializeField]
    public int m_Tama�oPool = 20;
    private Queue<GameObject> m_ColaPoolDeBalas = new Queue<GameObject>();

    [Header("Referencia a objetos")]
    [SerializeField]
    public GameObject m_BalaPrefab;

    private void Start()
    {
        InicializarPool();
    }

    private void InicializarPool()
    {
        for (int i = 0; i < m_Tama�oPool; i++)
        {
            GameObject BalaPool = Instantiate(m_BalaPrefab, transform.position, Quaternion.identity);
            BalaPool.transform.SetParent(gameObject.transform);
            BalaPool.SetActive(false);
            m_ColaPoolDeBalas.Enqueue(BalaPool);
        }
    }

    public GameObject PedirBala()
    {
        if (m_ColaPoolDeBalas.Count == 0)
        {
            Debug.LogWarning("Pool vac�a. Incrementando tama�o de la pool...");
            GameObject BalaNueva = Instantiate(m_BalaPrefab, transform.position, Quaternion.identity); // Con Quaternion.identity establecemos que no habra rotacion inicial en el objeto
            BalaNueva.transform.SetParent(gameObject.transform);
            BalaNueva.SetActive(false);
            m_ColaPoolDeBalas.Enqueue(BalaNueva); // Ponemos en la cola el objeto
        }

        GameObject BalaDeLaPool = m_ColaPoolDeBalas.Dequeue(); // Cogemos el primer elemento de la cola
        BalaDeLaPool.SetActive(true);
        return BalaDeLaPool;
    }

    public void DevolverBalaALaPool(GameObject bala)
    {
        bala.SetActive(false);
        bala.transform.position = transform.position;
        bala.GetComponent<Rigidbody>().velocity = Vector3.zero;
        m_ColaPoolDeBalas.Enqueue(bala);
    }
}
