using System.Collections;
using UnityEngine;

[RequireComponent(typeof(ControlDeLayers))]
[RequireComponent (typeof(AudioSource))]
public class BalaController : MonoBehaviour, IProyectil
{
    [Header("Componentes")]
    private Rigidbody m_Rigidbody;
    private ControlDeLayers m_ControlDeLayers;
    private MeshRenderer m_MeshRendererBala;
    private MeshRenderer m_MeshRendererBoomerang;

    [Header("Materiales")]
    [SerializeField]
    private Material m_MaterialPropietarioPlayer;
    [SerializeField]
    private Material m_MaterialPropietarioEnemigo;

    [Header("Parametros bala")]
    [SerializeField]
    private float m_VelocidadBala;
    private float m_DanyoBala;
    [SerializeField]
    private float m_TiempoDesactivacion;
    [SerializeField]
    private string m_EscudoPlayerTag;
    [SerializeField]
    private string m_RebotableTag;
    public enum PropietarioBalaEnum { PLAYER, ENEMIGO }
    [SerializeField]
    private PropietarioBalaEnum m_PropietarioBala;
    public PropietarioBalaEnum PropietarioBala { get => m_PropietarioBala; }
    [SerializeField]
    private bool m_Boomerang;

    [Header("Referencia a objetos")]
    private PoolDeBalas m_PoolDeBalasScript;
    private GameObject m_ModeloBala;
    private GameObject m_ModeloBoomerang;

    //[Header("Control boomerang")]
    private float m_TiempoRetorno;
    private bool m_Vuelta;

    [Header("Layers")]
    [SerializeField]
    private LayerMask m_EscenarioLayer;
    private LayerMask m_PlayerLayer;
    private LayerMask m_EnemigoLayer;
    private LayerMask m_ProyectilLayer;

    [Header("SFX")]
    [SerializeField]
    private AudioClip m_AudioReboteEscudo;

    //-------------------------------[ FUNCIONES UNITY ]-------------------------------//
    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_ControlDeLayers = GetComponent<ControlDeLayers>();
        m_EnemigoLayer = LayerMask.NameToLayer("Enemy");
        m_PlayerLayer = LayerMask.NameToLayer("Player");
        m_ProyectilLayer = LayerMask.NameToLayer("Proyectil");

        m_ModeloBala = transform.GetChild(0).transform.GetChild(0).gameObject;
        m_ModeloBoomerang = transform.GetChild(0).transform.GetChild(1).gameObject;
        m_MeshRendererBala = m_ModeloBala.GetComponent<MeshRenderer>();
        m_MeshRendererBoomerang = m_ModeloBala.GetComponent<MeshRenderer>();
    }

    private void OnEnable()
    {
        StartCoroutine(DesactivarBalaTranscurridoTiempo());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == m_PlayerLayer && m_Boomerang && m_Vuelta)
        {
            DevolverBoomerangAPool();
            return;
        }

        if ((other.gameObject.layer == m_EnemigoLayer && m_PropietarioBala == PropietarioBalaEnum.ENEMIGO) || (other.gameObject.layer == m_PlayerLayer && m_PropietarioBala == PropietarioBalaEnum.PLAYER)
            || (other.gameObject.layer == m_ProyectilLayer && m_PropietarioBala == other.GetComponent<BalaController>().PropietarioBala) 
            || (other.gameObject.CompareTag("Bomba") && other.gameObject.layer == m_ProyectilLayer))
        {
            return;
        }

        if (other.CompareTag(m_EscudoPlayerTag) || other.CompareTag(m_RebotableTag))
        {
            gameObject.GetComponent<AudioSource>().loop = false;
            gameObject.GetComponent<AudioSource>().clip = m_AudioReboteEscudo;
            gameObject.GetComponent<AudioSource>().Play();
            if (other.TryGetComponent<EscudoController>(out _) && other.GetComponent<EscudoController>().Parry)
            {
                m_PropietarioBala = PropietarioBalaEnum.PLAYER;
                RebotarProyectil(other.transform.forward);
                return;
            }

            if (other.TryGetComponent(out PilarElementalController pilarElementalController))
            {
                if (TryGetComponent(out TengoObjetoLiquido _))
                {
                    AplicarElemento(pilarElementalController.SOElemento);
                    m_PropietarioBala = PropietarioBalaEnum.ENEMIGO;
                    RebotarProyectil(other.transform.forward);
                    return;
                }
            }
        }
        else if (other.TryGetComponent(out IDamageable damageable))
        {
            if ((other.gameObject.layer == m_PlayerLayer && m_PropietarioBala == PropietarioBalaEnum.ENEMIGO) || (other.gameObject.layer == m_EnemigoLayer && m_PropietarioBala == PropietarioBalaEnum.PLAYER))
            {
                damageable.PerderVida(m_DanyoBala);
            }
            if (m_Boomerang)
                return;
        }
        else if (m_Boomerang)
        {
            if (!m_Vuelta && m_ControlDeLayers.ContieneLayer(m_EscenarioLayer, other.gameObject.layer))
            {
                DarLaVueltaBoomerang();
            }
            if (m_Vuelta && other.TryGetComponent(out ICargable cargable))
            {
                cargable.SerArrastrado(gameObject);
            }
            return;
        }
        else if (gameObject.layer == LayerMask.NameToLayer("ProyectilTeleportable") && other.gameObject.layer == LayerMask.NameToLayer("Portal"))
        {
            return;
        }

        StartCoroutine(ReturnToPool());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    //-------------------------------[ FUNCIONES ]-------------------------------//

    #region Iniciar Bala
    public void PonerCaracteristicasBala(Vector3 posicionBala, Vector3 forwardPropietario, float danyoBala, float velocidadBala, float tiempoRetorno, PoolDeBalas scriptPoolDeBalas, PropietarioBalaEnum propietario, bool mejoraDisparo)
    {
        m_PropietarioBala = propietario;
        gameObject.transform.position = posicionBala;
        transform.forward = forwardPropietario;
        m_DanyoBala = danyoBala;
        m_VelocidadBala = velocidadBala;
        m_PoolDeBalasScript = scriptPoolDeBalas;
        m_Boomerang = mejoraDisparo;
        this.gameObject.GetComponent<Collider>().enabled = true;
        if (m_Boomerang)
        {
            m_ModeloBala.SetActive(false);
            m_ModeloBoomerang.SetActive(true);
            m_TiempoRetorno = tiempoRetorno;
            m_TiempoDesactivacion = Mathf.Infinity;
            DispararBalaBoomerang();
        }
        else
        {
            m_ModeloBoomerang.SetActive(false);
            m_ModeloBala.SetActive(true);
            DispararBala();
        }
    }

    private IEnumerator DesactivarBalaTranscurridoTiempo()
    {
        yield return new WaitForSeconds(m_TiempoDesactivacion);
        m_PoolDeBalasScript.DevolverBalaALaPool(gameObject);
    }

    /// <summary>
    /// Dispara una bala en direccion a su Forward.
    /// </summary>
    private void DispararBala()
    {
        m_Rigidbody.velocity = transform.forward * m_VelocidadBala;
    }

    private void CambioColor()
    {
        if (name.Contains("BalaBossRicochet")) return;

        if (m_Boomerang)
        {
            if (m_PropietarioBala == PropietarioBalaEnum.ENEMIGO)
                m_MeshRendererBoomerang.material = m_MaterialPropietarioEnemigo;
            else
                m_MeshRendererBoomerang.material = m_MaterialPropietarioPlayer;
        }
        else
        {
            if (m_PropietarioBala == PropietarioBalaEnum.ENEMIGO)
                m_MeshRendererBala.material = m_MaterialPropietarioEnemigo;
            else
                m_MeshRendererBala.material = m_MaterialPropietarioPlayer;
        }
    }

    public void AplicarElemento(ScriptableObject liquidoSO)
    {
        LiquidosSO liquido = (LiquidosSO)liquidoSO;
        m_ModeloBala.transform.GetChild(0).GetComponent<MeshRenderer>().material = liquido.Material;
        m_ModeloBala.transform.GetChild(1).GetComponent<MeshRenderer>().material = liquido.Material;

        GetComponent<TengoObjetoLiquido>().Objeto = liquido;
    }

    #endregion


    #region Boomerang

    /// <summary>
    /// Dispara el boomerang y llama a la corrutina DevolverBoomerang para que espere el momento de hacerlo volver.
    /// </summary>
    private void DispararBalaBoomerang()
    {
        m_Vuelta = false;
        DispararBala();
        StartCoroutine(DevolverBoomerang());
    }

    /// <summary>
    /// Para todas las corrutinas, marca el estado Vuelta como true, gira el boomerang en direccion contraria y llama a DispararBala para mover el boomerang de vuelta.
    /// </summary>
    private void DarLaVueltaBoomerang()
    {
        StopAllCoroutines();
        m_Vuelta = true;
        transform.forward = -transform.forward;
        DispararBala();
        StartCoroutine(EsperarRetornoBoomerang());
    }

    /// <summary>
    /// Espera el tiempo indicado antes de dar la vuelta al boomerang.
    /// </summary>
    /// <returns>WaitForSeconds(TiempoRetorno): Espera los segundos indicados antes de dar la vuelta al boomerang</returns>
    private IEnumerator DevolverBoomerang()
    {
        yield return new WaitForSeconds(m_TiempoRetorno);
        DarLaVueltaBoomerang();
    }

    /// <summary>
    /// Espera el tiempo indicado a que el boomerang vuelva al player. Si no consigue volver en ese tiempo, se devuelve a la pool de balas.
    /// </summary>
    /// <returns>WaitForSeconds(TiempoRetorno):  Espera los segundos indicados antes devolver el boomerang a la pool</returns>
    private IEnumerator EsperarRetornoBoomerang()
    {
        yield return new WaitForSeconds(m_TiempoRetorno);
        DevolverBoomerangAPool();
    }

    /// <summary>
    /// Desliga a los objetos arrastrados del boomerang y devuelve el boomerang a la pool.
    /// </summary>
    private void DevolverBoomerangAPool()
    {
        if (transform.childCount > 1)
        {
            ICargable[] objetosCargados = transform.gameObject.GetComponentsInChildren<ICargable>();

            foreach (ICargable cargable in objetosCargados)
            {
                cargable.DesligarseBoomerang();
            }
        }
        m_PoolDeBalasScript.DevolverBalaALaPool(gameObject);
    }

    #endregion

    #region Rebotar proyectil
    /// <summary>
    /// Busca el Vector3 reflejado del Forward del objeto y coloca el Forward en esa posicion. Mueve el objeto en direccion al nuevo Forward.
    /// </summary>
    /// <param name="normal">Vector3, Normal del objeto con el que se ha chocado el proyectil</param>
    public void RebotarProyectil(Vector3 normal)
    {
        transform.forward = Vector3.Reflect(transform.forward, normal);
        m_Rigidbody.velocity = transform.forward * m_VelocidadBala;
        CambioColor();
    }
    #endregion

    private IEnumerator ReturnToPool()
    {
        gameObject.GetComponent<Collider>().enabled = false;
        yield return new WaitForSeconds(0.1f);
        m_PoolDeBalasScript.DevolverBalaALaPool(gameObject);
    }
}
