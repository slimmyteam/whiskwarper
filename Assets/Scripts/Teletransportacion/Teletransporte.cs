using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(ControlDeLayers))]
public class Teletransporte : MonoBehaviour
{
    //Componentes
    private ControlDeLayers m_ControlDeLayers;

    [Header("Referencias")]
    [SerializeField]
    private GameObject m_CamaraIzquierda;
    [SerializeField]
    private GameObject m_CamaraDerecha;
    private int m_PlayerLayer;
    private int m_BossLayer;
    private int m_ProyectilTeleportableLayer;

    [Header("Clones")]
    [SerializeField]
    private GameObject m_PlayerClonPrefab;
    private GameObject m_PlayerClon;
    [SerializeField]
    private GameObject m_BossRicochetClonPrefab;
    private GameObject m_BossRicochetClon;
    [SerializeField]
    private GameObject m_ProyectilTeleportableClonPrefab;
    private GameObject m_ProyectilTeleportableClon;
    [SerializeField]
    private GameObject m_ObjetoTeletransportableClonPrefab;
    private GameObject m_ObjetoTeletransportableClon;
    [SerializeField]
    private GameObject m_BombaClonPrefab;
    private GameObject m_BombaClon;

    [Header("Portal")]
    [SerializeField]
    private float m_MargenPortal; //Se suma al borde de la camara
    [SerializeField, Range(0f, 1f)]
    private float m_InterpoladorTeletransporte;
    private float m_InterpoladorActualTeletransporte;
    [SerializeField, Range(0f, 1f)]
    private float m_SumaInterpoladorTeletransporte; //Lo que augmenta la interpolacion 

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_TeletransportarPlayer;
    [SerializeField]
    private GameEvent m_TeletransportarBoss;
    [SerializeField]
    private GameEvent m_FinTeletransporteEvent;

    [Header("Control del teletransporte")]
    [SerializeField]
    private bool m_Teletransportando;
    public bool Teletransportando => m_Teletransportando;

    [Header("Obstaculos")]
    [SerializeField]
    private LayerMask m_ObstaculosLayerMask; //Obstaculos
    [SerializeField]
    private LayerMask m_EscenarioLayerMask; //Todo el escenario

    private void Awake()
    {
        m_ControlDeLayers = GetComponent<ControlDeLayers>();
        m_Teletransportando = false;
        m_PlayerLayer = LayerMask.NameToLayer("Player");
        m_BossLayer = LayerMask.NameToLayer("Boss");
        m_ProyectilTeleportableLayer = LayerMask.NameToLayer("ProyectilTeleportable");
    }

    private void Start()
    {
        SceneManager.sceneLoaded += PrepararClones;
        PrepararClones(SceneManager.GetActiveScene(), LoadSceneMode.Single);
    }

    #region Inicio

    public void PrepararClones(Scene scene, LoadSceneMode sceneMode)
    {
        m_PlayerClon = InstanciarClon(m_PlayerClonPrefab);
        m_BossRicochetClon = InstanciarClon(m_BossRicochetClonPrefab);
        m_ProyectilTeleportableClon = InstanciarClon(m_ProyectilTeleportableClonPrefab);
        m_ObjetoTeletransportableClon = InstanciarClon(m_ObjetoTeletransportableClonPrefab);
        m_BombaClon = InstanciarClon(m_BombaClonPrefab);

        m_CamaraIzquierda = GameObject.Find("LeftCamera");
        m_CamaraDerecha = GameObject.Find("RightCamera");
    }

    private GameObject InstanciarClon(GameObject prefab)
    {
        try
        {
            GameObject clon = Instantiate(prefab, gameObject.transform);
            clon.SetActive(false);
            return clon;
        }
        catch {
            Debug.Log("No se pudo instanciar clon");
            return null; }
    }

    #endregion

    #region Cruzar entre dimensiones

    /// <summary>
    /// Busca la posicion final del teletransporte y llama a ComprobarCaminoLibre para comprobar si no hay obstaculos. 
    /// Si los hay, llama a FinalizarTeletransporte. Si no los hay, guarda la velocidad original del objeto, asigna el clon en funcion del objeto que quiere 
    /// teletransportarse y comienza las corrutinas de SalirObjeto y EntrarObjeto.
    /// </summary>
    /// <param name="objeto">Objeto que quiere teletransportarse</param>
    public void CruzarDimension(GameObject objeto)
    {
        if (!objeto.TryGetComponent<Rigidbody>(out _))
            return;
        float diferenciaPlayerRespectoSuCamara = objeto.transform.position.x < 0 ?
            objeto.transform.position.z - m_CamaraIzquierda.transform.position.z
            : objeto.transform.position.z - m_CamaraDerecha.transform.position.z;
        Vector3 posicionFinalDestino = objeto.transform.position.x < 0 ?
              new Vector3(m_CamaraDerecha.transform.GetChild(0).transform.position.x + m_MargenPortal, objeto.transform.position.y, 
                    m_CamaraDerecha.transform.position.z + diferenciaPlayerRespectoSuCamara)
            : new Vector3(m_CamaraIzquierda.transform.GetChild(0).transform.position.x - m_MargenPortal, objeto.transform.position.y, 
                    m_CamaraIzquierda.transform.position.z + diferenciaPlayerRespectoSuCamara);
        
        if (!ComprobarCaminoLibre(posicionFinalDestino, objeto.transform.position))
        {
            FinalizarTeletransporte(objeto);
            return;
        }

        m_Teletransportando = true;

        Vector3 velocidadOriginalObjeto = objeto.GetComponent<Rigidbody>().velocity;
        GameObject clon;
        switch (objeto.name)
        {
            case "Bomba(Clone)":
                clon = m_BombaClon;
                break;
            case "Player":
                clon = m_PlayerClon;
                m_TeletransportarPlayer.Raise();
                break;
            case "BossRicochet":
                clon = m_BossRicochetClon;
                m_TeletransportarBoss.Raise();
                break;
            case "BalaBossRicochet(Clone)":
                clon = m_ProyectilTeleportableClon;
                break;
            default:
                clon = m_ObjetoTeletransportableClon;
                break;
        }

        StartCoroutine(SalirObjeto(objeto, diferenciaPlayerRespectoSuCamara));
        StartCoroutine(EntrarObjeto(objeto, clon, posicionFinalDestino, velocidadOriginalObjeto));
    }

    /// <summary>
    /// Desplaza al objeto fuera de la vision de la camara, en direccion al portal. Calcula una posicion final fuera de la vision de 
    /// la camara y llama a MoverObjeto para desplazar el objeto.
    /// </summary>
    /// <param name="objeto">Objeto que quiere teletransportarse</param>
    /// <returns>IEnumerator MoverObjeto: Corrutina que mueve un objeto a la direccion indicada</returns>
    private IEnumerator SalirObjeto(GameObject objeto, float diferenciaPlayerRespectoSuCamara)
    {
        Vector3 posicionFinal = objeto.transform.position.x < 0 ? 
            new Vector3(m_CamaraIzquierda.transform.GetChild(0).transform.position.x + m_MargenPortal, objeto.transform.position.y, 
                m_CamaraIzquierda.transform.position.z + diferenciaPlayerRespectoSuCamara)
            : new Vector3(m_CamaraDerecha.transform.GetChild(0).transform.position.x - m_MargenPortal, objeto.transform.position.y, 
                m_CamaraDerecha.transform.position.z + diferenciaPlayerRespectoSuCamara);

        yield return MoverObjeto(objeto, posicionFinal);
    }

    /// <summary>
    /// Desplaza al clon del objeto desde fuera a dentro de la vision de la camara, hasta la posicion final indicada. 
    /// Coloca al clon en una posicion fuera de la vista de la camara y lo desplaza dentro. 
    /// Llama a MoverObjeto para realizar el desplazamiento del clon. 
    /// Cuando termina la corrutina MoverObjeto, llama a la corrutina TeletransportarObjeto
    /// para traer al objeto desde su posicion a la posicion final del clon.
    /// </summary>
    /// <param name="objeto">Objeto que quiere teletransportarse</param>
    /// <param name="clon">Clon del objeto</param>
    /// <param name="posicionFinal">Posicion final del clon</param>
    /// <param name="velocidadOriginalObjeto">Velocidad original del objeto que quiere teletransportarse</param>
    /// <returns>IEnumerator MoverObjeto: Corrutina que mueve un objeto a la direccion indicada</returns>
    private IEnumerator EntrarObjeto(GameObject objeto, GameObject clon, Vector3 posicionFinal, Vector3 velocidadOriginalObjeto)
    {
        clon.transform.position = new Vector3(objeto.transform.position.x < 0 ? 
            m_CamaraDerecha.transform.GetChild(0).transform.position.x - 3 
            : m_CamaraIzquierda.transform.GetChild(0).transform.position.x + 3,
            objeto.transform.position.y,
            posicionFinal.z);
        clon.transform.forward = (posicionFinal - clon.transform.forward).normalized;
        clon.SetActive(true);

        yield return MoverObjeto(clon, posicionFinal);

        StartCoroutine(TeletransportarObjeto(objeto, clon, velocidadOriginalObjeto));
    }

    /// <summary>
    /// Desplaza el objeto indicado hasta la posicion final indicada gradualmente.
    /// </summary>
    /// <param name="objeto">Objeto que se quiere transportar</param>
    /// <param name="posicionFinal">Posicion hasta la que se quiere transportar al objeto</param>
    /// <returns>WaitForEndOfFrame: Espera hasta el final del frame</returns>
    private IEnumerator MoverObjeto(GameObject objeto, Vector3 posicionFinal)
    {
        if (objeto != null)
        {
            m_InterpoladorActualTeletransporte = m_InterpoladorTeletransporte;
            float tiempoLimite = 0;
            if (objeto.TryGetComponent(out BombaBossController bomba))
            {
                bomba.GetComponent<Rigidbody>().useGravity = false;
                bomba.teletransportandose = true;
            }
            while (objeto != null && objeto.transform.position != posicionFinal && Mathf.Abs(objeto.transform.position.x - posicionFinal.x) >= 0.5f)
            {
                Vector3 posicionInicial = objeto.transform.position;
                if (objeto == null || tiempoLimite >= 1)
                    break;
                objeto.transform.position = Vector3.Lerp(posicionInicial, posicionFinal, m_InterpoladorActualTeletransporte);
                m_InterpoladorActualTeletransporte += m_SumaInterpoladorTeletransporte;
                tiempoLimite += Time.deltaTime;
                yield return null;
            }
            yield return null;
        }
    }

    /// <summary>
    /// Transporta al objeto a la posicion del clon y desactiva el clon. Restaura la velocidad original del objeto. Llama a FinalizarTeletransporte.
    /// </summary>
    /// <param name="objeto">Objeto que se quiere transportar</param>
    /// <param name="clon">Clon del objeto que se quiere transportar</param>
    /// <param name="velocidadOriginalObjeto">Velocidad original del objeto que quiere teletransportarse</param>
    /// <returns>WaitForSeconds(0.05f): Espera 0.5 segundos antes de realizar el teletransporte</returns>
    private IEnumerator TeletransportarObjeto(GameObject objeto, GameObject clon, Vector3 velocidadOriginalObjeto)
    {
        if (objeto != null)
        {
            //Debug.Log("***Entro en TeletransportarObjeto");

            yield return new WaitForSeconds(0.05f);
            clon.SetActive(false);

            objeto.transform.position = new Vector3(clon.transform.position.x, clon.transform.position.y, clon.transform.position.z);
            objeto.GetComponent<Rigidbody>().velocity = velocidadOriginalObjeto;

            FinalizarTeletransporte(objeto);
        }
    }

    /// <summary>
    /// Lanza el evento de fin de teletransporte correspondiente al objeto que se ha teletransportado.
    /// </summary>
    /// <param name="objeto">Objeto que se ha teletransportado</param>
    private void FinalizarTeletransporte(GameObject objeto)
    {
        //Debug.Log("finalizar teletransporte");
        if (objeto != null)
        {
            //Debug.Log("***Entro en FinalizarTeletransporte");
            if (objeto.TryGetComponent(out BombaBossController bomba))
            {
                bomba.GetComponent<Rigidbody>().useGravity = true;
                bomba.teletransportandose = false;
            }

            m_FinTeletransporteEvent.Raise();

            m_Teletransportando = false;
        }
    }

    #endregion

    #region Obstaculos

    /// <summary>
    /// 
    /// </summary>
    /// <param name="destino">Destino donde se quiere transportar el objeto</param>
    /// <returns>bool: indica si el camino esta libre para teletransportarse (true) o no (false)</returns>
    private bool ComprobarCaminoLibre(Vector3 destino, Vector3 posicionObjeto)
    {
        Vector3 posicionCamara = posicionObjeto.x < 0 ? m_CamaraDerecha.transform.position : m_CamaraIzquierda.transform.position;
        Vector3 direccion = (destino - posicionCamara).normalized;

        //Debug.Log("Comprobando si puedes pasar");

        if (Physics.Raycast(posicionCamara, direccion, out RaycastHit hit, Mathf.Infinity, m_EscenarioLayerMask))
        {
            //Debug.Log("Hit: " + hit.collider.name);
            Debug.DrawLine(posicionCamara, hit.point, Color.magenta, 2f);
            if (m_ControlDeLayers.ContieneLayer(m_ObstaculosLayerMask, hit.collider.gameObject.layer))
            {
                //Debug.Log("OBSTACULO "+hit.collider.name);
                return false;
            }
            else
            {
                //Debug.Log("CAMINO LIBRE");
                return true;
            }
        }

        return false;
    }

    #endregion

}
