using UnityEngine;

public class HieloSeDerrite : MonoBehaviour, IReaccionador
{
    public void Reaccionar()
    {
        StartCoroutine(GetComponent<ObjetoCaeRotaPaulatinamente>().HundirObjeto());
    }
}
