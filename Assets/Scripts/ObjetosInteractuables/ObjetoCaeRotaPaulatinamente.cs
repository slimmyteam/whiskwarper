using System.Collections;
using UnityEngine;

public class ObjetoCaeRotaPaulatinamente : MonoBehaviour, IReaccionador
{
    [SerializeField]
    private string m_nombreObjetoConElQueChoca;
    [SerializeField]
    private float m_gradosRotacionZ;
    [SerializeField]
    private float m_posicionFinalY;
    [SerializeField]
    private float m_tiempoCaida;

    [Header("Quieres mover el objeto?")]
    [SerializeField]
    private bool m_moverObjeto = false;


    private void OnTriggerEnter(Collider other)
    {
        if (other.name.Contains(m_nombreObjetoConElQueChoca))
        {
            StartCoroutine(CaidaObjeto());
        }
    }

    private IEnumerator CaidaObjeto()
    {
        float elapsedTime = 0f;

        Quaternion rotacionDeseada = Quaternion.Euler(gameObject.transform.eulerAngles.x, gameObject.transform.eulerAngles.y, m_gradosRotacionZ);

        while (elapsedTime < m_tiempoCaida)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, rotacionDeseada, Time.deltaTime * m_tiempoCaida);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.rotation = rotacionDeseada;

        if (m_moverObjeto)
        {
            StartCoroutine(HundirObjeto());
        }

        yield return null;
    }

    internal IEnumerator HundirObjeto()
    {
        Vector3 posicionInicial = transform.position;
        float elapsedTime = 0f;

        Vector3 posicionFinal = new Vector3(transform.position.x, m_posicionFinalY, transform.position.z);

        while (elapsedTime < m_tiempoCaida)
        {
            float t = elapsedTime / m_tiempoCaida;
            transform.position = Vector3.Lerp(posicionInicial, posicionFinal, t);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.position = posicionFinal;
        yield return null;
    }

    public void Reaccionar()
    {
        if (m_moverObjeto)
        {
            StartCoroutine(HundirObjeto());
        }
        else
        {
            StartCoroutine(CaidaObjeto());
        }
    }
}
