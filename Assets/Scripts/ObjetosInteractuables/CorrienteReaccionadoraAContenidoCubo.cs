using UnityEngine;

public class CorrienteReaccionadoraAContenidoCubo : MonoBehaviour, IMecanismoReaccionadorObjetoCargado, IRompible
{
    [SerializeField]
    private GameObject m_ObjetoQueReacciona;

    public void ReaccionaContenidoCubo(ScriptableObject contenidoCubo)
    {
        if (contenidoCubo.name == "SOLava")
        {
            m_ObjetoQueReacciona.SetActive(true);
        }
        else if (contenidoCubo.name == "SOAgua")
        {
            m_ObjetoQueReacciona.SetActive(false);
        }
    }

    public bool ReaccionaObjetoCargado(string nombreObjetoCargado) { return false; }

    public void Romperse()
    {
        //TODO apagar vfx
        m_ObjetoQueReacciona.SetActive(false);
    }
    public void EjecutarMecanismo()
    {

    }

}
