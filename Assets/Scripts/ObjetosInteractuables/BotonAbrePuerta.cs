using UnityEngine;
[RequireComponent (typeof(AudioSource))]
public class BotonAbrePuerta : MonoBehaviour, IMecanismo, IReaccionador, IGuardable
{
    [SerializeField]
    private bool m_HayQueDesactivarObjeto;
    [SerializeField]
    private bool m_HayQueActivarObjeto;
    [SerializeField]
    private GameObject m_ObjetoQueAbreAlgo;
    [SerializeField]
    private GameObject m_ObjetoQueSeDesactiva;
    [SerializeField]
    private GameObject m_ObjetoQueSeActiva;

    [Header("Guardar partida")]
    [SerializeField]
    private int m_IdPuzzle;
    [SerializeField]
    private GEInt m_PuzzleResuelto;

    [Header("SFX")]
    [SerializeField]
    private AudioClip m_AudioMecanismo;


    public void EjecutarMecanismo()
    {
        Reaccionar();
        AvisarEstadoResuelto();
    }

    public void Reaccionar()
    {
        if (m_ObjetoQueAbreAlgo.TryGetComponent(out LlaveAbrePuerta llaveAbrePuerta))
        {
            this.gameObject.GetComponent<AudioSource>().clip = m_AudioMecanismo;
            this.gameObject.GetComponent<AudioSource>().Play();
            llaveAbrePuerta.Reaccionar();
        }
        else if (m_ObjetoQueAbreAlgo.TryGetComponent(out ObjetoCaeRotaPaulatinamente puerta))
        {
            this.gameObject.GetComponent<AudioSource>().clip = m_AudioMecanismo;
            this.gameObject.GetComponent<AudioSource>().Play();
            puerta.Reaccionar();
        }

        if (m_HayQueDesactivarObjeto)
        {
            m_ObjetoQueSeDesactiva.SetActive(false);
        }

        if (m_HayQueActivarObjeto)
        {
            m_ObjetoQueSeActiva.SetActive(true);
        }
    }
    public void AvisarEstadoResuelto()
    {
        m_PuzzleResuelto.Raise(m_IdPuzzle);
    }
}
