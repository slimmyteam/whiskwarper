using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TriggerMostradorInfo : MonoBehaviour
{
    [Header("Referencias a objetos")]
    [SerializeField]
    private GameObject m_CanvasMuroInvisible;
    [SerializeField]
    private Image m_imagenObjetoDeFondo;
    private Image m_imagenObjetoDeFondoSprite;
    [SerializeField]
    private GameObject m_marcoEImagen;
    [SerializeField]
    private Image m_imagenEnmarcada;
    private Image m_imagenEnmarcadaSprite;
    [SerializeField]
    private TextMeshProUGUI m_nombreObjeto;
    [SerializeField]
    private TextMeshProUGUI m_informacionMensaje;
    private Mensaje m_mensaje;
    [SerializeField]
    private SOHabilidades[] m_HabilidadQueNecesita;

    private void Awake()
    {
        m_imagenObjetoDeFondoSprite = m_imagenObjetoDeFondo.GetComponent<Image>();
        m_imagenEnmarcadaSprite = m_imagenEnmarcada.GetComponent<Image>();
        m_mensaje = GetComponent<Mensaje>();
        m_marcoEImagen.SetActive(false);
        m_CanvasMuroInvisible.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != LayerMask.NameToLayer("Player")) return;

        if (m_mensaje.SpriteSlime != null)
        {
            m_marcoEImagen.SetActive(true);
            m_imagenEnmarcadaSprite.sprite = m_mensaje.SpriteSlime;
        }

        m_imagenObjetoDeFondoSprite.sprite = m_mensaje.SpriteMensaje;
        m_nombreObjeto.text = m_mensaje.NombreMensaje;
        m_informacionMensaje.text = m_mensaje.InformacionMensaje;

        m_CanvasMuroInvisible.SetActive(true);
        foreach (SOHabilidades habilidad in m_HabilidadQueNecesita)
        {
            if (other.TryGetComponent(out PlayerHabilidades playerHabilidades) && playerHabilidades.ListaHabilidadesAdquiridas.Contains(habilidad))
                gameObject.SetActive(false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer != LayerMask.NameToLayer("Player")) return;
        m_marcoEImagen.SetActive(false);
        m_CanvasMuroInvisible.SetActive(false);
    }
}
