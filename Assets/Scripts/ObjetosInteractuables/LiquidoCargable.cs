using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(ObjetoDanyino))]
public class LiquidoCargable : MonoBehaviour, IRecogibleConCubo, IElectrificable
{
    [Header("Parametros del liquido")]
    [SerializeField]
    private Enums.EnumLiquidos m_Liquido;
    [SerializeField]
    private bool m_Infinito = false;
    public bool Infinito { get => m_Infinito; set => m_Infinito = value; }
    [SerializeField]
    private bool m_Electrificado = false;
    public bool Electrificado { get => m_Electrificado; set => m_Electrificado = value; }
    [SerializeField]
    private LayerMask m_LayersPararSuelo; //Normalmente: Default, Tierra, Escenario
    [SerializeField]
    private Enums.EnumLiquidos m_DebilidadAnte;
    float[] m_YGriegas = new float[2];
    [SerializeField]
    private bool m_LiquidoPuedeExpandir;

    [Header("Corutinas")]
    [SerializeField]
    private int m_TiempoDeDestruccion = 5;
    [SerializeField]
    private Coroutine m_DisminuirEscalaProgresivamente;
    [SerializeField]
    private float m_DuracionCaida = 1;

    //Componentes
    private ObjetoDanyino m_ObjetoDanyino;

    private void Awake()
    {
        ComprobarDebilidadSegunObjetoQueTengo(GetComponent<TengoObjetoLiquido>().Objeto.TipoLiquido);
        m_ObjetoDanyino = GetComponent<ObjetoDanyino>();
        m_LiquidoPuedeExpandir = false;
    }

    private void Start()
    {
        if(m_Liquido != Enums.EnumLiquidos.LAVA && !m_Electrificado)
            m_ObjetoDanyino.enabled = false;
    }

    /// <summary>
    /// Si los dos objetos que colisionan son del mismo tipo, se aumenta la escala de uno y el otro se destruye (el charco se hace mas grande)
    /// Si son de diferente tipo, se comprueba si uno es debilidad del otro
    /// Si el recurso es infinito, no se disminuye escala ni se destruye
    /// </summary>
    /// <param name="other">Collider, liquido con el que colisiona</param>
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.TryGetComponent(out LiquidoCargable liquidoCargable))
        {
            if (gameObject.GetComponent<TengoObjetoLiquido>().Objeto.name == other.gameObject.GetComponent<TengoObjetoLiquido>().Objeto.name) //Si son el mismo liquido
            {
                OrdenarYGriegas(other.gameObject);

                if ((liquidoCargable.Infinito || gameObject.transform.position.y == m_YGriegas[1]) && !m_Infinito) //Si el liquido con el que choco es infinito o mi 'y' es mas alta, me destruyo
                {
                    DestruirObjeto();
                }
                else if (gameObject.transform.position.y == m_YGriegas[0] && m_LiquidoPuedeExpandir) //Si mi 'y' es mas baja, me aumento la escala
                {
                    if (m_Infinito) return;
                    AumentarEscala();
                }
            }
            else if (other.gameObject.GetComponent<TengoObjetoLiquido>().Objeto.TipoLiquido == m_DebilidadAnte)
            {
                if (Infinito) //Si soy infinito, aunque me haya todo un liquido al que soy debil, se destruye ese liquido
                {
                    Destroy(other.gameObject);
                    return;
                }
                ComprobarQueLiquidoEstaMasArriba(other.gameObject);
            }
        }
        if (other.gameObject.TryGetComponent(out PosteController poste))
        {
            if (poste.TieneEnergia)
                Electrificar();
        }
        if(other.gameObject.TryGetComponent(out EnchufeController enchufe))
        {
            Debug.Log("ENCHUFE");
            if (m_Electrificado)
                enchufe.ReaccionaContactoElectrico();
        }
        else if(m_Electrificado && other.gameObject.TryGetComponent(out IElectrificable electrificable))
        {
            Debug.Log("ELECTRIFICABLE");
            electrificable.Electrificar();
        }
    }

    #region Caer
    /// <summary>
    /// Si el raycast toca alguna de las layers que representan el suelo, el objeto cae
    /// </summary>
    public void Caer()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, Mathf.Infinity, m_LayersPararSuelo))
        {
            Debug.Log("CAER");
            StartCoroutine(CaerObjeto(hit.point));
        }
    }
    #endregion

    #region CaerObjeto
    /// <summary>
    /// Corutina que hace caer el objeto hacia el suelo
    /// </summary>
    /// <param name="posicionFinal">Vector3, Posicion en la que ha de acabar el objeto</param>
    /// <returns></returns>
    private IEnumerator CaerObjeto(Vector3 posicionFinal)
    {
        Vector3 posicionInicial = transform.position;
        float elapsedTime = 0f;

        while (elapsedTime < m_DuracionCaida)
        {
            float t = elapsedTime / m_DuracionCaida;
            transform.position = Vector3.Lerp(posicionInicial, posicionFinal, t);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        transform.position = posicionFinal;
        m_LiquidoPuedeExpandir = true;
        if (m_DisminuirEscalaProgresivamente != null) yield return null;
        m_DisminuirEscalaProgresivamente = StartCoroutine(DisminuirEscalaProgresivamente());

        yield return null;
    }
    #endregion

    #region Cubo recoge
    /// <summary>
    /// El cubo recoge el liquido. Si es un recurso infinito, no padece cambios. Si no lo es, se hace mas peque�o o se destruye
    /// </summary>
    /// <returns></returns>
    public ScriptableObject CuboRecoge()
    {
        LiquidosSO liquido = GetComponent<TengoObjetoLiquido>().Objeto;

        if (m_Infinito)
        {
            liquido.Liquido.GetComponent<LiquidoCargable>().Infinito = false; // El liquido que el cubo recoge cuando lo instancie no sera infinito
            return liquido;
        }

        DisminuirEscalaYDestruir();

        return liquido;
    }
    #endregion

    #region Comprobar debilidad del liquido que tengo
    /// <summary>
    /// Segun el tipo de liquido que contiene este GameObject, se busca que liquido es su debilidad (el que causa su destruccion)
    /// </summary>
    /// <param name="tipoLiquido">Enums.EnumLiquidos, Tipo del liquido que contiene este GameObject</param>
    private void ComprobarDebilidadSegunObjetoQueTengo(Enums.EnumLiquidos tipoLiquido)
    {
        switch (tipoLiquido)
        {
            case Enums.EnumLiquidos.LAVA:
                m_DebilidadAnte = Enums.EnumLiquidos.AGUA;
                break;

            case Enums.EnumLiquidos.AGUA:
                m_DebilidadAnte = Enums.EnumLiquidos.NADA;
                break;
        }
    }
    #endregion

    #region Comprobar que objeto esta mas arriba
    /// <summary>
    /// Cuando he colisionado con un liquido que es mi debilidad, si soy el liquido que ya estaba en el suelo, me disminuyo la escala/me destruyo;
    /// si soy el liquido que tiro desde el cubo, me destruyo
    /// </summary>
    /// <param name="other">GameObject, Objeto a destruir en el caso de ser un recurso infinito</param>
    private void ComprobarQueLiquidoEstaMasArriba(GameObject other = null)
    {
        OrdenarYGriegas(other);

        if (gameObject.transform.position.y == m_YGriegas[1]) //Soy liquido que tiro desde el cubo (mi yGriega es mas alta)
        {
            DestruirObjeto();
        }

        if (gameObject.transform.position.y == m_YGriegas[0]) //Soy liquido que estaba en el suelo
        {
            DisminuirEscalaYDestruir();
            Destroy(other.gameObject);
        }
    }
    #endregion

    #region OrdenarYGriegas
    /// <summary>
    /// Cogemos la posicion en y tanto de este liquido como la del liquido con el que hemos colisionado y se ordena para ver que liquido esta mas alto
    /// </summary>
    /// <param name="other">GameObject, Liquido con el que este liquido ha colisionado</param>
    private void OrdenarYGriegas(GameObject other)
    {
        m_YGriegas[0] = gameObject.transform.position.y;
        m_YGriegas[1] = other.transform.position.y;

        Array.Sort(m_YGriegas);
    }
    #endregion

    #region Aumentar escala
    /// <summary>
    /// Se aumenta la escala del liquido en 1 tanto en x como en z
    /// </summary>
    private void AumentarEscala()
    {
        gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 1, gameObject.transform.localScale.y, gameObject.transform.localScale.z + 1);
    }
    #endregion

    #region Disminuir escala y destruir
    /// <summary>
    /// Se disminuye la escala del liquido en 1 tanto en x como en z. Si el liquido esta en su escala mas peque�a (0,1,0), se destruye
    /// </summary>
    private void DisminuirEscalaYDestruir()
    {
        gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - 1, gameObject.transform.localScale.y, gameObject.transform.localScale.z - 1);

        if (gameObject.transform.localScale == Vector3.up)
        {
            DestruirObjeto();
        }
    }
    #endregion

    #region Disminuir escala progresivamente
    /// <summary>
    /// Cada cierto tiempo, la escala del liquido se va reduciendo
    /// </summary>
    /// <returns></returns>
    private IEnumerator DisminuirEscalaProgresivamente()
    {
        while (true)
        {
            yield return new WaitForSeconds(m_TiempoDeDestruccion);
            DisminuirEscalaYDestruir();
        }
    }
    #endregion

    #region Destruir liquido
    /// <summary>
    /// Destruye el liquido y para todas las corutinas
    /// </summary>
    private void DestruirObjeto()
    {
        StopAllCoroutines();
        Destroy(gameObject);
    }
    #endregion

    #region Electrificar liquido

    public void Electrificar()
    {
        if (m_Liquido != Enums.EnumLiquidos.AGUA || m_Electrificado)
            return;
        m_Electrificado = true;
        m_ObjetoDanyino.enabled = true;
    }

    #endregion
}
