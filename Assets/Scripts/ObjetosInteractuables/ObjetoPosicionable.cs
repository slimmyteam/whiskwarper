using System.Collections;
using UnityEngine;

public class ObjetoPosicionable : MonoBehaviour, IPosicionable
{
    [SerializeField]
    private float m_Duracion = 1f;

    public IEnumerator PosicionarObjeto(Vector3 posicionFinal)
    {
        Vector3 posicionInicial = transform.position;
        float elapsedTime = 0f;

        while (elapsedTime < m_Duracion)
        {
            float t = elapsedTime / m_Duracion;
            transform.position = Vector3.Lerp(posicionInicial, posicionFinal, t);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.position = posicionFinal;
        yield return null;
    }
}
