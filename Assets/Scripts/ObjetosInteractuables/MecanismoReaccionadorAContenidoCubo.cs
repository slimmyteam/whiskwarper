using UnityEngine;

public class MecanismoReaccionadorAContenidoCubo : MonoBehaviour, IMecanismoReaccionadorObjetoCargado, IPuzzle, IMecanismo, IGuardable
{
    [Header("Referencias")]
    [SerializeField]
    private ScriptableObject m_ObjetoNecesario;
    [SerializeField]
    private MonoBehaviour m_ScriptParaReaccionar;

    [Header("Guardar partida")]
    [SerializeField]
    private int m_IdPuzzle;
    [SerializeField]
    private GEInt m_PuzzleResueltoEvent;

    [SerializeField]
    private bool m_Activado = false;
    public bool Activado { get => m_Activado; set => m_Activado = value; }

    public void PuzzleFinalizado()
    {
        m_Activado = true;
    }

    public void ReaccionaContenidoCubo(ScriptableObject contenidoCubo)
    {
        if (m_ObjetoNecesario.name == contenidoCubo.name)
        {
            HacerReaccionar();
        }
    }

    private void HacerReaccionar()
    {
        if (m_ScriptParaReaccionar is IReaccionador objetoReaccionador)
        {
            objetoReaccionador.Reaccionar();
            PuzzleFinalizado();
        }
    }

    public bool ReaccionaObjetoCargado(string nombreObjetoCargado)
    {
        return false;
    }

    public void EjecutarMecanismo()
    {
        HacerReaccionar();
    }

    public void AvisarEstadoResuelto()
    {
        m_PuzzleResueltoEvent.Raise(m_IdPuzzle);
    }
}
