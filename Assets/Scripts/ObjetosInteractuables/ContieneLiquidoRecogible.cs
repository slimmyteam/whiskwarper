using UnityEngine;

public class ContieneLiquidoRecogible : MonoBehaviour, IRecogibleConCubo
{
    [SerializeField]
    private GEEnumInt m_PozoRellenaPocion;

    public ScriptableObject CuboRecoge()
    {
        print("Entra en cuborecoge");

        if (TryGetComponent(out TengoObjetoPociones tengoObjetoPociones))
        {
            print("Entra en tengoObjetoPociones");
            m_PozoRellenaPocion.Raise(Enums.EnumPociones.VIDA, 0);
            return tengoObjetoPociones.Objeto;
        }
        else if (TryGetComponent(out TengoObjetoLiquido tengoObjetoLiquido))
        {
            return tengoObjetoLiquido.Objeto;
        }

        return null;
    }
}
