using UnityEngine;

public class TrofeoSlimeController : MonoBehaviour, IMecanismo, IGuardable
{
    public bool SlimeRecolectado;

    [Header("Guardar partida")]
    [SerializeField]
    private int m_IdSlime;
    [SerializeField]
    private int m_IdPuzzle;
    [SerializeField]
    private GEInt m_PuzzleResuelto;

    [Header("Game Events")]
    [SerializeField]
    private GEInt m_SlimeDesbloqueadoEvent;

    private void Awake()
    {
        SlimeRecolectado = false;
    }
    public void EjecutarMecanismo()
    {
        GameManager.Instance.AumentarNumeroSlimes(m_IdSlime);
        SlimeRecolectado = true;
        m_SlimeDesbloqueadoEvent.Raise(m_IdSlime);
        gameObject.SetActive(false);
    }

    public void AvisarEstadoResuelto()
    {
        m_PuzzleResuelto.Raise(m_IdPuzzle);
    }
}
