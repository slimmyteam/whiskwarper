using UnityEngine;

public class HieloResbalable : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out IResbalable iResbalable))
        {
            iResbalable.Resbalar();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.TryGetComponent(out IResbalable iResbalable))
        {
            iResbalable.DejarDeResbalar();
        }
    }
}
