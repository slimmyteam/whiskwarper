using UnityEngine;

[RequireComponent (typeof(AudioSource))]
public class ObjetoReaccionadorAContenidoCubo : MonoBehaviour, IMecanismoReaccionadorObjetoCargado, IGuardable
{
    [Header("Referencias")]
    [SerializeField]
    private GameObject m_Contenido;
    [SerializeField]
    private ScriptableObject m_ObjetoNecesario;
    [SerializeField]
    private Material m_MaterialBaseContenido;

    [Header("Guardar partida")]
    [SerializeField]
    private int m_IdPuzzle;
    [SerializeField]
    private GEInt m_PuzzleResuelto;

    [Header("Parametros")]
    [SerializeField]
    private int m_CantidadNecesaria;
    [SerializeField]
    private int m_CantidadActual;
    [SerializeField]
    private GameObject m_ObjetoQueReacciona;
    [SerializeField]
    private bool m_Activado = false;
    public bool Activado { get => m_Activado; set => m_Activado = value; }
    private Vector3 m_ContenidoLocalScaleOriginal;

    [Header("SFX")]
    [SerializeField]
    private AudioClip m_AudioRellenar;

    private void Start()
    {
        m_ContenidoLocalScaleOriginal = m_Contenido.transform.localScale;
    }

    public void ReaccionaContenidoCubo(ScriptableObject contenidoCubo)
    {
        if (m_ObjetoNecesario.name == contenidoCubo.name)
        {
            this.gameObject.GetComponent<AudioSource>().loop = false;
            this.gameObject.GetComponent<AudioSource>().clip = m_AudioRellenar;
            this.gameObject.GetComponent<AudioSource>().Play();
            m_CantidadActual++;

            if (!m_Contenido) return;

            m_Contenido.transform.localScale = new Vector3(m_Contenido.transform.localScale.x, m_Contenido.transform.localScale.y + 0.2f, m_Contenido.transform.localScale.z);
            m_Contenido.GetComponent<MeshRenderer>().material = EstablecerMaterialContenido();
        }

        if (m_CantidadActual != m_CantidadNecesaria)
            return;

        EjecutarMecanismo();
        AvisarEstadoResuelto();
    }

    private Material EstablecerMaterialContenido()
    {
        if (m_ObjetoNecesario.GetType().ToString().Equals(typeof(LiquidosSO).ToString()))
        {
            LiquidosSO liquido = (LiquidosSO)m_ObjetoNecesario;
            return liquido.Material;
        }
        return null;
    }

    public bool ReaccionaObjetoCargado(string nombreObjetoCargado)
    {
        return false;
    }

    public void PerderLiquido()
    {
        m_Contenido.GetComponent<MeshRenderer>().material = m_MaterialBaseContenido;
        m_CantidadActual = 0;
        m_Activado = false;
        m_Contenido.transform.localScale = m_ContenidoLocalScaleOriginal;
    }

    public void EjecutarMecanismo()
    {
        m_Activado = true;
        if (m_ObjetoQueReacciona.TryGetComponent(out IReaccionador iReacciona))
        {
            iReacciona.Reaccionar();
        }
    }

    public void AvisarEstadoResuelto()
    {
        if(m_PuzzleResuelto != null)
            m_PuzzleResuelto.Raise(m_IdPuzzle);
    }
}
