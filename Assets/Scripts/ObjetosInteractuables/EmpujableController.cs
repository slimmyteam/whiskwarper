using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class EmpujableController : MonoBehaviour, IEmpujable
{
    [SerializeField]
    private float m_Duracion = 1f;
    [SerializeField]
    private LayerMask layerAntiempujable;

    public void Empujar(Vector3 posiconOrigenEmpuje)
    {
        if (gameObject.CompareTag("ObjetoInamovible")) 
            return;

        if (TryGetComponent(out ObjetoEspejoEmpujable objEspejoscript))
        {
            objEspejoscript.MoverEspejo(posiconOrigenEmpuje);
        }

        Vector3 vectorDireccionEmpuje = (transform.position - posiconOrigenEmpuje).normalized;
        if (vectorDireccionEmpuje.x > 0.9f)
            vectorDireccionEmpuje.x = 1;

        else if (vectorDireccionEmpuje.x < -0.9f)
            vectorDireccionEmpuje.x = -1;

        else vectorDireccionEmpuje.x = 0;
        vectorDireccionEmpuje.y = 0;

        if (vectorDireccionEmpuje.z > 0.9f)
            vectorDireccionEmpuje.z = 1;

        else if (vectorDireccionEmpuje.z < -0.9f)
            vectorDireccionEmpuje.z = -1;

        else vectorDireccionEmpuje.z = 0;

        Vector3 vectorPosicionFinal = transform.position + vectorDireccionEmpuje;

        float anchuraCaja = vectorDireccionEmpuje.z != 0 ? GetComponent<BoxCollider>().size.z : GetComponent<BoxCollider>().size.x;
        if (Physics.Raycast(transform.position, vectorDireccionEmpuje, out RaycastHit hit, anchuraCaja * 1.5f, layerAntiempujable))
        {

            if (vectorDireccionEmpuje.x > 0)
                vectorPosicionFinal = new Vector3(hit.point.x - anchuraCaja / 2, transform.position.y, transform.position.z);
            else if (vectorDireccionEmpuje.x < 0)
                vectorPosicionFinal = new Vector3(hit.point.x + anchuraCaja / 2, transform.position.y, transform.position.z);
            else if (vectorDireccionEmpuje.z > 0)
                vectorPosicionFinal = new Vector3(transform.position.x, transform.position.y, hit.point.z - anchuraCaja / 2);
            else if (vectorDireccionEmpuje.z < 0)
                vectorPosicionFinal = new Vector3(transform.position.x, transform.position.y, hit.point.z + anchuraCaja / 2);
        }
        StartCoroutine(MoverCaja(vectorPosicionFinal));
    }

    private IEnumerator MoverCaja(Vector3 posicionFinal)
    {
        Vector3 posicionInicial = transform.position;
        float elapsedTime = 0f;
        gameObject.GetComponent<AudioSource>()?.Play();
        while (elapsedTime < m_Duracion)
        {
            float t = elapsedTime / m_Duracion;
            transform.position = Vector3.Lerp(posicionInicial, posicionFinal, t);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.position = posicionFinal;
        yield return null;
    }

}
