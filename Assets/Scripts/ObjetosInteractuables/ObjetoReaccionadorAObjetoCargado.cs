using UnityEngine;

public class ObjetoReaccionadorAObjetoCargado : MonoBehaviour, IMecanismoReaccionadorObjetoCargado, IPuzzle, IGuardable
{
    [Header("Referencias")]
    [SerializeField]
    private string m_ObjetoRequerido;
    [SerializeField]
    private MonoBehaviour m_ScriptParaReaccionar;

    [Header("Guardar partida")]
    [SerializeField]
    private int m_IdPuzzle;
    [SerializeField]
    private GEInt m_PuzzleResuelto;

    [Header("Estado del puzzle")]
    [SerializeField]
    private bool m_PuzzleFinalizado;

    private void Start()
    {
        if (m_PuzzleFinalizado)
            ReaccionaObjetoCargado(m_ObjetoRequerido);
    }

    public void PuzzleFinalizado()
    {
        m_PuzzleFinalizado = true;
        AvisarEstadoResuelto();
    }

    public void ReaccionaContenidoCubo(ScriptableObject contenidoCubo) { }

    public bool ReaccionaObjetoCargado(string nombreObjetoCargado)
    {
        if (nombreObjetoCargado == m_ObjetoRequerido)
        {
            EjecutarMecanismo();
            return true;
        }
        return false;
    }

    public void EjecutarMecanismo()
    {
        if (m_ScriptParaReaccionar is IReaccionador objetoReaccionador)
        {
            objetoReaccionador.Reaccionar();
            PuzzleFinalizado();
        }
    }

    public void AvisarEstadoResuelto()
    {
        m_PuzzleResuelto.Raise(m_IdPuzzle);
    }
}
