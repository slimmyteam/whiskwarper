using UnityEngine;

[RequireComponent (typeof(AudioSource))]
public class ObjetoEspejoEmpujable : MonoBehaviour
{
    [SerializeField]
    private GameObject m_ObjetoEspejo;
    private float m_DistanciaEntreMundosCubosX;
    private float m_DistanciaEntreMundosCubosZ;

    public void MoverEspejo(Vector3 posicionOrigenEmpuje)
    {
        m_DistanciaEntreMundosCubosX = Mathf.Abs(m_ObjetoEspejo.transform.position.x - this.transform.position.x);
        print(m_DistanciaEntreMundosCubosX);
        m_DistanciaEntreMundosCubosZ = m_ObjetoEspejo.transform.position.z - this.transform.position.z;
        print(m_DistanciaEntreMundosCubosZ);
        Vector3 posicionEmpujeEspejo;
        if (posicionOrigenEmpuje.x < 0)
            posicionEmpujeEspejo = new Vector3(posicionOrigenEmpuje.x + m_DistanciaEntreMundosCubosX, posicionOrigenEmpuje.y, posicionOrigenEmpuje.z);
        else
            posicionEmpujeEspejo = new Vector3(posicionOrigenEmpuje.x - m_DistanciaEntreMundosCubosX, posicionOrigenEmpuje.y, posicionOrigenEmpuje.z);
        posicionEmpujeEspejo = new Vector3(posicionEmpujeEspejo.x, posicionOrigenEmpuje.y, posicionEmpujeEspejo.z + m_DistanciaEntreMundosCubosZ);
        m_ObjetoEspejo.GetComponent<EmpujableController>().Empujar(posicionEmpujeEspejo);
    }
}
