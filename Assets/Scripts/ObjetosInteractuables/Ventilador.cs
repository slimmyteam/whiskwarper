using UnityEngine;

public class Ventilador : MonoBehaviour, IReaccionador, IRompible
{
    [Header("Estado del ventilador")]
    [SerializeField]
    private bool m_Activo;

    [Header("Referencias")]
    [SerializeField]
    private GameObject m_CorrienteDeAire;

    private void Start()
    {
        m_CorrienteDeAire = transform.GetChild(0).gameObject;
        if (m_Activo)
            Reaccionar();
    }

    public void Reaccionar()
    {
        m_Activo = true;
        m_CorrienteDeAire.SetActive(true);
    }

    public void Romperse()
    {
        m_Activo = false;
        m_CorrienteDeAire.SetActive(false);
    }
}
