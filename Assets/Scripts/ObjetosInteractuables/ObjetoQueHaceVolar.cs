using UnityEngine;

public class ObjetoQueHaceVolar : MonoBehaviour
{
    [SerializeField]
    private GameObject m_posicionPararViento;
    [SerializeField]
    private BoxCollider m_boxColliderUno;
    [SerializeField]
    private BoxCollider m_boxColliderDos;
    [SerializeField]
    private int m_CantidadQueHaceVolar = 55;
    [SerializeField]
    private Enums.EnumDireccionVuelo m_DireccionVuelo;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.TryGetComponent<IVolable>(out _))
        {
            other.gameObject.GetComponent<Rigidbody>().AddForce(HaciaDondeVuela(transform) * m_CantidadQueHaceVolar);

            if (other.gameObject.layer != LayerMask.NameToLayer("Player")) return;

            other.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!m_posicionPararViento) return;

        if (other.gameObject.TryGetComponent<IPosicionable>(out _))
        {
            StartCoroutine(other.gameObject.GetComponent<ObjetoPosicionable>().PosicionarObjeto(m_posicionPararViento.transform.position));
            m_boxColliderDos.enabled = true;
            m_boxColliderUno.enabled = false;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (!m_posicionPararViento) return;

        if (other.gameObject.TryGetComponent<IPosicionable>(out _))
        {
            m_boxColliderDos.enabled = false;
            m_boxColliderUno.enabled = true;
        }

        if (other.gameObject.TryGetComponent<IVolable>(out _))
        {
            if (other.gameObject.layer != LayerMask.NameToLayer("Player")) return;

            other.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        }
    }

    private Vector3 HaciaDondeVuela(Transform transformObjetoColisionado)
    {
        switch (m_DireccionVuelo)
        {
            case Enums.EnumDireccionVuelo.ARRIBA:
                return transformObjetoColisionado.transform.up;
            case Enums.EnumDireccionVuelo.IZQUIERDA:
                return -transformObjetoColisionado.transform.forward;
            case Enums.EnumDireccionVuelo.DERECHA:
                return transformObjetoColisionado.transform.forward;
        }

        return transformObjetoColisionado.transform.position;
    }
}
