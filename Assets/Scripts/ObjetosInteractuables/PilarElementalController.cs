using UnityEngine;

public class PilarElementalController : MonoBehaviour
{
    //Componentes
    private MeshRenderer m_MeshRenderer;

    [SerializeField]
    private LiquidosSO m_SOelemento;
    public LiquidosSO SOElemento { get => m_SOelemento; set => m_SOelemento = value; }

    private void Awake()
    {
        m_MeshRenderer = GetComponent<MeshRenderer>();
    }

    public void AsignarMaterialAlPilar(LiquidosSO elemento)
    {
        m_SOelemento = elemento;
        m_MeshRenderer.material = elemento.Material;
    }
}
