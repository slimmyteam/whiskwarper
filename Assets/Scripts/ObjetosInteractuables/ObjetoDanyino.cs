using UnityEngine;

public class ObjetoDanyino : MonoBehaviour
{
    [SerializeField]
    private int m_CantidadDanyo;

    private void OnTriggerStay(Collider other)
    {
        if (!enabled) return; //Para controlar los casos cuando no hace dano
        if (other.gameObject.TryGetComponent(out IDamageable damageable))
        {
            damageable.PerderVida(m_CantidadDanyo);

            //Si el player esta quieto, no detecta el OnCollisionStay y entonces no le quita danyo. Entonces le aplicamos fuerza para que se mueva brevemente y el OnCollisionStay le detecte
            if (other.gameObject.GetComponent<Rigidbody>().velocity.normalized == Vector3.zero)
            {
                other.gameObject.GetComponent<Rigidbody>().AddForce(-other.transform.forward * 2);
            }
        }
    }
}
