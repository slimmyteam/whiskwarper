using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class ObjetoCargable : MonoBehaviour, ICargable
{
    [SerializeField]
    private string m_NombreObjetoCargable;
    [SerializeField]
    private Collider m_Collider;
    [SerializeField]
    private LayerMask m_LayersPararSuelo;
    [SerializeField]
    private float m_DuracionCaida;

    private void Awake()
    {
        m_Collider = GetComponent<Collider>();
    }

    private void Start()
    {
        Caer();
    }

    public void RecogerObjeto(GameObject posicionCabeza)
    {
        StopAllCoroutines();
        transform.SetParent(posicionCabeza.transform);
        transform.position = posicionCabeza.transform.position;
        m_Collider.isTrigger = true;
    }

    public string RecuperarNombreObjetoCargable()
    {
        return m_NombreObjetoCargable;
    }

    public void SoltarObjeto(Vector3 posicionDejar)
    {
        m_Collider.isTrigger = false;
        transform.position = posicionDejar;
        Caer();
    }

    public void UsarObjeto()
    {
        Destroy(gameObject);
    }

    private void Caer()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, Mathf.Infinity, m_LayersPararSuelo))
        {
            Debug.DrawLine(transform.position, hit.point, Color.red, 5f);
            StartCoroutine(CaerObjeto(hit.point + Vector3.up * 0.5f));
        }
    }

    private IEnumerator CaerObjeto(Vector3 posicionFinal)
    {
        Vector3 posicionInicial = transform.position;
        float elapsedTime = 0f;

        while (elapsedTime < m_DuracionCaida)
        {
            float t = elapsedTime / m_DuracionCaida;
            transform.position = Vector3.Lerp(posicionInicial, posicionFinal, t);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.position = posicionFinal;
        yield return null;
    }

    public void SerArrastrado(GameObject boomerang)
    {
        transform.parent = boomerang.transform;
    }

    public void DesligarseBoomerang()
    {
        transform.parent = null;
        Caer();
    }
}
