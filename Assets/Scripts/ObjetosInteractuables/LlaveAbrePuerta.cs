using UnityEngine;

public class LlaveAbrePuerta : MonoBehaviour, IReaccionador
{
    [Header("Rotaciones que se quieren aplicar")]
    [SerializeField]
    private string m_RotacionX = null;
    [SerializeField]
    private string m_RotacionY = null;
    [SerializeField]
    private string m_RotacionZ = null;

    private float m_RotacionXFloat;
    private float m_RotacionYFloat;
    private float m_RotacionZFloat;

    public void Reaccionar()
    {
        m_RotacionXFloat = string.IsNullOrEmpty(m_RotacionX) ? gameObject.transform.rotation.x : float.Parse(m_RotacionX);
        m_RotacionYFloat = string.IsNullOrEmpty(m_RotacionY) ? gameObject.transform.rotation.y : float.Parse(m_RotacionY);
        m_RotacionZFloat = string.IsNullOrEmpty(m_RotacionZ) ? gameObject.transform.rotation.z : float.Parse(m_RotacionZ);

        gameObject.transform.localEulerAngles = new Vector3(m_RotacionXFloat, m_RotacionYFloat, m_RotacionZFloat);
    }
}
