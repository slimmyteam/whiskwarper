using UnityEngine;

public class TestingIDamageable : MonoBehaviour, IDamageable
{
    [SerializeField]
    private float m_Vida;

    public void MuerteInstantanea()
    {
    }

    public void PerderVida(float danoRecibido)
    {
        m_Vida -= danoRecibido;
    }


}
