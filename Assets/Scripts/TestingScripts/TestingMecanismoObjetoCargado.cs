using System;
using UnityEngine;

public class TestingMecanismoObjetoCargado : MonoBehaviour, IMecanismoReaccionadorObjetoCargado
{
    public void ReaccionaContenidoCubo(ScriptableObject contenidoCubo)
    {
        
    }
    public bool ReaccionaObjetoCargado(string nombreObjetoCargado)
    {
        if (nombreObjetoCargado == "cuboLava")
        {
            Destroy(gameObject);
            return true;
        }
        return false;
    }
    public void EjecutarMecanismo()
    {

    }

}
