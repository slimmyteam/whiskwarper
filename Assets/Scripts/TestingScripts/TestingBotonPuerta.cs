using System.Collections;
using UnityEngine;

public class TestingBotonPuerta : MonoBehaviour, IMecanismo, IGuardable //TODO NO ES TESTING
{
    [Header("Parametros")]
    public GameObject[] puertas;
    public float m_Duracion = 1f;
    private float m_PosicionAbierta;
    public bool abierta;

    [Header("Guardar partida")]
    [SerializeField]
    private int m_IdPuzzle;
    [SerializeField]
    private GEInt m_PuzzleResuelto;

    public void EjecutarMecanismo()
    {
        StartCoroutine(BajarPuerta());
        AvisarEstadoResuelto();
    }

    void Start()
    {
        m_PosicionAbierta =  -10;
        abierta = false;
    }

    private IEnumerator BajarPuerta()
    {
        Debug.Log("BAJAR PUERTA");
        foreach (GameObject puerta in puertas)
        {
            Vector3 posicionInicial = puerta.transform.position;
            Vector3 posicionFinal = new Vector3(puerta.transform.position.x, m_PosicionAbierta, puerta.transform.position.z);
            float elapsedTime = 0f;
            puerta.GetComponent<AudioSource>()?.Play();
            while (elapsedTime < m_Duracion)
            {
                float t = elapsedTime / m_Duracion;
                puerta.transform.position = Vector3.Lerp(posicionInicial, posicionFinal, t);
                elapsedTime += Time.deltaTime;
                yield return null;
            }

            Debug.Log("Bajar puertaaaa");
            puerta.transform.position = posicionFinal;
            abierta = true;
            puerta.SetActive(false);
        }
        yield return null;
    }

    public void AvisarEstadoResuelto()
    {
        m_PuzzleResuelto.Raise(m_IdPuzzle);
    }
}
