using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Scriptable Objects", menuName = "Scriptable Objects/Puzzles/PuzzleLuz")]
public class PuzzlesLuzSO : ScriptableObject
{
    [SerializeField]
    private int m_CantidadEspejos;
    public int CantidadEspejos { get { return m_CantidadEspejos; } set { m_CantidadEspejos = value; } }
    [SerializeField]
    private bool m_PuzzleFinalizado = false;
    public bool PuzzleFinalizado { get { return m_PuzzleFinalizado; } set { m_PuzzleFinalizado = value; } }
}
