using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Scriptable Objects", menuName = "Scriptable Objects/Puzzles/PuzzleBossRicochet")]
public class PuzzleBossRicochetSO : ScriptableObject
{
    [SerializeField]
    private bool m_PuzzleFinalizado = false;
    public bool PuzzleFinalizado { get { return m_PuzzleFinalizado; } set { m_PuzzleFinalizado = value; } }
}
