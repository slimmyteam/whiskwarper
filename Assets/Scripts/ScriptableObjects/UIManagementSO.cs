using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Scriptable Objects", menuName = "Scriptable Objects/UI Management")]
public class UIManagementSO : ScriptableObject
{
    [Header("Configuracion del sonido")]
    [Range(0f, 1f)]
    [SerializeField]
    private float m_ConfiguracionVolumenEfectos;
    [Range(0f, 1f)]
    [SerializeField]
    private float m_ConfiguracionVolumenMusica;

    public float ConfiguracionVolumenEfectos { get => m_ConfiguracionVolumenEfectos; set => m_ConfiguracionVolumenEfectos = value; }
    public float ConfiguracionVolumenMusica { get => m_ConfiguracionVolumenMusica; set => m_ConfiguracionVolumenMusica = value; }

    [Header("Slimes conseguidos")]
    [SerializeField]
    private bool m_TrofeoSlimePlata;
    [SerializeField]
    private bool m_TrofeoSlimeOro;
    [SerializeField]
    private bool m_TrofeoSlimeBronce;

    public bool TrofeoSlimePlata { get => m_TrofeoSlimePlata; set => m_TrofeoSlimePlata = value; }
    public bool TrofeoSlimeOro { get => m_TrofeoSlimeOro; set => m_TrofeoSlimeOro = value; }
    public bool TrofeoSlimeBronce { get => m_TrofeoSlimeBronce; set => m_TrofeoSlimeBronce = value; }

    [Header("Pociones desbloqueadas")]
    [SerializeField]
    private bool m_PocionesActivas;
    public bool PocionesActivas { get => m_PocionesActivas; set => m_PocionesActivas = value; }

    [Header("Trozos orbe")]
    [SerializeField]
    private int m_CantidadTrozos;
    public int CantidadTrozos { get => m_CantidadTrozos; set => m_CantidadTrozos = value; }

    [Header("Progreso")]
    [Range(0f, 100f)]
    [SerializeField]
    private float m_Progreso;
    public float Progreso { get => m_Progreso; set => m_Progreso = value; }
}
