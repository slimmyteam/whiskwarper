using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Scriptable Objects", menuName = "Scriptable Objects/Liquido")]
public class LiquidosSO : ScriptableObject
{
    [Header("Parametros de la pocion")]
    [SerializeField]
    private Enums.EnumLiquidos m_TipoLiquido;
    public Enums.EnumLiquidos TipoLiquido { get { return m_TipoLiquido; } }
    [SerializeField]
    private GameObject m_Liquido;
    public GameObject Liquido { get { return m_Liquido; } set { m_Liquido = value; } }
    [SerializeField]
    private Material m_Material;
    public Material Material { get { return m_Material; } }
}
