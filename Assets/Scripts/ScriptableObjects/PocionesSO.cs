using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Scriptable Objects", menuName = "Scriptable Objects/Pocion")]
public class PocionesSO : ScriptableObject
{
    [Header("Parametros de la pocion")]
    [SerializeField]
    private Enums.EnumPociones m_TipoPocion;
    public Enums.EnumPociones TipoPocion { get { return m_TipoPocion; } }
    [SerializeField]
    private int m_CantidadActual;
    public int CantidadActual { get { return m_CantidadActual; } set { m_CantidadActual = value; } }
    [SerializeField]
    private int m_CantidadMax;
    public int CantidadMax { get => m_CantidadMax; set => m_CantidadMax = value; }
    [SerializeField]
    private float m_PuntosEfecto;
    public float PuntosEfecto { get => m_PuntosEfecto; set => m_PuntosEfecto = value; }
    [SerializeField]
    private float m_DuracionEfecto;
    public float DuracionEfecto { get => m_DuracionEfecto; set => m_DuracionEfecto = value; }
}