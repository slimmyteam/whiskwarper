using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/Limites Camara")]
public class GELimitesCamara : GEGenerico<StructContainerScript.LimitesDeCamara> { }
