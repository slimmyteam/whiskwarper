using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/Vector3Vector3Float")]
public class GEVector3Vector3Float : GEGenerico<Vector3, Vector3, float> { }
