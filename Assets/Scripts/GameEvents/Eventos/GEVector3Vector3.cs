using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/Vector3Vector3")]
public class GEVector3Vector3 : GEGenerico<Vector3, Vector3> { }
