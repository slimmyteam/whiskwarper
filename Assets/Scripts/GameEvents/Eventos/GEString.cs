using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/String")]
public class GEString : GEGenerico<string> { }
