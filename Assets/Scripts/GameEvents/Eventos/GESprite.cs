using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/Sprite")]
public class GESprite : GEGenerico<Sprite> { }
