using UnityEngine;

public class Enums : MonoBehaviour
{
    public enum EnumEnemigos
    {
        MELEE, RANGE, BASICO, ESCUDO, ACORAZADO, VELOZ, BACKSTAB, BOSS
    }

    public enum EnumPociones
    {
        VIDA, FUERZA, VELOCIDAD, INVISIBILIDAD, NINGUNA
    }

    public enum EnumNPCS
    {
        POCIONES, HABILIDADES, ESCENARIO
    }

    public enum EnumHabilidades
    {
        ESCUDO, ESCUDOMAGICO, ARCO, BOOMERANG, CUBO, BOMBAS, MINAS 
    }

    public enum EnumPuertasLuz
    {
        BASICO, SOL, LUNA
    }

    public enum EnumLiquidos
    {
        LAVA, HIELO, AGUA, NADA
    }

    public enum EnumDireccionVuelo
    {
        ARRIBA, IZQUIERDA, DERECHA
    }

    public enum EnumDimension
    {
        IZQUIERDA, DERECHA
    }
}
