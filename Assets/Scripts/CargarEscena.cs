using UnityEngine;
using UnityEngine.SceneManagement;

public class CargarEscena : MonoBehaviour
{
    public void CambiarDeEscena(string nombreEscena)
    {
        SceneManager.LoadScene(nombreEscena);
    }
}
