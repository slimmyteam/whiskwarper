using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

[RequireComponent(typeof(ControlDeLayers))]
[RequireComponent(typeof(RotacionEnemigosEstaticos))]
[RequireComponent(typeof(BoxCollider))]
[RequireComponent (typeof(AudioSource))]
[RequireComponent(typeof(VisualEffect))]
public class EnemigoInstakillController : MonoBehaviour, IDamageable, IReactToSound, IGuardable
{
    [Header("Componentes")]
    private Animator m_Animator;
    private ControlDeLayers m_ControlDeLayers;
    private RotacionEnemigosEstaticos m_RotacionEnemigos;
    private MeshRenderer m_MeshRenderer;
    private LineRenderer m_LineRenderer;

    [Header("Parametros del Sr Escopeta")]
    [SerializeField]
    private EnemiesSO m_InstakillSO;
    [SerializeField]
    private float m_Vida;
    [SerializeField]
    private bool m_Vigilando;
    private GameObject m_PlayerEncontrado;

    [Header("Maquina de estados")]
    [SerializeField]
    private SwitchMachineStatesInstakill m_EstadoActual;
    private enum SwitchMachineStatesInstakill { NINGUNO, IDLE, VIGILAR, ATACAR, MUERTO }

    [Header("Guardar partida")]
    [SerializeField]
    private int m_IdEnemigo;
    [SerializeField]
    private GEInt m_EnemigoMuerto;

    [Header("SFX")]
    [SerializeField]
    private List<AudioClip> m_ListaAudioClips;

    [Header("VFX")]
    [SerializeField]
    private VisualEffectAsset m_PuffMuerte;

    private void Awake()
    {
        m_Animator = GetComponent<Animator>();
        m_ControlDeLayers = GetComponent<ControlDeLayers>();
        m_RotacionEnemigos = GetComponent<RotacionEnemigosEstaticos>();
        m_MeshRenderer = GetComponentInChildren<MeshRenderer>();
        m_LineRenderer = GetComponent<LineRenderer>();
    }

    void Start()
    {
        m_Vigilando = false;
        m_Vida = m_InstakillSO.VidaMax;
        InitStateEnemigoInstakill(SwitchMachineStatesInstakill.IDLE);
    }

    private void Update()
    {
        UpdateStateEnemigoInstakill();
    }

    //-------------------------------[ MAQUINA DE ESTADOS ]-------------------------------//
    #region Maquina de estados
    private void ChangeStateEnemigoInstakill(SwitchMachineStatesInstakill newState)
    {
        if (newState == m_EstadoActual || m_EstadoActual == SwitchMachineStatesInstakill.MUERTO)
            return;
        ExitStateEnemigoInstakill();
        InitStateEnemigoInstakill(newState);
    }

    private void InitStateEnemigoInstakill(SwitchMachineStatesInstakill currentState)
    {
        m_EstadoActual = currentState;
        switch (m_EstadoActual)
        {
            case SwitchMachineStatesInstakill.NINGUNO:
                break;
            case SwitchMachineStatesInstakill.IDLE:
                //m_Animator.CrossFade("EnemigoInstakillIdle");
                StartCoroutine(EsperaParaVigilar());
                break;
            case SwitchMachineStatesInstakill.VIGILAR:
                StartCoroutine(Vigilando());
                break;
            case SwitchMachineStatesInstakill.ATACAR:
                //m_Animator.CrossFade("EnemigoInstakillAtacar");
                MatarPlayer();
                break;
            case SwitchMachineStatesInstakill.MUERTO:
                //m_Animator.CrossFade("EnemigoInstakillMuerto");
                AvisarEstadoResuelto();
                StartCoroutine(Desaparecer());
                break;
            default:
                break;
        }
    }

    private void UpdateStateEnemigoInstakill()
    {
        switch (m_EstadoActual)
        {
            case SwitchMachineStatesInstakill.NINGUNO:
                break;
            case SwitchMachineStatesInstakill.IDLE:
                break;
            case SwitchMachineStatesInstakill.VIGILAR:
                break;
            case SwitchMachineStatesInstakill.ATACAR:
                break;
            case SwitchMachineStatesInstakill.MUERTO:
                break;
            default:
                break;
        }
    }

    private void ExitStateEnemigoInstakill()
    {
        switch (m_EstadoActual)
        {
            case SwitchMachineStatesInstakill.NINGUNO:
                break;
            case SwitchMachineStatesInstakill.IDLE:
                break;
            case SwitchMachineStatesInstakill.VIGILAR:
                break;
            case SwitchMachineStatesInstakill.ATACAR:
                break;
            case SwitchMachineStatesInstakill.MUERTO:
                break;
            default:
                break;
        }
    }

    #endregion

    //-------------------------------[ FUNCIONES ]-------------------------------//

    #region Vigilar y ataque

    /// <summary>
    /// Espera en Idle antes de comenzar a Vigilar.
    /// </summary>
    /// <returns></returns>
    private IEnumerator EsperaParaVigilar()
    {
        yield return new WaitForSeconds(m_InstakillSO.TiempoEsperaEnPatrullaje);
        ChangeStateEnemigoInstakill(SwitchMachineStatesInstakill.VIGILAR);
    }

    /// <summary>
    /// Detecta todos los colliders en una esfera delante del enemigo, con el radio indicado. Si detecta al player, se guarda el objeto y cambia al estado ATACAR.
    /// </summary>
    /// <returns></returns>
    private IEnumerator Vigilando()
    {
        m_Vigilando = true;
        while (m_Vigilando)
        {
            Collider[] objetivosDetectados = Physics.OverlapSphere(transform.position + transform.forward * m_InstakillSO.DesplazamientoEnElForwardDeEsferaDeteccion, m_InstakillSO.RadioEsferaDeteccion);
            foreach (Collider collider in objetivosDetectados)
            {
                if (m_ControlDeLayers.ContieneLayer(m_InstakillSO.MasksDisparo, collider.gameObject.layer))
                {
                    Vector3 direccion = collider.gameObject.transform.position - transform.position;
                    if (Physics.Raycast(transform.position, direccion, out RaycastHit hit, m_InstakillSO.DistanciaAlPlayer, m_InstakillSO.MasksEsferaAtaqueODisparo))
                    {
                        m_PlayerEncontrado = hit.collider.gameObject;
                        ChangeStateEnemigoInstakill(SwitchMachineStatesInstakill.ATACAR);
                        m_Vigilando = false;
                    }
                }
            }
            yield return null;
        }
    }

    /// <summary>
    /// Llama a la funcion MuerteInstantanea del player (IDamageable).
    /// </summary>
    private void MatarPlayer()
    {
        m_LineRenderer.positionCount = 2;
        m_LineRenderer.SetPosition(0, transform.position);
        m_LineRenderer.SetPosition(1, m_PlayerEncontrado.transform.position);
        gameObject.GetComponent<AudioSource>().loop = false;
        gameObject.GetComponent<AudioSource>().clip = m_ListaAudioClips[0]; //laser gun
        gameObject.GetComponent<AudioSource>().Play();
        m_PlayerEncontrado.GetComponent<IDamageable>().MuerteInstantanea();
    }

    #endregion

    #region Reaccion a sonido

    /// <summary>
    /// Si le llega un sonido, llama a la funcion que lo hace rotar en esa direccion.
    /// </summary>
    /// <param name="sound"></param>
    public void ReactToSound(Sound sound)
    {
        m_RotacionEnemigos.RotarHaciaSonido(sound);
    }

    #endregion

    #region Control de vida
    public void PerderVida(float danoRecibido)
    {
        return;
    }

    public void MuerteInstantanea()
    {
        this.gameObject.GetComponent<AudioSource>().loop = false;
        this.gameObject.GetComponent<AudioSource>().clip = m_ListaAudioClips[1]; //electric-shock
        this.gameObject.GetComponent<AudioSource>().Play();
        m_Vida = 0;
        ChangeStateEnemigoInstakill(SwitchMachineStatesInstakill.MUERTO);
    }


    private IEnumerator Desaparecer()
    {
        m_MeshRenderer.enabled = false;
        this.gameObject.GetComponent<VisualEffect>().visualEffectAsset = m_PuffMuerte;
        this.gameObject.GetComponent<VisualEffect>().Play();
        yield return new WaitForSeconds(0.5f);
        Destruir();    
    }

    private void Destruir()
    {
        Destroy(gameObject);
    }
    #endregion

    #region OnDrawGizmosSelected
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position + transform.forward * m_InstakillSO.DesplazamientoEnElForwardDeEsferaDeteccion, m_InstakillSO.RadioEsferaDeteccion);

    }

    #endregion

    #region Estado de partida

    public void AvisarEstadoResuelto()
    {
        m_EnemigoMuerto.Raise(m_IdEnemigo);
    }

    #endregion
}

