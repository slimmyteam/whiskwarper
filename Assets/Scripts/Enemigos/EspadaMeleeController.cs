using UnityEngine;

[RequireComponent (typeof(AudioSource))]
public class EspadaMeleeController : MonoBehaviour
{
    [SerializeField]
    private EnemigoMeleeController m_EnemigoMeleeController;
    private EnemiesSO m_EnemigoMeleeSO;
    [SerializeField]
    private AudioClip m_AudioReboteEscudo;

    private void Awake()
    {
        m_EnemigoMeleeSO = m_EnemigoMeleeController.MeleeEnemySO;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (m_EnemigoMeleeController.EstadoActual == EnemigoMeleeController.Estados.ATACAR && other.gameObject.CompareTag("Escudo"))
        {
            gameObject.GetComponent<AudioSource>().loop = false;
            gameObject.GetComponent<AudioSource>().clip = m_AudioReboteEscudo;
            gameObject.GetComponent<AudioSource>().Play();
            m_EnemigoMeleeController.RebotarEnEscudo();
        }
        else if (other.TryGetComponent(out IDamageable damageable) && other.TryGetComponent<PlayerController>(out _))
        {
            damageable.PerderVida(m_EnemigoMeleeSO.DanoAtaqueODisparo);
        }
    }
}
