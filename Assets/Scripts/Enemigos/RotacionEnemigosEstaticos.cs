using System.Collections;
using UnityEngine;

public class RotacionEnemigosEstaticos : MonoBehaviour
{
    [Header("Referencias")]
    //[SerializeField]
    //private GameObject m_ParteRotable;

    [Header("Parametros")]
    [SerializeField]
    private bool m_RotacionPeriodica;

    [Header("Rotacion Periodica")]
    [SerializeField]
    private float m_TiempoEsperaRotacion;
    [SerializeField]
    private float m_DistanciaRotacion;
    [SerializeField]
    private bool m_RotandoIzquierda;
    [SerializeField]
    private float m_TiempoLerp;
    [SerializeField]
    private float m_TiempoVolverAVigilar;
    private bool m_Rotando;
    private bool m_RotandoHaciaSonido;
    private Quaternion m_RotacionInicial;

    private void Start()
    {
        m_RotacionInicial = transform.rotation;
        m_RotandoHaciaSonido = false;

        if (m_RotacionPeriodica)
        {
            StartCoroutine(Rotar());
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    /// <summary>
    /// Calcula la rotacion final del enemigo y llama a la corrutina LerpHastaRotacionIndicada, que hace rotar al enemigo hasta esa rotacion final. Cambia
    /// el lado de izquierda a derecha y viceversa.
    /// </summary>
    /// <returns></returns>
    internal IEnumerator Rotar()
    {
        m_Rotando = true;
        while (m_Rotando)
        {
            yield return new WaitForSeconds(m_TiempoEsperaRotacion);
            Quaternion rotacionFinal = Quaternion.Euler(0, !m_RotandoIzquierda ? transform.rotation.eulerAngles.y + m_DistanciaRotacion : transform.rotation.eulerAngles.y - m_DistanciaRotacion, 0);
            yield return StartCoroutine(LerpHastaRotacionIndicada(rotacionFinal));
            m_RotandoIzquierda = !m_RotandoIzquierda;
        }

    }

    /// <summary>
    /// Hace rotar al enemigo desde la rotacion actual hasta la rotacion final a lo largo del tiempo indicado.
    /// </summary>
    /// <param name="rotacionFinal">Quaternion, indica la rotacion final deseada para el enemigo</param>
    /// <returns></returns>
    internal IEnumerator LerpHastaRotacionIndicada(Quaternion rotacionFinal)
    {
        float currentTime = 0;
        float lerpTime = m_TiempoLerp;

        while (currentTime <= lerpTime && m_Rotando)
        {
            Quaternion rotacionActual = transform.rotation;
            transform.rotation = Quaternion.Lerp(rotacionActual, rotacionFinal, currentTime / lerpTime);
            yield return null;
            currentTime += Time.deltaTime;
        }
        transform.rotation = rotacionFinal;
        yield return null;
    }

    #region Volverse a sonido

    /// <summary>
    /// Para el resto de corrutinas y rota hacia la fuente del sonido que ha recibido la funcion. Calcula la direccion del sonido y rota hacia �l.
    /// Llama a la corrutina VolverAPosicionVigilar para volver a su posicion inicial si deja de detectar sonidos.
    /// </summary>
    /// <param name="sound">Sound, sonido que el enemigo ha escuchado</param>
    internal void RotarHaciaSonido(Sound sound)
    {
        if (m_RotandoHaciaSonido)
            return;
        m_RotandoHaciaSonido = true;
        StopAllCoroutines();
        m_Rotando = false;
        
        Vector3 direccionSonido = sound.soundTransform.position - transform.position;
        Quaternion rotacionDeseada = Quaternion.LookRotation(direccionSonido);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotacionDeseada, m_TiempoLerp * Time.deltaTime);
        StartCoroutine(VolverAPosicionVigilar());
        m_RotandoHaciaSonido = false;
    }

    /// <summary>
    /// Devuelve al enemigo la rotacion original antes de girarse hacia un sonido. Si el enemigo tiene activada la rotacion periodica, llama a Rotar despues
    /// de haber vuelto a la rotacion original.
    /// </summary>
    /// <returns></returns>
    internal IEnumerator VolverAPosicionVigilar()
    {
        yield return new WaitForSeconds(m_TiempoVolverAVigilar);
        m_Rotando = true;
        yield return StartCoroutine(LerpHastaRotacionIndicada(m_RotacionInicial));
        if (m_RotacionPeriodica)
            StartCoroutine(Rotar());
    }

    #endregion
}
