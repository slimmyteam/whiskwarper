using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.VFX;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(ControlDeLayers))]
[RequireComponent (typeof(AudioSource))]
[RequireComponent(typeof(VisualEffect))]
public class EnemigoRangeController : MonoBehaviour, IDamageable, IReactToSound, IGuardable
{
    [Header("Componentes")]
    private NavMeshAgent m_Agent;
    private ControlDeLayers m_ControlDeLayers;

    [Header("Identificador partida")]
    [SerializeField]
    private int m_IdEnemigo;

    [Header("Parametros del Range Enemy")]
    [SerializeField]
    private EnemiesSO m_RangeEnemySO;
    [SerializeField]
    private float m_Vida;
    [SerializeField]
    private float m_Velocidad;
    [SerializeField]
    private bool m_Invulnerable = false;
    public bool Invulnerable { get { return m_Invulnerable; } }

    [Header("Maquina de estados")]
    [SerializeField]
    private m_Estados m_EstadoActual;
    [SerializeField]
    private m_Estados m_EstadoAnterior;
    private enum m_Estados { NINGUNO, IDLE, PATRULLA, PERSEGUIR, HUIR, BUSCANDOHUIDA, DISPARAR, HERIDO, MUERTO };

    [Header("Disparo range")]
    Vector3 m_OrigenEsferaDeteccion;
    Vector3 m_OrigenEsferaDisparo;
    private Vector3 m_PosicionDeDestino;

    [Header("Limites mapa")]
    [SerializeField]
    private float m_MapaXMin = -18;
    [SerializeField]
    private float m_MapaXMax = 10;
    [SerializeField]
    private float m_MapaZMin = -19;
    [SerializeField]
    private float m_MapaZMax = 7;

    [Header("Area")]
    private NavMeshHit m_hit;
    private int m_AreaPatrullaje;
    [SerializeField]
    private string m_NombreAreaPatrullaje;

    [Header("Control de tiempos")]
    [SerializeField]
    private float m_TiempoMostrandoDano = 0.3f;

    [Header("Referencias a objetos")]
    private Transform m_PosicionBala;
    private Transform m_PlayerTransform;

    [Header("Referencias a scripts")]
    [SerializeField]
    private PoolDeBalas m_PoolDeBalas;
    [SerializeField]
    private TocoYAviso m_TocoYAviso;

    [Header("Corutinas")]
    private Coroutine m_EsperandoEnPatrullaje;
    private Coroutine m_ReciboDano;
    private Coroutine m_Disparando;
    private Coroutine m_Huyendo;
    private Coroutine m_BuscandoHuida;
    private Coroutine m_Invulnerabilidad;

    [Header("Layers")]
    [SerializeField]
    private LayerMask m_LayerParedes; //Escenario
    [SerializeField]
    private LayerMask m_LayersDeteccionSonido; //Escenario, player

    [Header("Eventos")]
    [SerializeField]
    private GEEnumInt m_RellenarPocion;
    [SerializeField]
    private GEInt m_EnemigoMuerto;

    [Header("SFX")]
    [SerializeField]
    private AudioClip m_AudioDano;

    [Header("VFX")]
    [SerializeField]
    private VisualEffectAsset m_PuffMuerte;
    [SerializeField]
    private GameObject m_Modelo;
    [SerializeField]
    private ParticleSystem m_ParticleSystemEstrella;

    //-------------------------------[ FUNCIONES UNITY ]-------------------------------//
    private void Awake()
    {
        m_Agent = GetComponent<NavMeshAgent>();
        m_ControlDeLayers = GetComponent<ControlDeLayers>();

        m_PosicionBala = transform.GetChild(0).gameObject.transform;
        m_AreaPatrullaje = 1 << NavMesh.GetAreaFromName(m_NombreAreaPatrullaje);

        m_Vida = m_RangeEnemySO.VidaMax;
        m_Velocidad = m_RangeEnemySO.Velocidad;
        m_Agent.speed = m_Velocidad;
        m_PlayerTransform = transform;
    }

    void Start()
    {
        m_TocoYAviso.TocoPared += CambiarEstadoABuscandoHuida;
        IniciarEstado(m_Estados.IDLE);
    }

    void Update()
    {
        ActualizarEstado();
        DetectarPlayer();
    }

    private void OnDisable()
    {
        m_TocoYAviso.TocoPared -= CambiarEstadoABuscandoHuida;
        StopAllCoroutines();
    }

    //-------------------------------[ MAQUINA DE ESTADOS ]-------------------------------//
    private void CambiarEstado(m_Estados NuevoEstado)
    {
        if (NuevoEstado == m_EstadoActual) return;
        if (m_EstadoActual == m_Estados.MUERTO) return;

        m_EstadoAnterior = m_EstadoActual;
        FinalizarEstado();
        IniciarEstado(NuevoEstado);
    }

    private void IniciarEstado(m_Estados NuevoEstado)
    {
        m_EstadoActual = NuevoEstado;
        switch (m_EstadoActual)
        {
            case m_Estados.IDLE:
                m_Agent.speed = 0; //IMPORTANTE: con un agente movemos al agente, no tiene rigidbody ni collider
                m_EsperandoEnPatrullaje = StartCoroutine(EsperandoEnPatrullaje());
                break;

            case m_Estados.PATRULLA:
                m_Agent.speed = m_Velocidad;
                transform.forward = m_Agent.destination - transform.position;
                m_Agent.stoppingDistance = 0;
                m_Agent.SetDestination(m_PosicionDeDestino);
                break;

            case m_Estados.PERSEGUIR:
                m_Agent.speed = m_Velocidad;
                transform.forward = m_PlayerTransform.position - transform.position;
                m_Agent.stoppingDistance = m_RangeEnemySO.DistanciaAlPlayer;
                break;

            case m_Estados.HUIR:
                m_Agent.speed = m_Velocidad;
                transform.forward = m_Agent.destination - transform.position;
                m_Agent.stoppingDistance = 0;
                m_Huyendo = StartCoroutine(TiempoEspera(m_RangeEnemySO.TiempoHuyendo, m_Estados.IDLE));
                break;

            case m_Estados.BUSCANDOHUIDA:
                m_Agent.speed = m_Velocidad;
                m_Agent.stoppingDistance = 0;
                m_BuscandoHuida = StartCoroutine(BuscandoHuida());
                break;

            case m_Estados.DISPARAR:
                m_Agent.speed = 0;
                m_Disparando = StartCoroutine(Disparando());
                break;

            case m_Estados.HERIDO:
                m_Agent.speed = 0;
                m_ReciboDano = StartCoroutine(TiempoEspera(m_TiempoMostrandoDano, m_Estados.IDLE));
                break;

            case m_Estados.MUERTO:
                m_Agent.speed = 0;
                enabled = false;
                AvisarEstadoResuelto();
                StartCoroutine(Desaparecer());
                break;

            default:
                break;
        }
    }

    private void ActualizarEstado()
    {
        if (m_EstadoActual != m_Estados.MUERTO)
        {
            switch (m_EstadoActual)
            {
                case m_Estados.IDLE:
                    if (m_EstadoAnterior != m_Estados.HUIR && m_EstadoAnterior != m_Estados.BUSCANDOHUIDA) return;

                    // Si el estado anterior era HUIR o BUSCANDOHUIDA, cuando salimos del estado, miramos hacia el jugador
                    MirarHaciaElPlayer();
                    break;

                case m_Estados.PATRULLA:
                    if (Vector3.Distance(transform.position - Vector3.up * m_Agent.height, m_PosicionDeDestino) <= 0.3f)
                        CambiarEstado(m_Estados.IDLE);
                    break;

                case m_Estados.PERSEGUIR:
                    m_Agent.SetDestination(m_PlayerTransform.position);

                    ComprobarSiElPlayerEstaDemasiadoCerca();

                    ActivarAreaDisparo();
                    break;

                case m_Estados.DISPARAR:
                    ComprobarSiElPlayerEstaDemasiadoCerca();
                    break;

                case m_Estados.HUIR:
                    Vector3 DireccionContrariaAlPlayer = transform.position - m_PlayerTransform.position;
                    Vector3 PosicionDestino = transform.position + DireccionContrariaAlPlayer.normalized * 5;
                    m_Agent.SetDestination(PosicionDestino);
                    break;

                case m_Estados.BUSCANDOHUIDA:
                    //Si el agente ha acabado su ruta de huida
                    if (!m_Agent.hasPath)
                    {
                        CambiarEstado(m_Estados.IDLE);
                    }
                    break;
            }
        }
    }

    private void FinalizarEstado()
    {
        switch (m_EstadoActual)
        {
            case m_Estados.DISPARAR:
                PararCorutinaSiNoEsNull(m_Disparando);
                break;
            case m_Estados.IDLE:
                PararCorutinaSiNoEsNull(m_EsperandoEnPatrullaje);
                break;
            case m_Estados.HERIDO:
                PararCorutinaSiNoEsNull(m_ReciboDano);
                break;
            case m_Estados.HUIR:
                PararCorutinaSiNoEsNull(m_Huyendo);
                break;
            case m_Estados.BUSCANDOHUIDA:
                PararCorutinaSiNoEsNull(m_BuscandoHuida);
                break;
        }
    }

    //-------------------------------[ CORUTINAS ]-------------------------------//

    private IEnumerator EsperandoEnPatrullaje()
    {
        yield return new WaitForSeconds(m_RangeEnemySO.TiempoEsperaEnPatrullaje);
        while (!NavMesh.SamplePosition(new Vector3(Random.Range(m_MapaXMin, m_MapaXMax), 0, Random.Range(m_MapaZMin, m_MapaZMax)), out m_hit, Mathf.Infinity, m_AreaPatrullaje))//TODO Cambiar el Random.Range segun tama�o mapa
            yield return null;
        Debug.DrawLine(transform.position, m_hit.position, Color.red + Color.blue, 5f);
        m_PosicionDeDestino = m_hit.position;
        CambiarEstado(m_Estados.PATRULLA);
    }

    private IEnumerator Disparando()
    {
        //Esperamos unos segundos por si hace poco rato estabamos disparando, evitamos que lance dos tiros de golpe
        yield return new WaitForSeconds(2);
        while (true)
        {
            transform.forward = (m_PlayerTransform.position - transform.position).normalized;
            if (Vector3.Distance(transform.position, m_PlayerTransform.position) > m_RangeEnemySO.RadioEsferaAtaqueODisparo * 2)
            {
                CambiarEstado(m_Estados.IDLE);
                break;
            }

            Debug.DrawLine(transform.position, GetComponent<Collider>().gameObject.transform.position, Color.green, m_RangeEnemySO.TiempoEsperaAtaqueODisparo / 2);

            transform.LookAt(GetComponent<Collider>().gameObject.transform.position);

            GameObject bala = m_PoolDeBalas.PedirBala();
            Vector3 direccionBala = DisparoDesviado(transform.forward);
            bala.GetComponent<BalaController>().PonerCaracteristicasBala(m_PosicionBala.position, direccionBala, m_RangeEnemySO.DanoAtaqueODisparo, m_RangeEnemySO.VelocidadDanoAtaqueODisparo, 0, m_PoolDeBalas.GetComponent<PoolDeBalas>(), BalaController.PropietarioBalaEnum.ENEMIGO, false);
            yield return new WaitForSeconds(m_RangeEnemySO.TiempoEsperaAtaqueODisparo);
        }
    }

    private IEnumerator BuscandoHuida()
    {
        while (!NavMesh.SamplePosition(new Vector3(Random.Range(m_MapaXMin, m_MapaXMax), 0, Random.Range(m_MapaZMin, m_MapaZMax)), out m_hit, Mathf.Infinity, m_AreaPatrullaje)) //TODO Cambiar el Random.Range segun tama�o mapa
            yield return null;
        m_Agent.SetDestination(m_hit.position);
    }

    private IEnumerator TiempoEspera(float TiempoEspera, m_Estados EstadoAlQueCambiar)
    {
        yield return new WaitForSeconds(TiempoEspera);
        CambiarEstado(EstadoAlQueCambiar);
    }

    //-------------------------------[ FUNCIONES ]-------------------------------//
    #region ReceiveDamage
    private void ReceiveDamage(int damageReceived, GameObject shotOrigin)
    {
        m_Vida -= damageReceived;
        transform.forward = -shotOrigin.transform.forward;
        CambiarEstado(m_Estados.HERIDO);

        if (m_Vida <= 0)
        {
            m_Vida = 0;
            CambiarEstado(m_Estados.MUERTO);
        }
    }

    private IEnumerator Invulnerabilidad()
    {
        m_Invulnerable = true;
        yield return new WaitForSeconds(m_RangeEnemySO.TiempoInvulnerabilidad);
        m_Invulnerable = false;

        StopCoroutine(m_Invulnerabilidad);
    }
    #endregion

    private void DetectarPlayer()
    {
        m_OrigenEsferaDeteccion = transform.position + transform.forward * m_RangeEnemySO.DesplazamientoEnElForwardDeEsferaDeteccion;
        Collider[] colliders = Physics.OverlapSphere(m_OrigenEsferaDeteccion, m_RangeEnemySO.RadioEsferaDeteccion, m_RangeEnemySO.MasksEsferaDeteccion);
        if (colliders.Length > 0 && colliders[0].gameObject.TryGetComponent(out InvisibilidadController invisibilidadController) && invisibilidadController.EstaInvisible())
            return;
        if (m_EstadoActual == m_Estados.PERSEGUIR || m_EstadoActual == m_Estados.HERIDO || m_EstadoActual == m_Estados.HUIR || m_EstadoActual == m_Estados.BUSCANDOHUIDA) return;

        if (colliders.Length > 0)
        {
            m_PlayerTransform = colliders[0].gameObject.transform;

            if (Physics.Raycast(transform.position, colliders[0].gameObject.transform.position - transform.position, out RaycastHit hit, 20f, m_RangeEnemySO.MasksEsferaAtaqueODisparo))
            {
                Debug.DrawLine(transform.position, hit.point, Color.cyan, m_RangeEnemySO.TiempoEsperaAtaqueODisparo / 2);
                if (m_ControlDeLayers.ContieneLayer(m_LayerParedes, hit.transform.gameObject.layer))
                {
                    return;
                }
                if (m_EstadoActual == m_Estados.DISPARAR)
                    return;
                CambiarEstado(m_Estados.PERSEGUIR);
            }
        }
        else if (colliders.Length == 0 && m_EstadoActual == m_Estados.PERSEGUIR)
        {
            CambiarEstado(m_Estados.IDLE);
        }
    }

    private void ActivarAreaDisparo()
    {
        m_OrigenEsferaDisparo = transform.position + transform.forward * m_RangeEnemySO.DesplazamientoEnElForwardDeEsferaAtaqueODisparo;
        Collider[] colliders = Physics.OverlapSphere(m_OrigenEsferaDisparo, m_RangeEnemySO.RadioEsferaAtaqueODisparo, m_RangeEnemySO.MasksEsferaAtaqueODisparo);

        if (m_EstadoActual == m_Estados.DISPARAR) return;

        if (colliders.Length > 0)
        {
            foreach (Collider collider in colliders)
            {
                if (collider.gameObject.layer == LayerMask.NameToLayer("Player"))
                {
                    CambiarEstado(m_Estados.DISPARAR);
                    break;
                }
            }
        }
    }

    private void CambiarEstadoABuscandoHuida()
    {
        CambiarEstado(m_Estados.BUSCANDOHUIDA);
    }

    private void MirarHaciaElPlayer()
    {
        Vector3 direccionAlPlayer = m_PlayerTransform.position - transform.position;
        if(direccionAlPlayer!= Vector3.zero)
        {
            Quaternion rotacion = Quaternion.LookRotation(direccionAlPlayer);
            // Interpolamos gradualmente la rotacion
            transform.rotation = Quaternion.Lerp(transform.rotation, rotacion, m_RangeEnemySO.VelocidadRotacion * Time.deltaTime);

        }

    }

    private void ComprobarSiElPlayerEstaDemasiadoCerca()
    {
        float distanceToPlayer = Vector3.Distance(transform.position, m_PlayerTransform.position);

        if (distanceToPlayer < m_RangeEnemySO.DistanciaAlPlayer)
        {
            CambiarEstado(m_Estados.HUIR);
        }
    }

    private Vector3 DisparoDesviado(Vector3 position)
    {
        int numRandom = Random.Range(0, 2);

        /*if (numRandom == 0)
        {
            float randomX = Random.Range(-0.2f, 0.2f); //Desvío en la X
            float randomY = Random.Range(-0.1f, 0.1f); //Desvío en la Y

            position = position + transform.up * randomY + transform.right * randomX;
        }*/

        return position;
    }

    private void PararCorutinaSiNoEsNull(Coroutine corutina)
    {
        if (corutina != null)
        {
            StopCoroutine(corutina);
        }
    }
    public void ReactToSound(Sound sound)
    {
        float intensidad = sound.intensity;
        RaycastHit[] detectados;
        detectados = Physics.RaycastAll(transform.position, (sound.soundTransform.position - transform.position).normalized, Vector3.Distance(transform.position, sound.soundTransform.position), m_LayersDeteccionSonido);
        Debug.DrawLine(transform.position, sound.soundTransform.position, Color.black, 4f);
        foreach (RaycastHit hit in detectados)
        {
            if (hit.transform.TryGetComponent(out AtenuadorDeIntensidadSonora atenuador))
            {
                intensidad = atenuador.ReducirSonido(intensidad);
            }
        }

        if ((m_EstadoActual == m_Estados.IDLE || m_EstadoActual == m_Estados.PATRULLA) && intensidad >= 0)
        {
            m_PosicionDeDestino = sound.soundTransform.position;
            CambiarEstado(m_Estados.IDLE);
            CambiarEstado(m_Estados.PATRULLA);
        }
    }

    #region Perder vida
    public void PerderVida(float damageReceived)
    {
        if (m_Invulnerable) return;
        this.gameObject.GetComponent<AudioSource>().loop = false;
        this.gameObject.GetComponent<AudioSource>().clip = m_AudioDano;
        this.gameObject.GetComponent<AudioSource>().Play();
        m_ParticleSystemEstrella.Play();
        m_Vida -= damageReceived;
        CambiarEstado(m_Estados.HERIDO);

        m_Invulnerabilidad = StartCoroutine(Invulnerabilidad());

        if (m_Vida <= 0)
        {
            m_RellenarPocion.Raise(m_RangeEnemySO.TipoPocion, m_RangeEnemySO.PuntosPocion);
            Morirse();
        }
    }
    #endregion

    public void MuerteInstantanea()
    {
        m_RellenarPocion.Raise(Enums.EnumPociones.INVISIBILIDAD, m_RangeEnemySO.PuntosPocion);
        Morirse();
    }

    private IEnumerator Desaparecer()
    {
        m_Modelo.SetActive(false);
        this.gameObject.GetComponent<VisualEffect>().visualEffectAsset = m_PuffMuerte;
        this.gameObject.GetComponent<VisualEffect>().Play();
        yield return new WaitForSeconds(1.5f);
        this.gameObject.SetActive(false);
    }

    private void Morirse()
    {
        m_Vida = 0;
        CambiarEstado(m_Estados.MUERTO);
    }

    #region OnDrawGizmosSelected
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(m_OrigenEsferaDeteccion, m_RangeEnemySO.RadioEsferaDeteccion);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(m_OrigenEsferaDisparo, m_RangeEnemySO.RadioEsferaAtaqueODisparo);
    }

    #endregion

    #region Estado de partida

    public void AvisarEstadoResuelto()
    {
        m_EnemigoMuerto.Raise(m_IdEnemigo);
    }

    #endregion
}
