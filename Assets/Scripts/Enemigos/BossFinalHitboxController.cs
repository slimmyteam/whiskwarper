using UnityEngine;

[RequireComponent(typeof(ControlDeLayers))]
public class BossFinalHitboxController : MonoBehaviour
{
    [Header("Caracteristicas")]
    [SerializeField]
    private LayerMask m_LayerAtacable;
    [SerializeField]
    private float m_Dano;
    public float Dano { get => m_Dano; set => m_Dano = value; }

    //Componentes
    private ControlDeLayers m_ControlDeLayers;

    private void Awake()
    {
        m_ControlDeLayers = GetComponent<ControlDeLayers>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Nombre impacto hitbox: "+other.name);
        if(other.TryGetComponent(out IRompible rompible))
            rompible.Romperse();
        else if (!m_ControlDeLayers.ContieneLayer(m_LayerAtacable, other.gameObject.layer))
            return;
        if (other.TryGetComponent(out IDamageable damageable))
        {
            damageable.PerderVida(m_Dano);
        }
    }
}
