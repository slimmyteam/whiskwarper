using System.Collections;
using UnityEngine;
using UnityEngine.VFX;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(RellenadorPocionesBoss))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(VisualEffect))]
public class BossExplosivoController : MonoBehaviour, IDamageable
{
    [Header("SO")]
    [SerializeField]
    private BossSO m_BossSO;

    [Header("Maquina de estados")]
    [SerializeField]
    private Estados m_EstadoActual;
    public Estados EstadoActual => m_EstadoActual;
    public enum Estados { NONE, IDLE, MOVIMIENTO, ATACAR, VULNERABLE, MUERTO };

    [Header("Puntos De Movimiento")]
    [SerializeField]
    private Transform[] m_ListaDePuntosMovimiento;
    private int puntoRandom;
    private int lastPuntoRandom;

    [Header("Componentes")]
    [SerializeField]
    private Rigidbody m_Rigidbody;
    [SerializeField]
    private RellenadorPocionesBoss m_RellenadorPocionesBoss;
    [SerializeField] private BarraVidaBossController m_BarraVidaBossController;

    [Header("SpawnersDeBombas")]
    [SerializeField]
    private BombaSpawners[] m_ListaBombaSpawners;

    [Header("Variables")]
    [SerializeField]
    private float m_TiempoEsperaPasarAtaque;

    [Header("Game Events")]
    [SerializeField]
    private GameEvent m_BossMuertoEvent;

    [Header("SFX")]
    [SerializeField]
    private AudioClip m_AudioDano;

    [Header("VFX")]
    [SerializeField]
    private VisualEffectAsset m_PuffMuerte;
    [SerializeField]
    private GameObject m_Modelo;

    private void Awake()
    {
        m_BossSO.VidaActual = m_BossSO.VidaMax;
        m_Rigidbody = GetComponent<Rigidbody>();
        m_RellenadorPocionesBoss = GetComponent<RellenadorPocionesBoss>();
    }

    void Start()
    {
        CambiarEstado(Estados.IDLE);
    }

    void Update()
    {
        ActualizarEstado();
    }

    public void MuerteInstantanea()
    {

    }

    public void PerderVida(float danoRecibido)
    {
        if (EstadoActual == Estados.VULNERABLE)
        {
            this.gameObject.GetComponent<AudioSource>().loop = false;
            this.gameObject.GetComponent<AudioSource>().clip = m_AudioDano;
            this.gameObject.GetComponent<AudioSource>().Play();
            m_BossSO.VidaActual -= danoRecibido;
            m_BarraVidaBossController.PerderVida();            
        }
        if (m_BossSO.VidaActual <= 0)
            CambiarEstado(Estados.MUERTO);
    }
    private IEnumerator Desaparecer()
    {
        m_Modelo.SetActive(false);
        this.gameObject.GetComponent<VisualEffect>().visualEffectAsset = m_PuffMuerte;
        this.gameObject.GetComponent<VisualEffect>().Play();
        yield return new WaitForSeconds(1.5f);
        this.gameObject.SetActive(false);
    }


    public void VolverseVulnerable()
    {
        CambiarEstado(Estados.VULNERABLE);
    }



    //-------------------------------[ MAQUINA DE ESTADOS ]-------------------------------//
    private void CambiarEstado(Estados nuevoEstado)
    {
        if (nuevoEstado == m_EstadoActual) 
            return;
        if (m_EstadoActual == Estados.MUERTO) 
            return;

        FinalizarEstado();
        IniciarEstado(nuevoEstado);
    }

    private void IniciarEstado(Estados nuevoEstado)
    {
        m_EstadoActual = nuevoEstado;
        switch (m_EstadoActual)
        {
            case Estados.IDLE:
                StartCoroutine(EsperarEnIdle());
                break;
            case Estados.MOVIMIENTO:
                lastPuntoRandom = puntoRandom;
                puntoRandom = Random.Range(0, m_ListaDePuntosMovimiento.Length);
                if (lastPuntoRandom == puntoRandom)
                    CambiarEstado(Estados.ATACAR);
                else
                {
                    transform.position = m_ListaDePuntosMovimiento[puntoRandom].position;
                    StartCoroutine(EsperarAtaque());
                }
                break;
            case Estados.ATACAR:
                m_ListaBombaSpawners[Random.Range(0, m_ListaBombaSpawners.Length)].SpawnearBomba();
                CambiarEstado(Estados.IDLE);
                break;
            case Estados.VULNERABLE:
                StopAllCoroutines();
                StartCoroutine(EsperarEnVulnerable());
                break;
            case Estados.MUERTO:
                StopAllCoroutines();
                m_RellenadorPocionesBoss.RellenarTodasPociones();
                m_BossMuertoEvent.Raise();
                StartCoroutine(Desaparecer());
                break;
            default:
                break;
        }
    }

    private void ActualizarEstado()
    {
        if (m_EstadoActual == Estados.MUERTO) return;

        switch (m_EstadoActual)
        {
            case Estados.IDLE:
                break;
            case Estados.MOVIMIENTO:
                break;
            case Estados.ATACAR:
                break;
            case Estados.VULNERABLE:
                break;
            case Estados.MUERTO:
                break;
            default:
                break;
        }

    }

    private void FinalizarEstado()
    {
        switch (m_EstadoActual)
        {
            case Estados.IDLE:
                break;
            case Estados.MOVIMIENTO:
                m_Rigidbody.velocity = Vector3.zero;
                break;
            case Estados.ATACAR:
                break;
            case Estados.VULNERABLE:
                break;
            case Estados.MUERTO:
                break;
            default:
                break;
        }
    }

    //-------------------------------[ CORRUTINAS ]-------------------------------//
    private IEnumerator EsperarEnIdle()
    {
        yield return new WaitForSeconds(m_BossSO.TiempoEsperaMoverse);
        CambiarEstado(Estados.MOVIMIENTO);
    }

    private IEnumerator EsperarEnVulnerable()
    {
        yield return new WaitForSeconds(m_BossSO.TiempoEsperaDejarVulnerable);
        CambiarEstado(Estados.MOVIMIENTO);
    }
    private IEnumerator EsperarAtaque()
    {
        yield return new WaitForSeconds(m_TiempoEsperaPasarAtaque);
        CambiarEstado(Estados.ATACAR);
    }
}
