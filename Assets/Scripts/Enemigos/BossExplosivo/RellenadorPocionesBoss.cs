using UnityEngine;

public class RellenadorPocionesBoss : MonoBehaviour
{
    [Header("Eventos")]
    [SerializeField]
    private GEEnumInt m_RellenarPocion;

    public void RellenarTodasPociones()
    {
        m_RellenarPocion.Raise(Enums.EnumPociones.FUERZA, 999999);
        m_RellenarPocion.Raise(Enums.EnumPociones.VIDA, 999999);
        m_RellenarPocion.Raise(Enums.EnumPociones.INVISIBILIDAD, 999999);
        m_RellenarPocion.Raise(Enums.EnumPociones.VELOCIDAD, 999999);
    }
}
