using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AudioSource))]
public class BombaBossController : MonoBehaviour, ITeleportable
{
    [SerializeField]
    private float BombaDano;
    [SerializeField]
    private float m_RadioExplosion;
    public bool teletransportandose;
    private ParticleSystem m_ParticleSystem;

    private void Awake()
    {
        m_ParticleSystem = GetComponent<ParticleSystem>();
    }

    [SerializeField]
    private AudioClip m_AudioEstallar;

    public void SerTeletransportado()
    {
        throw new System.NotImplementedException();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (LayerMask.LayerToName(collision.gameObject.layer) != "Default")
            Estallar();

    }

    private void Estallar()
    {
        if (teletransportandose)
            return;
        StartCoroutine(Estalla());
    }

    private IEnumerator Estalla()
    {
        this.gameObject.GetComponent<AudioSource>().loop = false;
        this.gameObject.GetComponent<AudioSource>().clip = m_AudioEstallar;
        this.gameObject.GetComponent<AudioSource>().Play();
        m_ParticleSystem.Play();
        yield return new WaitForSeconds(0.5f);
        Collider[] objetivosAlcanzados = Physics.OverlapSphere(transform.position, m_RadioExplosion);
        foreach (Collider collider in objetivosAlcanzados)
        {
            if (collider.gameObject.TryGetComponent(out BossExplosivoController boss))
                boss.VolverseVulnerable();
            if (collider.gameObject.TryGetComponent(out IDamageable damageable))
                damageable.PerderVida(BombaDano);
        }
        Destroy(gameObject);
    }
}
