using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableObjects", menuName = "Scriptable Objects/Boss")]
public class BossSO : ScriptableObject
{
    [Header("Variables")]
    [SerializeField]
    private float m_VidaMax;
    [SerializeField]
    private float m_VidaActual;
    [SerializeField]
    private float m_Velocidad;
    [SerializeField]
    private float m_TiempoEsperaMoverse;
    [SerializeField]
    private float m_TiempoEsperaDejarVulnerable;

    public float VidaMax { get => m_VidaMax; set => m_VidaMax = value; }
    public float VidaActual { get => m_VidaActual; set => m_VidaActual = value; }
    public float Velocidad { get => m_Velocidad; set => m_Velocidad = value; }
    public float TiempoEsperaMoverse { get => m_TiempoEsperaMoverse; set => m_TiempoEsperaMoverse = value; }
    public float TiempoEsperaDejarVulnerable { get => m_TiempoEsperaDejarVulnerable; set => m_TiempoEsperaDejarVulnerable = value; }
}
