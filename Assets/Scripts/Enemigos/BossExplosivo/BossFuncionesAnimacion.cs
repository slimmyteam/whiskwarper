using UnityEngine;

[RequireComponent(typeof(Animator))]
public class BossAnimacionesController : MonoBehaviour
{
    [Header("Referencias")]
    private BossFinalAtaques m_BossFinalAtaques;

    private void Awake()
    {
        m_BossFinalAtaques = GetComponentInParent<BossFinalAtaques>();
    }

    public void FinAnimacionGolpeFisico()
    {
        m_BossFinalAtaques.VolverAlCentro();
    }

    public void FinAnimacionExplosion()
    {
        m_BossFinalAtaques.TerminarExplosion();
    }
}
