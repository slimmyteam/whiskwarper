using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.VFX;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(ControlDeLayers))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(VisualEffect))]
public class EnemigoMeleeController : MonoBehaviour, IDamageable, IReactToSound, IGuardable
{
    [Header("Componentes")]
    private NavMeshAgent m_Agent;
    [SerializeField]
    private Animator m_Animator;
    private ControlDeLayers m_ControlDeLayers;

    [Header("Identificador partida")]
    [SerializeField]
    private int m_IdEnemigo;

    [Header("Parametros del Melee Enemy")]
    [SerializeField]
    private EnemiesSO m_MeleeEnemySO;
    public EnemiesSO MeleeEnemySO
    {
        get { return m_MeleeEnemySO; }
        set { m_MeleeEnemySO = value; }
    }
    [SerializeField]
    private float m_Vida;
    [SerializeField]
    private float m_Velocidad;
    [SerializeField]
    private bool m_Invulnerable = false;
    public bool Invulnerable { get { return m_Invulnerable; } }

    [Header("Maquina de estados")]
    [SerializeField]
    private Estados m_EstadoActual;
    public Estados EstadoActual => m_EstadoActual;
    [SerializeField]
    private Estados m_EstadoGuardado;
    public enum Estados { NINGUNO, IDLE, PATRULLA, PERSEGUIR, ATACAR, DEFENSA, HERIDO, MUERTO };

    [Header("Ataque Melee")]
    Vector3 m_OrigenEsferaDeteccion;
    Vector3 m_OrigenEsferaAtaque;
    private Vector3 m_PosicionDeDestino;
    [SerializeField]
    private LayerMask m_AtaqueMask;

    [Header("Limites mapa")]
    [SerializeField]
    private float m_MapaXMin = -18;
    [SerializeField]
    private float m_MapaXMax = 10;
    [SerializeField]
    private float m_MapaZMin = -19;
    [SerializeField]
    private float m_MapaZMax = 7;

    [Header("Control de tiempos")]
    [SerializeField]
    private float m_TiempoMostrandoDano = 0.3f;

    [Header("Referencias a objetos")]
    private Transform m_PlayerTransform;
    [SerializeField]
    private GameObject m_HitboxMelee;

    [Header("Corutinas")]
    private Coroutine m_EsperandoEnPatrullaje;
    private Coroutine m_ReciboDano;
    private Coroutine m_Atacando;
    private Coroutine m_Escudo;
    private Coroutine m_Invulnerabilidad;

    [Header("Eventos")]
    [SerializeField]
    private GEEnumInt m_RellenarPocion;
    [SerializeField]
    private GEInt m_EnemigoMuerto;

    [Header("Layers")]
    [SerializeField]
    private LayerMask m_LayersDeteccionSonido;

    [Header("Area")]
    [SerializeField]
    private string m_NombreAreaPatrullaje;

    [Header("SFX")]
    [SerializeField]
    private AudioClip m_AudioDano;

    [Header("VFX")]
    [SerializeField]
    private VisualEffectAsset m_PuffMuerte;
    [SerializeField]
    private GameObject m_Modelo;
    [SerializeField]
    private ParticleSystem m_ParticleSystemEstrella;

    //-------------------------------[ FUNCIONES UNITY ]-------------------------------//
    private void Awake()
    {
        m_Agent = GetComponent<NavMeshAgent>();
        m_Animator = GetComponentInChildren<Animator>();
        m_ControlDeLayers = GetComponent<ControlDeLayers>();

        m_Vida = m_MeleeEnemySO.VidaMax;
        m_Velocidad = m_MeleeEnemySO.Velocidad;
        m_Agent.speed = m_Velocidad;
    }

    private void Start()
    {
        IniciarEstado(Estados.IDLE);
    }

    private void Update()
    {
        ActualizarEstado();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    //-------------------------------[ MAQUINA DE ESTADOS ]-------------------------------//
    private void CambiarEstado(Estados NuevoEstado)
    {
        if (NuevoEstado == m_EstadoActual) return;
        if (m_EstadoActual == Estados.MUERTO) return;

        //m_EstadoGuardado = m_EstadoActual;
        ControlAtaqueYDefensa(NuevoEstado);
        FinalizarEstado();
        IniciarEstado(NuevoEstado);
    }

    private void IniciarEstado(Estados nuevoEstado)
    {
        m_EstadoActual = nuevoEstado;
        switch (m_EstadoActual)
        {
            case Estados.IDLE:
                m_Agent.speed = 0;
                m_Animator.CrossFade("MeleeIdle", 0.3f);
                m_Agent.stoppingDistance = 0;
                m_EsperandoEnPatrullaje = StartCoroutine(EsperandoEnPatrullaje());
                break;

            case Estados.PATRULLA:
                m_Agent.speed = m_Velocidad;
                m_Animator.CrossFade("MeleeWalk", 0.3f);
                m_Agent.stoppingDistance = 0;
                m_Agent.SetDestination(m_PosicionDeDestino);
                break;

            case Estados.PERSEGUIR:
                m_Agent.speed = m_Velocidad;
                transform.forward = m_PlayerTransform.position - transform.position;
                m_Animator.CrossFade("MeleeRun", 0.3f);
                m_Agent.stoppingDistance = m_MeleeEnemySO.DistanciaAlPlayer;
                break;

            case Estados.ATACAR:
                m_Agent.speed = 0;
                m_Agent.stoppingDistance = m_MeleeEnemySO.DistanciaAlPlayer;
                m_Atacando = StartCoroutine(EsperaAtaque());
                break;

            case Estados.DEFENSA:
                m_Agent.speed = 0;
                m_Agent.stoppingDistance = m_MeleeEnemySO.DistanciaAlPlayer;
                m_Escudo = StartCoroutine(EsperaEscudo());
                break;

            case Estados.HERIDO:
                m_Agent.speed = 0;
                m_ReciboDano = StartCoroutine(ReciboDano());
                break;

            case Estados.MUERTO:
                m_Agent.speed = 0;
                enabled = false;
                AvisarEstadoResuelto();
                StartCoroutine(Desaparecer());
                break;

            default:
                break;
        }
    }

    private void ActualizarEstado()
    {
        if (m_EstadoActual == Estados.MUERTO) return;

        switch (m_EstadoActual)
        {
            case Estados.IDLE:
                DetectarPlayer();
                break;
            case Estados.PATRULLA:
                //Debug.Log("Enemigo: " + gameObject.name + " distancia al punto de patrulla x: " + Mathf.Abs(m_PosicionDeDestino.x - transform.position.x) + " distancia al punto de patrulla z: " + Mathf.Abs(m_PosicionDeDestino.z - transform.position.z));
                //if (Vector3.Distance(transform.position - Vector3.up * m_Agent.height, m_PosicionDeDestino) <= 0.3f)
                if(Mathf.Abs(m_PosicionDeDestino.x - transform.position.x) <= 0.3 && Mathf.Abs(m_PosicionDeDestino.z - transform.position.z) <= 0.3)
                    CambiarEstado(Estados.IDLE);
                DetectarPlayer();
                break;
            case Estados.PERSEGUIR:
                m_Agent.SetDestination(m_PlayerTransform.position);
                Atacar();
                break;
        }

    }

    private void FinalizarEstado()
    {
        switch (m_EstadoActual)
        {
            case Estados.ATACAR:
                PararCorutina(m_Atacando);
                break;

            case Estados.DEFENSA:
                PararCorutina(m_Escudo);
                break;

            case Estados.IDLE:
                PararCorutina(m_EsperandoEnPatrullaje);
                break;

            case Estados.HERIDO:
                PararCorutina(m_ReciboDano);
                break;
        }
    }

    //-------------------------------[ CORUTINAS ]-------------------------------//

    private IEnumerator EsperandoEnPatrullaje()
    {
        yield return new WaitForSeconds(m_MeleeEnemySO.TiempoEsperaEnPatrullaje);

        NavMeshHit hit;
        int area = 1 << NavMesh.GetAreaFromName(m_NombreAreaPatrullaje); //Area en la que queremos que el SamplePosition nos encuentre un punto
        while (!NavMesh.SamplePosition(new Vector3(Random.Range(m_MapaXMin, m_MapaXMax), 0, Random.Range(m_MapaZMin, m_MapaZMax)), out hit, Mathf.Infinity, area))
        {
            yield return null;
        }
        Debug.DrawLine(transform.position, hit.position, Color.red + Color.blue, 5f);
        m_PosicionDeDestino = hit.position;
        CambiarEstado(Estados.PATRULLA);
    }

    private IEnumerator ReciboDano()
    {
        yield return new WaitForSeconds(m_TiempoMostrandoDano);
        CambiarEstado(Estados.IDLE);
    }

    private IEnumerator EsperaAtaque()
    {
        if (EsEstadoAtaqueODefensa())
            yield return new WaitForSeconds(1);
        m_Animator.CrossFade("MeleeAtaque", 0.3f);
        yield return new WaitForSeconds(m_MeleeEnemySO.TiempoEsperaAtaqueODisparo);
        CambiarEstado(Estados.IDLE);
    }

    private IEnumerator EsperaEscudo()
    {
        if (EsEstadoAtaqueODefensa()) yield return new WaitForSeconds(1);

        m_HitboxMelee.SetActive(true);
        m_Animator.CrossFade("MeleeDefensa", 0.3f);

        yield return new WaitForSeconds(m_MeleeEnemySO.TiempoEsperaDefensa);
        m_HitboxMelee.SetActive(false);
        CambiarEstado(Estados.IDLE);
    }

    private IEnumerator Invulnerabilidad()
    {
        m_Invulnerable = true;
        yield return new WaitForSeconds(m_MeleeEnemySO.TiempoInvulnerabilidad);
        m_Invulnerable = false;

        StopCoroutine(m_Invulnerabilidad);
    }

    //-------------------------------[ FUNCIONES ]-------------------------------//

    #region Detectar player
    private void DetectarPlayer()
    {
        m_OrigenEsferaDeteccion = transform.position + transform.forward * m_MeleeEnemySO.DesplazamientoEnElForwardDeEsferaDeteccion;
        Collider[] colliders = Physics.OverlapSphere(m_OrigenEsferaDeteccion, m_MeleeEnemySO.RadioEsferaDeteccion, m_MeleeEnemySO.MasksEsferaDeteccion);
        if (colliders.Length > 0 && colliders[0].gameObject.TryGetComponent(out InvisibilidadController invisibilidadController) && invisibilidadController.EstaInvisible())
            return;
        if (colliders.Length > 0 && m_EstadoActual != Estados.PERSEGUIR && m_EstadoActual != Estados.HERIDO && m_EstadoActual != Estados.ATACAR && m_EstadoActual != Estados.DEFENSA)
        {
            m_PlayerTransform = colliders[0].gameObject.transform;

            RaycastHit hit;
            if (Physics.Raycast(transform.position, colliders[0].gameObject.transform.position - transform.position, out hit, 20f, m_MeleeEnemySO.MasksEsferaAtaqueODisparo))
            {
                Debug.DrawLine(transform.position, hit.point, Color.cyan, m_MeleeEnemySO.TiempoEsperaAtaqueODisparo / 2);
                if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Muro")) return;
                else CambiarEstado(Estados.PERSEGUIR);
            }

        }
        else if (colliders.Length == 0 && m_EstadoActual == Estados.PERSEGUIR)
        {
            CambiarEstado(Estados.IDLE);
        }
    }

    #endregion

    #region Atacar
    private void Atacar()
    {
        m_OrigenEsferaAtaque = transform.position + transform.forward * m_MeleeEnemySO.DesplazamientoEnElForwardDeEsferaAtaqueODisparo;
        Collider[] colliders = Physics.OverlapSphere(m_OrigenEsferaAtaque, m_MeleeEnemySO.RadioEsferaAtaqueODisparo, m_MeleeEnemySO.MasksEsferaAtaqueODisparo);

        if (colliders.Length > 0 && m_EstadoActual != Estados.ATACAR && m_EstadoActual != Estados.DEFENSA)
        {
            foreach (Collider collider in colliders)
            {
                if (collider.gameObject.layer == LayerMask.NameToLayer("Player"))
                {
                    ComprobarTipoDeEnemigo();
                }
            }
        }
    }

    public void RebotarEnEscudo()
    {
        m_Animator.CrossFade("MeleeReboteEscudo", 0.3f);
    }

    #endregion

    #region Control ataque y defensa

    /* CONTROL ATAQUE Y DEFENSA 
     * Guardamos el estado para que cuando cambiemos de estado de ataque/defensa a ataque/defensa 
     * no saque el escudo/espada inmediatamente, sino que respete un tiempo respecto al anterior estado
     * */
    private void ControlAtaqueYDefensa(Estados NuevoEstado)
    {
        if (NuevoEstado != Estados.IDLE) return;

        if ((m_EstadoActual == Estados.DEFENSA || m_EstadoActual == Estados.ATACAR) && NuevoEstado == Estados.IDLE)
        {
            m_EstadoGuardado = m_EstadoActual;
        }
    }

    /* CONTROL ATAQUE Y DEFENSA */
    private bool EsEstadoAtaqueODefensa()
    {
        if (m_EstadoGuardado != Estados.ATACAR && m_EstadoGuardado != Estados.DEFENSA)
        {
            return false;
        }
        return true;
    }
    #endregion

    #region Estado del enemigo
    private void ComprobarTipoDeEnemigo()
    {
        switch (m_MeleeEnemySO.TipoEnemigo)
        {
            case Enums.EnumEnemigos.BASICO:
            case Enums.EnumEnemigos.MELEE:
            case Enums.EnumEnemigos.VELOZ:
            case Enums.EnumEnemigos.ACORAZADO:
                CambiarEstado(Estados.ATACAR);
                break;

            case Enums.EnumEnemigos.ESCUDO:
                int numRandom = Random.Range(0, 2);

                if (numRandom == 0)
                {
                    CambiarEstado(Estados.DEFENSA);
                    return;
                }

                CambiarEstado(Estados.ATACAR);
                break;
        }
    }

    #region Perder vida
    public void PerderVida(float damageReceived)
    {
        if (m_Invulnerable) return;
        this.gameObject.GetComponent<AudioSource>().loop = false;
        this.gameObject.GetComponent<AudioSource>().clip = m_AudioDano;
        this.gameObject.GetComponent<AudioSource>().Play();
        m_ParticleSystemEstrella.Play();
        m_Vida -= damageReceived;
        CambiarEstado(Estados.HERIDO);

        m_Invulnerabilidad = StartCoroutine(Invulnerabilidad());

        if (m_Vida <= 0)
        {
            print("Hace raise de rellenar poción");
            m_RellenarPocion.Raise(m_MeleeEnemySO.TipoPocion, m_MeleeEnemySO.PuntosPocion);
            Morirse();
        }
    }

    private IEnumerator Desaparecer()
    {
        m_Modelo.SetActive(false);
        this.gameObject.GetComponent<VisualEffect>().visualEffectAsset = m_PuffMuerte;
        this.gameObject.GetComponent<VisualEffect>().Play();
        yield return new WaitForSeconds(1.5f);
        this.gameObject.SetActive(false);
    }
    #endregion

    private void PararCorutina(Coroutine CorutinaAParar)
    {
        if (CorutinaAParar != null)
        {
            StopCoroutine(CorutinaAParar);
        }
    }
    #endregion

    public void ReactToSound(Sound sound)
    {
        float intensidad = sound.intensity;
        RaycastHit[] detectados;
        detectados = Physics.RaycastAll(transform.position, (sound.soundTransform.position - transform.position).normalized, Vector3.Distance(transform.position, sound.soundTransform.position), m_LayersDeteccionSonido);
        Debug.DrawLine(transform.position, sound.soundTransform.position, Color.black, 4f);
        foreach (RaycastHit hit in detectados)
        {
            if (hit.transform.TryGetComponent(out AtenuadorDeIntensidadSonora atenuador))
            {
                intensidad = atenuador.ReducirSonido(intensidad);
            }
        }

        if ((m_EstadoActual == Estados.IDLE || m_EstadoActual == Estados.PATRULLA) && intensidad >= 0)
        {
            m_PosicionDeDestino = sound.soundTransform.position;
            CambiarEstado(Estados.NINGUNO);
            CambiarEstado(Estados.PATRULLA);
        }
    }

    public void MuerteInstantanea()
    {
        m_RellenarPocion.Raise(Enums.EnumPociones.INVISIBILIDAD, m_MeleeEnemySO.PuntosPocion);
        Morirse();
    }

    private void Morirse()
    {
        m_Vida = 0;
        CambiarEstado(Estados.MUERTO);
    }

    #region Dibujo de las esferas
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(m_OrigenEsferaDeteccion, m_MeleeEnemySO.RadioEsferaDeteccion);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(m_OrigenEsferaAtaque, m_MeleeEnemySO.RadioEsferaAtaqueODisparo);
    }
    #endregion

    #region Estado de partida

    public void AvisarEstadoResuelto()
    {
        m_EnemigoMuerto.Raise(m_IdEnemigo);
    }

    #endregion
}
