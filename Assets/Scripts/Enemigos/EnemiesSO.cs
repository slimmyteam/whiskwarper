using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableObjects", menuName = "Scriptable Objects/Enemy")]
public class EnemiesSO : ScriptableObject
{
    [Header("Parametros del enemigo")]
    [SerializeField]
    private Enums.EnumEnemigos m_TipoEnemigo;
    [SerializeField]
    private Enums.EnumPociones m_TipoPocion;
    [SerializeField]
    private int m_VidaMax = 50;
    [SerializeField]
    private float m_Velocidad = 5;
    [SerializeField]
    private float m_VelocidadRotacion = 2;
    [SerializeField]
    private MeshRenderer m_Renderer;
    [SerializeField]
    private int m_TiempoInvulnerabilidad = 3;

    public Enums.EnumEnemigos TipoEnemigo { get { return m_TipoEnemigo; } }
    public Enums.EnumPociones TipoPocion { get { return m_TipoPocion; } }
    public int VidaMax { get { return m_VidaMax; } }
    public float Velocidad { get { return m_Velocidad; } }
    public float VelocidadRotacion { get { return m_VelocidadRotacion; } }
    public MeshRenderer Renderer { get { return m_Renderer; } }
    public int TiempoInvulnerabilidad { get { return m_TiempoInvulnerabilidad; } }

    [Header("Area de deteccion")]
    [SerializeField]
    float m_DesplazamientoEnElForwardDeEsferaDeteccion = 0.88f; //Desplazamiento de la sphere en el forward
    [SerializeField]
    float m_RadioEsferaDeteccion = 5;
    [SerializeField]
    private LayerMask m_MasksEsferaDeteccion; //Player

    public float DesplazamientoEnElForwardDeEsferaDeteccion { get { return m_DesplazamientoEnElForwardDeEsferaDeteccion; } }
    public float RadioEsferaDeteccion { get { return m_RadioEsferaDeteccion; } }
    public LayerMask MasksEsferaDeteccion { get { return m_MasksEsferaDeteccion; } }

    [Header("Area de ataque/disparo")]
    [SerializeField]
    float m_DesplazamientoEnElForwardDeEsferaAtaqueODisparo = 6; //Desplazamiento de la sphere en el forward
    [SerializeField]
    float m_RadioEsferaAtaqueODisparo = 1;
    [SerializeField]
    private float m_DistanciaAlPlayer = 1;
    [SerializeField]
    private float m_DanoAtaqueODisparo = 5;
    [SerializeField]
    private int m_VelocidadDanoAtaqueODisparo = 3;
    [SerializeField]
    private float m_TiempoEsperaAtaqueODisparo = 1;
    [SerializeField]
    private float m_TiempoHuyendo = 2;
    [SerializeField]
    private float m_TiempoEsperaDefensa = 3;
    [SerializeField]
    private LayerMask m_MasksEsferaAtaqueODisparo; //Player y Escenario
    [SerializeField]
    private LayerMask m_MasksDisparo;

    public float DesplazamientoEnElForwardDeEsferaAtaqueODisparo { get { return m_DesplazamientoEnElForwardDeEsferaAtaqueODisparo; } }
    public float RadioEsferaAtaqueODisparo { get { return m_RadioEsferaAtaqueODisparo; } }
    public float DistanciaAlPlayer { get { return m_DistanciaAlPlayer; } }
    public float DanoAtaqueODisparo { get { return m_DanoAtaqueODisparo; } }
    public int VelocidadDanoAtaqueODisparo { get { return m_VelocidadDanoAtaqueODisparo; } }
    public float TiempoEsperaAtaqueODisparo { get { return m_TiempoEsperaAtaqueODisparo; } }
    public float TiempoHuyendo { get { return m_TiempoHuyendo; } }
    public float TiempoEsperaDefensa { get { return m_TiempoEsperaDefensa; } }

    public LayerMask MasksEsferaAtaqueODisparo { get { return m_MasksEsferaAtaqueODisparo; } }
    public LayerMask MasksDisparo { get { return m_MasksDisparo; } }

    [Header("Patrullaje")]
    [SerializeField]
    private string m_AreaPatrullaje;
    [SerializeField]
    private float m_TiempoEsperaEnPatrullaje = 2;

    public string AreaPatrullaje { get { return m_AreaPatrullaje; } }
    public float TiempoEsperaEnPatrullaje { get { return m_TiempoEsperaEnPatrullaje; } }

    [Header("Pociones")]
    [SerializeField]
    private int m_PuntosPocion = 5;

    public int PuntosPocion { get { return m_PuntosPocion; } }
}



