using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BarraVidaBossController : MonoBehaviour
{
    [Header("Parmetros Barra Vida")]    
    private float m_lastHp;
    [SerializeField]
    private float m_VidaActual;
    [SerializeField]
    private float m_lerpTime;

    [Header("Scriptable objects")]
    [SerializeField]
    private BossSO m_SOBoss;

    [Header("Referencias a objetos")]
    [SerializeField]
    private Image m_fill;
    [SerializeField]
    private Slider m_slider;

    void Start()
    {
        m_VidaActual = m_SOBoss.VidaActual;
        m_slider.value = m_SOBoss.VidaActual / m_SOBoss.VidaMax;
    }

    //----------------------------------[ CORUTINAS }----------------------------------//

    private IEnumerator ActualizarVida()
    {
        float currentTime = 0f;
        while (currentTime <= m_lerpTime)
        {
            if (m_SOBoss.VidaActual == 0)
            {
                m_fill.enabled = false;
            }
            m_slider.value = Mathf.Lerp(m_lastHp / m_SOBoss.VidaMax, m_SOBoss.VidaActual / m_SOBoss.VidaMax, currentTime / m_lerpTime);
            yield return null;
            currentTime += Time.deltaTime;
        }
    }

    //----------------------------------[ FUNCIONES }----------------------------------//

    public void PerderVida()
    {
        StopAllCoroutines();
        m_lastHp = m_VidaActual;
        m_VidaActual = m_SOBoss.VidaActual;
        StartCoroutine(ActualizarVida());
    }
}
