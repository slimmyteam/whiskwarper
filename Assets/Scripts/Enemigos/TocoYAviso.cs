using System;
using UnityEngine;

public class TocoYAviso : MonoBehaviour
{
    [Header("Delegados")]
    public Action TocoPared;
    [SerializeField]
    private LayerMask m_LayersConLasQueChoco;

    private void OnTriggerEnter(Collider other)
    {
        //Esto comprueba si la layer del objeto con el que hemos chocado esta entre las 'm_LayersConLasQueChoco'
        if (m_LayersConLasQueChoco == (m_LayersConLasQueChoco | (1 << other.gameObject.layer)))
        {
            TocoPared?.Invoke();
        }
    }
}
