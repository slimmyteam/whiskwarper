using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(VisualEffect))]
public class EnemigoTeletransportableController : MonoBehaviour, IDamageable, IGuardable
{
    [Header("Parametros")]
    [SerializeField]
    private EnemiesSO m_EnemigoSO;
    [SerializeField]
    private float m_Vida;
    bool m_Invulnerable = false;
    public bool Invulnerable { get { return m_Invulnerable; } }
    [SerializeField]
    private bool m_Invulnerabilidad;
    private Color m_ColorOriginal = new (200, 73, 255, 0);

    [Header("Identificador partida")]
    [SerializeField]
    private int m_IdEnemigo;

    [Header("Variables de Huida")]
    [SerializeField]
    private float m_TiempoDeEsperaEntreHuidas;
    [SerializeField]
    private bool m_PuedoHuir;

    [Header("Maquina de estados")]
    [SerializeField]
    private m_Estados m_EstadoActual;
    private enum m_Estados { NINGUNO, IDLE, HUIR, HERIDO, MUERTO };

    [Header("Lista de Puntos de teletransporte")]
    [SerializeField]
    private List<Transform> m_ListaPuntosTeletransporte;

    [Header("Eventos")]
    [SerializeField]
    private GEEnumInt m_RellenarPocion;

    [Header("SFX")]
    [SerializeField]
    private AudioClip m_AudioDano;

    [Header("VFX")]
    [SerializeField]
    private VisualEffectAsset m_PuffMuerte;
    [SerializeField]
    private GameObject m_Modelo;
    [SerializeField]
    private GEInt m_EnemigoMuerto;

    [Header("Referencias")]
    [SerializeField]
    private GameObject m_ReaccionadoQueActivar;

    //Componentes
    private Animator m_Animator;

    void Start()
    {
        IniciarEstado(m_Estados.IDLE);
        m_PuedoHuir = true;
    }

    private void Awake()
    {
        m_Animator = GetComponentInChildren<Animator>();
    }


    void Update()
    {
        ActualizarEstado();
    }

    #region Funciones IDamageable
    public void MuerteInstantanea()
    {
        m_RellenarPocion.Raise(Enums.EnumEnemigos.BACKSTAB, m_EnemigoSO.PuntosPocion);
        Morirse();
    }

    public void PerderVida(float danoRecibido)
    {
        if (m_Invulnerable) return;
        this.gameObject.GetComponent<AudioSource>().loop = false;
        this.gameObject.GetComponent<AudioSource>().clip = m_AudioDano;
        this.gameObject.GetComponent<AudioSource>().Play();
        m_Vida -= danoRecibido;
        CambiarEstado(m_Estados.HERIDO);

        if (m_Vida <= 0)
        {
            m_RellenarPocion.Raise(m_EnemigoSO.TipoPocion, m_EnemigoSO.PuntosPocion);
            Morirse();
        }
    }

    private IEnumerator Desaparecer()
    {
        m_Modelo.SetActive(false);
        this.gameObject.GetComponent<VisualEffect>().visualEffectAsset = m_PuffMuerte;
        this.gameObject.GetComponent<VisualEffect>().Play();
        yield return new WaitForSeconds(1.5f);
        this.gameObject.SetActive(false);
    }
    #endregion

    #region Maquina de Estados
    private void CambiarEstado(m_Estados NuevoEstado)
    {
        if (NuevoEstado == m_EstadoActual) return;
        if (m_EstadoActual == m_Estados.MUERTO) return;

        FinalizarEstado();
        IniciarEstado(NuevoEstado);
    }

    private void IniciarEstado(m_Estados NuevoEstado)
    {
        m_EstadoActual = NuevoEstado;
        switch (m_EstadoActual)
        {
            case m_Estados.IDLE:
                m_Animator.CrossFade("idle" , 0.3f);
                break;
            case m_Estados.HERIDO:
                StartCoroutine(Invulnerabilidad());
                break;
            case m_Estados.HUIR:
                m_Animator.CrossFade("surprise", 0.3f);
                transform.position = m_ListaPuntosTeletransporte[0].position;
                PasarSiguientePunto();
                StartCoroutine(VolverseDespistado());
                break;
            case m_Estados.MUERTO:
                if(m_ReaccionadoQueActivar != null)
                    m_ReaccionadoQueActivar.GetComponent<IReaccionador>().Reaccionar();
                //gameObject.SetActive(false);
                StartCoroutine(Desaparecer());
                break;
            default: break;
        }
    }

    private void ActualizarEstado()
    {
        if (m_EstadoActual != m_Estados.MUERTO)
        {
            switch (m_EstadoActual)
            {
                case m_Estados.IDLE:
                    Collider[] colliders = Physics.OverlapSphere(transform.position, m_EnemigoSO.RadioEsferaDeteccion, m_EnemigoSO.MasksEsferaDeteccion);
                    if(colliders.Length > 0 && m_PuedoHuir)
                    {
                        CambiarEstado(m_Estados.HUIR);
                    }
                    break;
                case m_Estados.HERIDO:
                    break;
                case m_Estados.HUIR:
                    break;
                case m_Estados.MUERTO:
                    break;
                default: break;
            }
        }
    }

    private void FinalizarEstado()
    {
        switch (m_EstadoActual)
        {
            case m_Estados.IDLE:
                break;
            case m_Estados.HERIDO:
                break;
            case m_Estados.HUIR:
                break;
            case m_Estados.MUERTO:
                break;
            default: break;
        }
    }
    #endregion

    private void PasarSiguientePunto()
    {
        m_ListaPuntosTeletransporte.Add(m_ListaPuntosTeletransporte[0]);
        m_ListaPuntosTeletransporte.RemoveAt(0);
    }

    private IEnumerator VolverseDespistado()
    {
        m_PuedoHuir = false;
        yield return new WaitForSeconds(m_TiempoDeEsperaEntreHuidas);
        m_PuedoHuir = true;
        CambiarEstado(m_Estados.IDLE);
    }
    private void Morirse()
    {
        m_Vida = 0;
        CambiarEstado(m_Estados.MUERTO);
    }

    private IEnumerator Invulnerabilidad()
    {
        m_Animator.CrossFade("surprised", 0.3f);
        yield return new WaitForSeconds(0.5f);
        m_Invulnerable = true;
        yield return new WaitForSeconds(m_EnemigoSO.TiempoInvulnerabilidad);
        m_Invulnerable = false;
        CambiarEstado(m_Estados.IDLE);
        StopCoroutine(Invulnerabilidad());
    }

    public void AvisarEstadoResuelto()
    {
        m_EnemigoMuerto.Raise(m_IdEnemigo);
    }

    #region OnDrawGizmosSelected
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, m_EnemigoSO.RadioEsferaDeteccion);
    }

    #endregion
}
