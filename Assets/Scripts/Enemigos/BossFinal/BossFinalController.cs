using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.VFX;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(BossFinalAtaques))]
[RequireComponent(typeof(RellenadorPocionesBoss))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(VisualEffect))]
[RequireComponent(typeof(ParticleSystem))]
public class BossFinalController : MonoBehaviour, IDamageable
{

    [Header("Caracteristicas del boss")]
    [SerializeField]
    private BossSO m_BossSO;
    [SerializeField]
    private BossFinalSO m_BossFinalSO;
    [SerializeField]
    private bool m_BossIzquierdo;
    public enum DebilidadBossFinal { AGUA, LAVA }
    [SerializeField]
    private DebilidadBossFinal m_DebilidadActual;
    public DebilidadBossFinal DebilidadActual { get => m_DebilidadActual; set => m_DebilidadActual = value; }
    private RigidbodyConstraints m_RigidbodyConstraintsOriginales;

    [Header("Maquina de estados")]
    [SerializeField]
    private SwitchMachineStatesBossFinal m_EstadoActual;
    public SwitchMachineStatesBossFinal EstadoActual { get => m_EstadoActual; }
    public enum SwitchMachineStatesBossFinal { NINGUNO, IDLE, MOVIMIENTO, ATACAR, CARGANDOEXPLOSION, EXPLOSION, VULNERABLE, MUERTO, INACTIVO }

    [Header("Estado del boss")]
    private bool m_Rotando;

    [Header("Referencias")]
    private GameObject m_Player;
    [SerializeField]
    private GameObject m_OtroBoss;
    private BossFinalController m_OtroBossController;
    [SerializeField]
    private GameObject m_PuntoDebil;
    [SerializeField]
    private Material m_MaterialPuntoDebilAgua;
    [SerializeField]
    private Material m_MaterialPuntoDebilLava;
    [SerializeField]
    private BarraVidaBossController m_BarraVidaBossController;

    [Header("Game Events")]
    [SerializeField]
    private GameEvent m_BatallaFinalizada;
    [SerializeField]
    private GameEvent m_BossEntraVulnerable;
    [SerializeField]
    private GameEvent m_BossSaleVulnerable;

    [Header("SFX")]
    [SerializeField]
    private AudioClip m_AudioDano;

    [Header("VFX")]
    [SerializeField]
    private VisualEffectAsset m_PuffMuerte;
    [SerializeField]
    private GameObject m_Modelo;

    //[Header("Componentes")]
    private Animator m_Animator;
    private Rigidbody m_Rigidbody;
    private CapsuleCollider m_CapsuleCollider;
    private BossFinalAtaques m_Ataques;
    private RellenadorPocionesBoss m_RellenadorPocionesBoss;
    private ParticleSystem m_ParticleSystem;

    private void Awake()
    {
        m_Animator = GetComponentInChildren<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_CapsuleCollider = GetComponent<CapsuleCollider>();
        m_Ataques = GetComponent<BossFinalAtaques>();
        m_OtroBossController = m_OtroBoss.GetComponent<BossFinalController>();
        m_RellenadorPocionesBoss = GetComponent<RellenadorPocionesBoss>();
        m_ParticleSystem = GetComponent<ParticleSystem>();
        m_RigidbodyConstraintsOriginales = m_Rigidbody.constraints;
    }

    private void Start()
    {
        m_BossSO.VidaActual = m_BossSO.VidaMax;
        m_BarraVidaBossController.PerderVida();
        m_Player = GameObject.Find("Player");
        InitStateBoss(SwitchMachineStatesBossFinal.IDLE);
    }

    private void Update()
    {
        UpdateStateBoss();
    }

    //-------------------------------[ MAQUINA DE ESTADOS ]-------------------------------//
    #region Maquina de estados
    public void ChangeStateBoss(SwitchMachineStatesBossFinal newState)
    {
        if (newState == m_EstadoActual || m_EstadoActual == SwitchMachineStatesBossFinal.MUERTO)
            return;
        ExitStateBoss();
        InitStateBoss(newState);
    }

    private void InitStateBoss(SwitchMachineStatesBossFinal currentState)
    {
        m_EstadoActual = currentState;
        switch (m_EstadoActual)
        {
            case SwitchMachineStatesBossFinal.IDLE:
                m_Animator.CrossFade("BossFinalIdle", 0.5f);
                StartCoroutine(EsperaCambioEstado(m_BossSO.TiempoEsperaMoverse, SwitchMachineStatesBossFinal.MOVIMIENTO));
                break;
            case SwitchMachineStatesBossFinal.MOVIMIENTO:
                m_Animator.CrossFade("BossFinalIdle", 0.5f);
                StartCoroutine(EsperaCambioEstado(m_BossFinalSO.tiempoEsperaAtaque, SwitchMachineStatesBossFinal.ATACAR));
                break;
            case SwitchMachineStatesBossFinal.ATACAR:                
                ElegirAtaque();
                break;
            case SwitchMachineStatesBossFinal.CARGANDOEXPLOSION:
                m_ParticleSystem.Play();
                StartCoroutine(CargarExplosion());
                break;
            case SwitchMachineStatesBossFinal.EXPLOSION:
                Explosion();
                break;
            case SwitchMachineStatesBossFinal.VULNERABLE:
                StopAllCoroutines();
                m_Ataques.PararAtaques();
                StartCoroutine(EstarDebilucho());
                CaractersiticasRigidbodyColliderTumbado();
                m_BossEntraVulnerable.Raise();
                break;
            case SwitchMachineStatesBossFinal.MUERTO:
                m_Animator.CrossFade("BossFinalMorido", 0.5f);
                CaractersiticasRigidbodyColliderTumbado();
                m_RellenadorPocionesBoss.RellenarTodasPociones();
                StartCoroutine(Desaparecer());
                break;
            case SwitchMachineStatesBossFinal.INACTIVO:
                break;
            default:
                break;
        }
    }

    private void UpdateStateBoss()
    {
        switch (m_EstadoActual)
        {
            case SwitchMachineStatesBossFinal.IDLE:
                ComprobarActivo();
                break;
            case SwitchMachineStatesBossFinal.MOVIMIENTO:
                ComprobarActivo();
                MirarEnDireccionPlayer();
                break;
            case SwitchMachineStatesBossFinal.ATACAR:
                if (!m_Rotando)
                    MirarEnDireccionPlayer();
                break;
            case SwitchMachineStatesBossFinal.CARGANDOEXPLOSION:
                break;
            case SwitchMachineStatesBossFinal.EXPLOSION:
                break;
            case SwitchMachineStatesBossFinal.VULNERABLE:
                break;
            case SwitchMachineStatesBossFinal.MUERTO:
                break;
            case SwitchMachineStatesBossFinal.INACTIVO:
                SalirInactividad();
                break;
            default:
                break;
        }
    }

    private void ExitStateBoss()
    {
        switch (m_EstadoActual)
        {
            case SwitchMachineStatesBossFinal.IDLE:
                break;
            case SwitchMachineStatesBossFinal.MOVIMIENTO:
                break;
            case SwitchMachineStatesBossFinal.ATACAR:
                m_Rotando = false;
                break;
            case SwitchMachineStatesBossFinal.CARGANDOEXPLOSION:
                m_ParticleSystem.Stop();
                break;
            case SwitchMachineStatesBossFinal.EXPLOSION:
                break;
            case SwitchMachineStatesBossFinal.VULNERABLE:
                m_BossSaleVulnerable?.Raise();
                RestaurarRigidbodyCollider();
                break;
            case SwitchMachineStatesBossFinal.MUERTO:
                break;
            case SwitchMachineStatesBossFinal.INACTIVO:
                break;
            default:
                break;
        }
    }

    #endregion

    #region Control de tiempo

    private IEnumerator EsperaCambioEstado(float tiempo, SwitchMachineStatesBossFinal estado)
    {
        yield return new WaitForSeconds(tiempo);
        if (m_EstadoActual != SwitchMachineStatesBossFinal.INACTIVO)
            ChangeStateBoss(estado);
    }
    public void TerminarAtaqueYVolverAMovimiento()
    {
        StartCoroutine(EsperaCambioEstado(m_BossFinalSO.cooldownAtaque, SwitchMachineStatesBossFinal.MOVIMIENTO));
    }
    public void TerminarExplosionYVolverAMovimiento()
    {
        StartCoroutine(EsperaCambioEstado(m_BossFinalSO.cooldownExplosion, SwitchMachineStatesBossFinal.MOVIMIENTO));
    }

    #endregion

    #region Movimiento y ataques

    /// <summary>
    /// Rota al boss colocando su transform.forward en direccion al player.
    /// </summary>
    private void MirarEnDireccionPlayer()
    {
        Vector3 direccion = (m_Player.transform.position - transform.position).normalized;
        if (Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit))
        {
            Vector3 direccionProyectada = Vector3.ProjectOnPlane(direccion, hit.normal);
            Rotar(direccionProyectada);
            m_OtroBossController.Rotar(direccionProyectada);
        }
    }

    /// <summary>
    /// Coloca el forward del boss en la direccion indicada.
    /// </summary>
    /// <param name="direccion">Vector3, direccion deseada para el forward</param>
    public void Rotar(Vector3 direccion)
    {
        transform.forward = direccion;
    }

    /// <summary>
    /// Elige el siguiente ataque del boss de manera aleatoria, basandose en los porcentajes indicados para cada ataque en BossFinalSO.
    /// </summary>
    private void ElegirAtaque()
    {
        int eleccion = Random.Range(0, 101);
        if (eleccion <= m_BossFinalSO.porcentajeAtaqueBarridoVertical)
        {
            int eleccionBarrido = Random.Range(0, 2);

            switch (eleccionBarrido)
            {
                case 0:
                    BarridoVertical();
                    m_OtroBossController.BarridoVertical();
                    break;
                case 1:
                    BarridoVerticalInverso();
                    m_OtroBossController.BarridoVerticalInverso();
                    break;
            }

        }
        else if (eleccion <= m_BossFinalSO.porcentajeAtaqueBarridoVertical + m_BossFinalSO.porcentajeAtaqueBarridoHorizontal)
        {
            BarridoHorizontal();
            m_OtroBossController.BarridoHorizontal();
        }
        else if (eleccion <= m_BossFinalSO.porcentajeAtaqueBarridoVertical + m_BossFinalSO.porcentajeAtaqueBarridoHorizontal + m_BossFinalSO.porcentajeAtaqueGolpeFisico)
        {
            GolpeFisico();
        }
        else
        {
            ChangeStateBoss(SwitchMachineStatesBossFinal.CARGANDOEXPLOSION);
        }
    }

    public void BarridoVertical()
    {
        //m_Animator.CrossFade("BossAtaque1");
        m_Ataques.AtaqueBarridoVertical(m_BossFinalSO.danoProyectiles, m_BossFinalSO.velocidadProyectiles, m_BossFinalSO.proyectilesPorRafaga, m_BossFinalSO.rafagasPorAtaque, 1);
    }

    public void BarridoVerticalInverso()
    {
        //m_Animator.CrossFade("BossAtaque2");
        m_Ataques.AtaqueBarridoVertical(m_BossFinalSO.danoProyectiles, m_BossFinalSO.velocidadProyectiles, m_BossFinalSO.proyectilesPorRafaga, m_BossFinalSO.rafagasPorAtaque, -1);
    }

    public void BarridoHorizontal()
    {
        m_Rotando = true;
        m_Ataques.AtaqueBarridoHorizontal(m_BossFinalSO.danoProyectiles, m_BossFinalSO.velocidadProyectiles, m_BossFinalSO.proyectilesPorRafaga);
    }

    public void GolpeFisico()
    {
        m_Ataques.GolpeFisico(m_BossFinalSO.danoFisico, m_BossSO.Velocidad, m_Player.transform);
    }

    private IEnumerator CargarExplosion()
    {
        yield return new WaitForSeconds(m_BossFinalSO.tiempoEsperaExplosion);
        ChangeStateBoss(SwitchMachineStatesBossFinal.EXPLOSION);
    }

    private void Explosion()
    {
        m_Ataques.Explosion(m_BossFinalSO.danoExplosion);
    }

    #endregion

    #region Gestion Boss Activo

    /// <summary>
    /// Comprueba si el player se encuentra en el mismo mundo que el boss. Si no, el boss cambia al estado INACTIVO.
    /// </summary>
    private void ComprobarActivo()
    {
        if ((m_BossIzquierdo && m_Player.transform.position.x > 0) || (!m_BossIzquierdo && m_Player.transform.position.x < 0))
            ChangeStateBoss(SwitchMachineStatesBossFinal.INACTIVO);
    }

    /// <summary>
    /// Comprueba si el player se encuentra en el mismo mundo que el boss. Si es asi, cambia al estado MOVIMIENTO.
    /// </summary>
    private void SalirInactividad()
    {
        if (m_OtroBossController.EstadoActual == SwitchMachineStatesBossFinal.CARGANDOEXPLOSION || m_OtroBossController.EstadoActual == SwitchMachineStatesBossFinal.EXPLOSION)
            return;
        if ((m_BossIzquierdo && m_Player.transform.position.x < 0) || (!m_BossIzquierdo && m_Player.transform.position.x > 0))
            ChangeStateBoss(SwitchMachineStatesBossFinal.MOVIMIENTO);
    }

    #endregion

    #region Debilidad y vulnerable

    /// <summary>
    /// Cambia la debilidad del punto debil a la indicada.
    /// </summary>
    /// <param name="debilidad">DebilidadBossFinal(enum), indica la nueva debilidad</param>
    public void CambioDebilidad(DebilidadBossFinal debilidad)
    {
        m_DebilidadActual = debilidad;
        m_PuntoDebil.GetComponent<MeshRenderer>().material = m_DebilidadActual == DebilidadBossFinal.AGUA ? m_MaterialPuntoDebilAgua : m_MaterialPuntoDebilLava;
    }

    /// <summary>
    /// Comprueba si el ataque que ha recibido coincide con el tipo de debilidad que tiene el boss. Si coincide, cambia al estado VULNERABLE.
    /// No puede hacerse vulnerable si esta cargando o lanzando la explosion.
    /// </summary>
    /// <param name="ataque"></param>
    public void ComprobarAtaqueCanon(DebilidadBossFinal ataque)
    {
        if (m_EstadoActual == SwitchMachineStatesBossFinal.CARGANDOEXPLOSION || m_EstadoActual == SwitchMachineStatesBossFinal.EXPLOSION || m_DebilidadActual != ataque)
            return;

        ChangeStateBoss(SwitchMachineStatesBossFinal.VULNERABLE);
        m_OtroBossController.ChangeStateBoss(SwitchMachineStatesBossFinal.VULNERABLE);
    }

    /// <summary>
    /// Ejecuta la animacion de estado vulnerable. Despues del tiempo de vulnerabilidad, abandona el estado y realiza la animacion de levantarse. 
    /// Despues del cooldown indicado, vuelve al estado MOVIMIENTO.
    /// </summary>
    /// <returns></returns>
    private IEnumerator EstarDebilucho()
    {
        m_Animator.CrossFade("BossFinalVulnerable", 0.5f);
        yield return new WaitForSeconds(m_BossSO.TiempoEsperaDejarVulnerable);
        if(m_EstadoActual != SwitchMachineStatesBossFinal.MUERTO)
        {
            m_Animator.CrossFade("BossFinalLevantarse", 0.5f);
            ChangeStateBoss(SwitchMachineStatesBossFinal.NINGUNO);
            yield return new WaitForSeconds(m_BossFinalSO.cooldownExplosion);
            ChangeStateBoss(SwitchMachineStatesBossFinal.MOVIMIENTO);
        }
    }

    private void CaractersiticasRigidbodyColliderTumbado()
    {
        m_Rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        m_CapsuleCollider.direction = 2;
        m_CapsuleCollider.center = new Vector3(0, -0.5f, 0);
    }

    private void RestaurarRigidbodyCollider()
    {
        m_Rigidbody.constraints = m_RigidbodyConstraintsOriginales;
        m_CapsuleCollider.direction = 1;
        m_CapsuleCollider.center = Vector3.zero;
    }

    #endregion        

    #region Damageable

    public void PerderVida(float danoRecibido)
    {
        if (m_EstadoActual != SwitchMachineStatesBossFinal.VULNERABLE)
            return;
        this.gameObject.GetComponent<AudioSource>().loop = false;
        this.gameObject.GetComponent<AudioSource>().clip = m_AudioDano;
        this.gameObject.GetComponent<AudioSource>().Play();
        m_BossSO.VidaActual -= danoRecibido;
        m_BarraVidaBossController.PerderVida();
        if (m_BossSO.VidaActual <= 0)
        {
            ChangeStateBoss(SwitchMachineStatesBossFinal.MUERTO);
            m_OtroBossController.ChangeStateBoss(SwitchMachineStatesBossFinal.MUERTO);
            m_BatallaFinalizada.Raise();
        }
    }

    private IEnumerator Desaparecer()
    {
        m_Modelo.SetActive(false);
        this.gameObject.GetComponent<VisualEffect>().visualEffectAsset = m_PuffMuerte;
        this.gameObject.GetComponent<VisualEffect>().Play();
        yield return new WaitForSeconds(3);
        //this.gameObject.SetActive(false);
        SceneManager.LoadScene("Creditos");
    }

    public void MuerteInstantanea()
    {

    }

    #endregion
}
