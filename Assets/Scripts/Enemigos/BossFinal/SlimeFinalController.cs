using System.Collections;
using UnityEngine;

public class SlimeFinalController : MonoBehaviour
{
    [SerializeField]
    private GameObject m_BossFinal;
    [SerializeField]
    private PoolDeBalas m_PoolDeBalas;
    [SerializeField]
    private float m_DanoBala;
    [SerializeField]
    private float m_VelocidadBala;
    [SerializeField]
    private float m_TiempoEsperaDisparos;
    private Coroutine m_CorrutinaDisparo;
    private bool m_SeguirDisparando;


    public void ComenzarDisparo()
    {
        m_SeguirDisparando = true;
        m_CorrutinaDisparo = StartCoroutine(DisparandoBoss());
    }

    public void PararDisparo()
    {
        m_SeguirDisparando = false;
        if (m_CorrutinaDisparo != null)
            StopCoroutine(m_CorrutinaDisparo);
    }

    private IEnumerator DisparandoBoss()
    {
        while (m_SeguirDisparando)
        {
            GameObject bala = m_PoolDeBalas.PedirBala();
            Vector3 direccionBala = (m_BossFinal.transform.position - transform.position).normalized;
            bala.GetComponent<BalaController>().PonerCaracteristicasBala(transform.position + transform.forward * 1.5f, direccionBala, m_DanoBala, m_VelocidadBala, 0, m_PoolDeBalas.GetComponent<PoolDeBalas>(), BalaController.PropietarioBalaEnum.PLAYER, false);
            yield return new WaitForSeconds(m_TiempoEsperaDisparos);
        }
        yield return null;
    }
}
