using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableObjects", menuName = "Scriptable Objects/BossFinal")]
public class BossFinalSO : ScriptableObject
{
    [Header("Caracteristicas")]
    public float danoProyectiles;
    public float danoFisico;
    public float danoExplosion;
    public float rangoExplosion;
    public float velocidadProyectiles;
    [Min(1)] public int proyectilesPorRafaga;
    [Min(1)] public int rafagasPorAtaque;

    [Header("Tiempos")]
    public float tiempoEsperaAtaque; // Espera entre MOVIMIENTO y ATAQUE
    public float tiempoEsperaExplosion; // Espera entre CARGANDOEXPLOSION y EXPLOSION
    public float cooldownAtaque; // Espera despues de ATAQUE para volver a MOVIMIENTO
    public float cooldownExplosion; // Espera despues de EXPLOSION para volver a MOVIMIENTO

    [Header("Eleccion de ataque")] //Deben sumar 100
    [Range(0, 100)]
    public int porcentajeAtaqueBarridoVertical;
    [Range(0, 100)]
    public int porcentajeAtaqueBarridoHorizontal;
    [Range(0, 100)]
    public int porcentajeAtaqueGolpeFisico;
    [Range(0, 100)]
    public int porcentajeAtaqueExplosion;
}
