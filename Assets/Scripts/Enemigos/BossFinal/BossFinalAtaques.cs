using System.Collections;
using UnityEngine;

public class BossFinalAtaques : MonoBehaviour
{
    [Header("Referencias")]
    [SerializeField]
    private GameObject m_PoolBalas;
    [SerializeField]
    private Transform m_PosicionAtaque;
    [SerializeField]
    private BossFinalHitboxController m_Hitbox;
    private Vector3 m_PosicionOriginalBoss;

    [Header("Caracteristicas ataques")]
    [SerializeField]
    private float m_DiferenciaAlturaEntreRafagas;
    [SerializeField]
    private float m_DistanciaEntreBalas;
    [SerializeField]
    private float m_DuracionRotacionCompleta;
    private float m_VelocidadMovimiento;

    [Header("Componentes")]
    private BossFinalController m_BossFinalController;
    private Rigidbody m_Rigidbody;
    private Animator m_Animator;

    private void Awake()
    {
        m_BossFinalController = GetComponent<BossFinalController>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Animator = GetComponentInChildren<Animator>();

        m_PosicionOriginalBoss = transform.position;
    }

    #region Barridos verticales

    public void AtaqueBarridoVertical(float dano, float velocidad, int proyectilesPorRafaga, int rafagasPorAtaque, int direccionBarrido)
    {
        StartCoroutine(CrearRafagas(dano, velocidad, proyectilesPorRafaga, rafagasPorAtaque, direccionBarrido));
    }

    /// <summary>
    /// Lanza tantas rafagas de ataques como se indican, con el numero de proyectiles por rafaga indicado. Lanza los ataques en direccion forward de arriba a abajo o viceversa.
    /// Al finalizar, llama a TerminarAtaque.
    /// </summary>
    /// <param name="dano">float, Dano que hace cada proyectil</param>
    /// <param name="velocidad">float, Velocidad a la que se mueven los proyectiles</param>
    /// <param name="proyectilesPorRafaga">int, Numero de proyectiles que contiene cada rafaga</param>
    /// <param name="rafagasPorAtaque">int, Numero de rafagas que contiene el ataque</param>
    /// <param name="direccionBarrido">int, 1 o -1, Indica la direccion del ataque (arriba a abajo o viceversa)</param>
    /// <returns></returns>
    private IEnumerator CrearRafagas(float dano, float velocidad, int proyectilesPorRafaga, int rafagasPorAtaque, int direccionBarrido)
    {
        Vector3 direccion = transform.forward + transform.up * -direccionBarrido;

        for (int i = 0; i < rafagasPorAtaque; i++)
        {
            LanzarRafaga(dano, velocidad, proyectilesPorRafaga, direccion);
            direccion += direccionBarrido * m_DiferenciaAlturaEntreRafagas * Vector3.up;
            yield return new WaitForSeconds(0.5f);
        }

        TerminarAtaque();
    }

    /// <summary>
    /// Recoge el numero de proyectiles indicado de la pool de proyectiles. Lanza cada proyectil en la direccion indicada sumandole una desviacion cada vez mayor. 
    /// La desviacion va alternando entre izquierda y derecha.
    /// </summary>
    /// <param name="dano">float, Dano del proyectil</param>
    /// <param name="velocidad">float, Velocidad a la que se mueve el proyectil</param>
    /// <param name="proyectilesPorRafaga">int, Numero de proyectiles que tiene la rafaga</param>
    /// <param name="direccion">Vector3, direccion a la que ira el proyectil central de la rafaga</param>
    private void LanzarRafaga(float dano, float velocidad, int proyectilesPorRafaga, Vector3 direccion)
    {

        GameObject[] proyectiles = RecuperarProyectiles(proyectilesPorRafaga);

        float multiplicador = 0;
        int multiplicadorIzquierdaDerecha = -1;
        foreach (GameObject proyectil in proyectiles)
        {
            direccion += multiplicador * multiplicadorIzquierdaDerecha * transform.right;
            LanzarProyectil(dano, velocidad, proyectil, direccion);
            multiplicador += m_DistanciaEntreBalas;
            multiplicadorIzquierdaDerecha *= -1;
        }
    }

    #endregion

    #region Barrido horizontal

    public void AtaqueBarridoHorizontal(float dano, float velocidad, int proyectilesPorRafaga)
    {
        //m_Animator.CrossFade("BossAtaque3");
        StartCoroutine(CrearRafagaHorizontal(dano, velocidad, proyectilesPorRafaga));
    }

    /// <summary>
    /// Crea una rafaga horizontal de proyectiles. Recupera los proyectiles indicados de la pool de proyectiles. Posteriormente, llama a la corrutina VueltaCompleta para que el
    /// boss rote y lanza los proyectiles en direccion forward cada 0.5 segundos. Al terminar, llama a la funcion TerminarAtaque.
    /// </summary>
    /// <param name="dano">float, Dano del proyectil</param>
    /// <param name="velocidad">float, Velocidad de movimiento del proyectil</param>
    /// <param name="proyectilesPorRafaga">int, Numero de proyectiles de la rafaga</param>
    /// <returns>float, 0.5f segundos de espera entre cada proyectil disparado</returns>
    private IEnumerator CrearRafagaHorizontal(float dano, float velocidad, int proyectilesPorRafaga)
    {
        GameObject[] proyectiles = RecuperarProyectiles(proyectilesPorRafaga);

        StartCoroutine(VueltaCompleta());
        for (int i = 0; i < proyectilesPorRafaga; i++)
        {
            Vector3 direccion = transform.forward;
            LanzarProyectil(dano, velocidad, proyectiles[i], direccion);
            yield return new WaitForSeconds(0.5f);
        }

        TerminarAtaque();
    }
      
    /// <summary>
    /// Hace girar al boss 360 grados en el tiempo indicado.
    /// </summary>
    /// <returns></returns>
    private IEnumerator VueltaCompleta()
    {
        Vector3 rotacionOriginal = transform.eulerAngles;
        float rotacionFinal = transform.eulerAngles.y + 360;
        float tiempoRotacion = 0;

        while (tiempoRotacion < m_DuracionRotacionCompleta)
        {
            tiempoRotacion += Time.deltaTime;
            float lerpRotacion = Mathf.Lerp(rotacionOriginal.y, rotacionFinal, tiempoRotacion / m_DuracionRotacionCompleta) % 360;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, lerpRotacion, transform.eulerAngles.z);
            yield return null;
        }
    }

    #endregion

    #region Golpe fisico

    /// <summary>
    /// Guarda la velocidad de movimiento actual y la indica a la hitbox el dano del ataque. Llama a Moverse para desplazar al boss hacia el punto indicado y comienza la corrutina
    /// MovimientoGolpeFisico.
    /// </summary>
    /// <param name="dano">float, Dano del ataque</param>
    /// <param name="velocidad">float, Velocidad de desplazamiento del boss</param>
    /// <param name="playerTransform">Transform del player</param>
    public void GolpeFisico(float dano, float velocidad, Transform playerTransform)
    {
        m_VelocidadMovimiento = velocidad;
        m_Hitbox.Dano = dano;
        Moverse(playerTransform.position);
        StartCoroutine(MovimientoGolpeFisico());
    }

    /// <summary>
    /// Llama y espera a la corrutina EsperaCaminar. Finalmente, hace que el boss realice la animacion de ataque.
    /// </summary>
    /// <returns></returns>
    private IEnumerator MovimientoGolpeFisico()
    {
        yield return StartCoroutine(EsperaCaminar());
        m_Animator.CrossFade("BossFinalGolpeFisico", 0.5f);
    }

    /// <summary>
    /// Espera 1 segundo antes de frenar al boss para que se quede quieto.
    /// </summary>
    /// <returns></returns>
    private IEnumerator EsperaCaminar()
    {
        yield return new WaitForSeconds(1f);
        m_Rigidbody.velocity = Vector3.zero;
    }

    /// <summary>
    /// Devuelve al boss a su posicion original mediante la funcion Moverse. Llama a la corrutina EsperaVueltaAlCentro para comprobar cuando llega a la posicion indicada.
    /// </summary>
    public void VolverAlCentro()
    {
        Moverse(m_PosicionOriginalBoss);
        StartCoroutine(EsperaVueltaAlCentro());
    }

    /// <summary>
    /// Cuando el boss llega cerca de la posicion original, lo para dejando su velocidad a cero. Finalmente, llama a TerminarAtaque.
    /// </summary>
    /// <returns></returns>
    private IEnumerator EsperaVueltaAlCentro()
    {
        while ((m_PosicionOriginalBoss.x - transform.position.x) > 0.5f || (m_PosicionOriginalBoss.z - transform.position.z) > 0.5f)
        {            
            Debug.Log("BOSS B: "+m_Rigidbody.velocity);
            yield return null;
        }
        m_Rigidbody.velocity = Vector3.zero;
        TerminarAtaque();
    }

    #endregion

    #region Explosion

    /// <summary>
    /// Aplica el dano a la hitbox y llama a la animacion de explosion.
    /// </summary>
    /// <param name="dano"></param>
    public void Explosion(float dano)
    {
        Debug.Log("Le pongo a hitbox: " + dano);
        m_Hitbox.Dano = dano;
        Debug.Log("Dano hitbox: " + m_Hitbox.Dano);
        m_Animator.CrossFade("BossFinalExplosion", 0.5f);
    }

    public void TerminarExplosion()
    {
        m_BossFinalController.TerminarExplosionYVolverAMovimiento();
    }

    #endregion

    #region Utilidades

    /// <summary>
    /// Avisa al script Controller de que el ataque se ha finalizado.
    /// </summary>
    private void TerminarAtaque()
    {
        m_BossFinalController.TerminarAtaqueYVolverAMovimiento();
    }

    /// <summary>
    /// Recupera el numero de proyectiles indicado de la pool de proyectiles.
    /// </summary>
    /// <param name="cantidad">int, Cantidad deseada de proyectiles</param>
    /// <returns>GameObject[], Proyectiles recuperados</returns>
    private GameObject[] RecuperarProyectiles(int cantidad)
    {
        GameObject[] proyectiles = new GameObject[cantidad];
        for (int i = 0; i < cantidad; i++)
        {
            proyectiles[i] = m_PoolBalas.GetComponent<PoolDeBalas>().PedirBala();
        }
        return proyectiles;
    }

    /// <summary>
    /// Lanza el proyectil indicado en la direccion indicada, con los parametros indicados.
    /// </summary>
    /// <param name="dano">float, Dano del proyectil</param>
    /// <param name="velocidad">float, Velocidad de movimiento del proyectil</param>
    /// <param name="proyectil">GameObject, Proyectil que se desea lanzar</param>
    /// <param name="direccion">Vector3, Direccion en la que se desea lanzar el proyectil</param>
    private void LanzarProyectil(float dano, float velocidad, GameObject proyectil, Vector3 direccion)
    {
        proyectil.SetActive(true);
        proyectil.GetComponent<BalaController>().PonerCaracteristicasBala(m_PosicionAtaque.position, direccion, dano, velocidad, 0, m_PoolBalas.GetComponent<PoolDeBalas>(), BalaController.PropietarioBalaEnum.ENEMIGO, false);
    }

    /// <summary>
    /// Mueve al boss hasta la posicion indicada, dandole velocidad en la direccion necesaria.
    /// </summary>
    /// <param name="posicionFinal">Vector3, Posicion donde se desea llegar con el desplazamiento</param>
    private void Moverse(Vector3 posicionFinal)
    {
        Vector3 direccion = (posicionFinal - transform.position).normalized;
        m_Rigidbody.velocity = direccion * m_VelocidadMovimiento;
    }

    public void PararAtaques()
    {
        StopAllCoroutines();
    }

    #endregion
}
