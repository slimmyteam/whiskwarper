using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class ContenedorSlimeFinales : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> m_ListaSlimes;
    private void Start()
    {
        if (GameManager.Instance.ObtenerNumeroSlimes() < 3)
        {
            foreach(GameObject go in m_ListaSlimes)
                go.SetActive(false);
        }
            
    }

    public void ActivarSlimes()
    {
        foreach (GameObject go in m_ListaSlimes)
            go.SetActive(true);
    }
}
