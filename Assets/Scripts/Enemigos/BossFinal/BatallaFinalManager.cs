using System.Collections;
using UnityEngine;

public class BatallaFinalManager : BattleManager
{
    [Header("Referencias")]
    [SerializeField]
    private BossFinalController m_BossVersionA;
    [SerializeField]
    private BossFinalController m_BossVersionB;

    [Header("Caracteristicas")]
    [SerializeField]
    private float m_TiempoCambioDebilidad;
    [SerializeField]
    private bool m_BatallaActiva;


    public void EmpezarBatalla()
    {
        m_BatallaActiva = true;        
        StartCoroutine(CambioDebilidad());
    }

    public void FinalizarBatalla()
    {
        //TODO avisar de que termine batalla
        m_BatallaActiva = false;
    }

    public override void ActivarBoss()
    {
        m_BossVersionA.gameObject.SetActive(true);
        m_BossVersionB.gameObject.SetActive(true);
        EmpezarBatalla();
    }

    #region Gestion debilidad Boss Final
    private IEnumerator CambioDebilidad()
    {
        while (m_BatallaActiva)
        {
            ElegirDebilidad();
            yield return new WaitForSeconds(m_TiempoCambioDebilidad);
        }
    }

    private void ElegirDebilidad()
    {
        int opcion = Random.Range(0, 2);
        switch (opcion)
        {
            case 0:
                m_BossVersionA.CambioDebilidad(BossFinalController.DebilidadBossFinal.AGUA);
                m_BossVersionB.CambioDebilidad(BossFinalController.DebilidadBossFinal.AGUA);
                break;
            case 1:
                m_BossVersionA.CambioDebilidad(BossFinalController.DebilidadBossFinal.LAVA);
                m_BossVersionB.CambioDebilidad(BossFinalController.DebilidadBossFinal.LAVA);
                break;
        }
    }

    #endregion
}
