using System.Collections;
using UnityEngine;
using UnityEngine.VFX;

[RequireComponent(typeof(RellenadorPocionesBoss))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(VisualEffect))]
public class BossRicochet : MonoBehaviour, ITeleportable, IDamageable
{
    [Header("Componentes")]
    private SkinnedMeshRenderer m_MeshRenderer;
    [SerializeField]
    private BarraVidaBossController m_BarraVidaBossController;
    private RellenadorPocionesBoss m_RellenadorPocionesBoss;


    [Header("Parametros del Boss Ricochet")]
    [SerializeField]
    private BossSO m_SOBossRicochet;
    [SerializeField]
    private Enums.EnumLiquidos m_Elemento;
    public Enums.EnumLiquidos Elemento { get => m_Elemento; set => m_Elemento = value; }


    [SerializeField]
    private Transform[] m_PosicionesMapaIzquierdo, m_PosicionesMapaDerecho;
    [SerializeField]
    private int m_TiempoMinCambioDimension, m_TiempoMaxCambioDimension;
    [SerializeField]
    private bool m_TocaCambiarDeDimension;

    [SerializeField]
    private Vector3 m_PosicionDeDestino;
    [SerializeField]
    private LiquidosSO m_SOLava, m_SOHielo;
    [SerializeField]
    private Enums.EnumDimension m_Dimension;

    [Header("Disparo")]
    [SerializeField]
    private Transform m_PosicionBala;
    [SerializeField]
    private int m_DanyoBala = 10;
    [SerializeField]
    private int m_VelocidadBala = 5;

    [Header("Maquinas de Estado")]
    [SerializeField]
    private SwitchMachineStates m_CurrentState;
    public SwitchMachineStates CurrentState => m_CurrentState;

    public enum SwitchMachineStates { NONE, IDLE, MOVIMIENTO, DISPARANDO, TELETRANSPORTANDOSE, VULNERABLE, MUERTO }

    [Header("Referencias a objetos")]
    [SerializeField]
    private GameObject m_ContenedorPosicionesMapaIzquierdo;
    [SerializeField]
    private GameObject m_ContenedorPosicionesMapaDerecho;

    [Header("Referencias a scripts")]
    [SerializeField]
    private PoolDeBalas m_PoolDeBalas;

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_CambioDeElemento;
    [SerializeField]
    private GameEvent m_BossMuerto;

    [Header("Corutinas")]
    [SerializeField]
    private Coroutine m_Disparando;
    private Coroutine m_Vulnerable;

    [Header("SFX")]
    [SerializeField]
    private AudioClip m_AudioDano;

    [Header("VFX")]
    [SerializeField]
    private VisualEffectAsset m_PuffMuerte;
    [SerializeField]
    private GameObject m_Modelo;

    private void Awake()
    {
        m_MeshRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
        m_RellenadorPocionesBoss = GetComponent<RellenadorPocionesBoss>();
        m_PosicionesMapaIzquierdo = new Transform[m_ContenedorPosicionesMapaIzquierdo.transform.childCount];
        m_PosicionesMapaDerecho = new Transform[m_ContenedorPosicionesMapaDerecho.transform.childCount];

        m_Dimension = Enums.EnumDimension.IZQUIERDA;
        m_TocaCambiarDeDimension = false;
        m_SOBossRicochet.VidaActual = m_SOBossRicochet.VidaMax;
        ObtenerGameObjectsHijos();
        BossEscogeElemento();
    }

    // Start
    void Start()
    {
        StartCoroutine(TocaCambiarDeDimension());
        ChangeState(SwitchMachineStates.IDLE);
    }

    // Update
    void Update()
    {
        UpdateState();
    }

    //----------------------------------[ MAQUINAS DE ESTADOS }----------------------------------//
    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState || m_CurrentState == SwitchMachineStates.MUERTO)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                MoverseAPosicion();
                break;
            case SwitchMachineStates.DISPARANDO:
                transform.position = m_PosicionDeDestino;
                m_Disparando = StartCoroutine(Disparo());
                break;
            case SwitchMachineStates.MOVIMIENTO:
                break;
            case SwitchMachineStates.VULNERABLE:
                StopAllCoroutines();
                m_Vulnerable = StartCoroutine(Vulnerable());
                break;
            case SwitchMachineStates.TELETRANSPORTANDOSE:
                break;

            case SwitchMachineStates.MUERTO:
                m_RellenadorPocionesBoss.RellenarTodasPociones();
                m_BossMuerto.Raise();
                StartCoroutine(Desaparecer());
                break;
            default:
                break;
        }
    }

    private void UpdateState()
    {

        if (m_CurrentState == SwitchMachineStates.MUERTO) return;

        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;
            case SwitchMachineStates.DISPARANDO:
                break;

            case SwitchMachineStates.MOVIMIENTO:
                transform.position = Vector3.MoveTowards(transform.position, m_PosicionDeDestino, m_SOBossRicochet.Velocidad * Time.deltaTime);

                if (Vector3.Distance(transform.position, m_PosicionDeDestino) <= 0.3f)
                {
                    if (m_TocaCambiarDeDimension) m_TocaCambiarDeDimension = false;
                    ChangeState(SwitchMachineStates.DISPARANDO);
                }
                break;
            case SwitchMachineStates.TELETRANSPORTANDOSE:
                break;

            case SwitchMachineStates.MUERTO:
                break;
            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;
            case SwitchMachineStates.DISPARANDO:
                break;
            case SwitchMachineStates.MOVIMIENTO:
                break;
            case SwitchMachineStates.TELETRANSPORTANDOSE:
                break;
            case SwitchMachineStates.VULNERABLE:
                StartCoroutine(TocaCambiarDeDimension());
                break;
            case SwitchMachineStates.MUERTO:
                break;
            default:
                break;
        }
    }

    private void MoverseAPosicion()
    {
        int numRandom;
        if (m_TocaCambiarDeDimension)
        {
            switch (m_Dimension)
            {
                case Enums.EnumDimension.IZQUIERDA:
                    m_Dimension = Enums.EnumDimension.DERECHA;
                    numRandom = Random.Range(0, m_ContenedorPosicionesMapaDerecho.transform.childCount);
                    m_PosicionDeDestino = m_PosicionesMapaDerecho[numRandom].position;
                    break;

                case Enums.EnumDimension.DERECHA:
                    m_Dimension = Enums.EnumDimension.IZQUIERDA;
                    numRandom = Random.Range(0, m_ContenedorPosicionesMapaDerecho.transform.childCount);
                    m_PosicionDeDestino = m_PosicionesMapaIzquierdo[numRandom].position;
                    break;
            }
        }
        else
        {
            switch (m_Dimension)
            {
                case Enums.EnumDimension.IZQUIERDA:
                    numRandom = Random.Range(0, m_ContenedorPosicionesMapaIzquierdo.transform.childCount);
                    m_PosicionDeDestino = m_PosicionesMapaIzquierdo[numRandom].position;
                    break;

                case Enums.EnumDimension.DERECHA:
                    numRandom = Random.Range(0, m_ContenedorPosicionesMapaDerecho.transform.childCount);
                    m_PosicionDeDestino = m_PosicionesMapaDerecho[numRandom].position;
                    break;
            }
        }

        ChangeState(SwitchMachineStates.MOVIMIENTO);
    }

    private IEnumerator Disparo()
    {
        GameObject bala = m_PoolDeBalas.PedirBala();
        Vector3 direccionBala = transform.forward;
        LiquidosSO liquido = Elemento == Enums.EnumLiquidos.LAVA ? m_SOLava : m_SOHielo;
        bala.GetComponent<BalaController>().AplicarElemento(liquido);
        bala.GetComponent<BalaController>().PonerCaracteristicasBala(m_PosicionBala.position, direccionBala, m_DanyoBala, m_VelocidadBala, 0, m_PoolDeBalas.GetComponent<PoolDeBalas>(), BalaController.PropietarioBalaEnum.ENEMIGO, false);

        yield return new WaitForSeconds(m_SOBossRicochet.TiempoEsperaMoverse);

        m_PosicionDeDestino = Vector3.zero;
        ChangeState(SwitchMachineStates.IDLE);
    }

    private IEnumerator TocaCambiarDeDimension()
    {
        while (true)
        {
            if (!m_TocaCambiarDeDimension)
            {
                int tiempoParaCambioDimension = Random.Range(m_TiempoMinCambioDimension, m_TiempoMaxCambioDimension);
                yield return new WaitForSeconds(tiempoParaCambioDimension);
                m_TocaCambiarDeDimension = true;
            }
            yield return null;

        }
    }

    private IEnumerator Vulnerable()
    {
        yield return new WaitForSeconds(m_SOBossRicochet.TiempoEsperaDejarVulnerable);

        if (CurrentState == SwitchMachineStates.MUERTO) yield break;

        BossEscogeElemento();
        m_CambioDeElemento?.Raise();
        ChangeState(SwitchMachineStates.IDLE);

    }

    //----------------------------------[ TELETRANSPORTE }----------------------------------//
    #region Gestion del teletransporte
    public void SerTeletransportado()
    {
        ChangeState(SwitchMachineStates.TELETRANSPORTANDOSE);
    }

    public void TerminarTeletransporte()
    {
        if (m_CurrentState != SwitchMachineStates.TELETRANSPORTANDOSE)
            return;

        CambiarEstadoDespuesDeTeletransporte();
    }

    public void CambiarEstadoDespuesDeTeletransporte()
    {
        if (m_PosicionDeDestino != Vector3.zero)
            ChangeState(SwitchMachineStates.MOVIMIENTO);
        else
            ChangeState(SwitchMachineStates.IDLE);
    }
    #endregion

    //----------------------------------[ FUNCIONES }----------------------------------//
    #region Obtenemos posiciones de mapa izquierdo y derecho
    private void ObtenerGameObjectsHijos()
    {
        for (int i = 0; i < m_ContenedorPosicionesMapaIzquierdo.transform.childCount; ++i)
            m_PosicionesMapaIzquierdo[i] = m_ContenedorPosicionesMapaIzquierdo.transform.GetChild(i).gameObject.transform;

        for (int i = 0; i < m_ContenedorPosicionesMapaDerecho.transform.childCount; ++i)
            m_PosicionesMapaDerecho[i] = m_ContenedorPosicionesMapaDerecho.transform.GetChild(i).gameObject.transform;
    }
    #endregion

    #region Boss empieza siendo de elemento aleatorio
    private void BossEscogeElemento()
    {
        int numRandom = Random.Range(0, 2);

        if (numRandom == 0)
        {
            Elemento = m_SOLava.TipoLiquido;
            m_MeshRenderer.material = m_SOLava.Material;
            return;
        }

        Elemento = m_SOHielo.TipoLiquido;
        m_MeshRenderer.material = m_SOHielo.Material;
    }
    #endregion

    public void PerderVida(float danoRecibido)
    {
        if (CurrentState != SwitchMachineStates.VULNERABLE) return;
        this.gameObject.GetComponent<AudioSource>().loop = false;
        this.gameObject.GetComponent<AudioSource>().clip = m_AudioDano;
        this.gameObject.GetComponent<AudioSource>().Play();
        m_SOBossRicochet.VidaActual -= danoRecibido;
        m_BarraVidaBossController.PerderVida();

        if (m_SOBossRicochet.VidaActual <= 0)
        {
            print("Hace raise de rellenar poci�n");
            //m_RellenarPocion.Raise(m_MeleeEnemySO.TipoPocion, m_MeleeEnemySO.PuntosPocion);
            Morirse();
        }
    }

    private IEnumerator Desaparecer()
    {
        m_Modelo.SetActive(false);
        this.gameObject.GetComponent<VisualEffect>().visualEffectAsset = m_PuffMuerte;
        this.gameObject.GetComponent<VisualEffect>().Play();
        yield return new WaitForSeconds(1.5f);
        this.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out TengoObjetoLiquido tengoObjetoLiquidos))
        {

            if (BossDebilABala(tengoObjetoLiquidos.Objeto.TipoLiquido))
            {
                ChangeState(SwitchMachineStates.VULNERABLE);
            }
        }
    }

    private bool BossDebilABala(Enums.EnumLiquidos elemento)
    {
        if (elemento == Elemento) return false;
        return true;
    }

    private void Morirse()
    {
        m_SOBossRicochet.VidaActual = 0;
        ChangeState(SwitchMachineStates.MUERTO);
    }

    public void MuerteInstantanea() { }
}
