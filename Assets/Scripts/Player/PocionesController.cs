using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.VFX;

[RequireComponent(typeof(PlayerHealthManager))]
[RequireComponent(typeof(InvisibilidadController))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(VisualEffect))]
public class PocionesController : MonoBehaviour
{
    [Header("Pociones")]
    [SerializeField]
    private bool m_PocionesActivas = false;
    public bool PocionesActivas { get => m_PocionesActivas; }
    [SerializeField]
    private List<PocionesSO> m_ListaPociones = new List<PocionesSO>();
    private bool m_HaDejadoDeRotarPocion = true;
    [SerializeField]
    private AudioClip m_AudioPocionConsumida;

    [Header("VFX")]
    [SerializeField]
    private VisualEffectAsset m_ParticulaHabilidadFuerza;
    [SerializeField]
    private VisualEffectAsset m_ParticulaHabilidadVelocidad;

    //Componentes
    private PlayerHealthManager m_PlayerHealthManager;
    private InvisibilidadController m_InvisibilidadController;

    //Referencias
    private SOPlayer m_PlayerSO;
    public SOPlayer PlayerSO { get => m_PlayerSO; set => m_PlayerSO = value; }

    //Actions
    private InputAction m_RotarPocionDerechaAction;
    private InputAction m_RotarPocionIzquierdaAction;
    private InputAction m_UsarPocion;
    public InputAction RotarPocionDerechaAction { get => m_RotarPocionDerechaAction; set => m_RotarPocionDerechaAction = value; }
    public InputAction RotarPocionIzquierdaAction { get => m_RotarPocionIzquierdaAction; set => m_RotarPocionIzquierdaAction = value; }
    public InputAction UsarPocion { get => m_UsarPocion; set => m_UsarPocion = value; }

    //Eventos
    [SerializeField]
    private GameEvent m_RotarPocionesIzquierda, m_RotarPocionesDerecha, m_ActivarPociones;
    [SerializeField]
    private GEEnumInt m_PocionUsada;

    private void Awake()
    {
        m_PlayerHealthManager = GetComponent<PlayerHealthManager>();
        m_InvisibilidadController = GetComponent<InvisibilidadController>();
    }

    private void Start()
    {
        m_RotarPocionDerechaAction.performed += RotarPocionesIzquierda;
        m_RotarPocionIzquierdaAction.performed += RotarPocionesDerecha;
        m_UsarPocion.performed += UsarPociones;
    }

    internal void ActivarPociones()
    {
        m_PocionesActivas = true;
        m_ActivarPociones.Raise();
    }

    public void InitPociones()
    {
        foreach( PocionesSO pocion in m_ListaPociones)
        {
            pocion.CantidadActual = 0;
        }
    }

    #region Rellenar pocion
    public void RellenarPocion(Enum pocionARellenar, int cantidadPocion)
    {
        foreach (PocionesSO pocion in m_ListaPociones)
        {
            if ((Enums.EnumPociones)pocionARellenar == pocion.TipoPocion)
            {
                if (pocion.CantidadActual + cantidadPocion > pocion.CantidadMax)
                    pocion.CantidadActual = pocion.CantidadMax;
                else
                    pocion.CantidadActual += cantidadPocion;
            }
        }
    }

    public void VaciarPociones()
    {
        foreach (PocionesSO pocion in m_ListaPociones)
        {
            pocion.CantidadActual = 0;
        }
    }

    #endregion

    #region Obtener pocion por tipo
    public PocionesSO ObtenerPocionPorTipo(Enums.EnumPociones TipoPocion)
    {
        foreach (PocionesSO pocion in m_ListaPociones)
        {
            if (pocion.TipoPocion == TipoPocion)
            {
                return pocion;
            }
        }

        return null;
    }
    #endregion

    #region Rotar Pociones
    private void RotarPocionesDerecha(InputAction.CallbackContext context)
    {
        if (!m_PocionesActivas) return;
        if (!m_HaDejadoDeRotarPocion) return;
        m_ListaPociones.Add(m_ListaPociones[0]);
        m_ListaPociones.RemoveAt(0);
        m_RotarPocionesDerecha.Raise();
        m_HaDejadoDeRotarPocion = false;
    }

    private void RotarPocionesIzquierda(InputAction.CallbackContext context)
    {
        if (!m_PocionesActivas) return;
        if (!m_HaDejadoDeRotarPocion) return;
        m_ListaPociones.Insert(0, m_ListaPociones[^1]);
        m_ListaPociones.RemoveAt(m_ListaPociones.Count - 1);
        m_RotarPocionesIzquierda.Raise();
        m_HaDejadoDeRotarPocion = false;
    }

    public void HanDejadoDeRotarPociones()
    {
        m_HaDejadoDeRotarPocion = true;
    }
    #endregion

    #region Usar Pociones
    private void UsarPociones(InputAction.CallbackContext context)
    {
        GetComponent<AudioSource>().loop = false;
        GetComponent<AudioSource>().clip = m_AudioPocionConsumida;
        switch (m_ListaPociones[0].TipoPocion)
        {
            case Enums.EnumPociones.VIDA:
                PocionesSO pocion = m_ListaPociones[0];
                if (pocion.CantidadActual >= pocion.CantidadMax)
                {
                    GetComponent<AudioSource>().Play();
                    pocion.CantidadActual = 0;
                    m_PlayerHealthManager.RecuperarVida();
                    m_PocionUsada.Raise(Enums.EnumPociones.VIDA, 0);
                }
                break;
            case Enums.EnumPociones.FUERZA:
                StartCoroutine(AplicarBuffFuerza(m_ListaPociones[0]));
                break;
            case Enums.EnumPociones.VELOCIDAD:
                StartCoroutine(AplicarBuffVelocidad(m_ListaPociones[0]));
                break;
            case Enums.EnumPociones.INVISIBILIDAD:
                PocionesSO pocionIn = m_ListaPociones[0];
                if (pocionIn.CantidadActual >= pocionIn.CantidadMax)
                {
                    GetComponent<AudioSource>().Play();
                    pocionIn.CantidadActual = 0;
                    m_PocionUsada.Raise(Enums.EnumPociones.INVISIBILIDAD, 0);
                    StartCoroutine(m_InvisibilidadController.VolverseInvisible(m_ListaPociones[0].DuracionEfecto));
                }
                break;
            default:
                break;
        }
    }
    #endregion

    #region Corrutinas de buffos
    private IEnumerator AplicarBuffFuerza(PocionesSO pocion)
    {
        if (pocion.CantidadActual >= pocion.CantidadMax)
        {
            m_PocionUsada.Raise(pocion.TipoPocion, 0);
            this.GetComponent<AudioSource>().Play();
            pocion.CantidadActual = 0;
            m_PlayerSO.danyoAtaque += pocion.PuntosEfecto;
            this.gameObject.GetComponent<VisualEffect>().visualEffectAsset = m_ParticulaHabilidadFuerza;
            this.gameObject.GetComponent<VisualEffect>().Play();
            yield return new WaitForSeconds(pocion.DuracionEfecto);
            m_PlayerSO.danyoAtaque -= pocion.PuntosEfecto;
            this.gameObject.GetComponent<VisualEffect>().Stop();
        }
        yield return null;
    }

    private IEnumerator AplicarBuffVelocidad(PocionesSO pocion)
    {
        if (pocion.CantidadActual >= pocion.CantidadMax)
        {
            m_PocionUsada.Raise(pocion.TipoPocion, 0);
            this.GetComponent<AudioSource>().Play();
            pocion.CantidadActual = 0;
            m_PlayerSO.velocidad += pocion.PuntosEfecto;
            this.gameObject.GetComponent<VisualEffect>().visualEffectAsset = m_ParticulaHabilidadVelocidad;
            this.gameObject.GetComponent<VisualEffect>().Play();
            yield return new WaitForSeconds(pocion.DuracionEfecto);
            m_PlayerSO.velocidad -= pocion.PuntosEfecto;
            this.gameObject.GetComponent<VisualEffect>().Stop();
        }
        yield return null;
    }
    #endregion
}
