using UnityEngine;

public class EspadaController : MonoBehaviour
{
    [Header("Referencias a scripts/SO")]
    [SerializeField]
    private SOPlayer m_PlayerSO;
    private PlayerController m_PlayerController;
    private InflingidorDeDanoPlayerController m_InflingidorDeDanoPlayerController;

    [Header("Variables de control")]
    [SerializeField]
    private bool m_PuedoHacerDano = true;
    public bool HaciendoBackstab = false;

    private void Awake()
    {
        m_PlayerController = GetComponentInParent<PlayerController>();
        m_InflingidorDeDanoPlayerController = GetComponentInParent<InflingidorDeDanoPlayerController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (m_PlayerController.ObtenerEstadoMaquinaBase() == "ATACANDO" && other.gameObject.CompareTag("Escudo"))
        {
            m_PuedoHacerDano = false;
            m_InflingidorDeDanoPlayerController.ReboteConEscudo();
        }
        else if (m_PlayerController.ObtenerEstadoMaquinaBase() == "ATACANDO" && other.TryGetComponent(out IDamageable damageable) && m_PuedoHacerDano)
        {
            if (!HaciendoBackstab)
                damageable.PerderVida(m_PlayerSO.danyoAtaque);
            else
            {
                damageable.MuerteInstantanea();
            }
        }
    }

    public void VuelvoAPoderHacerDano()
    {
        m_PuedoHacerDano = true;
        HaciendoBackstab = false;
    }
}
