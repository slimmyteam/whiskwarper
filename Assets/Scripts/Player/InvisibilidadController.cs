using System.Collections;
using UnityEngine;
using UnityEngine.VFX;

[RequireComponent(typeof(VisualEffect))]
public class InvisibilidadController : MonoBehaviour
{
    [Header("Materiales")]
    [SerializeField]
    private SkinnedMeshRenderer[] m_SkinnedRenderer;
    [SerializeField]
    private MeshRenderer[] m_MeshesRenderer;
    [SerializeField]
    private Material m_MaterialOriginalSkinned;
    [SerializeField]
    private Material m_MaterialInvisibleSkinned;
    [SerializeField]
    private Material m_MaterialOriginalMesh;
    [SerializeField]
    private Material m_MaterialInvisibleMesh;

    [Header("Maquina de Estados")]
    [SerializeField]
    private SwitchMachineStatesMaquinaVisibilidad m_EstadoActual;
    private enum SwitchMachineStatesMaquinaVisibilidad { NONE, VISIBLE, INVISIBLE }

    [Header("VFX")]
    [SerializeField]
    private VisualEffectAsset m_ParticulaEstarInvisible;

    

    // Start is called before the first frame update
    void Start()
    {
        ChangeStateMaquinaVisibilidad(SwitchMachineStatesMaquinaVisibilidad.VISIBLE);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateStateMaquinaVisibilidad();
    }

    public bool EstaInvisible()
    {
        if (m_EstadoActual == SwitchMachineStatesMaquinaVisibilidad.VISIBLE)
            return false;
        return true;
    }

    private void CambiarMaterialPlayer(Material materialSkinned, Material materialMesh)
    {
        foreach (SkinnedMeshRenderer meshRenderer in m_SkinnedRenderer)
            meshRenderer.material = materialSkinned;
        foreach (MeshRenderer meshRenderer in m_MeshesRenderer)
            meshRenderer.material = materialMesh;
        
    }

    public IEnumerator VolverseInvisible(float duracion)
    {
        this.gameObject.GetComponent<VisualEffect>().visualEffectAsset = m_ParticulaEstarInvisible;
        this.gameObject.GetComponent<VisualEffect>().Play();
        ChangeStateMaquinaVisibilidad(SwitchMachineStatesMaquinaVisibilidad.INVISIBLE);
        yield return new WaitForSeconds(duracion);
        ChangeStateMaquinaVisibilidad(SwitchMachineStatesMaquinaVisibilidad.VISIBLE);
        this.gameObject.GetComponent<VisualEffect>().Stop();
    }

    #region Maquina de estados
    private void ChangeStateMaquinaVisibilidad(SwitchMachineStatesMaquinaVisibilidad newState)
    {
        if (newState == m_EstadoActual)
            return;
        ExitStateMaquinaVisibilidad();
        InitStateMaquinaVisibilidad(newState);
    }

    private void InitStateMaquinaVisibilidad(SwitchMachineStatesMaquinaVisibilidad currentState)
    {
        m_EstadoActual = currentState;
        switch (m_EstadoActual)
        {
            case SwitchMachineStatesMaquinaVisibilidad.VISIBLE:
                CambiarMaterialPlayer(m_MaterialOriginalSkinned, m_MaterialOriginalMesh);
                break;
            case SwitchMachineStatesMaquinaVisibilidad.INVISIBLE:
                CambiarMaterialPlayer(m_MaterialInvisibleSkinned, m_MaterialInvisibleMesh);
                break;
            default:
                break;
        }
    }

    private void UpdateStateMaquinaVisibilidad()
    {
        switch (m_EstadoActual)
        {
            case SwitchMachineStatesMaquinaVisibilidad.VISIBLE:
                break;
            case SwitchMachineStatesMaquinaVisibilidad.INVISIBLE:
                break;
            default:
                break;
        }
    }

    private void ExitStateMaquinaVisibilidad()
    {
        switch (m_EstadoActual)
        {
            case SwitchMachineStatesMaquinaVisibilidad.VISIBLE:
                break;
            case SwitchMachineStatesMaquinaVisibilidad.INVISIBLE:
                break;
            default:
                break;
        }
    }
    #endregion
}
