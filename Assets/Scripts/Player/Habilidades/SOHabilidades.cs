using UnityEngine;

[CreateAssetMenu(fileName = "Scriptable Objects", menuName = "Scriptable Objects/Habilidades")]
public class SOHabilidades : ScriptableObject
{
    public Enums.EnumHabilidades nombre;
    public float dano;
    public float cooldown;
    public float tiempo;
    public float velocidad;
    public float distancia;
    public Sprite imagen;
}
