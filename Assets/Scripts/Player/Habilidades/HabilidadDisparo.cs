using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class HabilidadDisparo : MonoBehaviour
{
    //Referencias
    private GameObject m_ContenedorHabilidades;
    public GameObject ContenedorHabilidades { get => m_ContenedorHabilidades; set => m_ContenedorHabilidades = value; }
    private GameObject m_Arco;

    [Header("Prefabs")]
    [SerializeField]
    private GameObject m_PoolBalas;

    [Header("Estado de la habilidad")]
    [SerializeField]
    private bool m_Cooldown;

    [Header("SFX")]
    [SerializeField]
    private AudioClip m_AudioDisparo;
    //Scripts
    private PlayerHabilidades m_PlayerHabilidades;

    private void Awake()
    {
        m_PlayerHabilidades = GetComponent<PlayerHabilidades>();
    }

    void Start()
    {
        m_Cooldown = false;
        m_Arco = m_ContenedorHabilidades.transform.GetChild(1).gameObject;
    }

    internal void Disparar(float velocidad, float dano, float tiempo, float cooldown, Enums.EnumHabilidades nombre)
    {
        if (!m_Cooldown)
        {
            this.GetComponent<AudioSource>().loop = false;
            this.GetComponent<AudioSource>().clip = m_AudioDisparo;
            this.GetComponent<AudioSource>().Play();
            bool mejoraArco = nombre == Enums.EnumHabilidades.BOOMERANG;
            if(!mejoraArco)
                m_Arco.SetActive(true);
            GameObject flecha = m_PoolBalas.GetComponent<PoolDeBalas>().PedirBala();
            flecha.SetActive(true);
            flecha.GetComponent<BalaController>().PonerCaracteristicasBala(m_Arco.transform.position, transform.forward, dano, velocidad, tiempo, m_PoolBalas.GetComponent<PoolDeBalas>(), BalaController.PropietarioBalaEnum.PLAYER, mejoraArco);
            StartCoroutine(CooldownDisparo(cooldown));
        }
        StartCoroutine(m_PlayerHabilidades.VolverANoHabilidadDespuesEspera());
    }

    private IEnumerator CooldownDisparo(float cooldown)
    {
        m_Cooldown = true;
        yield return new WaitForSeconds(cooldown);
        m_Cooldown = false;
    }
}
