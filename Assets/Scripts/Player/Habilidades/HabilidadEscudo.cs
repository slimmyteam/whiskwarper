using System.Collections;
using UnityEngine;

public class HabilidadEscudo : MonoBehaviour
{
    //GameObjects
    private GameObject m_ContenedorHabilidades;
    public GameObject ContenedorHabilidades { get => m_ContenedorHabilidades; set => m_ContenedorHabilidades = value; }
    private GameObject m_Escudo;
    private GameObject m_AreaProteccionMagica;

    [Header("Caracteristicas")]
    [SerializeField]
    private SOHabilidades m_EscudoSO;
    [SerializeField]
    private SOHabilidades m_EscudoMagicoSO;
    [SerializeField]
    private bool m_Cooldown;
    [SerializeField]
    private bool m_MejoraEscudo;

    private void Start()
    {
        m_Escudo = m_ContenedorHabilidades.transform.GetChild(0).gameObject;
        m_Escudo.GetComponent<EscudoController>().EscudoSO = m_EscudoSO;
        m_AreaProteccionMagica = m_Escudo.transform.GetChild(0).gameObject;
        m_Cooldown = false;
    }

    internal bool ActivarEscudo(Enums.EnumHabilidades habilidad)
    {
        if (m_Cooldown)
            return false;
        if (habilidad == Enums.EnumHabilidades.ESCUDOMAGICO)
        {
            m_MejoraEscudo = true;
            m_Escudo.GetComponent<EscudoController>().MejoraEscudo = true;
            m_Escudo.GetComponent<EscudoController>().EscudoSO = m_EscudoMagicoSO;
            m_AreaProteccionMagica.GetComponent<EscudoController>().MejoraEscudo = true;
        }
        m_Escudo.SetActive(true);
        if (m_MejoraEscudo)
            StartCoroutine(ActivarAreaProteccionMagica());
        return true;
    }

    internal void DesactivarEscudo()
    {
        m_Escudo.SetActive(false);
        StartCoroutine(CooldownEscudo());
    }

    private IEnumerator CooldownEscudo()
    {
        m_Cooldown = true;
        yield return new WaitForSeconds(m_MejoraEscudo ? m_EscudoSO.cooldown : m_EscudoMagicoSO.cooldown);
        m_Cooldown = false;
    }

    private IEnumerator ActivarAreaProteccionMagica()
    {
        m_AreaProteccionMagica.SetActive(true);
        yield return new WaitForSeconds(m_EscudoMagicoSO.tiempo);
        m_AreaProteccionMagica.SetActive(false);
    }
}

