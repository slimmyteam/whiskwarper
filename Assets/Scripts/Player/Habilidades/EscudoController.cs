using System.Collections;
using UnityEngine;

public class EscudoController : MonoBehaviour
{
    [Header("Estados")]
    [SerializeField]
    private bool m_Parry;

    public bool Parry { get => m_Parry; set => m_Parry = value; }

    [SerializeField]
    private bool m_MejoraEscudo;
    public bool MejoraEscudo { get => m_MejoraEscudo; set => m_MejoraEscudo = value; }

    //Componentes
    private SOHabilidades m_EscudoSO;
    public SOHabilidades EscudoSO { get => m_EscudoSO; set => m_EscudoSO = value; }

    private void OnEnable()
    {
        if (!m_MejoraEscudo)
            StartCoroutine(VentanaDeParry());
        else
            m_Parry = true;
    }

    private IEnumerator VentanaDeParry()
    {
        Parry = true;
        yield return new WaitForSeconds(m_EscudoSO.tiempo);
        m_Parry = false;
    }

}
