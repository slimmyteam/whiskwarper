using System.Collections;
using UnityEngine;

[RequireComponent(typeof(ControlDeLayers))]
[RequireComponent(typeof(AudioSource))]
public class BombaController : MonoBehaviour
{
    //[Header("Estado de la bomba")]
    private float m_VelocidadBomba;
    private float m_DanyoBomba;
    private bool m_MejoraBomba;
    private float m_TiempoEstallar;

    [Header("Parametros")]
    [SerializeField]
    private float m_RadioExplosion;
    [SerializeField]
    private float m_RadioDeteccion;
    [SerializeField]
    private float m_TiempoActivacion;

    //[Header("Referencia a objetos")]
    private PoolDeBalas m_PoolDeBalasScript;
    private GameObject m_ModeloBomba;
    private GameObject m_ModeloMina;

    //Componentes
    private Rigidbody m_Rigidbody;
    private ControlDeLayers m_ControlDeLayers;
    private ParticleSystem m_ParticleSystem;

    [SerializeField]
    private LayerMask m_EscenarioLayers;

    [Header("Sonidos")]
    [SerializeField]
    private Sound m_SonidoExplosion;
    [SerializeField]
    private AudioClip m_AudioExplosion;

    //Corrutinas
    private Coroutine m_ActivarMina;
    private Coroutine m_EsperarEstallar;
    private Coroutine m_DetectarObjetivos;

    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_ControlDeLayers = GetComponent<ControlDeLayers>();
        m_ParticleSystem = GetComponent<ParticleSystem>();
        m_SonidoExplosion = new Sound(20, transform, SoundType.Type.Interesting, 5);
        m_ModeloBomba = transform.GetChild(0).transform.GetChild(0).gameObject;
        m_ModeloMina = transform.GetChild(0).transform.GetChild(1).gameObject;
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    #region Inicio y lanzar bomba

    /// <summary>
    /// Asigna las caracteristicas iniciales de la bomba y llama a la funcion para lanzarla.
    /// </summary>
    /// <param name="posicionInicialBomba">Vector3, Posicion inicial de la bomba</param>
    /// <param name="forwardPropietario">Vector3, Forward del objeto que lanza la bomba</param>
    /// <param name="danyoBomba">float, Dano que hace la bomba al explotar</param>
    /// <param name="velocidadBomba">float, Velocidad de lanzamiento de la bomba</param>
    /// <param name="tiempoEstallar">float Tiempo de espera antes de estallar (solo para mejora (mina))</param>
    /// <param name="scriptPoolDeBalas">Script PoolDeBalas de la pool a la que pertenece</param>
    /// <param name="mejoraBomba">bool, indica si la bomba esta mejorada (mina)</param>
    public void InitBomba(Vector3 posicionInicialBomba, Vector3 forwardPropietario, float danyoBomba, float velocidadBomba, float tiempoEstallar, PoolDeBalas scriptPoolDeBalas, bool mejoraBomba)
    {
        m_MejoraBomba = mejoraBomba;
        gameObject.transform.position = m_MejoraBomba ? posicionInicialBomba - transform.up * 1.5f : posicionInicialBomba;
        transform.forward = forwardPropietario;
        m_DanyoBomba = danyoBomba;
        m_VelocidadBomba = velocidadBomba;
        m_PoolDeBalasScript = scriptPoolDeBalas;
        m_TiempoEstallar = tiempoEstallar;
        m_ModeloBomba.SetActive(!m_MejoraBomba);
        m_ModeloMina.SetActive(m_MejoraBomba);
        if (!m_MejoraBomba)
        {
            LanzarBomba();
        }
        else
            StartCoroutine(ActivarMina());
    }

    /// <summary>
    /// Lanza la bomba en direccion al Forward + Up, teniendo en cuenta la velocidad indicada.
    /// </summary>
    private void LanzarBomba()
    {
        m_Rigidbody.AddForce(transform.up * m_VelocidadBomba + transform.forward * m_VelocidadBomba, ForceMode.Impulse);
    }

    #endregion

    #region Estallar

    /// <summary>
    /// Si la bomba esta mejorada (mina), se para cuando choca con una layer del escenario y comienza la espera para estallar. 
    /// Si no esta mejorada, estalla.
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (m_MejoraBomba)
        {
            if (m_ControlDeLayers.ContieneLayer(m_EscenarioLayers, other.gameObject.layer))
            {
                m_Rigidbody.velocity = Vector3.zero;
                m_Rigidbody.useGravity = false;
                m_ActivarMina = StartCoroutine(ActivarMina());
            }
            return;
        } //La mina no estalla si choca con el escenario
        StartCoroutine(Estallar());
    }

    /// <summary>
    /// Detecta las colisiones en el radio indicado alrededor de la bomba y hace dano a todos los objetos que ha encontrado que son IDamageable.
    /// Finalmente, devuelve la bomba a la pool.
    /// </summary>
    private IEnumerator Estallar()
    {
        m_SonidoExplosion.MakeSound();
        this.GetComponent<AudioSource>().loop = false;
        this.GetComponent<AudioSource>().clip = m_AudioExplosion;
        this.GetComponent<AudioSource>().Play();
        if (m_ActivarMina != null && m_EsperarEstallar != null && m_DetectarObjetivos != null)
        {
            StopCoroutine(m_ActivarMina);
            StopCoroutine(m_EsperarEstallar);
            StopCoroutine(m_DetectarObjetivos);
        }
        
        m_ParticleSystem.Play();
        Collider[] objetivosAlcanzados = Physics.OverlapSphere(transform.position, m_RadioExplosion);
        foreach (Collider collider in objetivosAlcanzados)
        {
            if (collider.gameObject.TryGetComponent(out IDamageable damageable))
            {
                damageable.PerderVida(m_DanyoBomba);
            }
        }
        yield return new WaitForSeconds(0.4f);
        m_PoolDeBalasScript.DevolverBalaALaPool(gameObject);
        yield return null;
        StopAllCoroutines();
    }

    #endregion

    #region Mejora - Mina

    /// <summary>
    /// Espera unos segundos antes de que la mina busque colliders cercanos para estallar.
    /// </summary>
    /// <returns></returns>
    private IEnumerator ActivarMina()
    {
        yield return new WaitForSeconds(m_TiempoActivacion);
        m_EsperarEstallar = StartCoroutine(EsperarParaEstallar());
    }

    /// <summary>
    /// Llama a la corrutina DetectarObjetivos y espera los segundos indicados antes de llamar a Estallar.
    /// </summary>
    /// <returns></returns>
    private IEnumerator EsperarParaEstallar()
    {
        m_DetectarObjetivos = StartCoroutine(DetectarObjetivos());
        yield return new WaitForSeconds(m_TiempoEstallar);
        StartCoroutine(Estallar());
    }

    /// <summary>
    /// Detecta colliders en el radio indicado. Si detecta un collider que no forma parte de las layers de Escenario, estalla la mina.
    /// </summary>
    /// <returns></returns>
    private IEnumerator DetectarObjetivos()
    {
        while (true)
        {
            Collider[] objetivosDetectados = Physics.OverlapSphere(transform.position, m_RadioDeteccion);
            foreach (Collider collider in objetivosDetectados)
            {
                //Debug.Log("Colliders detectados " + collider.name);
                if (!m_ControlDeLayers.ContieneLayer(m_EscenarioLayers, collider.gameObject.layer) && collider.gameObject != gameObject)
                {
                    //Debug.Log("Detectado objetivo " + collider.gameObject.name);
                    StartCoroutine(Estallar());
                }
            }
            yield return null;
        }
    }

    #endregion

    #region OnDrawGizmosSelected
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, m_RadioExplosion);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, m_RadioDeteccion);
    }
    #endregion
}