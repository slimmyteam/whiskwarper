using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(HabilidadDisparo))]
[RequireComponent(typeof(HabilidadEscudo))]
[RequireComponent(typeof(HabilidadBomba))]
[RequireComponent(typeof(HabilidadCubo))]
[RequireComponent(typeof(ControladorActivadorObjetos))]
public class PlayerHabilidades : MonoBehaviour
{

    //Inputs
    private InputAction m_UsarHabilidadAction;
    public InputAction UsarHabilidadAction { get => m_UsarHabilidadAction; set => m_UsarHabilidadAction = value; }


    private InputAction m_RotarHabilidadAction;
    public InputAction RotarHabilidadAction { get => m_RotarHabilidadAction; set => m_RotarHabilidadAction = value; }

    //[Header("Componentes")]
    private PlayerController m_PlayerController;
    private HabilidadDisparo m_HabilidadDisparo;
    private HabilidadEscudo m_HabilidadEscudo;
    private HabilidadBomba m_HabilidadBomba;
    private HabilidadCubo m_HabilidadCubo;

    [Header("Maquina de estados habilidades")]
    [SerializeField]
    internal SwitchMachineStatesMaquinaHabilidades m_CurrentStateHabilidades;
    internal enum SwitchMachineStatesMaquinaHabilidades { NONE, NOHABILIDAD, ESCUDO, HABILIDAD }

    [Header("Habilidades")]
    [SerializeField]
    private List<SOHabilidades> m_ListaHabilidades;
    public List<SOHabilidades> ListaHabilidades { get => ListaHabilidades; set => ListaHabilidades = value; }
    [SerializeField]
    private List<SOHabilidades> m_ListaHabilidadesAdquiridas = new();
    public List<SOHabilidades> ListaHabilidadesAdquiridas { get => m_ListaHabilidadesAdquiridas; set => m_ListaHabilidadesAdquiridas = value; }
    [SerializeField]
    private SOHabilidades m_HabilidadEquipada;
    private bool m_HaDejadoDeRotarHabilidad = true;

    [Header("Referencias")]
    [SerializeField]
    private GameObject m_ContenedorHabilidades;
    private SOPlayer m_PlayerSO;
    public SOPlayer PlayerSO { get => m_PlayerSO; set => m_PlayerSO = value; }
    private GameObject m_Arco;
    private GameObject m_Cubo;

    [Header("Parametros")]
    [SerializeField]
    private float m_TiempoEsperaVolverANoHabilidad;

    [Header("Sonidos")]
    [SerializeField]
    private Sound m_SonidoUsarCubo;

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_RotarHabilidadesIzquierda;
    [SerializeField]
    private GameEvent m_RotarHabilidadesDerecha;
    [SerializeField]
    private GESprite m_EnviarImagenHabilidadDesbloqueada;


    private void Awake()
    {
        m_PlayerController = GetComponent<PlayerController>();
        m_HabilidadDisparo = GetComponent<HabilidadDisparo>();
        m_HabilidadEscudo = GetComponent<HabilidadEscudo>();
        m_HabilidadBomba = GetComponent<HabilidadBomba>();
        m_HabilidadCubo = GetComponent<HabilidadCubo>();

        m_HabilidadDisparo.ContenedorHabilidades = m_ContenedorHabilidades;
        m_HabilidadEscudo.ContenedorHabilidades = m_ContenedorHabilidades;
        m_HabilidadCubo.ContenedorHabilidades = m_ContenedorHabilidades;
        m_Arco = m_ContenedorHabilidades.transform.GetChild(1).gameObject;
        m_Cubo = m_ContenedorHabilidades.transform.GetChild(2).gameObject;

        m_SonidoUsarCubo = new Sound(5, transform, SoundType.Type.Interesting, 0.25f);
    }

    void Start()
    {
        m_UsarHabilidadAction.performed += CambiarEstadoHabilidad;
        m_RotarHabilidadAction.performed += RotarHabilidades;
        m_UsarHabilidadAction.canceled += SoltarHabilidad;

        ChangeStateMaquinaHabilidades(SwitchMachineStatesMaquinaHabilidades.NOHABILIDAD);

        m_HabilidadEquipada = m_ListaHabilidadesAdquiridas.Count > 0 ? m_ListaHabilidadesAdquiridas.First() : null;

    }

    void Update()
    {
        UpdateStateMaquinaHabilidades();
    }

    private void OnDestroy()
    {
        m_UsarHabilidadAction.performed -= CambiarEstadoHabilidad;
        m_RotarHabilidadAction.performed -= RotarHabilidades;
        m_UsarHabilidadAction.canceled -= SoltarHabilidad;
    }

    #region State Machine Habilidades

    internal void ChangeStateMaquinaHabilidades(SwitchMachineStatesMaquinaHabilidades newState)
    {
        if (newState == m_CurrentStateHabilidades || m_PlayerController.CurrentStateBase == PlayerController.SwitchMachineStatesMaquinaBase.MUERTO)
            return;
        ExitStateMaquinaHabilidades();
        InitStateMaquinaHabilidades(newState);
    }

    private void InitStateMaquinaHabilidades(SwitchMachineStatesMaquinaHabilidades currentState)
    {
        m_CurrentStateHabilidades = currentState;
        switch (m_CurrentStateHabilidades)
        {
            case SwitchMachineStatesMaquinaHabilidades.NOHABILIDAD:
                break;
            case SwitchMachineStatesMaquinaHabilidades.ESCUDO:
                if (m_HabilidadEquipada.nombre == Enums.EnumHabilidades.ESCUDO)
                {
                    m_PlayerSO.velocidad /= 2;
                }
                if (!m_HabilidadEscudo.ActivarEscudo(m_HabilidadEquipada.nombre))
                    ChangeStateMaquinaHabilidades(SwitchMachineStatesMaquinaHabilidades.NOHABILIDAD);
                break;
            case SwitchMachineStatesMaquinaHabilidades.HABILIDAD:
                UsarHabilidad();
                break;
            default:
                break;
        }
    }

    private void UpdateStateMaquinaHabilidades()
    {
        switch (m_CurrentStateHabilidades)
        {
            case SwitchMachineStatesMaquinaHabilidades.NOHABILIDAD:
                break;
            case SwitchMachineStatesMaquinaHabilidades.ESCUDO:
                break;
            case SwitchMachineStatesMaquinaHabilidades.HABILIDAD:
                break;
            default:
                break;
        }
    }

    private void ExitStateMaquinaHabilidades()
    {
        switch (m_CurrentStateHabilidades)
        {
            case SwitchMachineStatesMaquinaHabilidades.NOHABILIDAD:
                break;
            case SwitchMachineStatesMaquinaHabilidades.ESCUDO:
                m_PlayerSO.velocidad = m_PlayerSO.velocidadMax;
                m_HabilidadEscudo.DesactivarEscudo();
                break;
            case SwitchMachineStatesMaquinaHabilidades.HABILIDAD:
                m_Arco.SetActive(false);
                m_Cubo.SetActive(false);

                if (m_PlayerController.NombreObjetoCabeza == "Cubo")
                {
                    m_HabilidadCubo.DesequiparCubo();
                }
                break;
            default:
                break;
        }
    }

    #endregion

    #region Usar habilidades

    private void UsarHabilidad()
    {
        if (m_HabilidadEquipada == null)
            return;
        switch (m_HabilidadEquipada.nombre)
        {
            case Enums.EnumHabilidades.ARCO:
            case Enums.EnumHabilidades.BOOMERANG:
                m_HabilidadDisparo.Disparar(m_HabilidadEquipada.velocidad, m_HabilidadEquipada.dano, m_HabilidadEquipada.tiempo, m_HabilidadEquipada.cooldown, m_HabilidadEquipada.nombre);
                break;
            case Enums.EnumHabilidades.BOMBAS:
            case Enums.EnumHabilidades.MINAS:
                m_HabilidadBomba.LanzarBomba(m_HabilidadEquipada.velocidad, m_HabilidadEquipada.dano, m_HabilidadEquipada.tiempo, m_HabilidadEquipada.cooldown, m_HabilidadEquipada.nombre);
                break;
            case Enums.EnumHabilidades.CUBO:
                m_HabilidadCubo.ActivarCubo();
                //El cubo no funciona igual que el arco, porque el arco cambia a NO HABILIDAD.
                m_SonidoUsarCubo.MakeSound();
                break;
            case Enums.EnumHabilidades.ESCUDO:
            case Enums.EnumHabilidades.ESCUDOMAGICO:
                CambiarEstadoEscudo();
                break;
        }
    }

    private void CambiarEstadoHabilidad(InputAction.CallbackContext context)
    {
        ChangeStateMaquinaHabilidades(SwitchMachineStatesMaquinaHabilidades.HABILIDAD);
    }
    private void CambiarEstadoEscudo()
    {
        ChangeStateMaquinaHabilidades(SwitchMachineStatesMaquinaHabilidades.ESCUDO);
    }

    public void VolverANoHabilidad()
    {
        ChangeStateMaquinaHabilidades(SwitchMachineStatesMaquinaHabilidades.NOHABILIDAD);
    }
    public IEnumerator VolverANoHabilidadDespuesEspera()
    {
        yield return new WaitForSeconds(m_TiempoEsperaVolverANoHabilidad);
        VolverANoHabilidad();
    }

    private void SoltarHabilidad(InputAction.CallbackContext context)
    {
        if (m_CurrentStateHabilidades == SwitchMachineStatesMaquinaHabilidades.ESCUDO)
            ChangeStateMaquinaHabilidades(SwitchMachineStatesMaquinaHabilidades.NOHABILIDAD);
    }

    #endregion

    #region Desbloquear y rotar habilidades

    public void DesbloquearHabilidad(Enums.EnumHabilidades nombreHabilidad)
    {
        if (ComprobarSiTengoHabilidad(nombreHabilidad))
        {
            return; //Si ya tengo la habilidad, hago return
        }

        foreach (SOHabilidades habilidad in m_ListaHabilidades)
        {
            if (habilidad.nombre == nombreHabilidad)
            {
                m_ListaHabilidadesAdquiridas.Add(habilidad);
                m_EnviarImagenHabilidadDesbloqueada.Raise(habilidad.imagen);
                SOHabilidades habilidadQueSustituir;
                switch (habilidad.nombre)
                {
                    case Enums.EnumHabilidades.ESCUDOMAGICO:
                        habilidadQueSustituir = m_ListaHabilidades[0];
                        m_ListaHabilidadesAdquiridas.Remove(habilidadQueSustituir);
                        break;
                    case Enums.EnumHabilidades.BOOMERANG:
                        habilidadQueSustituir = m_ListaHabilidades[1];
                        m_ListaHabilidadesAdquiridas.Remove(habilidadQueSustituir);
                        break;
                    case Enums.EnumHabilidades.MINAS:
                        habilidadQueSustituir = m_ListaHabilidades[2];
                        m_ListaHabilidadesAdquiridas.Remove(habilidadQueSustituir);
                        break;
                }
            }
        }
        CambiarHabilidadEquipada();
    }

    private void RotarHabilidades(InputAction.CallbackContext context)
    {
        if (!m_HaDejadoDeRotarHabilidad) return;
        if (m_ListaHabilidadesAdquiridas.Count < 1)
            return;
        VolverANoHabilidad();
        Vector2 direccion = context.ReadValue<Vector2>();
        if (direccion.y > 0)
        {
            m_ListaHabilidadesAdquiridas.Add(m_ListaHabilidadesAdquiridas[0]);
            m_ListaHabilidadesAdquiridas.RemoveAt(0);
            m_RotarHabilidadesDerecha.Raise();
            m_HaDejadoDeRotarHabilidad = false;
        }
        else
        {
            m_ListaHabilidadesAdquiridas.Insert(0, m_ListaHabilidadesAdquiridas[^1]);
            m_ListaHabilidadesAdquiridas.RemoveAt(m_ListaHabilidadesAdquiridas.Count - 1);
            m_RotarHabilidadesIzquierda.Raise();
            m_HaDejadoDeRotarHabilidad = false;
        }
        CambiarHabilidadEquipada();
    }

    public void HanDejadoDeRotarHabilidades()
    {
        m_HaDejadoDeRotarHabilidad = true;
    }

    private void CambiarHabilidadEquipada()
    {
        m_HabilidadEquipada = m_ListaHabilidadesAdquiridas.First();
    }

    public bool ComprobarSiTengoHabilidad(Enums.EnumHabilidades nombreHabilidad)
    {
        foreach (SOHabilidades habilidad in m_ListaHabilidadesAdquiridas)
        {
            if (habilidad.nombre == nombreHabilidad)
            {
                return true;
            }

        }
        return false;
    }

    #endregion

}
