using Unity.VisualScripting;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class HabilidadCubo : MonoBehaviour
{
    [Header("Parametros cubo")]
    [SerializeField]
    private ScriptableObject m_Contenido;
    public ScriptableObject Contenido { get { return m_Contenido; } }
    [SerializeField]
    private AudioClip m_AudioUsarCubo;

    [Header("Referencias a objetos")]
    private GameObject m_ContenedorHabilidades;
    public GameObject ContenedorHabilidades { get => m_ContenedorHabilidades; set => m_ContenedorHabilidades = value; }
    private GameObject m_Cubo;
    private GameObject m_gameObjectContenido;
    private PlayerController m_PlayerController;
    private PocionesController m_PocionesController;

    private void Awake()
    {
        m_PlayerController = GetComponent<PlayerController>();
        m_PocionesController = GetComponent<PocionesController>();
        m_Contenido = null;
    }

    private void Start()
    {
        m_Cubo = m_ContenedorHabilidades.transform.GetChild(2).gameObject;
        m_gameObjectContenido = m_Cubo.transform.GetChild(2).gameObject;
    }

    #region Activar cubo
    /// <summary>
    /// El player carga el cubo en la cabeza
    /// </summary>
    internal void ActivarCubo()
    {
        if (m_Cubo.activeSelf) 
            return;

        m_Cubo.SetActive(true);
        m_PlayerController.NombreObjetoCabeza = "Cubo";
    }
    #endregion

    #region Usar cubo
    /// <summary>
    /// Si lo que ha recogido es una pocion (de vida), la pocion se rellenara al maximo
    /// Si el cubo tiene contenido, lo tirara
    /// Si el cubo no tenia contenido, ahora su contenido sera lo que haya recogido
    /// </summary>
    /// <param name="recogidoPorCubo">ScriptableObject, Objeto que ha recogido el cubo</param>
    public void UsarCubo(ScriptableObject recogidoPorCubo)
    {
        this.GetComponent<AudioSource>().loop = false;
        this.GetComponent<AudioSource>().clip = m_AudioUsarCubo;
        if (!m_Contenido.IsUnityNull())
        {
            TirarContenido();
            return;
        }

        if (recogidoPorCubo.GetType().ToString().Equals(typeof(PocionesSO).ToString()))
        {
            this.GetComponent<AudioSource>().Play();
            PocionesSO pocion = (PocionesSO)recogidoPorCubo;
            m_PocionesController.RellenarPocion(pocion.TipoPocion, pocion.CantidadMax);
            return;
        }

        m_Contenido = recogidoPorCubo;

        if (m_Contenido.GetType().ToString().Equals(typeof(LiquidosSO).ToString()))
        {
            this.GetComponent<AudioSource>().Play();
            LiquidosSO liquido = (LiquidosSO)m_Contenido;
            ActivarContenido();
            m_gameObjectContenido.GetComponent<MeshRenderer>().material = liquido.Material;
        }
    }
    #endregion

    private void ActivarContenido()
    {
        m_gameObjectContenido.SetActive(true);
    }

    private void DesactivarContenido()
    {
        m_gameObjectContenido.SetActive(false);
    }

    #region Tirar contenido
    /// <summary>
    /// El contenido del cubo se tira delante del player
    /// </summary>
    internal void TirarContenido()
    {
        LiquidosSO contenidoCubo = (LiquidosSO)m_Contenido;
        GameObject contenidoInstanciado = Instantiate(contenidoCubo.Liquido);
        contenidoInstanciado.transform.position = transform.position + transform.forward * 1.5f;
        contenidoInstanciado.GetComponent<LiquidoCargable>().Caer();
        m_Contenido = null;
        m_gameObjectContenido.GetComponent<MeshRenderer>().material = null;
        DesactivarContenido();
    }
    #endregion

    #region Desequipar cubo
    /// <summary>
    /// Al desequipar el cubo, se tira el contenido
    /// </summary>
    internal void DesequiparCubo()
    {
        m_PlayerController.NombreObjetoCabeza = "";
        if (m_Contenido.IsUnityNull()) return;
        TirarContenido();
    }
    #endregion

    /// <summary>
    /// Cuando se usa el contenido (para algun mecanismo), se vacia el contenido del cubo
    /// </summary>
    #region Contenido usado
    internal void ContenidoUsado()
    {
        m_Contenido = null;
        m_gameObjectContenido.GetComponent<MeshRenderer>().material = null;
        DesactivarContenido();
    }
    #endregion
}
