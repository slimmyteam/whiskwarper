using System.Collections;
using UnityEngine;


public class HabilidadBomba : MonoBehaviour
{
    [Header("Referencias")]
    [SerializeField]
    private GameObject m_PoolBombas;

    [Header("Estado de la habilidad")]
    [SerializeField]
    private bool m_Cooldown;

    //Scripts
    private PlayerHabilidades m_PlayerHabilidades;

    private void Awake()
    {
        m_PlayerHabilidades = GetComponent<PlayerHabilidades>();
    }

    private void Start()
    {
        m_Cooldown = false;
    }

    internal void LanzarBomba(float velocidad, float dano, float tiempo, float cooldown, Enums.EnumHabilidades habilidad)
    {
        if (!m_Cooldown)
        {
            bool mejoraBomba = habilidad == Enums.EnumHabilidades.MINAS;
            GameObject bomba = m_PoolBombas.GetComponent<PoolDeBalas>().PedirBala();
            bomba.SetActive(true);
            bomba.GetComponent<BombaController>().InitBomba(transform.position + transform.forward + transform.up, transform.forward, dano, velocidad, tiempo, m_PoolBombas.GetComponent<PoolDeBalas>(), mejoraBomba);
            StartCoroutine(CooldownBomba(cooldown));
        }

        StartCoroutine(m_PlayerHabilidades.VolverANoHabilidadDespuesEspera());

    }

    private IEnumerator CooldownBomba(float cooldown)
    {
        m_Cooldown = true;
        yield return new WaitForSeconds(cooldown);
        m_Cooldown = false;
    }

}
