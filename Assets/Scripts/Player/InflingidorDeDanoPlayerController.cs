using UnityEngine;
[RequireComponent (typeof(AudioSource))]
public class InflingidorDeDanoPlayerController : MonoBehaviour
{
    private Animator m_Animator;
    [Header("Animaciones y duracion")]
    [SerializeField]
    private Animator m_CarrotAnimator;
    [SerializeField]
    private StructContainerScript.AnimacionesDuracion[] m_ListaAnimaciones;
    [Header("Layers a als que puedes atacar")]
    [SerializeField]
    private LayerMask m_LayerAtacable;
    [Header("Sonidos")]
    [SerializeField]
    private Sound m_SonidoReboteEscudo;
    [SerializeField]
    private AudioClip m_AudioRebote;

    private void Awake()
    {
        m_Animator = GetComponent<Animator>();
        m_SonidoReboteEscudo = new Sound(10, transform , SoundType.Type.Interesting, 0.2f);
    }

    public void AtacarConEspada()
    {
        float dotVector = 0;
        RaycastHit hit;
        GetComponentInChildren<EspadaController>().VuelvoAPoderHacerDano();
        if (Physics.Raycast(transform.position, transform.forward, out hit, 2, m_LayerAtacable))
        {
            dotVector = Vector3.Dot(hit.transform.forward, transform.forward);
            Debug.DrawLine(transform.position, hit.point, Color.magenta, 2f);
            if (dotVector >= 0.9f)
            {
                GetComponentInChildren<EspadaController>().HaciendoBackstab = true;
                m_Animator.CrossFade(m_ListaAnimaciones[2].nombreAnimacion, m_ListaAnimaciones[2].duracionTransicionAnimacion);
                m_CarrotAnimator.CrossFade(m_ListaAnimaciones[4].nombreAnimacion, m_ListaAnimaciones[4].duracionTransicionAnimacion);
            }
            else
            {
                GetComponentInChildren<EspadaController>().HaciendoBackstab = false;
                m_Animator.CrossFade(m_ListaAnimaciones[0].nombreAnimacion, m_ListaAnimaciones[0].duracionTransicionAnimacion);
                m_CarrotAnimator.CrossFade(m_ListaAnimaciones[3].nombreAnimacion, m_ListaAnimaciones[3].duracionTransicionAnimacion);
            }
        }
        else
        {
            m_Animator.CrossFade(m_ListaAnimaciones[0].nombreAnimacion, m_ListaAnimaciones[0].duracionTransicionAnimacion);
            m_CarrotAnimator.CrossFade(m_ListaAnimaciones[3].nombreAnimacion, m_ListaAnimaciones[3].duracionTransicionAnimacion);
        }
    }

    public void ReboteConEscudo()
    {
        this.gameObject.GetComponent<AudioSource>().loop = false;
        this.gameObject.GetComponent<AudioSource>().clip = m_AudioRebote;
        this.gameObject.GetComponent<AudioSource>().Play();
        m_Animator.CrossFade(m_ListaAnimaciones[1].nombreAnimacion, m_ListaAnimaciones[1].duracionTransicionAnimacion);
        m_CarrotAnimator.CrossFade(m_ListaAnimaciones[5].nombreAnimacion, m_ListaAnimaciones[5].duracionTransicionAnimacion);
        m_SonidoReboteEscudo.MakeSound();
    }
}
