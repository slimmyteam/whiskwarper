using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(PlayerCamaraControl))]
[RequireComponent(typeof(PlayerHabilidades))]
[RequireComponent(typeof(PlayerHealthManager))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(InflingidorDeDanoPlayerController))]
[RequireComponent(typeof(InteraccionesController))]
[RequireComponent(typeof(AnimacionesPlayerController))]
[RequireComponent(typeof(PocionesController))]
[RequireComponent(typeof(InvisibilidadController))]
[RequireComponent(typeof(AudioSource))]
public class PlayerController : MonoBehaviour, ITeleportable, IResbalable, IVolable
{
    [Header("Parametros del player")]
    [SerializeField]
    private SOPlayer m_PlayerSO;
    public SOPlayer PlayerSO { get { return m_PlayerSO; } }
    [SerializeField]
    private bool m_Invulnerable = false;
    public bool Invulnerable { get { return m_Invulnerable; } }

    [Header("Inputs")]
    [SerializeField]
    private InputActionAsset m_InputActionAsset;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;
    private InputAction m_MovementEscalando;
    private InputActionMap m_PlayerMap;

    public InputActionAsset Input { get => m_Input; set => m_Input = value; }

    [Header("Componentes")]
    [SerializeField]
    private Rigidbody m_Rigidbody;
    [SerializeField]
    private CapsuleCollider m_CapsuleCollider;
    private PlayerHabilidades m_PlayerHabilidades;
    private PocionesController m_PocionesController;
    private PlayerCamaraControl m_PlayerCamaraControl;
    private PlayerHealthManager m_PlayerHealthManager;
    private InteraccionesController m_InteraccionesController;
    private HabilidadCubo m_HabilidadCubo;
    private Animator m_Animator;
    private InvisibilidadController m_InvisibilidadController;
    private InflingidorDeDanoPlayerController m_InfligidorDeDanoController;

    [Header("Maquinas de Estado")]
    [SerializeField]
    private SwitchMachineStatesMaquinaBase m_CurrentStateBase;
    public SwitchMachineStatesMaquinaBase CurrentStateBase => m_CurrentStateBase;
    public enum SwitchMachineStatesMaquinaBase { NONE, IDLE, MOVIMIENTO, ATACANDO, CAIDA, ESCALAR, TELETRANSPORTANDOSE, RESBALANDOSE, MUERTO }
    [SerializeField]
    private SwitchMachineStatesMaquinaDano m_CurrentStateDano;
    private enum SwitchMachineStatesMaquinaDano { NONE, NO, DANO }

    [Header("Variables de Estado")]
    [SerializeField]
    private bool m_Agachado;
    [SerializeField]
    private bool m_EnTierra;
    [SerializeField]
    private bool m_EnHielo = false;

    private Vector3 m_Movement;
    [SerializeField]
    private bool m_ActivadorDejaEscalar;

    [Header("Variables de check si esta en tierra")]
    [SerializeField]
    private LayerMask m_LayerTierra;
    [SerializeField]
    private float m_AlturaPlayer;
    [SerializeField]
    private float m_RozamientoTierra;

    [Header("Animaciones y duracion")]
    [SerializeField]
    private Animator m_CarrotAnimator;
    [SerializeField]
    private StructContainerScript.AnimacionesDuracion[] m_ListaAnimaciones;

    [Header("Variables objetos interactuables")]
    [SerializeField]
    private bool m_EnPosicionEscalada;
    [SerializeField]
    private string m_NombreObjetoCabeza;

    public string NombreObjetoCabeza { get => m_NombreObjetoCabeza; set => m_NombreObjetoCabeza = value; }

    [Header("Sonidos")]
    [SerializeField]
    private Sound m_SonidoCaminar;
    [SerializeField]
    private Sound m_SonidoDejarCaer;

    [Header("Corutinas")]
    private Coroutine m_Invulnerabilidad;
    private Coroutine m_Resbalando;
    private Coroutine m_DejoDeAtacarPorSiAcaso;

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_UIMoverCamara;

    [Header("SFX")]
    [SerializeField]
    private List<AudioClip> m_ListaSonidos;
    private AudioSource m_AudioSource;

    [Header("Game Events")]
    [SerializeField]
    private GameEvent m_PlayerMuertoEvent;
    [SerializeField]
    private GameEvent m_UIDejarDeMoverCamara;
    [SerializeField]
    private GameEvent m_AbrirMenuPausa;

    private void Awake()
    {
        //Componentes
        m_Rigidbody = GetComponent<Rigidbody>();
        m_CapsuleCollider = GetComponent<CapsuleCollider>();
        m_PlayerHabilidades = GetComponent<PlayerHabilidades>();
        m_HabilidadCubo = GetComponent<HabilidadCubo>();
        m_PocionesController = GetComponent<PocionesController>();
        m_PlayerCamaraControl = GetComponent<PlayerCamaraControl>();
        m_PlayerHealthManager = GetComponent<PlayerHealthManager>();
        m_AlturaPlayer = GetComponent<CapsuleCollider>().height;
        m_Animator = GetComponent<Animator>();
        m_InteraccionesController = GetComponent<InteraccionesController>();
        m_InvisibilidadController = GetComponent<InvisibilidadController>();
        m_InfligidorDeDanoController = GetComponent<InflingidorDeDanoPlayerController>();
        m_AudioSource = GetComponent<AudioSource>();

        //Inputs ("PlayerMap")
        m_Input = Instantiate(m_InputActionAsset);
        m_PlayerMap = m_Input.FindActionMap("PlayerMap");
        m_MovementAction = m_PlayerMap.FindAction("Movement");
        m_PlayerMap.FindAction("Ataque").performed += ActivadorEstadoAtaque;
        m_PlayerMap.FindAction("Interactuar").performed += Interactuar;
        m_PlayerMap.FindAction("Agacharse").performed += Agacharse;
        m_PlayerMap.FindAction("MenuPausa").performed += AbrirMenuPausa;
        m_PlayerMap.Enable();
        //Inputs ("PlayerClimbingMap")
        m_MovementEscalando = m_Input.FindActionMap("PlayerClimbingMap").FindAction("Movement");
        //Inputs ("PlayerFijoMap")
        m_Input.FindActionMap("PlayerFijoMap").FindAction("Interactuar").performed += Interactuar;

        //Otros scripts
        m_PlayerCamaraControl.VistaDobleAction = m_PlayerMap.FindAction("VistaDoble");
        m_PlayerCamaraControl.CongelarCamaraAction = m_PlayerMap.FindAction("CongelarCamara");
        m_PlayerHabilidades.UsarHabilidadAction = m_PlayerMap.FindAction("UsarHabilidad");
        m_PocionesController.UsarPocion = m_PlayerMap.FindAction("UsarPocion");
        m_PlayerHabilidades.RotarHabilidadAction = m_PlayerMap.FindAction("RotarHabilidades");
        m_PocionesController.RotarPocionDerechaAction = m_PlayerMap.FindAction("RotarPocionesDerecha");
        m_PocionesController.RotarPocionIzquierdaAction = m_PlayerMap.FindAction("RotarPocionesIzquierda");
        m_PlayerHealthManager.PlayerSO = m_PlayerSO;
        m_PlayerHabilidades.PlayerSO = m_PlayerSO;
        m_PocionesController.PlayerSO = m_PlayerSO;

        //Variables
        m_EnPosicionEscalada = false;
        m_NombreObjetoCabeza = "";
        m_SonidoCaminar = new Sound(15, transform, SoundType.Type.Interesting, 0.4f);
        m_SonidoDejarCaer = new Sound(15, transform, SoundType.Type.Interesting, 0.4f);
    }

    // Start
    void Start()
    {
        ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.IDLE);
        ChangeStateMaquinaDano(SwitchMachineStatesMaquinaDano.NO);
    }

    // Update
    void Update()
    {
        //Debug.Log("Velocidad en el update: "+m_Rigidbody.velocity);
        m_EnTierra = Physics.Raycast(new Vector3(transform.position.x - 0.1f, transform.position.y, transform.position.z), Vector3.down, m_AlturaPlayer * 0.7f, m_LayerTierra);
        //Debug.DrawRay(new Vector3(transform.position.x - 0.1f, transform.position.y, transform.position.z), Vector3.down, Color.red);
        UpdateStateMaquinaBasica();
        UpdateStateMaquinaDano();
    }

    private void OnDestroy()
    {
        m_PlayerMap.FindAction("Ataque").performed -= ActivadorEstadoAtaque;
        m_PlayerMap.FindAction("Interactuar").performed -= Interactuar;
        m_PlayerMap.FindAction("Agacharse").performed -= Agacharse;
        m_Input.FindActionMap("PlayerFijoMap").FindAction("Interactuar").performed -= Interactuar;
        m_PlayerMap.Disable();

    }

    //----------------------------------[ MAQUINAS DE ESTADOS }----------------------------------//

    //----Maquina basica-----
    private void ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase newState)
    {
        if (newState == m_CurrentStateBase)
            return;
        if (m_CurrentStateBase == SwitchMachineStatesMaquinaBase.RESBALANDOSE && newState != SwitchMachineStatesMaquinaBase.IDLE)
            return;
        ExitStateMaquinaBasica();
        InitStateMaquinaBasica(newState);
    }

    private void InitStateMaquinaBasica(SwitchMachineStatesMaquinaBase currentState)
    {
        m_CurrentStateBase = currentState;
        switch (m_CurrentStateBase)
        {
            case SwitchMachineStatesMaquinaBase.IDLE:
                m_Animator.CrossFade(m_ListaAnimaciones[0].nombreAnimacion, m_ListaAnimaciones[0].duracionTransicionAnimacion);
                m_CarrotAnimator.CrossFade(m_ListaAnimaciones[1].nombreAnimacion, m_ListaAnimaciones[1].duracionTransicionAnimacion);
                m_Rigidbody.drag = m_RozamientoTierra;
                m_Rigidbody.angularVelocity = Vector3.zero;
                break;
            case SwitchMachineStatesMaquinaBase.ESCALAR:
                m_PlayerMap.Disable();
                m_Input.FindActionMap("PlayerClimbingMap").Enable();
                m_Rigidbody.useGravity = false;
                break;
            case SwitchMachineStatesMaquinaBase.CAIDA:
                m_CarrotAnimator.CrossFade(m_ListaAnimaciones[4].nombreAnimacion, m_ListaAnimaciones[4].duracionTransicionAnimacion);
                m_Rigidbody.useGravity = true;
                m_Rigidbody.drag = 0;
                transform.rotation = Quaternion.identity;
                m_Rigidbody.angularVelocity = Vector3.zero;
                break;
            case SwitchMachineStatesMaquinaBase.ATACANDO:
                m_AudioSource.loop = false;
                m_AudioSource.clip = m_ListaSonidos[1];
                m_AudioSource.Play();
                m_InfligidorDeDanoController.AtacarConEspada();
                if (m_NombreObjetoCabeza == "Cubo")
                {
                    m_HabilidadCubo.DesequiparCubo();
                    m_PlayerHabilidades.ChangeStateMaquinaHabilidades(PlayerHabilidades.SwitchMachineStatesMaquinaHabilidades.NOHABILIDAD);
                }
                m_DejoDeAtacarPorSiAcaso = StartCoroutine(DejoAtacarPorsiAcaso());
                break;
            case SwitchMachineStatesMaquinaBase.MOVIMIENTO:
                if (!m_Agachado)
                {
                    if (!m_InvisibilidadController.EstaInvisible())
                    {
                        m_AudioSource.loop = true;
                        m_AudioSource.clip = m_ListaSonidos[0];
                        m_AudioSource.Play();
                    }
                    m_CarrotAnimator.CrossFade(m_ListaAnimaciones[2].nombreAnimacion, m_ListaAnimaciones[2].duracionTransicionAnimacion);
                }
                else
                    m_CarrotAnimator.CrossFade(m_ListaAnimaciones[3].nombreAnimacion, m_ListaAnimaciones[3].duracionTransicionAnimacion);
                break;
            case SwitchMachineStatesMaquinaBase.TELETRANSPORTANDOSE:
                //m_Animator.CrossFade("Caminar");
                break;
            case SwitchMachineStatesMaquinaBase.RESBALANDOSE:
                m_Rigidbody.drag = 0;
                m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
                m_Resbalando = StartCoroutine(Resbalando());
                break;
            case SwitchMachineStatesMaquinaBase.MUERTO:
                m_PlayerMuertoEvent.Raise(); //TODO animacion, transicion, etc, antes de lanzar el evento
                Debug.Log("@@ Player ha muerto");
                break;
            default:
                break;
        }
    }

    private void UpdateStateMaquinaBasica()
    {
        switch (m_CurrentStateBase)
        {
            case SwitchMachineStatesMaquinaBase.IDLE:
                if (!m_EnTierra)
                    ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.CAIDA);
                else if (m_MovementAction.ReadValue<Vector3>().x != 0 || m_MovementAction.ReadValue<Vector3>().y != 0 || m_MovementAction.ReadValue<Vector3>().z != 0)
                    ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.MOVIMIENTO);
                break;

            case SwitchMachineStatesMaquinaBase.ESCALAR:

                m_Movement = Vector3.zero;

                if (m_MovementEscalando.ReadValue<Vector3>().y > 0)
                {
                    m_Movement += Vector3.up;
                }
                else if (m_MovementEscalando.ReadValue<Vector3>().y < 0)
                {
                    m_Movement -= Vector3.up;
                }

                m_Rigidbody.AddForce(m_Movement.normalized * m_PlayerSO.velocidad * 0.5f, ForceMode.Force);
                ControladorVelocidad();
                break;

            case SwitchMachineStatesMaquinaBase.CAIDA:
                if (m_EnTierra)
                    ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.IDLE);
                else
                {
                    m_Movement = Vector3.zero;
                    m_Movement += Vector3.down;
                    m_Rigidbody.AddForce(m_Movement.normalized * m_PlayerSO.velocidad * 0.3f, ForceMode.Force);
                    ControladorVelocidad();

                }
                break;

            case SwitchMachineStatesMaquinaBase.ATACANDO:
                break;

            case SwitchMachineStatesMaquinaBase.MOVIMIENTO:
                if (!m_EnTierra)
                    ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.CAIDA);
                m_Movement = Vector3.zero;



                if (!m_EnHielo)
                {
                    if (m_MovementAction.ReadValue<Vector3>().z > 0)
                    {
                        m_Movement += Vector3.forward;
                    }
                    else if (m_MovementAction.ReadValue<Vector3>().z < 0)
                    {
                        m_Movement -= Vector3.forward;
                    }

                    if (m_MovementAction.ReadValue<Vector3>().x > 0)
                    {
                        m_Movement += Vector3.right;
                    }
                    else if (m_MovementAction.ReadValue<Vector3>().x < 0)
                    {
                        m_Movement -= Vector3.right;
                    }
                }

                else
                {
                    if (m_MovementAction.ReadValue<Vector3>().z > 0)
                    {
                        m_Movement += Vector3.forward;
                    }
                    else if (m_MovementAction.ReadValue<Vector3>().z < 0)
                    {
                        m_Movement -= Vector3.forward;
                    }

                    else if (m_MovementAction.ReadValue<Vector3>().x > 0)
                    {
                        m_Movement += Vector3.right;
                    }
                    else if (m_MovementAction.ReadValue<Vector3>().x < 0)
                    {
                        m_Movement -= Vector3.right;
                    }
                }

                if (m_EnHielo && m_Movement != Vector3.zero)
                {
                    transform.forward = m_Movement;
                    Resbalar();
                    return;
                }

                //Esto es para evitar el error de Unity 'Look rotation vector is zero'
                if (m_Movement != Vector3.zero)
                {
                    if (!m_Agachado && !m_InvisibilidadController.EstaInvisible())
                        m_SonidoCaminar.MakeSound();
                    transform.forward = m_Movement;
                    ControladorAnguloForward();
                }
                ControladorVelocidad();
                m_Rigidbody.AddForce(m_Movement.normalized * m_PlayerSO.velocidad + Vector3.up * m_Rigidbody.velocity.y, ForceMode.Force);
                
                if (m_Rigidbody.velocity == Vector3.zero)
                    ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.IDLE);

                if (m_MovementAction.ReadValue<Vector3>().x == 0 && m_MovementAction.ReadValue<Vector3>().y == 0 && m_MovementAction.ReadValue<Vector3>().z == 0)
                    ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.IDLE);

                break;
            case SwitchMachineStatesMaquinaBase.TELETRANSPORTANDOSE:
                break;
            case SwitchMachineStatesMaquinaBase.RESBALANDOSE:
                if (m_Rigidbody.velocity.magnitude < 0.1f)
                {
                    ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.IDLE);
                }
                break;
            case SwitchMachineStatesMaquinaBase.MUERTO:
                break;
            default:
                break;
        }
    }

    private void ExitStateMaquinaBasica()
    {
        switch (m_CurrentStateBase)
        {
            case SwitchMachineStatesMaquinaBase.IDLE:
                break;
            case SwitchMachineStatesMaquinaBase.ESCALAR:
                m_Input.FindActionMap("PlayerClimbingMap").Disable();
                m_PlayerMap.Enable();
                m_Movement = Vector3.zero;
                m_Movement += transform.forward;
                m_Rigidbody.AddForce(m_Movement.normalized * m_PlayerSO.velocidad * 0.3f + Vector3.up * m_Rigidbody.velocity.y, ForceMode.Impulse);
                m_Rigidbody.useGravity = true;
                break;
            case SwitchMachineStatesMaquinaBase.CAIDA:
                m_Rigidbody.useGravity = false;
                m_SonidoDejarCaer.MakeSound();
                break;
            case SwitchMachineStatesMaquinaBase.ATACANDO:
                if (m_DejoDeAtacarPorSiAcaso != null)
                    StopCoroutine(m_DejoDeAtacarPorSiAcaso);
                break;
            case SwitchMachineStatesMaquinaBase.MOVIMIENTO:
                m_AudioSource.Stop();
                break;
            case SwitchMachineStatesMaquinaBase.TELETRANSPORTANDOSE:
                break;
            case SwitchMachineStatesMaquinaBase.RESBALANDOSE:
                StopCoroutine(m_Resbalando);
                break;
            case SwitchMachineStatesMaquinaBase.MUERTO:
                break;
            default:
                break;
        }
    }

    //----MaquinaDano----
    private void ChangeStateMaquinaDano(SwitchMachineStatesMaquinaDano newState)
    {
        if (newState == m_CurrentStateDano || m_CurrentStateBase == SwitchMachineStatesMaquinaBase.MUERTO)
            return;
        ExitStateMaquinaDano();
        InitStateMaquinaDano(newState);
    }

    private void InitStateMaquinaDano(SwitchMachineStatesMaquinaDano currentState)
    {
        m_CurrentStateDano = currentState;
        switch (m_CurrentStateDano)
        {
            case SwitchMachineStatesMaquinaDano.NO:
                break;
            case SwitchMachineStatesMaquinaDano.DANO:
                m_AudioSource.loop = false;
                m_AudioSource.clip = m_ListaSonidos[2];
                m_AudioSource.Play();
                break;
            default:
                break;
        }
    }

    private void UpdateStateMaquinaDano()
    {
        switch (m_CurrentStateDano)
        {
            case SwitchMachineStatesMaquinaDano.NO:
                break;
            case SwitchMachineStatesMaquinaDano.DANO:
                break;
            default:
                break;
        }
    }

    private void ExitStateMaquinaDano()
    {
        switch (m_CurrentStateDano)
        {
            case SwitchMachineStatesMaquinaDano.NO:
                break;
            case SwitchMachineStatesMaquinaDano.DANO:
                m_AudioSource.Stop();
                if (m_PlayerSO.vidaActual <= 0 && m_CurrentStateBase != SwitchMachineStatesMaquinaBase.MUERTO)
                    ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.MUERTO);
                break;
            default:
                break;
        }
    }

    //----------------------------------[ Metodos para controlar al Player }----------------------------------//

    #region Iniciar jugador

    public void InitPlayer()
    {
        m_PlayerHealthManager.VidaMaxima();
        m_PlayerSO.velocidad = m_PlayerSO.velocidadMax;
        this.gameObject.GetComponent<PocionesController>().InitPociones();
    }

    #endregion

    private void ControladorVelocidad()
    {
        //Debug.Log("Velocidad en el control de velocidad: " + m_Rigidbody.velocity);
        if (m_Rigidbody.velocity.magnitude > m_PlayerSO.velocidad)
        {
            m_Rigidbody.velocity = m_Rigidbody.velocity.normalized * m_PlayerSO.velocidad;
           // Debug.Log("Velocidad en el control de velocidad RETOCADO: " + m_Rigidbody.velocity);
        }
    }

    private void ControladorAnguloForward()
    {
        if (Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit, m_AlturaPlayer * 0.7f, m_LayerTierra))
        {
            Vector3 direccionForward = Vector3.ProjectOnPlane(transform.forward, hit.normal);
            transform.forward = direccionForward;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Escala"))
        {
            if (m_CurrentStateBase == SwitchMachineStatesMaquinaBase.ESCALAR)
            {
                m_EnPosicionEscalada = false;
                ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.IDLE);
            }
            else
            {
                m_EnPosicionEscalada = true;
            }
        }
        else if (m_CurrentStateBase == SwitchMachineStatesMaquinaBase.ESCALAR && other.gameObject.CompareTag("ActivadorDejaEscalar"))
            m_ActivadorDejaEscalar = true;
        else if (other.gameObject.name == "Dano")
            ChangeStateMaquinaDano(SwitchMachineStatesMaquinaDano.DANO);
    }

    private void Interactuar(InputAction.CallbackContext context)
    {
        m_InteraccionesController.Interactuar(m_EnPosicionEscalada);

    }

    private void Agacharse(InputAction.CallbackContext context)
    {
        m_Agachado = !m_Agachado;
        if (m_Agachado)
        {
            m_AudioSource.loop = false;
            m_AudioSource.Stop();
            if (m_Rigidbody.velocity.magnitude >= Vector3.one.magnitude)
                m_CarrotAnimator.CrossFade(m_ListaAnimaciones[3].nombreAnimacion, m_ListaAnimaciones[3].duracionTransicionAnimacion);
            m_PlayerSO.velocidad /= 2;
            m_CapsuleCollider.center -= new Vector3(0, 0.5f, 0);
            m_CapsuleCollider.height -= 1;
        }
        else
        {
            if (!m_InvisibilidadController.EstaInvisible())
            {
                m_AudioSource.loop = true;
                m_AudioSource.clip = m_ListaSonidos[0];
                m_AudioSource.Play();
            }
            if (m_Rigidbody.velocity.magnitude >= Vector3.one.magnitude)
                m_CarrotAnimator.CrossFade(m_ListaAnimaciones[2].nombreAnimacion, m_ListaAnimaciones[2].duracionTransicionAnimacion);
            m_PlayerSO.velocidad *= 2;
            m_CapsuleCollider.center += new Vector3(0, 0.5f, 0);
            m_CapsuleCollider.height += 1;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("ActivadorDejaEscalar"))
            m_ActivadorDejaEscalar = false;
        if (other.gameObject.CompareTag("DejaEscalar") && m_ActivadorDejaEscalar)
            ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.IDLE);
        if (other.gameObject.CompareTag("Escala"))
            m_EnPosicionEscalada = false;
    }

    public void VolverAIdle()
    {
        ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.IDLE);
    }

    public void CambiarControlCamara(bool playeFijo)
    {
        if (playeFijo)
        {
            m_UIMoverCamara.Raise();
            m_PlayerMap.Disable();
            m_Input.FindActionMap("PlayerFijoMap").Enable();
        }
        else
        {
            m_UIDejarDeMoverCamara.Raise();
            m_Input.FindActionMap("PlayerFijoMap").Disable();
            m_PlayerMap.Enable();
        }
    }
    private void ActivadorEstadoAtaque(InputAction.CallbackContext context)
    {
        ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.ATACANDO);
    }

    public void FinalizadorDeAtaques()
    {
        if (m_CurrentStateBase == SwitchMachineStatesMaquinaBase.ATACANDO)
            ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.IDLE);
    }

    public void PonerseAEscalar()
    {
        ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.ESCALAR);
    }

    public string ObtenerEstadoMaquinaBase()
    {
        return m_CurrentStateBase.ToString();
    }

    public void HeSidoDanado()
    {
        if (m_PlayerSO.vidaActual <= 0 && m_CurrentStateBase != SwitchMachineStatesMaquinaBase.MUERTO)
            ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.MUERTO);
        ChangeStateMaquinaDano(SwitchMachineStatesMaquinaDano.DANO);
        m_Invulnerabilidad = StartCoroutine(Invulnerabilidad());
    }

    private void AbrirMenuPausa(InputAction.CallbackContext context)
    {
        m_AbrirMenuPausa.Raise();
    }

    //----------------------------------[ Corutinas }----------------------------------//

    private IEnumerator Invulnerabilidad()
    {
        m_Invulnerable = true;
        yield return new WaitForSeconds(m_PlayerSO.TiempoInvulnerabilidad);
        m_Invulnerable = false;
        ChangeStateMaquinaDano(SwitchMachineStatesMaquinaDano.NO);
        StopCoroutine(m_Invulnerabilidad);
    }

    private IEnumerator Resbalando()
    {
        while (true)
        {
            m_Rigidbody.AddForce(0.5f * m_PlayerSO.velocidadMax * transform.forward, ForceMode.Force);
            yield return new WaitForSeconds(0.01f);
        }
    }

    private IEnumerator DejoAtacarPorsiAcaso()
    {
        yield return new WaitForSeconds(1);
        ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.IDLE);
    }

    //----------------------------------[ Teletransporte }----------------------------------//

    #region Gestion del teletransporte
    public void SerTeletransportado()
    {
        ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.TELETRANSPORTANDOSE);
    }

    public void TerminarTeletransporte()
    {
        if (m_CurrentStateBase != SwitchMachineStatesMaquinaBase.TELETRANSPORTANDOSE)
            return;
        VolverAIdle();
    }
    #endregion

    public void Resbalar()
    {
        ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.RESBALANDOSE);
        m_EnHielo = true;
    }

    public void DejarDeResbalar()
    {
        m_EnHielo = false;
        m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        ChangeStateMaquinaBasica(SwitchMachineStatesMaquinaBase.IDLE);
    }

    //-------------------------------[ Eventos ]-------------------------------------//
    public void DesactivarMapa()
    {
        if (m_PlayerMap.enabled)
        {
            m_PlayerMap.Disable();
            return;
        }

        m_PlayerMap.Enable();
    }
}
