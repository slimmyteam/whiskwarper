using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerCamaraControl : MonoBehaviour
{
    //Input Actions
    private InputAction m_VistaDobleAction;
    public InputAction VistaDobleAction
    {
        get { return m_VistaDobleAction; }
        set { m_VistaDobleAction = value; }
    }

    private InputAction m_CongelarCamaraAction;
    public InputAction CongelarCamaraAction
    {
        get { return m_CongelarCamaraAction; }
        set { m_CongelarCamaraAction = value; }
    }

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_CambioDeVistaCamara;
    [SerializeField]
    private GameEvent m_CongelarCamara;

    private void Start()
    {
        m_VistaDobleAction.performed += CambioDeVistaCamara;
        m_CongelarCamaraAction.performed += CongelarCamara;
    }

    public void CambioDeVistaCamara(InputAction.CallbackContext context)
    {
        if (SceneManager.GetActiveScene().name != "MapaBossExplosivo")
            m_CambioDeVistaCamara.Raise();
    }
    public void CongelarCamara(InputAction.CallbackContext context)
    {
        m_CongelarCamara.Raise();
    }
}
