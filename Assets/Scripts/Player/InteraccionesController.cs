using Unity.VisualScripting;
using UnityEngine;

public class InteraccionesController : MonoBehaviour
{
    [Header("Layer de las cosas interactuables")]
    [SerializeField]
    private LayerMask m_Interactuables;

    [Header("Contenedor de la cabeza")]
    [SerializeField]
    private GameObject m_PosicionCabeza;

    [Header("Sonidos")]
    [SerializeField]
    private Sound m_SonidoEmpujar;
    [SerializeField]
    private Sound m_SonidoMecanismo;

    [Header("Componentes")]
    private HabilidadCubo m_HabilidadCubo;

    private void Awake()
    {
        m_SonidoEmpujar = new Sound(5, transform, SoundType.Type.Interesting, 0.4f);
        m_SonidoMecanismo = new Sound(10, transform, SoundType.Type.Interesting, 0.4f);
        m_HabilidadCubo = GetComponentInChildren<HabilidadCubo>();
    }

    public void Interactuar(bool puedeEscalar)
    {
        RaycastHit[] hitList = Physics.BoxCastAll(transform.position - Vector3.up * 0.5f + transform.forward * 1.7f, new Vector3(1, 0.5f, 1), transform.forward, Quaternion.identity, 1, m_Interactuables);
        if (hitList.Length > 0)
        {
            foreach (RaycastHit hit in hitList)
            {
                //Debug.Log("HIT " + hit.collider.name);
                Collider collider = hit.collider;
                if (collider.gameObject.CompareTag("Escalable") && puedeEscalar && GetComponent<PlayerController>().NombreObjetoCabeza == "")
                {
                    //Debug.Log("ESCALABLE " + hit.collider.name);
                    transform.forward = hit.transform.forward;
                    GetComponent<PlayerController>().PonerseAEscalar();
                }
                else if (collider.gameObject.CompareTag("EscalableBajada") && puedeEscalar && GetComponent<PlayerController>().NombreObjetoCabeza == "")
                {
                    //Debug.Log("BAJADA " + hit.collider.name);
                    transform.forward = hit.transform.forward;
                    transform.position = transform.position - collider.transform.forward * 2f - collider.transform.up * 1.2f;
                    GetComponent<PlayerController>().PonerseAEscalar();
                }
                else if (collider.TryGetComponent(out IEmpujable empujable))
                {
                    //Debug.Log("EMPUJABLE " + hit.collider.name);
                    m_SonidoEmpujar.MakeSound();
                    empujable.Empujar(transform.position);
                }
                else if (collider.TryGetComponent(out IMecanismoReaccionadorObjetoCargado mecanismoObjeto))
                {
                    if (TryGetComponent(out PlayerController playerController) && playerController.NombreObjetoCabeza != "")
                    {
                        if (playerController.NombreObjetoCabeza == "Cubo")
                        {
                            if (!m_HabilidadCubo.Contenido) return; //Si el cubo no tiene contenido, volvemos //TODO LANZAR MENSAJE DE ERROR (??)

                            if (collider.gameObject.TryGetComponent<ObjetoReaccionadorAContenidoCubo>(out _))
                            {
                                if (collider.gameObject.GetComponent<ObjetoReaccionadorAContenidoCubo>().Activado) return; //Si el mecanismo ya esta activado, volvemos //TODO LANZAR MENSAJE DE ERROR (??)
                            }
                            mecanismoObjeto.ReaccionaContenidoCubo(m_HabilidadCubo.Contenido);
                            m_HabilidadCubo.ContenidoUsado();
                            m_SonidoMecanismo.MakeSound();
                        }
                        else
                        {
                            bool mecanismoActivado = mecanismoObjeto.ReaccionaObjetoCargado(playerController.NombreObjetoCabeza);
                            if (mecanismoActivado)
                            {
                                GetComponentInChildren<ICargable>().UsarObjeto();
                                m_PosicionCabeza.transform.DetachChildren();
                                playerController.NombreObjetoCabeza = "";
                                m_SonidoMecanismo.MakeSound();
                            }
                        }
                    }
                }
                else if (collider.TryGetComponent(out IMecanismo mecanismo))
                {
                    //Debug.Log("MECANISMO " + hit.collider.name);
                    m_SonidoMecanismo.MakeSound();
                    mecanismo.EjecutarMecanismo();
                }
                else if (collider.TryGetComponent(out ICargable cargable))
                {
                    //Debug.Log("CARGABLE " + hit.collider.name);
                    if (TryGetComponent(out PlayerController playerController) && playerController.NombreObjetoCabeza == "")
                    {
                        cargable.RecogerObjeto(m_PosicionCabeza);
                        playerController.NombreObjetoCabeza = cargable.RecuperarNombreObjetoCargable();
                    }

                }
                else if (collider.TryGetComponent(out EstatuaDeGuardado estatuaGuardado))
                {
                    //Debug.Log("ESTATUA");
                    estatuaGuardado.GuardarPartida();
                }

                if (collider.TryGetComponent(out IRecogibleConCubo recogibleConCubo))
                {
                    //Debug.Log("CUBO "+recogibleConCubo);
                    if (TryGetComponent(out PlayerController playerController) && playerController.NombreObjetoCabeza == "Cubo")
                    {
                        //Debug.Log("TENGO CUBO");
                        if (!m_HabilidadCubo.Contenido.IsUnityNull())
                        {
                            //Debug.Log("NULL");
                            m_HabilidadCubo.TirarContenido();
                            return;
                        }
                        //Debug.Log("USAR CUBO");
                        m_HabilidadCubo.UsarCubo(recogibleConCubo.CuboRecoge());
                    }


                }
                if (collider.TryGetComponent(out INPC _))
                {
                    //Debug.Log("NPC " + hit.collider.name);
                    if (TryGetComponent(out PlayerController _))
                    {
                        Enums.EnumNPCS TipoNPC;
                        if (collider.gameObject.TryGetComponent(out NPCController npcController))
                        {
                            TipoNPC = collider.gameObject.GetComponent<NPCController>().ObtenerTipoNPC;
                            collider.GetComponent<NPCController>().ComenzarDialogoNPC();
                        }
                        else
                        {
                            TipoNPC = collider.gameObject.GetComponent<NPCGenericoController>().ObtenerTipoNPC;
                            collider.GetComponent<NPCGenericoController>().ComenzarDialogoNPC();
                        }
                        gameObject.GetComponent<ControladorActivadorObjetos>().ActivacionObjetoSegunNPC(TipoNPC, collider.gameObject);
                    }
                }
            }
        }
        else
        {
            if (TryGetComponent(out PlayerController playerController) && playerController.NombreObjetoCabeza != "")
            {

                HabilidadCubo m_HabilidadCubo = GetComponentInChildren<HabilidadCubo>();

                if (playerController.NombreObjetoCabeza == "Cubo" && !m_HabilidadCubo.Contenido.IsUnityNull())
                {
                    m_HabilidadCubo.UsarCubo(m_HabilidadCubo.Contenido);
                    return;
                }
                else if (playerController.NombreObjetoCabeza == "Cubo")
                {
                    //TODO LANZAR SONIDO DE ERROR
                    return;
                }

                GetComponentInChildren<ICargable>().SoltarObjeto(transform.position + transform.forward * 1.5f);
                m_PosicionCabeza.transform.DetachChildren();
                playerController.NombreObjetoCabeza = "";
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position - Vector3.up * 0.5f + transform.forward *1.7f, new Vector3(2, 1, 2));
    }
}
