using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Scriptable Objects", menuName = "Scriptable Objects/Player")]
public class SOPlayer : ScriptableObject
{
    public float vidaMax;
    public float vidaActual;
    public float danyoAtaque;
    public float velocidad;
    public float velocidadMax;
    public float cooldownAtaque;
    [SerializeField]
    private int m_TiempoInvulnerabilidad = 3;
    public int TiempoInvulnerabilidad { get { return m_TiempoInvulnerabilidad; } }
}
