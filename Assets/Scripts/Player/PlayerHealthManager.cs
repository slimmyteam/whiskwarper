using System.Collections;
using UnityEngine;
using UnityEngine.VFX;

[RequireComponent(typeof(VisualEffect))]
[RequireComponent(typeof(InvisibilidadController))]
public class PlayerHealthManager : MonoBehaviour, IDamageable
{
    [Header("Referencias a objetos")]
    private SOPlayer m_PlayerSO;
    public SOPlayer PlayerSO { get => m_PlayerSO; set => m_PlayerSO = value; }
    private PlayerController m_PlayerController;
    [SerializeField]
    private GEFloat m_PlayerPierdeVida;

    [Header("VFX")]
    [SerializeField]
    private VisualEffectAsset m_ParticulaGanarVida;
    [SerializeField]
    private Material m_MaterialSinDano;
    [SerializeField]
    private Material m_MaterialConDano;
    [SerializeField]
    private SkinnedMeshRenderer m_SkinnedRenderer;

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_RecuperarVidaUI;

    private void Awake()
    {
        m_PlayerController = GetComponent<PlayerController>();
    }

    public void VidaMaxima()
    {
        m_PlayerSO.vidaActual = m_PlayerSO.vidaMax;
    }

    public void RecuperarVida()
    {
        GetComponent<VisualEffect>().visualEffectAsset = m_ParticulaGanarVida;
        GetComponent<VisualEffect>().Play();
        if (m_PlayerSO.vidaActual < m_PlayerSO.vidaMax)
        {
            m_PlayerSO.vidaActual+=2;
            m_RecuperarVidaUI.Raise();
        }
    }

    public void PerderVida(float danoRecibido)
    {
        Debug.Log("####" + danoRecibido);
        if (m_PlayerController.Invulnerable) return;
        if (!GetComponent<InvisibilidadController>().EstaInvisible())
            StartCoroutine(DanoVisual());
        m_PlayerPierdeVida.Raise(danoRecibido);
        m_PlayerController.HeSidoDanado();
        m_PlayerSO.vidaActual = m_PlayerSO.vidaActual - (float)danoRecibido <= 0 ? 0 : m_PlayerSO.vidaActual - (float)danoRecibido;
    }

    public void MuerteInstantanea()
    {
        m_PlayerSO.vidaActual = 0;
        m_PlayerController.HeSidoDanado();
    }

    private IEnumerator DanoVisual()
    {
        m_SkinnedRenderer.material = m_MaterialConDano;
        yield return new WaitForSeconds(1);
        m_SkinnedRenderer.material = m_MaterialSinDano;
    }
}
