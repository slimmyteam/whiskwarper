using UnityEngine;

public class ControladorActivadorObjetos : MonoBehaviour
{
    public void ActivacionObjetoSegunNPC(Enums.EnumNPCS TipoNPC, GameObject NPC)
    {
        switch (TipoNPC)
        {
            case Enums.EnumNPCS.POCIONES:
                gameObject.GetComponent<PocionesController>().ActivarPociones();
                break;

            case Enums.EnumNPCS.HABILIDADES:
                gameObject.GetComponent<PlayerHabilidades>().DesbloquearHabilidad(NPC.GetComponent<TengoObjetoHabilidades>().Objeto);
                break;
        }
    }
}
