using UnityEngine;

public class PasaPuzzlesTrucardo : MonoBehaviour
{
    [SerializeField]
    private LayerMask m_TodasLayers;
    [SerializeField]
    private GameEvent m_EventoActivarSlimes;

    // Update is called once per frame
    void Update()
    {
        //---Pasa puzzles---
        if (Input.GetKeyDown(KeyCode.M))
        {
            RaycastHit[] hitList = Physics.SphereCastAll(transform.position, 2, transform.forward, 2, m_TodasLayers);
            if (hitList.Length > 0)
            {
                foreach (RaycastHit hit in hitList)
                {
                    if (hit.collider.gameObject.TryGetComponent(out IMecanismo mecanismo))
                    {
                        mecanismo.EjecutarMecanismo();
                    }
                    else if (hit.collider.gameObject.TryGetComponent(out IReaccionador reaccionador))
                    {
                        reaccionador.Reaccionar();
                    }
                }
            }
        }

        //---Matar matar matar----
        else if (Input.GetKeyDown(KeyCode.N))
        {
            RaycastHit[] hitList = Physics.SphereCastAll(transform.position, 2, transform.forward, 2, m_TodasLayers);
            if (hitList.Length > 0)
            {
                foreach (RaycastHit hit in hitList)
                {
                    if (hit.collider.gameObject.TryGetComponent(out IDamageable damageable))
                    {
                        damageable.PerderVida(9999);
                    }
                }
            }
        }

        //---Slimes----
        else if (Input.GetKeyDown(KeyCode.O))
        {
            m_EventoActivarSlimes?.Raise();
        }

        //---Debloquea Mejoras----
        else if (Input.GetKeyDown(KeyCode.H))
        {
            this.gameObject.GetComponent<PlayerHabilidades>().DesbloquearHabilidad(Enums.EnumHabilidades.CUBO);
            this.gameObject.GetComponent<PlayerHabilidades>().DesbloquearHabilidad(Enums.EnumHabilidades.BOOMERANG);
            this.gameObject.GetComponent<PlayerHabilidades>().DesbloquearHabilidad(Enums.EnumHabilidades.MINAS);
            this.gameObject.GetComponent<PlayerHabilidades>().DesbloquearHabilidad(Enums.EnumHabilidades.ESCUDOMAGICO);
        }

        //----Bosses------
        else if (Input.GetKeyDown(KeyCode.C))
        {
            ControlDePartida.Instance.CambiarEscena("MapaBossRicochet", new Vector3(-50, 0.8f, -20));
        }

        else if (Input.GetKeyDown(KeyCode.V))
        {
            ControlDePartida.Instance.CambiarEscena("MapaBossExplosivo", new Vector3(-99.5f, 1, 245));
        }

        else if (Input.GetKeyDown(KeyCode.B))
        {
            ControlDePartida.Instance.CambiarEscena("MapaBossFinal", new Vector3(-101, 11.5f, -93));
        }

        //-----Mapas---------
        else if (Input.GetKeyDown(KeyCode.J))
        {
            ControlDePartida.Instance.CambiarEscenaSinEstatua("MapaZona1", new Vector3(-990, 1.38f, 263));
        }

        else if (Input.GetKeyDown(KeyCode.K))
        {
            ControlDePartida.Instance.CambiarEscenaSinEstatua("MapaZona2", new Vector3(201, 1, -14));
        }

        else if (Input.GetKeyDown(KeyCode.L))
        {
            ControlDePartida.Instance.CambiarEscenaSinEstatua("MapaZona3", new Vector3(200, 1.5f, 290));
        }
    }
}
