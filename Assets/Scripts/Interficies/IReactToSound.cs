using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IReactToSound 
{    void ReactToSound(Sound sound);
}
