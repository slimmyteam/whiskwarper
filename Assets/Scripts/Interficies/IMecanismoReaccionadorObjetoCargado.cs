using UnityEngine;

public interface IMecanismoReaccionadorObjetoCargado : IMecanismo
{
    public bool ReaccionaObjetoCargado(string nombreObjetoCargado);
    public void ReaccionaContenidoCubo(ScriptableObject contenidoCubo);

}
