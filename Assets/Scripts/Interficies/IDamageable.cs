public interface IDamageable
{
    public void PerderVida(float danoRecibido);

    public void MuerteInstantanea();
}
