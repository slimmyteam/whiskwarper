using UnityEngine;

public interface IEmpujable
{
    public void Empujar(Vector3 posiconOrigenEmpuje);
}
