using UnityEngine;

public interface ICargable
{
    public void RecogerObjeto(GameObject posicionCabeza);

    public string RecuperarNombreObjetoCargable();

    public void SoltarObjeto(Vector3 posicionDejar);

    public void UsarObjeto();

    public void SerArrastrado(GameObject boomerang);
    public void DesligarseBoomerang();
}
