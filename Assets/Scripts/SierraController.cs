using UnityEngine;

public class SierraController : MonoBehaviour
{
    private Rigidbody m_Rigidbody;
    [SerializeField]
    private float velocidadRotacionZ = 10f;
    [SerializeField]
    private float m_magnitudMaxima = 2.0f;
    private Vector3 m_direccion;


    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        int direccionRandom = Random.Range(0, 2);

        if (direccionRandom == 0)
        {
            m_direccion = Vector3.right;
            return;
        }

        m_direccion = -Vector3.right;
    }

    private void Update()
    {
        transform.Rotate(Time.deltaTime * velocidadRotacionZ * -Vector3.right);
        transform.Translate(m_direccion * Time.deltaTime, Space.World);

        if (m_Rigidbody.velocity.magnitude > m_magnitudMaxima)
        {
            // Reducimos la magnitud de la velocidad al limite maximo permitido para controlar que no se vaya a�adiendo cada vez mas fuerza y las sierras se salgan de su rail
            m_Rigidbody.velocity = m_Rigidbody.velocity.normalized * m_magnitudMaxima;
        }
    }
}
