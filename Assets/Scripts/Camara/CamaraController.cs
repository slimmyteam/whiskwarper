using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Camera))]
[RequireComponent(typeof(PortalTeletransporte))]
[RequireComponent(typeof(CongelarCamara))]
[RequireComponent(typeof(CamaraOffsetController))]
public class CamaraController : MonoBehaviour
{
    [Header("Referencias")]
    [SerializeField]
    private Transform m_PlayerTransform;
    public Transform PlayerTransform { get => m_PlayerTransform; set => m_PlayerTransform = value; }

    [Header("Shaders")]
    [SerializeField]
    private UniversalRendererData m_Renderer;
    private List<ScriptableRendererFeature> m_RendererFeatures;
    public List<ScriptableRendererFeature> RendererFeatures { get => m_RendererFeatures; }
    private ScriptableRendererFeature m_ShaderPortal;

    [Header("Estado de la camara")]
    [SerializeField]
    private bool m_CamaraIzquierda;
    public bool CamaraIzquierda => m_CamaraIzquierda;
    [SerializeField]
    private bool m_CamaraActiva;
    public bool CamaraActiva => m_CamaraActiva;
    [SerializeField]
    private bool m_VistaDobleActiva;
    public bool VistaDobleActiva => m_VistaDobleActiva;
    [SerializeField]
    private bool m_CamaraBloqueada;
    public bool CamaraBloqueada => m_CamaraBloqueada;

    [Header("Estado del offset")]
    private Vector3 m_OffsetCamara;
    [SerializeField]
    private bool m_PlayerControlandoCamara;
    [SerializeField]
    private bool m_MantenerOffset;
    [SerializeField]
    private bool m_FueraDelLimite;

    [Header("Parametros")]
    [SerializeField]
    private float m_SizePorDefecto;

    [Header("Distancias de la camara")]
    [SerializeField]
    private float m_DistanciaYPlayerCamara; //Se suma a la posicion del player
    public float DistanciaYPlayerCamara => m_DistanciaYPlayerCamara;
    [SerializeField]
    private float m_DistanciaZPlayerCamara; //Se resta a la posicion del player
    public float DistanciaZPlayerCamara => m_DistanciaZPlayerCamara;
    [SerializeField]
    private float m_OffsetXCamaraIzquierdaCamaraDerecha;

    [Header("Cambio de camara")]
    [SerializeField, Range(0f, 1f)]
    private float m_InterpoladorLerpTransicionCamara; //La t de Vector4.Lerp (busca el punto indicado entre el primer vector y el segundo)
    private float m_InterpoladorActualTransicion;
    [SerializeField, Range(0f, 1f)]
    private float m_SumaInterpoladorTransicion; //Lo que aumenta la interpolacion en cada frame

    [Header("Componentes")]
    private Camera m_Camera;
    private PortalTeletransporte m_PortalTeletransporte;
    private CongelarCamara m_CongelarCamara;

    //[Header("Bounds de la camara")]
    private Bounds m_CameraBounds = new();
    public Bounds CameraBounds => m_CameraBounds;

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_DesactivarVistaDobleEvento;

    private void Awake()
    {
        m_Camera = GetComponent<Camera>();
        m_PortalTeletransporte = GetComponent<PortalTeletransporte>();
        m_CongelarCamara = GetComponent<CongelarCamara>();
        m_RendererFeatures = m_Renderer.rendererFeatures;
        m_ShaderPortal = m_RendererFeatures[1];
        m_OffsetCamara = Vector3.zero;
        m_PlayerControlandoCamara = false;
        m_FueraDelLimite = false;
        m_ShaderPortal.SetActive(false);

    }

    private void Start()
    {
        ComprobarCamaraActiva();
        m_Camera.rect = m_CamaraActiva ? new Rect(0, 0, 1, 1) : new Rect(0, 0, 0, 0);

        if (m_CamaraIzquierda) //Si la camara es la izquierda, el offset es negativo
            m_OffsetXCamaraIzquierdaCamaraDerecha *= -1;
        if (SceneManager.GetActiveScene().name == "MapaBossExplosivo")
            ActivarDesactivarVistaDoble();        
    }

    void Update()
    {
        ComprobarCamaraActiva();
        CalcularPosicionCamara();
    }

    #region Posicionar Camara

    /// <summary>
    /// Comprueba si el player se encuentra en el rango de la camara izquierda o la camara derecha y marca como activa la camara donde se encuentra el player. Tambien activa o desactiva el componente AudioListener dependiendo de si la camara es la camara activa o no.
    /// </summary>
    private void ComprobarCamaraActiva()
    {
        m_CamaraActiva = (m_PlayerTransform.position.x < 0 && m_CamaraIzquierda) || (m_PlayerTransform.position.x > 0 && !m_CamaraIzquierda); //Se activa la camara si el player esta en su mapa (lado izquierdo o derecho)
        gameObject.GetComponent<AudioListener>().enabled = m_CamaraActiva;
    }

    /// <summary>
    ///
    /// Si es la camara activa, se coloca la camara siguiendo al player. Si no lo es, la coloca en la misma posicion mas un offset en X (la distancia entre los dos mapas).
    /// </summary>
    private void CalcularPosicionCamara()
    {
        if (m_CongelarCamara.CamaraCongelada || m_CamaraBloqueada)
            return;
        Vector3 posicionCamaraActiva = new(m_PlayerTransform.position.x, m_PlayerTransform.position.y + m_DistanciaYPlayerCamara, m_PlayerTransform.position.z - m_DistanciaZPlayerCamara);
        gameObject.transform.position = m_CamaraActiva ?
            posicionCamaraActiva : m_PlayerControlandoCamara && !m_FueraDelLimite ?
            new Vector3(transform.position.x + m_OffsetCamara.x, posicionCamaraActiva.y, transform.position.z + m_OffsetCamara.z) : DeboMantenerOffset(posicionCamaraActiva);
    }

    #endregion

    #region Vista Unica / Doble

    /// <summary>
    /// Comprueba si la vista doble esta activa y si la camara es la izquierda para crear un nuevo Rect con las medidas que debe tener la camara al terminar la transicion.
    /// Llama a la corrutina TransicionCamara indicandole el Rect final e invierte el estado de VistaDobleActiva.
    /// </summary>
    public void ActivarDesactivarVistaDoble()
    {
        if (GameManager._Teletransporte.Teletransportando || m_CamaraBloqueada)
            return;
        Rect rectanguloFinal;
        if (m_VistaDobleActiva)
            rectanguloFinal = m_CamaraActiva ? new Rect(0, 0, 1, 1) : new Rect(0, 0, 0, 0);
        else
            rectanguloFinal = m_CamaraIzquierda ? new Rect(0, 0, 0.5f, 1) : new Rect(0.5f, 0, 1, 1);

        m_VistaDobleActiva = !m_VistaDobleActiva;

        m_ShaderPortal.SetActive(m_VistaDobleActiva);

        if (!m_VistaDobleActiva)
        {
            m_DesactivarVistaDobleEvento.Raise();
            m_RendererFeatures[2].SetActive(false);
        }

        StartCoroutine(TransicionCamara(rectanguloFinal));
    }

    /// <summary>
    /// Si la vista doble se ha desactivado, la camara no puede estar congelada.
    /// Aumenta o disminuye el tamano de la camara gradualmente, a partir de las medidas del rectangulo final que debe tener la camara. 
    /// </summary>
    /// <param name="rectanguloFinal">Rect con las medidas finales que debe tener la camara</param>
    /// <returns>WaitForEndOfFrame: Espera al final del frame</returns>
    private IEnumerator TransicionCamara(Rect rectanguloFinal)
    {
        if (!m_VistaDobleActiva)
            m_CongelarCamara.CamaraCongelada = false;

        Vector4 vectorFinal = new(rectanguloFinal.x, rectanguloFinal.y, rectanguloFinal.width, rectanguloFinal.height);
        m_InterpoladorActualTransicion = m_InterpoladorLerpTransicionCamara;

        while (m_Camera.rect != rectanguloFinal)
        {
            Vector4 vectorInicial = new(m_Camera.rect.x, rectanguloFinal.y, m_Camera.rect.width, rectanguloFinal.height);
            Vector4 vectorLerp = Vector4.Lerp(vectorInicial, vectorFinal, m_InterpoladorActualTransicion);
            m_Camera.rect = new(vectorLerp.x, vectorLerp.y, vectorLerp.z, vectorLerp.w);
            m_InterpoladorActualTransicion += m_SumaInterpoladorTransicion;
            yield return new WaitForEndOfFrame();
        }

        m_PortalTeletransporte.ColocarPortalCamara();
    }

    #endregion

    #region Bloquear Camara

    public void BloquearCamara(Vector3 posicionCamaraIzquierda, Vector3 posicionCamaraDerecha, float size)
    {
        if (!m_VistaDobleActiva)
            ActivarDesactivarVistaDoble();
       transform.position = m_CamaraIzquierda ? posicionCamaraIzquierda : posicionCamaraDerecha;
        m_Camera.orthographicSize = size;
        m_CongelarCamara.CongelarBloquearCamara();
        m_CamaraBloqueada = true;
    }

    public void DesbloquearCamara()
    {
        m_CamaraBloqueada = false;
        m_Camera.orthographicSize = m_SizePorDefecto;
        m_CongelarCamara.CongelarDescongelarCamara();
    }

    #endregion

    #region Metodos Offset Camara
    public void AnadirOffsetCamara(Vector3 offset)
    {
        m_OffsetCamara = offset;
    }

    public void ReiniciarMantenerOffset()
    {
            m_MantenerOffset = false;

    }

    public void ActivarMantenerOffset()
    {
        if (!m_CamaraActiva)
            m_MantenerOffset = true;
    }

    public void AlternarPlayerControlandoCamara()
    {
        m_PlayerControlandoCamara = !m_PlayerControlandoCamara;
        if (!m_PlayerControlandoCamara)
        {
            m_OffsetCamara = Vector3.zero;
        }
    }

    private Vector3 DeboMantenerOffset(Vector3 posicionCamaraActiva)
    {
        if (!m_MantenerOffset)
            return new Vector3(posicionCamaraActiva.x + m_OffsetXCamaraIzquierdaCamaraDerecha, posicionCamaraActiva.y, posicionCamaraActiva.z);
        return transform.position;
    }

    public void DetectarEstoyFueraLimite(bool estado)
    {
        m_FueraDelLimite = estado;
    }

    #endregion
}
