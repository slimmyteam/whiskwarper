using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class CongelarCamara : MonoBehaviour
{
    //Componentes
    private Camera m_Camera;
    private CamaraController m_CamaraController;
    private PortalTeletransporte m_PortalTeletransporte;

    [Header("Estado de la camara")]
    [SerializeField]
    private bool m_CamaraCongelada;
    public bool CamaraCongelada
    {
        get { return m_CamaraCongelada; }
        set { m_CamaraCongelada = value; }
    }

    [Header("Bordes de la camara")]
    [SerializeField]
    private List<GameObject> m_BordesCamara;
    private GameObject m_BordeIzquierdo;
    private GameObject m_BordeDerecho;
    private GameObject m_BordeSuperior;
    private GameObject m_BordeInferior;
    [SerializeField]
    private float m_MargenBordesIzquierdaDerecha;
    [SerializeField]
    private float m_MargenBordeSuperior;
    [SerializeField]
    private float m_MargenBordeInferior;

    [Header("Parametros")]
    [SerializeField]
    private float m_TiempoEsperaBloqueoCamara;

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_CamaraDescongeladaEvento;

    //Shader
    private ScriptableRendererFeature m_ShaderCongelarCamara;
    private ScriptableRendererFeature m_ShaderPortal;

    private void Awake()
    {
        m_Camera = GetComponent<Camera>();
        m_CamaraController = GetComponent<CamaraController>();
        m_PortalTeletransporte = GetComponent<PortalTeletransporte>();
    }

    private void Start()
    {
        m_CamaraCongelada = false;
        m_ShaderPortal = m_CamaraController.RendererFeatures[1];
        m_ShaderCongelarCamara = m_CamaraController.RendererFeatures[2];
        m_ShaderCongelarCamara.SetActive(false);

        BuscarBordesCamara();
    }

    /// <summary>
    /// Si la vista doble esta activa, cambia el estado de la camara (congelada true o false)
    /// </summary>
    public void CongelarDescongelarCamara()
    {
        if (!m_CamaraController.VistaDobleActiva || GameManager._Teletransporte.Teletransportando || m_CamaraController.CamaraBloqueada)
            return;

        m_CamaraCongelada = !m_CamaraCongelada;
        m_ShaderCongelarCamara.SetActive(m_CamaraCongelada);
        m_ShaderPortal.SetActive(!m_CamaraCongelada);

        if (m_CamaraCongelada)
            ColocarBordesCamara();
        else
        {
            m_CamaraDescongeladaEvento.Raise();
            m_CamaraController.ReiniciarMantenerOffset();
        }

        ActivarDesactivarBordesCamara();
    }

    /// <summary>
    /// Activa o desactiva los bordes de la camara en funcion de si la camara se encuentra congelada
    /// </summary>
    private void ActivarDesactivarBordesCamara()
    {
        foreach (GameObject borde in m_BordesCamara)
        {
            borde.SetActive(m_CamaraCongelada);
        }
    }

    /// <summary>
    /// Coloca los bordes de la camara en la posicion correcta, con el margen indicado. 
    /// </summary>
    private void ColocarBordesCamara()
    {
        Vector3 worldPointDerecho = m_CamaraController.CamaraIzquierda ? m_Camera.ViewportToWorldPoint(new Vector3(1, 0.5f, m_CamaraController.DistanciaYPlayerCamara)) :
            m_Camera.ViewportToWorldPoint(new Vector3(1, 1, m_CamaraController.DistanciaYPlayerCamara));
        m_BordeDerecho.transform.position = new Vector3(worldPointDerecho.x + m_MargenBordesIzquierdaDerecha, m_BordeDerecho.transform.position.y, worldPointDerecho.z);

        Vector3 worldPointIzquierdo = m_CamaraController.CamaraIzquierda ? m_Camera.ViewportToWorldPoint(new Vector3(0, 0, m_CamaraController.DistanciaYPlayerCamara)) :
            m_Camera.ViewportToWorldPoint(new Vector3(0, 0.5f, m_CamaraController.DistanciaYPlayerCamara));
        m_BordeIzquierdo.transform.position = new Vector3(worldPointIzquierdo.x - m_MargenBordesIzquierdaDerecha, m_BordeIzquierdo.transform.position.y, worldPointIzquierdo.z);

        Vector3 worldPointSuperior = m_Camera.ViewportToWorldPoint(new Vector3(0, 1, transform.position.y - m_CamaraController.PlayerTransform.position.y));
        m_BordeSuperior.transform.position = new Vector3(worldPointSuperior.x, worldPointSuperior.y + m_MargenBordeSuperior, worldPointSuperior.z + m_MargenBordeSuperior);

        Vector3 worldPointInferior = m_Camera.ViewportToWorldPoint(new Vector3(1, 0, transform.position.y - m_CamaraController.PlayerTransform.position.y));
        m_BordeInferior.transform.position = new Vector3(worldPointInferior.x, worldPointSuperior.y - m_MargenBordeInferior, worldPointInferior.z - m_MargenBordeInferior);

        m_PortalTeletransporte.ColocarPortalCamara();
    }

    /// <summary>
    /// Busca los cuatro bordes de la camara y los a�ade a la lista BordesCamara. Ademas, se guarda referencias al borde izquierdo y al borde derecho. 
    /// Por ultimo, llama a ActivarDesactivarBordesCamara para desactivar los bordes.
    /// </summary>
    private void BuscarBordesCamara()
    {
        foreach (Transform child in transform.GetChild(1))
        {
            GameObject borde = child.gameObject;
            m_BordesCamara.Add(borde);

            if (borde.name == "BordeIzquierdo")
                m_BordeIzquierdo = borde;
            else if (borde.name == "BordeDerecho")
                m_BordeDerecho = borde;
            if (borde.name == "BordeSuperior")
                m_BordeSuperior = borde;
            else if (borde.name == "BordeInferior")
                m_BordeInferior = borde;
        }

        ActivarDesactivarBordesCamara();
    }

    /// <summary>
    /// Marca la camara como congelada y llama a una corrutina para colocar los bordes de la camara.
    /// </summary>
    public void CongelarBloquearCamara()
    {
        m_CamaraCongelada = true;
        StartCoroutine(ColocarBordesCamaraBloqueada());
    }

    /// <summary>
    /// Coloca los bordes de la camara y los activa despues de esperar el tiempo indicado.
    /// </summary>
    /// <returns>WaitForSeconds(TiempoEsperaBloqueoCamara): Espera el tiempo indicado en segundos antes de colocar y activar los bordes</returns>
    private IEnumerator ColocarBordesCamaraBloqueada()
    {
        yield return new WaitForSeconds(m_TiempoEsperaBloqueoCamara);
        ColocarBordesCamara();
        ActivarDesactivarBordesCamara();
    }
}
