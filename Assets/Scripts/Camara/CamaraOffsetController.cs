using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CamaraController))]
public class CamaraOffsetController : MonoBehaviour
{
    //Actions
    private InputActionAsset m_Input;
    private InputAction m_MovimientoCamaraOffset;
    //Componentes
    private CamaraController m_CamaraController;
    //Variables
    private Vector3 m_Movement;
    [SerializeField]
    private float m_VelocidadDesplazamientoCamara;
    [SerializeField]
    private bool m_ButtonHold;
    [SerializeField]
    private StructContainerScript.LimitesDeCamara m_LimitesDeCamara;

    // Start is called before the first frame update
    void Start()
    {
        m_ButtonHold = false;
        m_CamaraController = GetComponent<CamaraController>();
        m_Input = m_CamaraController.PlayerTransform.gameObject.GetComponent<PlayerController>().Input;
        m_MovimientoCamaraOffset = m_Input.FindActionMap("PlayerFijoMap").FindAction("Movement");
        m_Input.FindActionMap("PlayerFijoMap").FindAction("MovementButton").performed += AlternarHoldeoDeBoton;
        m_Input.FindActionMap("PlayerFijoMap").FindAction("MovementButton").canceled += AlternarHoldeoDeBoton;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_ButtonHold)
        {
            if (!m_CamaraController.CamaraActiva)
            {
                m_Movement = Vector3.zero;
                m_CamaraController.DetectarEstoyFueraLimite(false);

                if (m_MovimientoCamaraOffset.ReadValue<Vector3>().z > 0 && transform.position.z < m_LimitesDeCamara.LimiteSuperior)
                {
                    m_Movement += Vector3.forward;
                }
                else if (m_MovimientoCamaraOffset.ReadValue<Vector3>().z < 0 && transform.position.z > m_LimitesDeCamara.LimiteInferior)
                {
                    m_Movement -= Vector3.forward;
                }
                else
                {
                    m_CamaraController.DetectarEstoyFueraLimite(true);
                }

                if (m_MovimientoCamaraOffset.ReadValue<Vector3>().x > 0 && transform.position.x < m_LimitesDeCamara.LimiteDerecha)
                {
                    m_Movement += Vector3.right;
                }
                else if (m_MovimientoCamaraOffset.ReadValue<Vector3>().x < 0 && transform.position.x > m_LimitesDeCamara.LimiteIzquierda)
                {
                    m_Movement -= Vector3.right;
                }
                else
                {
                    m_CamaraController.DetectarEstoyFueraLimite(true);
                }

                if (m_Movement != Vector3.zero)
                {
                    m_CamaraController.DetectarEstoyFueraLimite(false);
                    //print("@@@ game object name: " + gameObject.name + " movement: " + m_Movement);
                    m_Movement *= m_VelocidadDesplazamientoCamara;
                    //print("@ game object name: " + gameObject.name + " movement2: " + m_Movement);
                    m_CamaraController.AnadirOffsetCamara(m_Movement);
                }
            }
        }
    }

    public void PlayerControlaCamara()
    {
        m_CamaraController.AlternarPlayerControlandoCamara();
    }

    private void AlternarHoldeoDeBoton(InputAction.CallbackContext context)
    {
        m_ButtonHold = !m_ButtonHold;
        PlayerControlaCamara();
    }

    public void EstablecerLimites(StructContainerScript.LimitesDeCamara limites)
    {
        m_LimitesDeCamara = limites;
    }
}
