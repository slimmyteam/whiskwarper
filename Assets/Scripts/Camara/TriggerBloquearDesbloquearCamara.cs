using UnityEngine;

public class TriggerBloquearDesbloquearCamara : MonoBehaviour
{
    [Header("Estado del trigger")]
    [SerializeField]
    private bool m_Bloquear;

    [Header("Eventos")]
    [SerializeField]
    private GEVector3Vector3Float m_BloquearCamara;
    [SerializeField]
    private GameEvent m_DesbloquearCamara, m_UIBloquearCamara;

    [Header("Estado deseado de las camaras")]
    [SerializeField]
    private Vector3 m_PosicionDeseadaCamaraIzquierda;
    [SerializeField]
    private Vector3 m_PosicionDeseadaCamaraDerecha;
    [SerializeField]
    private float m_SizeDeseadaCamara;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player"))
            return;
        if (m_Bloquear)
        {
            m_BloquearCamara.Raise(m_PosicionDeseadaCamaraIzquierda, m_PosicionDeseadaCamaraDerecha, m_SizeDeseadaCamara);
            m_UIBloquearCamara.Raise();
        }
        else
        {
            m_DesbloquearCamara.Raise();
        }
        m_Bloquear = !m_Bloquear;
    }
}
