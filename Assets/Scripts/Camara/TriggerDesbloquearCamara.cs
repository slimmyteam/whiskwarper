using UnityEngine;

public class TriggerDesbloquearCamara : MonoBehaviour
{
    [SerializeField]
    private GameEvent m_DesbloquearCamara;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player"))
            return;

        m_DesbloquearCamara.Raise();
    }
}
