using UnityEngine;

public class MapaDerecho : MonoBehaviour
{
    [SerializeField]
    private Transform m_MapaIzquierdo;
    [SerializeField]
    private float m_OffsetXEntreMapas;
    
    void Start()
    {
        gameObject.transform.position = new Vector3(m_MapaIzquierdo.position.x + m_OffsetXEntreMapas, m_MapaIzquierdo.position.y, m_MapaIzquierdo.position.z);
    }
}
