using System.Collections;
using UnityEngine;

public class PortalTeletransporte : MonoBehaviour
{
    private int m_PlayerLayer;
    private int m_BossLayer;

    //[Header("Componentes")]
    private Camera m_Camera;
    private CamaraController m_CamaraController;

    [Header("Eventos")]
    [SerializeField]
    private GEGameObject m_TeletransportarEvent;

    [Header("Control del portal")]
    [SerializeField]
    private float m_CooldownPortal;
    [SerializeField]
    private float m_DesplazamientoPortal;
    private GameObject m_Portal;

    private void Awake()
    {
        m_Camera = GetComponent<Camera>();
        m_CamaraController = GetComponent<CamaraController>();

        m_PlayerLayer = LayerMask.NameToLayer("Player");
        m_BossLayer = LayerMask.NameToLayer("Boss");
        m_Portal = transform.GetChild(0).gameObject;
    }

    /// <summary>
    /// Se comprueba si el Game Object que ha colisionado implementa ITeleportable y no esta siendo teletransportado ya. 
    /// Si el game object ha cruzado el centro del portal, se debe teletransportar. Se guarda la velocidad del personaje en el momento de comenzar el teletransporte.
    /// Si es la camara activa, llama a la corrutina SalirPersonaje, que desplaza al jugador fuera de la camara.
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerStay(Collider other)
    {
        GameObject gameObjectCollision = other.gameObject;

        if (!gameObjectCollision.TryGetComponent<ITeleportable>(out _))
            return;

        m_TeletransportarEvent.Raise(gameObjectCollision);

        DesactivarPortales();
    }

    /// <summary>
    /// Activa o desactiva el BoxCollider dependiendo de si la vista doble esta activa. Si lo esta, coloca el portal en el borde derecho (camara izquierda) o izquierdo (camara derecha)
    /// </summary>
    public void ColocarPortalCamara()
    {
        m_Portal.SetActive(m_CamaraController.VistaDobleActiva);

        if (!m_CamaraController.VistaDobleActiva)
            return;

        Vector3 worldPoint = m_CamaraController.CamaraIzquierda ? m_Camera.ViewportToWorldPoint(new Vector3(1, 1, m_CamaraController.DistanciaYPlayerCamara)) + new Vector3(m_DesplazamientoPortal, 0, 0) 
            : m_Camera.ViewportToWorldPoint(new Vector3(0, 1, m_CamaraController.DistanciaYPlayerCamara)) - new Vector3(m_DesplazamientoPortal, 0, 0);        
        m_Portal.transform.position = worldPoint;
    }

    public void DesactivarPortales()
    {
        m_Portal.SetActive(false);
    }

    public void FinTeletransporte()
    {
        StartCoroutine(ActivarPortalesCorrutina());
    }

    private IEnumerator ActivarPortalesCorrutina()
    {
        yield return new WaitForSeconds(m_CooldownPortal);
        m_Portal.SetActive(true);
    }
}
